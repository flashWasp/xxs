package com.xxs.company.service.repository;

import com.xxs.company.service.entity.CompanyDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung123
 * @since 2018-12-11
 */
public interface CompanyDetailRepository extends IService<CompanyDetail> {

    CompanyDetail selectOne(String companyNo);
}
