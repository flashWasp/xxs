package com.xxs.company.service.dao;

import com.xxs.company.service.entity.CompanySortMapping;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-12-11
 */
public interface CompanySortMappingDao extends BaseMapper<CompanySortMapping> {

}
