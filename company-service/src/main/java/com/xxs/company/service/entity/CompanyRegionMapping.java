package com.xxs.company.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public class CompanyRegionMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String companyNo;
    private Long regionId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Override
    public String toString() {
        return "CompanyRegionMapping{" +
        ", id=" + id +
        ", companyNo=" + companyNo +
        ", regionId=" + regionId +
        "}";
    }
}
