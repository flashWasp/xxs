package com.xxs.company.service.repository;

import com.xxs.company.service.entity.CompanyRegionMapping;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public interface CompanyRegionMappingRepository extends IService<CompanyRegionMapping> {

    CompanyRegionMapping selectOne(String companyNo);

    /**
     * 根据地域
     * @param regionId
     * @return
     */
    List<CompanyRegionMapping> selectList(Long regionId);

    /**
     * 根据公司编号获取地域Id集合
     * @param companyNo
     * @return
     */
    List<CompanyRegionMapping> selectList(String companyNo);

}
