package com.xxs.company.service.service;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanyBase;
import com.xxs.company.service.entity.CompanyDetail;

import java.util.List;

public interface CompanyService {

    /**
     * 添加公司种类
     *
     * @param name
     * @param summary
     * @param publishTime
     * @param state
     * @param picNo
     * @param regionId
     * @param companySortId
     * @return
     */
    Boolean add(String name, String summary,
                Long publishTime, String state,
                String picNo, Long regionId, Long companySortId
    );

    /**
     * 更新公司信息
     *
     * @param companyNo
     * @param name
     * @param summary
     * @param publishTime
     * @param state
     * @param picNo
     * @param regionId
     * @param companySortId
     * @return
     */
    Boolean update(String companyNo, String name,
                   String summary, Long publishTime,
                   String state, String picNo,
                   Long regionId, Long companySortId);

    /**
     * 获取公司内容
     *
     * @param companyNo
     * @return
     */
    CompanyBase get(String companyNo);

    /**
     * 删除公司内容
     *
     * @param companyNo
     * @return
     */
    Boolean del(String companyNo);

    /**
     * 根据条件获取Id集合
     *
     * @param name
     * @param publicStartTime
     * @param publicEndTime
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param companyNos
     * @param state
     * @return
     */
    List<Long> listIds(String name,
                       Long publicStartTime,
                       Long publicEndTime,
                       Long createStartTime,
                       Long createEndTime,
                       Long updateStartTime,
                       Long updateEndTime,
                       List<String> companyNos,
                       String state
    );

    /**
     * 根据条件获取公司内容集合
     *
     * @param name
     * @param publicStartTime
     * @param publicEndTime
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param companyNos
     * @param state
     * @return
     */
    List<CompanyBase> list(String name,
                           Long publicStartTime,
                           Long publicEndTime,
                           Long createStartTime,
                           Long createEndTime,
                           Long updateStartTime,
                           Long updateEndTime,
                           List<String> companyNos,
                           String state
    );

    /**
     * 分页获取公司内容集合
     *
     * @param name
     * @param publicStartTime
     * @param publicEndTime
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param companyNos
     * @param state
     * @return
     */
    Page<CompanyBase> page(
            String name,
            Long publicStartTime,
            Long publicEndTime,
            Long createStartTime,
            Long createEndTime,
            Long updateStartTime,
            Long updateEndTime,
            List<String> companyNos,
            String state,
            Integer pageNo,
            Integer pageSize
    );


    /**
     * 获取详细信息
     *
     * @param companyNo
     * @return
     */
    CompanyDetail detail(String companyNo);


    /**
     * 补充详细信息
     *
     * @param founderNo           创始人
     * @param url                 网址
     * @param address             地址
     * @param businessStartTime   营业开始时间
     * @param businessEndTime     营业结束时间
     * @param phone               联系方式
     * @param fullName            公司全名
     * @param establishTime       建立时间
     * @param legalRepresentative 法人
     * @param paidupCapital       公司类型
     * @param content             内容
     * @param advantage           优势
     * @param history             历史
     * @param team                团队
     * @param picNos              图片集合
     * @param companyNo           公司编号
     * @return
     */
    Boolean support(String founderNo, String url, String address,
                    Long businessStartTime, Long businessEndTime,
                    String phone, String fullName, Long establishTime,
                    String legalRepresentative, String paidupCapital, String content,
                    String advantage, String history, String team, String picNos, String companyNo
    );

    /**
     * 根据地域Id获取公司编号
     * @param regionId
     * @return
     */
    List<String> listCompanyNosByRegionId(Long regionId);

    /**
     * 根据公司编号获取地域Id
     * @param compangNo
     * @return
     */
    List<Long> listRegionIdsByCompanyNo(String compangNo);

    /**
     * 根据分类Id获取公司编号集合
     * @param sortId
     * @return
     */
    List<String> listCompanyNosBySortId(Long sortId);

    /**
     * 根据公司编号获取分类Id集合
     * @param compangNo
     * @return
     */
    List<Long> listSortIdsByCompanyNo(String compangNo);

}
