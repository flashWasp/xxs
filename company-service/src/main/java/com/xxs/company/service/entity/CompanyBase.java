package com.xxs.company.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public class CompanyBase implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 公司名称
     */
    private String name;
    /**
     * 简介
     */
    private String summary;
    /**
     * 发布时间
     */
    private Long publishTime;
    /**
     * 业务分类名称
     */
    private String companySortName;
    /**
     * 状态
     */
    private String state;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 更新时间
     */
    private Long updateTime;
    /**
     * 图片
     */
    private String picNo;
    /**
     * 地方名
     */
    private String regionName;
    /**
     * 公司编号
     */
    private String companyNo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Long getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Long publishTime) {
        this.publishTime = publishTime;
    }

    public String getCompanySortName() {
        return companySortName;
    }

    public void setCompanySortName(String companySortName) {
        this.companySortName = companySortName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getPicNo() {
        return picNo;
    }

    public void setPicNo(String picNo) {
        this.picNo = picNo;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    @Override
    public String toString() {
        return "CompanyBase{" +
        ", id=" + id +
        ", name=" + name +
        ", summary=" + summary +
        ", publishTime=" + publishTime +
        ", companySortName=" + companySortName +
        ", state=" + state +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", picNo=" + picNo +
        ", regionName=" + regionName +
        ", companyNo=" + companyNo +
        "}";
    }
}
