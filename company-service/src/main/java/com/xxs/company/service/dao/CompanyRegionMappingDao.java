package com.xxs.company.service.dao;

import com.xxs.company.service.entity.CompanyRegionMapping;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public interface CompanyRegionMappingDao extends BaseMapper<CompanyRegionMapping> {

}
