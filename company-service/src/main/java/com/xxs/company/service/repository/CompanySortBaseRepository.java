package com.xxs.company.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanySortBase;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public interface CompanySortBaseRepository extends IService<CompanySortBase> {

    Page<CompanySortBase> selectPage(String state, String name, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime,Integer pageNo,Integer pageSize);

    List<Long> selectIds(String state, String name, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime);

    List<CompanySortBase> selectList(CompanySortBase req);

}
