package com.xxs.company.service.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public class CompanyRegionBase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 
     */
    private Long id;
    /**
     * 区域名称
     */
    private String name;
    /**
     * 父Id
     */
    private Long parentId;
    /**
     * 父Id集合
     */
    @TableField("parentIds")
    private String parentIds;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Long createTime;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Long updateTime;
    /**
     * 状态： normal :正常  isDelete：删除
     */
    private String state;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "CompanyRegionBase{" +
        ", id=" + id +
        ", name=" + name +
        ", parentId=" + parentId +
        ", parentIds=" + parentIds +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", state=" + state +
        "}";
    }
}
