package com.xxs.company.service.repository;

import com.xxs.company.service.entity.CompanyRegionMapping;
import com.xxs.company.service.entity.CompanySortMapping;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-11
 */
public interface CompanySortMappingRepository extends IService<CompanySortMapping> {

    CompanySortMapping selectOne(String companyNo);

    /**
     * 根据地域
     * @param sortId
     * @return
     */
    List<CompanySortMapping> selectList(Long sortId);

    /**
     * 根据公司编号获取地域Id集合
     * @param companyNo
     * @return
     */
    List<CompanySortMapping> selectList(String companyNo);
}
