package com.xxs.company.service.entity.req;

import java.util.List;

/**
 * @author Administrator
 * @Title: CompanySearchReq
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1223:31
 */
public class CompanySearchReq {
    /**
     * 公司名称
     */
    private String name;

    /**
     * 公布时间开始
     */
    private Long publicStartTime;

    /**
     * 公布时间结束
     */
    private Long publicEndTime;

    /**
     * 创建时间开始
     */
    private Long createStartTime;

    /**
     * 创建时间结束
     */
    private Long createEndTime;

    /**
     * 更新时间开始
     */
    private Long updateStartTime;

    /**
     * 更新时间结束
     */
    private Long updateEndTime;

    /**
     * 分类Id
     */
    private Long sortId;

    /**
     * 地域Id
     */
    private Long regionId;

    /**
     * 状态
     */
    private String state;

    private Integer page;

    private Integer rows;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPublicStartTime() {
        return publicStartTime;
    }

    public void setPublicStartTime(Long publicStartTime) {
        this.publicStartTime = publicStartTime;
    }

    public Long getPublicEndTime() {
        return publicEndTime;
    }

    public void setPublicEndTime(Long publicEndTime) {
        this.publicEndTime = publicEndTime;
    }

    public Long getCreateStartTime() {
        return createStartTime;
    }

    public void setCreateStartTime(Long createStartTime) {
        this.createStartTime = createStartTime;
    }

    public Long getCreateEndTime() {
        return createEndTime;
    }

    public void setCreateEndTime(Long createEndTime) {
        this.createEndTime = createEndTime;
    }

    public Long getUpdateStartTime() {
        return updateStartTime;
    }

    public void setUpdateStartTime(Long updateStartTime) {
        this.updateStartTime = updateStartTime;
    }

    public Long getUpdateEndTime() {
        return updateEndTime;
    }

    public void setUpdateEndTime(Long updateEndTime) {
        this.updateEndTime = updateEndTime;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
}
