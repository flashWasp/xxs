package com.xxs.company.service.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.company.service.entity.CompanyBase;
import com.xxs.company.service.entity.CompanyRegionBase;
import com.xxs.company.service.entity.CompanySortBase;
import com.xxs.company.service.entity.req.*;
import com.xxs.company.service.res.CompanyRegionBaseRes;
import com.xxs.company.service.res.CompanySortBaseRes;
import com.xxs.company.service.service.CompanyRegionService;
import com.xxs.company.service.service.CompanyService;
import com.xxs.company.service.service.CompanySortService;
import com.xxs.company.service.service.impl.CompanyServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Administrator
 * @Title: CompanyController
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/11 21:05
 */
@RestController
public class CompanyController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    @Autowired
    private CompanyRegionService companyRegionService;

    @Autowired
    private CompanySortService companySortService;

    @Autowired
    private CompanyService companyService;

    /*======== 分类业务 =============*/

    @ApiOperation("获取分类对象")
    @GetMapping("/sort/get")
    public AjaxResult getSort(Long id) {
        CompanySortBase companySortBase = companySortService.get(id);
        return AjaxResult.getOK(companySortBase);
    }

    @ApiOperation("添加分类对象")
    @PostMapping("/sort/add")
    public AjaxResult addSort(@RequestBody CompanySortBase companySortBase) {
        Boolean flag = companySortService.save(companySortBase.getName());
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("删除分类对象")
    @GetMapping("/sort/del")
    public AjaxResult delSort(Long id, Boolean isAll) {
        Boolean del = companySortService.del(id, isAll);
        if (del) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("更新分类对象")
    @PostMapping("/sort/update")
    public AjaxResult updateSort(@RequestBody CompanySortBase companySortBase) {
        Boolean update = companySortService.update(companySortBase.getId(), companySortBase.getName(), companySortBase.getState());
        if (update){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("分页获取分类对象")
    @PostMapping("/sort/page")
    public AjaxResult pageSort(@RequestBody CompanySortSearchReq req){
        String name = req.getName();
        Long createEndTime = req.getCreateEndTime();
        Long createStartTime = req.getCreateStartTime();
        Integer pageNo = req.getPageNo();
        Integer pageSize = req.getPageSize();
        String state = req.getState();
        Long updateEndTime = req.getUpdateEndTime();
        Long updateStartTime = req.getUpdateStartTime();
        Page<CompanySortBase> page = companySortService.page(state, name, createStartTime, createEndTime,
                updateStartTime, updateEndTime, pageNo, pageSize);

        //  long 丢失精度
        List<CompanySortBaseRes> result = new ArrayList<>();
        page.getRecords().forEach(item -> {
            CompanySortBaseRes res = new CompanySortBaseRes();
            BeanUtils.copyProperties(item, res);
            res.setId(item.getId().toString());
            result.add(res);
        });
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows", result);
        map.put("total", page.getTotal());
        return AjaxResult.getOK(map);
    }

    @ApiOperation("根据条件获取分类对象集合")
    @PostMapping("/sort/list")
    public AjaxResult listSort(@RequestBody CompanySortBase req){
        List<CompanySortBase> list = companySortService.list(req);
        if (!CollectionUtils.isEmpty(list)){
            //  long 丢失精度
            List<CompanySortBaseRes> result = new ArrayList<>();
            list.forEach(item -> {
                CompanySortBaseRes res = new CompanySortBaseRes();
                BeanUtils.copyProperties(item, res);
                res.setId(item.getId().toString());
                result.add(res);
            });
            return AjaxResult.getOK(result);
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /*======== 地方业务 =============*/

    @ApiOperation("添加公司地方对象")
    @PostMapping("/region/add")
    public AjaxResult addRegion(@RequestBody CompanyRegionBase req ){
        Boolean flag = companyRegionService.add(req.getName(), req.getParentId());
        if (flag){
            return AjaxResult.getOK();
        }
        return  AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("获取地方对象")
    @GetMapping("/region/get")
    public AjaxResult get(Long id){
        CompanyRegionBase companyRegionBase = companyRegionService.get(id);
        if (null != companyRegionBase){
            return AjaxResult.getOK(companyRegionBase);
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("更新地方对象")
    @PostMapping("/region/update")
    public AjaxResult update(@RequestBody CompanyRegionBase req){
        Boolean flag = companyRegionService.update(req.getId(), req.getName(), req.getParentId(), req.getState());
        if (flag){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("根据条件获取id集合")
    @PostMapping("/region/listIds")
    public AjaxResult listIds(@RequestBody CompanyRegionSearchReq req){
        List<Long> ids = companyRegionService.listIds(req.getName(),
                req.getParentId(),
                req.getCreateStartTime(),
                req.getCreateEndTime(),
                req.getUpdateStartTime(),
                req.getUpdateEndTime(),
                req.getState()
        );
        if (!CollectionUtils.isEmpty(ids)){
            return AjaxResult.getOK(ids);
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("根据条件获取地方对象集合")
    @PostMapping("/region/list")
    public AjaxResult list(@RequestBody CompanyRegionSearchReq req){
        List<CompanyRegionBase> list = companyRegionService.list(req.getName(),
                req.getParentId(),
                req.getCreateStartTime(),
                req.getCreateEndTime(),
                req.getUpdateStartTime(),
                req.getUpdateEndTime(),
                req.getState()
        );
        if (!CollectionUtils.isEmpty(list)){
            List<CompanyRegionBaseRes> result = this.getRegionList(list);
            return AjaxResult.getOK(result);
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }


    @ApiOperation("地方模块 -- 分页 ")
    @PostMapping("/region/page")
    public AjaxResult page(@RequestBody CompanyRegionSearchReq req){
        Page<CompanyRegionBase> page = companyRegionService.page(req.getName(),
                req.getParentId(),
                req.getCreateStartTime(),
                req.getCreateEndTime(),
                req.getUpdateStartTime(),
                req.getUpdateEndTime(),
                req.getState(),
                req.getPage(),
                req.getRows()
        );
        //  long 丢失精度
        List<CompanyRegionBaseRes> result = this.getRegionList(page.getRecords());
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows", result);
        map.put("total", page.getTotal());
        return AjaxResult.getOK(map);
    }

    /**
     * 获取区域列表 返回string类型
     * @param list
     * @return
     */
    private List<CompanyRegionBaseRes> getRegionList(List<CompanyRegionBase> list) {
        List<CompanyRegionBaseRes> result = new ArrayList<>();
        list.forEach(item -> {
            CompanyRegionBaseRes res = new CompanyRegionBaseRes();
            BeanUtils.copyProperties(item, res);
            res.setId(item.getId().toString());
            res.setParentId(item.getParentId().toString());
            result.add(res);
        });
        return result;
    }

    /*============= 公司模块  ==============*/

    @ApiOperation("添加基本信息")
    @PostMapping("/company/add")
    public AjaxResult addCompany(@RequestBody CompanyAddReq req) {
        String name = req.getName();
        String picNo = req.getPicNo();
        Long regionId = req.getRegionId();
        Long sortId = req.getSortId();
        String state = req.getState();
        String summary = req.getSummary();
        Long publish = System.currentTimeMillis();
        Boolean add = companyService.add(name, summary, publish, state, picNo, regionId, sortId);
        if (add){
            return  AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("补充详细内容")
    @PostMapping("/company/support")
    public AjaxResult support(@RequestBody CompanyDetailAddReq req) {
        String address = req.getAddress();
        String advantage = req.getAdvantage();
        Long businessEndTime = req.getBusinessEndTime();
        Long businessStartTime = req.getBusinessStartTime();
        String companyNo = req.getCompanyNo();
        String content = req.getContent();
        Long establishTime = req.getEstablishTime();
        String founderNo = req.getFounderNo();
        String fullName = req.getFullName();
        String history = req.getHistory();
        String legalRepresentative = req.getLegalRepresentative();
        String paidupCapital = req.getPaidupCapital();
        String phone = req.getPhone();
        String picNos = req.getPicNos();
        String team = req.getTeam();
        String url = req.getUrl();
        Boolean support = companyService.support(
                founderNo, url, address, businessStartTime, businessEndTime,
                phone, fullName, establishTime, legalRepresentative, paidupCapital, content,
                advantage, history, team, picNos, companyNo
        );
        if (support) {
            return AjaxResult.getOK();
        }
        return  AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("获取公司基本信息")
    @RequestMapping(value = "/company/get",method = RequestMethod.GET)
    public AjaxResult get(String companyNo) {
        long startTime = System.currentTimeMillis();
        AjaxResult ajaxResult = AjaxResult.getOK(companyService.get(companyNo));
        long endTime = System.currentTimeMillis();
        LOGGER.info(startTime - endTime + "");
        return ajaxResult;
    }

    @ApiOperation("获取公司详细内容")
    @RequestMapping(value = "/company/detail",method = RequestMethod.GET)
    public AjaxResult detail(String companyNo) {
        return AjaxResult.getOK(companyService.detail(companyNo));
    }

    @ApiOperation("删除公司内容")
    @GetMapping("/company/del")
    public AjaxResult del(String companyNo){
        Boolean del = companyService.del(companyNo);
        if (del){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("获取公司id集合")
    @PostMapping("/company/listIds")
    public AjaxResult listIds(@RequestBody CompanySearchReq req){
        Long createEndTime = req.getCreateEndTime();
        Long createStartTime = req.getCreateStartTime();
        String name = req.getName();
        Long publicEndTime = req.getPublicEndTime();
        Long publicStartTime = req.getPublicStartTime();
        Long regionId = req.getRegionId();
        Long sortId = req.getSortId();
        String state = req.getState();
        Long updateEndTime = req.getUpdateEndTime();
        Long updateStartTime = req.getUpdateStartTime();
        List<String> companyNos = companyService.listCompanyNosByRegionId(regionId);
        companyNos.addAll(companyService.listCompanyNosBySortId(sortId));
        List<Long> ids = companyService.listIds(
                name,
                publicStartTime,
                publicEndTime,
                createStartTime,
                createEndTime,
                updateStartTime,
                updateEndTime,
                companyNos,
                state
        );
        return AjaxResult.getOK(ids);
    }

    @ApiOperation("分页获取公司内容")
    @PostMapping("/company/page")
    public AjaxResult page(@RequestBody CompanySearchReq req){
        Long createEndTime = req.getCreateEndTime();
        Long createStartTime = req.getCreateStartTime();
        String name = req.getName();
        Long publicEndTime = req.getPublicEndTime();
        Long publicStartTime = req.getPublicStartTime();
        Long regionId = req.getRegionId();
        Long sortId = req.getSortId();
        String state = req.getState();
        Long updateEndTime = req.getUpdateEndTime();
        Long updateStartTime = req.getUpdateStartTime();
        Integer page = req.getPage();
        Integer rows = req.getRows();
        List<String> companyNos = companyService.listCompanyNosByRegionId(regionId);
        companyNos.addAll(companyService.listCompanyNosBySortId(sortId));
        Page<CompanyBase> pageObj = companyService.page(
                name,
                publicStartTime,
                publicEndTime,
                createStartTime,
                createEndTime,
                updateStartTime,
                updateEndTime,
                companyNos,
                state,
                page,
                rows
        );
        return  AjaxResult.getOK(pageObj);
    }




}
