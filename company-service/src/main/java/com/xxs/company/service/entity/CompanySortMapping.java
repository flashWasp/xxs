package com.xxs.company.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-11
 */
public class CompanySortMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String companyNo;
    private Long sortId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    @Override
    public String toString() {
        return "CompanySortMapping{" +
        ", id=" + id +
        ", companyNo=" + companyNo +
        ", sortId=" + sortId +
        "}";
    }
}
