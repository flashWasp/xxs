package com.xxs.company.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanySortBase;

import java.util.List;

/**
 * 公司分类服务
 */
public interface CompanySortService {

    /**
     * 添加公司分类
     * @param name
     * @return
     */
    Boolean  save(String name);

    /**
     * 更新添加分类
     * @param id
     * @param name
     * @param state
     * @return
     */
    Boolean update(Long id,String name,String state);

    /**
     * 根据id删除
     * @param id id
     * @param isAll 全部删除标记 包括删除分类对象
     * @return
     */
    Boolean del(Long id,Boolean isAll);

    /**
     * 获取公司分类详情
     * @param id
     * @return
     */
    CompanySortBase get(Long id);

    List<CompanySortBase> list(CompanySortBase req);

    /**
     * 根据条件获取Id集合
     * @param state
     * @param name
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @return
     */
    List<Long> listIds(String state, String name,
                       Long createStartTime,Long createEndTime,
                       Long updateStartTime,Long updateEndTime
    );

    /**
     * 根据条件获取分类对象集合
     * @param state
     * @param name
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @return
     */
    Page<CompanySortBase> page(String state, String name,
                               Long createStartTime, Long createEndTime,
                               Long updateStartTime, Long updateEndTime,
                               Integer pageNo,Integer pageSize

    );

}
