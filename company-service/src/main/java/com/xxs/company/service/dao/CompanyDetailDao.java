package com.xxs.company.service.dao;

import com.xxs.company.service.entity.CompanyDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung123
 * @since 2018-12-11
 */
public interface CompanyDetailDao extends BaseMapper<CompanyDetail> {

}
