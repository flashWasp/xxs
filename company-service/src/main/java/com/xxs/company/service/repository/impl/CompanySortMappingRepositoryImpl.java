package com.xxs.company.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.xxs.company.service.entity.CompanyRegionMapping;
import com.xxs.company.service.entity.CompanySortMapping;
import com.xxs.company.service.dao.CompanySortMappingDao;
import com.xxs.company.service.repository.CompanySortMappingRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-11
 */
@Service
public class CompanySortMappingRepositoryImpl extends ServiceImpl<CompanySortMappingDao, CompanySortMapping> implements CompanySortMappingRepository {

    @Override
    public CompanySortMapping selectOne(String companyNo) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(companyNo)){
            condition.eq("company_no",companyNo);
        }
        return selectOne(condition);
    }

    @Override
    public List<CompanySortMapping> selectList(Long sortId) {
        Condition condition = new Condition();
        if (sortId != null){
            condition.eq("sort_id",sortId);
        }
        return selectList(condition);
    }

    @Override
    public List<CompanySortMapping> selectList(String companyNo) {
        Condition condition = new Condition();
        if (companyNo != null){
            condition.eq("company_no",companyNo);
        }
        return selectList(condition);
    }
}
