package com.xxs.company.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanyBase;
import com.xxs.company.service.dao.CompanyBaseDao;
import com.xxs.company.service.entity.CompanyDetail;
import com.xxs.company.service.entity.CompanyRegionBase;
import com.xxs.company.service.repository.CompanyBaseRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
@Service
public class CompanyBaseRepositoryImpl extends ServiceImpl<CompanyBaseDao, CompanyBase> implements CompanyBaseRepository {

    @Override
    public CompanyBase selectByNo(String no) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(no)){
            condition.eq("company_no",no);
            return selectOne(condition);
        }
        return null;
    }

    @Override
    public List<Long> selectIds(String name, Long publicStartTime, Long publicEndTime, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, List<String> companyNos, String state) {
        List<CompanyBase> companyBases = selectList(name, publicStartTime, publicEndTime, createStartTime, createEndTime,
                updateStartTime, updateEndTime, companyNos, state);
        if (!CollectionUtils.isEmpty(companyBases)){
            return companyBases.stream().map(CompanyBase::getId).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<CompanyBase> selectList(String name,
                                        Long publicStartTime, Long publicEndTime,
                                        Long createStartTime, Long createEndTime,
                                        Long updateStartTime, Long updateEndTime,
                                        List<String> companyNos, String state) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(name)){
            condition.eq("name",name);
        }
        if (publicStartTime != null & publicEndTime != null){
            condition.between("public_time",publicStartTime,publicEndTime);
        }
        if (createStartTime != null & publicEndTime != null){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (updateStartTime != null & updateEndTime != null){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        if (StringUtils.isNotEmpty(state)){
            condition.eq("state",state);
        }
        if (!CollectionUtils.isEmpty(companyNos)){
            condition.in("company_no",companyNos);
        }
        return selectList(condition);
    }

    @Override
    public Page<CompanyBase> selectPage(String name, Long publicStartTime, Long publicEndTime, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, List<String> companyNos, String state, Integer pageNo, Integer pageSize) {
        Page<CompanyBase> page = new Page<>(pageNo,pageSize);
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(state)){
            condition.eq("state",state);
        }
        if (StringUtils.isNotEmpty(name)){
            condition.like("name",name);
        }
        if (!CollectionUtils.isEmpty(companyNos)){
            condition.in("company_no",companyNos);
        }
        if (publicStartTime != null & publicEndTime != null){
            condition.between("public_time",publicStartTime,publicEndTime);
        }
        if (createStartTime != null & createEndTime != null){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (updateStartTime != null & updateEndTime != null){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        return selectPage(page,condition);
    }


}
