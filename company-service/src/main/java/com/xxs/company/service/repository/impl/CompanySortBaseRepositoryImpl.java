package com.xxs.company.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanySortBase;
import com.xxs.company.service.dao.CompanySortBaseDao;
import com.xxs.company.service.repository.CompanySortBaseRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
@Service
public class CompanySortBaseRepositoryImpl extends ServiceImpl<CompanySortBaseDao, CompanySortBase> implements CompanySortBaseRepository {

    @Override
    public Page<CompanySortBase> selectPage(String state, String name,
                                            Long createStartTime, Long createEndTime,
                                            Long updateStartTime, Long updateEndTime,
                                            Integer pageNo,Integer pageSize) {
        if (pageNo == null || pageSize == null){
            pageNo = 1;
            pageSize = Integer.MAX_VALUE;
        }
        Page<CompanySortBase> page = new Page<>(pageNo, pageSize);
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(state)){
            condition.eq("state",state);
        }
        if (null != createStartTime & null != createEndTime){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (null != updateStartTime & null != updateEndTime){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        if (StringUtils.isNotEmpty(name)){
            condition.eq("name",name);
        }
        return selectPage(page,condition);
    }

    @Override
    public List<CompanySortBase> selectList(CompanySortBase req) {
        Condition condition = new Condition();
        if (StringUtils.isNotBlank(req.getState())) {
            condition.eq("state",req.getState());
        }
        return selectList(condition);
    }

    @Override
    public List<Long> selectIds(String state, String name, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(state)){
            condition.eq("state",state);
        }
        if (null != createStartTime & null != createEndTime){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (null != updateStartTime & null != updateEndTime){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        if (StringUtils.isNotEmpty(name)){
            condition.eq("name",name);
        }
        List<CompanySortBase> companySortBases = selectList(condition);
        if(CollectionUtils.isEmpty(companySortBases)){
            return companySortBases.stream().map(CompanySortBase::getId).collect(Collectors.toList());
        }
        return null;
    }
}
