package com.xxs.company.service.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.cache.CacheObj;
import com.xxs.common.service.cache.manager.CacheRequestManager;
import com.xxs.common.service.cache.manager.request.DelCacheRequest;
import com.xxs.common.service.cache.manager.request.QueryCacheRequest;
import com.xxs.common.service.cache.manager.request.UpdateCacheRequest;
import com.xxs.common.service.constant.XxsConstant;
import com.xxs.company.service.CompanyConstant;
import com.xxs.company.service.entity.CompanyBase;
import com.xxs.company.service.entity.CompanySortBase;
import com.xxs.company.service.repository.CompanySortBaseRepository;
import com.xxs.company.service.service.CompanySortService;
import groovy.util.IFileNameFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.plugin.cache.CacheUpdateHelper;

import java.util.List;

@Service("companySortService")
public class CompanySortServiceImpl implements CompanySortService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    private static final String COMPANY_SORT = "company_sort";

    @Autowired
    private CompanySortBaseRepository companySortBaseRepository;

    @Override
    public Boolean save(String name) {
        CompanySortBase companySortBase = new CompanySortBase();
        companySortBase.setName(name);
        companySortBase.setCreateTime(System.currentTimeMillis());
        companySortBase.setUpdateTime(System.currentTimeMillis());
        companySortBase.setState(XxsConstant.XXS_STATE_ISNORMAL);
        boolean b = companySortBaseRepository.insertAllColumn(companySortBase);
        if (b){
            if (b){
                CacheHelper.update(COMPANY_SORT,companySortBase.getId().toString(),JSON.toJSONString(companySortBase));
                LOGGER.info(COMPANY_SORT + " insert : id " + companySortBase.getId().toString());
            }
        }
        return b;
    }

    @Override
    public Boolean update(Long id, String name, String state) {
        CompanySortBase companySortBase = companySortBaseRepository.selectById(id);
        if (companySortBase != null){
            companySortBase.setName(name);
            companySortBase.setState(state);
            boolean b = companySortBaseRepository.updateAllColumnById(companySortBase);
            if (b){
                CacheHelper.del(COMPANY_SORT,companySortBase.getId().toString());
            }
            return b;
        }
        return false;
    }

    @Override
    public Boolean del(Long id, Boolean isAll) {
        CompanySortBase companySortBase = companySortBaseRepository.selectById(id);
        boolean b = false;
        if (companySortBase != null){
            companySortBase.setState(XxsConstant.XXS_STATE_ISDELETE);
            companySortBase.setUpdateTime(System.currentTimeMillis());
            b = companySortBaseRepository.updateAllColumnById(companySortBase);
            if (b){
                CacheHelper.del(COMPANY_SORT,companySortBase.getId().toString());
            }
            // 批量删除
            if (b && isAll){
                return true;
            }
        }
        return b;
    }

    @Override
    public CompanySortBase get(Long id) {
        // 缓存查询
        try {
//            Object cacheObj = CacheHelper.read(COMPANY_SORT, id.toString());
//            if (cacheObj != null){
//                return JSONObject.parseObject(cacheObj.toString(),CompanySortBase.class);
//            }
            /**
             * 数据库值查询
             */
            CompanySortBase companySortBase = companySortBaseRepository.selectById(id);
            if (null != companySortBase) {
                CacheHelper.update(COMPANY_SORT,companySortBase.getId().toString(),JSON.toJSONString(companySortBase));
                return companySortBase;
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Long> listIds(String state, String name,
                              Long createStartTime, Long createEndTime,
                              Long updateStartTime, Long updateEndTime) {
        return companySortBaseRepository.selectIds(state,name,createStartTime,createEndTime,updateStartTime,updateEndTime);
    }

    @Override
    public List<CompanySortBase> list(CompanySortBase req) {
        return companySortBaseRepository.selectList(req);
    }

    @Override
    public Page<CompanySortBase> page(String state, String name,
                                      Long createStartTime, Long createEndTime,
                                      Long updateStartTime, Long updateEndTime,
                                      Integer pageNo,Integer pageSize) {
        return companySortBaseRepository.selectPage(state,name,
                createStartTime,createEndTime,
                updateStartTime,updateEndTime,pageNo,pageSize);
    }

}
