package com.xxs.company.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanyRegionBase;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public interface CompanyRegionBaseRepository extends IService<CompanyRegionBase> {

    /**
     * 根据条件查询id集合
     * @param name
     * @param parentId
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param state
     * @return
     */
    List<Long> selectIds(String name, Long parentId, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, String state);

    /**
     * 根据条件查询对象集合
     * @param name
     * @param parentId
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param state
     * @return
     */
    List<CompanyRegionBase> selectList(String name, Long parentId, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, String state);


    /**
     * 分页获取地方对象
     * @param name
     * @param parentId
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param state
     * @param pageNo
     * @param pageSize
     * @return
     */
    Page<CompanyRegionBase> selectPage(String name, Long parentId, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, String state, Integer pageNo, Integer pageSize);

}
