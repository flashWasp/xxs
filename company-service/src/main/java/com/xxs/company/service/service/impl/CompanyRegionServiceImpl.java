package com.xxs.company.service.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.constant.XxsConstant;
import com.xxs.company.service.entity.CompanyRegionBase;
import com.xxs.company.service.repository.CompanyRegionBaseRepository;
import com.xxs.company.service.service.CompanyRegionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("companyRegionService")
public class CompanyRegionServiceImpl implements CompanyRegionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    private static final String COMPANY_REGION = "company_region";

    @Autowired
    private CompanyRegionBaseRepository companyRegionBaseRepository;

    @Override
    public Boolean add(String name, Long parentId) {
        CompanyRegionBase companyRegionBase = new CompanyRegionBase();
        companyRegionBase.setName(name);
        companyRegionBase.setParentId(parentId);
        companyRegionBase.setCreateTime(System.currentTimeMillis());
        companyRegionBase.setUpdateTime(System.currentTimeMillis());
        companyRegionBase.setState(XxsConstant.XXS_STATE_ISNORMAL);
        CompanyRegionBase parentCompany = companyRegionBaseRepository.selectById(parentId);
        if (null != parentCompany) {
            String parentIds = parentCompany.getParentIds();
            companyRegionBase.setParentIds(parentIds + "/" + parentId);
        }
        boolean b = companyRegionBaseRepository.insert(companyRegionBase);
        if (b){
            CacheHelper.update(COMPANY_REGION,companyRegionBase.getId().toString(), JSON.toJSONString(companyRegionBase));
        }
        LOGGER.info(COMPANY_REGION + " insert " + companyRegionBase.getId());
        return b;
    }

    @Override
    public CompanyRegionBase get(Long id) {

        return companyRegionBaseRepository.selectById(id);
    }

    @Override
    public Boolean update(Long id, String name, Long parentId, String state) {
        CompanyRegionBase companyRegionBase = companyRegionBaseRepository.selectById(id);
        if (null != companyRegionBase) {
            companyRegionBase.setName(name);
            companyRegionBase.setParentId(parentId);
            companyRegionBase.setUpdateTime(System.currentTimeMillis());
            companyRegionBase.setState(XxsConstant.XXS_STATE_ISNORMAL);
            CompanyRegionBase parentCompany = companyRegionBaseRepository.selectById(parentId);
            if (null != parentCompany) {
                String parentIds = parentCompany.getParentIds();
                companyRegionBase.setParentIds(parentIds + "/" + parentId);
            }
            return companyRegionBaseRepository.updateAllColumnById(companyRegionBase);
        }
        return false;
    }

    @Override
    public Boolean del(Long id, Boolean isAll) {
        CompanyRegionBase companyRegionBase = companyRegionBaseRepository.selectById(id);
        if (null != companyRegionBase) {
            companyRegionBase.setState(XxsConstant.XXS_STATE_ISDELETE);
            return companyRegionBaseRepository.updateAllColumnById(companyRegionBase);
        }
        return false;
    }

    @Override
    public List<Long> listIds(String name, Long parentId,
                              Long createStartTime, Long createEndTime,
                              Long updateStartTime, Long updateEndTime,
                              String state) {
        return companyRegionBaseRepository.selectIds(
                name, parentId, createStartTime, createEndTime,
                updateStartTime, updateEndTime, state
        );
    }

    @Override
    public List<CompanyRegionBase> list(String name, Long parentId,
                                        Long createStartTime, Long createEndTime,
                                        Long updateStartTime, Long updateEndTime,
                                        String state) {
        return companyRegionBaseRepository.selectList(name, parentId,
                createStartTime, createEndTime,
                updateStartTime, updateEndTime,
                state
        );
    }

    @Override
    public Page<CompanyRegionBase> page(String name, Long parentId, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, String state, Integer pageNo, Integer pageSize) {
        return companyRegionBaseRepository.selectPage(name, parentId,
                createStartTime, createEndTime,
                updateStartTime, updateEndTime,
                state, pageNo, pageSize);
    }
}
