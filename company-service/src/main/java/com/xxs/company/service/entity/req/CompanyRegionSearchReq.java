package com.xxs.company.service.entity.req;

/**
 * @author Administrator
 * @Title: CompanyRegionSearchReq
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/120:18
 */
public class CompanyRegionSearchReq {

    /**
     *
     */
    private String name;

    /**
     * 父类Id
     */
    private Long parentId;

    /**
     * 创建开始时间
     */
    private Long createStartTime;

    /**
     * 创建结束时间
     */
    private Long createEndTime;

    /**
     * 更新开始时间
     */
    private Long updateStartTime;

    /**
     * 更新结束时间
     */
    private Long updateEndTime;

    /**
     * 状态值
     */
    private String state;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 页大小
     */
    private Integer rows;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getCreateStartTime() {
        return createStartTime;
    }

    public void setCreateStartTime(Long createStartTime) {
        this.createStartTime = createStartTime;
    }

    public Long getCreateEndTime() {
        return createEndTime;
    }

    public void setCreateEndTime(Long createEndTime) {
        this.createEndTime = createEndTime;
    }

    public Long getUpdateStartTime() {
        return updateStartTime;
    }

    public void setUpdateStartTime(Long updateStartTime) {
        this.updateStartTime = updateStartTime;
    }

    public Long getUpdateEndTime() {
        return updateEndTime;
    }

    public void setUpdateEndTime(Long updateEndTime) {
        this.updateEndTime = updateEndTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
}
