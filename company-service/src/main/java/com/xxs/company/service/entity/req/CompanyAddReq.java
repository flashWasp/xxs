package com.xxs.company.service.entity.req;

/**
 * @author Administrator
 * @Title: CompanyAddReq
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1222:35
 */
public class CompanyAddReq {

    /**
     * 公司名
     */
    private String name;

    /**
     * 简介
     */
    private String summary;

    /**
     * 分类Id
     */
    private Long sortId;

    /**
     * 状态
     */
    private String state;

    /**
     * 图片编号
     */
    private String picNo;

    /**
     * 地方编号
     */
    private Long regionId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPicNo() {
        return picNo;
    }

    public void setPicNo(String picNo) {
        this.picNo = picNo;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }
}
