package com.xxs.company.service.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.cache.Cache;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.cache.CacheObj;
import com.xxs.common.service.cache.manager.CacheRequestManager;
import com.xxs.common.service.cache.manager.request.DelCacheRequest;
import com.xxs.common.service.cache.manager.request.QueryCacheRequest;
import com.xxs.common.service.cache.manager.request.UpdateCacheRequest;
import com.xxs.common.service.constant.XxsConstant;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.company.service.CompanyConstant;
import com.xxs.company.service.entity.*;
import com.xxs.company.service.repository.CompanyBaseRepository;
import com.xxs.company.service.repository.CompanyDetailRepository;
import com.xxs.company.service.repository.CompanyRegionMappingRepository;
import com.xxs.company.service.repository.CompanySortMappingRepository;
import com.xxs.company.service.service.CompanyRegionService;
import com.xxs.company.service.service.CompanyService;
import com.xxs.company.service.service.CompanySortService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    private static final String COMPANY_BASE = "company_base";

    private static final String COMPANY_DETAIL ="company_detail";

    @Autowired
    private CompanyBaseRepository companyBaseRepository;

    @Autowired
    private CompanyRegionMappingRepository companyRegionMappingRepository;

    @Autowired
    private CompanySortMappingRepository companySortMappingRepository;

    @Autowired
    private CompanyDetailRepository companyDetailRepository;

    @Autowired
    private CompanySortService companySortService;

    @Autowired
    private CompanyRegionService companyRegionService;

    @Override
    public Boolean add(String name, String summary,
                       Long publishTime, String state,
                       String picNo, Long regionId, Long companySortId) {
        CompanyBase companyBase = new CompanyBase();
        companyBase.setName(name);
        companyBase.setSummary(summary);
        companyBase.setCompanyNo(KeyUtil.getUniqueKey());
        companyBase.setPublishTime(publishTime);
        companyBase.setState(state);
        companyBase.setPicNo(picNo);
        // 获取分类对象
        CompanySortBase companySortBase = companySortService.get(companySortId);
        if (null != companySortBase) {
            companyBase.setCompanySortName(companySortBase.getName());
            CompanySortMapping companySortMapping = new CompanySortMapping();
            companySortMapping.setCompanyNo(companyBase.getCompanyNo());
            companySortMapping.setSortId(companySortId);
            companySortMappingRepository.insertAllColumn(companySortMapping);
        }
        CompanyRegionBase companyRegionBase = companyRegionService.get(regionId);
        if (null != companyRegionBase) {
            companyBase.setRegionName(companyRegionBase.getName());
            CompanyRegionMapping companyRegionMapping = new CompanyRegionMapping();
            companyRegionMapping.setCompanyNo(companyBase.getCompanyNo());
            companyRegionMapping.setRegionId(regionId);
            companyRegionMappingRepository.insertAllColumn(companyRegionMapping);
        }
        companyBase.setUpdateTime(System.currentTimeMillis());
        companyBase.setCreateTime(System.currentTimeMillis());
        boolean b = companyBaseRepository.insertAllColumn(companyBase);
        if (b){
            CacheHelper.update(COMPANY_BASE,companyBase.getCompanyNo(),JSON.toJSONString(companyBase));
            LOGGER.info(COMPANY_BASE + " insert : no " + companyBase.getCompanyNo());
        }
        return b;
    }

    @Override
    public Boolean update(String no, String name, String summary,
                          Long publishTime, String state,
                          String picNo, Long regionId, Long companySortId) {
        CompanyBase companyBase = companyBaseRepository.selectByNo(no);
        if (null != companyBase) {
            companyBase.setName(name);
            companyBase.setSummary(summary);
            companyBase.setPublishTime(publishTime);
            companyBase.setState(state);
            companyBase.setPicNo(picNo);
            // 封装公司地址对应
            CompanyRegionMapping companyRegionMapping = companyRegionMappingRepository.selectOne(companyBase.getCompanyNo());
            if (null != companyRegionMapping) {
                companyRegionMapping.setRegionId(regionId);
                companyRegionMappingRepository.updateAllColumnById(companyRegionMapping);
            }
            // 封装分类对应
            CompanySortMapping companySortMapping = companySortMappingRepository.selectOne(companyBase.getCompanyNo());
            if (null != companySortMapping) {
                companySortMapping.setSortId(companySortId);
                companySortMappingRepository.updateAllColumnById(companySortMapping);
            }

            CompanyRegionBase companyRegionBase = companyRegionService.get(regionId);
            if (null != companyRegionBase) {
                companyBase.setRegionName(companyRegionBase.getName());
            }
            CompanySortBase companySortBase = companySortService.get(companySortId);
            if (null != companySortBase) {
                companyBase.setCompanySortName(companySortBase.getName());
            }
            boolean b = companyBaseRepository.updateAllColumnById(companyBase);
            if (b){
                CacheHelper.del(COMPANY_BASE,companyBase.getCompanyNo());
                LOGGER.info(COMPANY_BASE + " del : no " + companyBase.getCompanyNo());
            }
            return b;
        }
        return false;
    }

    @Override
    public CompanyBase get(String companyNo) {
        // 缓存查询
//        Object cacheObj = CacheHelper.read(COMPANY_BASE, companyNo);
//        if (cacheObj != null){
//            return JSONObject.parseObject(cacheObj.toString(),CompanyBase.class);
//        }
        /**
         * 数据库值查询
         */
        CompanyBase companyBase = companyBaseRepository.selectByNo(companyNo);
        if (null != companyBase) {
//            CacheHelper.update(COMPANY_BASE,companyNo,JSONObject.toJSONString(companyBase));
            return companyBase;
        }
        return null;
    }

    @Override
    public Boolean del(String companyNo) {
        CompanyBase companyBase = companyBaseRepository.selectByNo(companyNo);
        if (null != companyBase) {
            companyBase.setState(XxsConstant.XXS_STATE_ISDELETE);
            boolean b = companyBaseRepository.updateAllColumnById(companyBase);
            if (b){
                CacheHelper.del(COMPANY_BASE,companyNo);
                LOGGER.info(COMPANY_BASE + " del : no " + companyBase.getCompanyNo());
            }
            return b;
        }
        return false;
    }

    @Override
    public List<Long> listIds(String name, Long publicStartTime, Long publicEndTime, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, List<String> companyNos, String state) {
        return companyBaseRepository.selectIds(
                name, publicStartTime, publicEndTime, createStartTime, createEndTime,
                updateStartTime, updateEndTime, companyNos, state);
    }

    @Override
    public List<CompanyBase> list(String name,
                                  Long publicStartTime, Long publicEndTime,
                                  Long createStartTime, Long createEndTime,
                                  Long updateStartTime, Long updateEndTime,
                                  List<String> companyNos,
                                  String state) {
        return companyBaseRepository.selectList(
                name, publicStartTime, publicEndTime, createStartTime, createEndTime,
                updateStartTime, updateEndTime, companyNos, state);
    }

    @Override
    public Page<CompanyBase> page(String name,
                                  Long publicStartTime, Long publicEndTime,
                                  Long createStartTime, Long createEndTime,
                                  Long updateStartTime, Long updateEndTime,
                                  List<String> companyNos,
                                  String state, Integer pageNo, Integer pageSize) {
       return  companyBaseRepository.selectPage(name, publicStartTime, publicEndTime,
                createStartTime, createEndTime, updateStartTime, updateEndTime,
                companyNos, state, pageNo, pageSize
        );
    }

    @Override
    public CompanyDetail detail(String companyNo) {
        // 缓存查询
//        Object cacheObj = CacheHelper.read(COMPANY_DETAIL, companyNo);
//        if (cacheObj != null){
//            LOGGER.info(COMPANY_DETAIL + " read : no " + companyNo);
//            return JSONObject.parseObject(cacheObj.toString(),CompanyDetail.class);
//        }
        /*
         * 数据库值查询
         */
        CompanyDetail companyDetail = companyDetailRepository.selectOne(companyNo);
        if (null != companyDetail) {
//            CacheHelper.update(COMPANY_DETAIL,companyNo,JSONObject.toJSONString(companyDetail));
            LOGGER.info(COMPANY_DETAIL + " update : no " + companyDetail.getCompanyNo());
            return companyDetail;
        }
        return null;
    }

    @Override
    public Boolean support(String founderNo, String url, String address,
                           Long businessStartTime, Long businessEndTime,
                           String phone, String fullName, Long establishTime,
                           String legalRepresentative, String paidupCapital,
                           String content, String advantage, String history,
                           String team, String picNos, String companyNo) {
        CompanyDetail companyDetail = new CompanyDetail();
        companyDetail.setFounderNo(founderNo);
        companyDetail.setUrl(url);
        companyDetail.setAddress(address);
        companyDetail.setBusinessStartTime(businessStartTime);
        companyDetail.setBusinessEndTime(businessEndTime);
        companyDetail.setPhone(phone);
        companyDetail.setFullName(fullName);
        companyDetail.setEstablishTime(establishTime);
        companyDetail.setLegalRepresentative(legalRepresentative);
        companyDetail.setPaidupCapital(paidupCapital);
        companyDetail.setContent(content);
        companyDetail.setAdvantage(advantage);
        companyDetail.setHistory(history);
        companyDetail.setTeam(team);
        companyDetail.setPicNos(picNos);
        companyDetail.setCompanyNo(companyNo);
        CompanyDetail findOne = companyDetailRepository.selectOne(companyNo);
        boolean flag;
        if (null == findOne) {
            flag = companyDetailRepository.insert(companyDetail);
        } else {
            companyDetail.setId(findOne.getId());
            flag = companyDetailRepository.updateAllColumnById(companyDetail);
        }
        return flag;
    }

    @Override
    public List<String> listCompanyNosByRegionId(Long regionId) {
        List<CompanyRegionMapping> companyRegionMappings = companyRegionMappingRepository.selectList(regionId);
        if (!CollectionUtils.isEmpty(companyRegionMappings)) {
            return companyRegionMappings.stream().map(CompanyRegionMapping::getCompanyNo).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<Long> listRegionIdsByCompanyNo(String compangNo) {
        List<CompanyRegionMapping> companyRegionMappings = companyRegionMappingRepository.selectList(compangNo);
        if (!CollectionUtils.isEmpty(companyRegionMappings)) {
            return companyRegionMappings.stream().map(CompanyRegionMapping::getRegionId).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<String> listCompanyNosBySortId(Long sortId) {
        List<CompanySortMapping> companySortMappings = companySortMappingRepository.selectList(sortId);
        if (!CollectionUtils.isEmpty(companySortMappings)) {
            return companySortMappings.stream().map(CompanySortMapping::getCompanyNo).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<Long> listSortIdsByCompanyNo(String compangNo) {
        List<CompanySortMapping> companyRegionMappings = companySortMappingRepository.selectList(compangNo);
        if (!CollectionUtils.isEmpty(companyRegionMappings)) {
            return companyRegionMappings.stream().map(CompanySortMapping::getSortId).collect(Collectors.toList());
        }
        return null;
    }

}


