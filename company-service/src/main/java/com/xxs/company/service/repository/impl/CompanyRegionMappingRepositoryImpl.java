package com.xxs.company.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.xxs.company.service.entity.CompanyRegionMapping;
import com.xxs.company.service.dao.CompanyRegionMappingDao;
import com.xxs.company.service.repository.CompanyRegionMappingRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
@Service
public class CompanyRegionMappingRepositoryImpl extends ServiceImpl<CompanyRegionMappingDao, CompanyRegionMapping> implements CompanyRegionMappingRepository {

    @Override
    public CompanyRegionMapping selectOne(String companyNo) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(companyNo)){
            condition.eq("company_no",companyNo);
        }
        return selectOne(condition);
    }

    @Override
    public List<CompanyRegionMapping> selectList(Long regionId) {
        Condition condition = new Condition();
        if (regionId != null){
            condition.eq("region_id",regionId);
        }
        return selectList(condition);
    }

    @Override
    public List<CompanyRegionMapping> selectList(String companyNo) {
        Condition condition = new Condition();
        if (companyNo != null){
            condition.eq("company_no",companyNo);
        }
        return selectList(condition);
    }
}
