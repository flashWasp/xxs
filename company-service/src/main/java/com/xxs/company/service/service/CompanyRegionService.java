package com.xxs.company.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanyRegionBase;

import java.util.List;

public interface CompanyRegionService {

    Boolean add(String name,Long parentId);

    CompanyRegionBase get(Long id);

    Boolean update(Long id,String name,Long parentId,String state);

    Boolean del(Long id,Boolean isAll);

    List<Long> listIds(String name,
                       Long parentId,
                       Long createStartTime,
                       Long createEndTime,
                       Long updateStartTime,
                       Long updateEndTime,
                       String state);

    List<CompanyRegionBase> list(
            String name,
            Long parentId,
            Long createStartTime,
            Long createEndTime,
            Long updateStartTime,
            Long updateEndTime,
            String state
    );

    Page<CompanyRegionBase> page(
            String name,
            Long parentId,
            Long createStartTime,
            Long createEndTime,
            Long updateStartTime,
            Long updateEndTime,
            String state,
            Integer pageNo,
            Integer pageSize


    );
}
