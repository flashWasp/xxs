package com.xxs.company.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanyRegionBase;
import com.xxs.company.service.dao.CompanyRegionBaseDao;
import com.xxs.company.service.entity.CompanySortBase;
import com.xxs.company.service.repository.CompanyRegionBaseRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
@Service
public class CompanyRegionBaseRepositoryImpl extends ServiceImpl<CompanyRegionBaseDao, CompanyRegionBase> implements CompanyRegionBaseRepository {

    @Override
    public List<Long> selectIds(String name, Long parentId,
                                Long createStartTime, Long createEndTime,
                                Long updateStartTime, Long updateEndTime,
                                String state) {
        List<CompanyRegionBase> companyRegionBases = selectList(name, parentId, createStartTime, createEndTime, updateStartTime, updateEndTime, state);
        if (!CollectionUtils.isEmpty(companyRegionBases)){
            return companyRegionBases.stream().map(CompanyRegionBase::getId).collect(Collectors.toList());
        }
        return  new ArrayList<>();
    }

    @Override
    public List<CompanyRegionBase> selectList(String name, Long parentId,
                                              Long createStartTime, Long createEndTime,
                                              Long updateStartTime, Long updateEndTime,
                                              String state) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(name)){
            condition.like("name",name);
        }
        if (null != parentId){
            condition.eq("parent_id",parentId);
        }
        if (null != createStartTime & null != createEndTime){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (null != updateStartTime & null != updateEndTime){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        if (StringUtils.isNotEmpty(state)){
            condition.eq("state",state);
        }
       return  selectList(condition);
    }

    @Override
    public Page<CompanyRegionBase> selectPage(String name, Long parentId,
                                              Long createStartTime, Long createEndTime,
                                              Long updateStartTime, Long updateEndTime,
                                              String state,
                                              Integer pageNo, Integer pageSize) {
        if (pageNo == null || pageSize == null){
            pageNo = 1;
            pageSize = Integer.MAX_VALUE;
        }
        Page<CompanyRegionBase> page = new Page<>(pageNo, pageSize);

        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(name)){
            condition.like("name",name);
        }
        if (null != parentId){
            condition.eq("parent_id",parentId);
        }
        if (null != createStartTime & null != createEndTime){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (null != updateStartTime & null != updateEndTime){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        if (StringUtils.isNotEmpty(state)){
            condition.eq("state",state);
        }
        return selectPage(page,condition);
    }
}
