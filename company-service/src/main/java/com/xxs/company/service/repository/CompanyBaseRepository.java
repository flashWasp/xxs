package com.xxs.company.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.company.service.entity.CompanyBase;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.company.service.entity.CompanyDetail;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-10
 */
public interface CompanyBaseRepository extends IService<CompanyBase> {

    /**
     * 获取公司信息
     * @param no
     * @return
     */
    CompanyBase selectByNo(String no);

    /**
     * 根据条件获取公司信息Id集合
     * @param name
     * @param publicStartTime
     * @param publicEndTime
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param companyNos
     * @param state
     * @return
     */
    List<Long> selectIds(String name, Long publicStartTime, Long publicEndTime, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, List<String> companyNos, String state);


    /**
     * 获取公司对象集合
     * @param name
     * @param publicStartTime
     * @param publicEndTime
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param companyNos
     * @param state
     * @return
     */
    List<CompanyBase> selectList(String name, Long publicStartTime, Long publicEndTime, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, List<String> companyNos, String state);

    /**
     * 分页公司对象集合
     * @param name
     * @param publicStartTime
     * @param publicEndTime
     * @param createStartTime
     * @param createEndTime
     * @param updateStartTime
     * @param updateEndTime
     * @param companyNos
     * @param state
     * @return
     */
    Page<CompanyBase> selectPage(String name, Long publicStartTime, Long publicEndTime, Long createStartTime, Long createEndTime, Long updateStartTime, Long updateEndTime, List<String> companyNos, String state, Integer pageNo, Integer pageSize);


}
