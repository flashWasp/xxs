package com.xxs.company.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.xxs.company.service.entity.CompanyDetail;
import com.xxs.company.service.dao.CompanyDetailDao;
import com.xxs.company.service.repository.CompanyDetailRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung123
 * @since 2018-12-11
 */
@Service
public class CompanyDetailRepositoryImpl extends ServiceImpl<CompanyDetailDao, CompanyDetail> implements CompanyDetailRepository {


    @Override
        public CompanyDetail selectOne(String companyNo) {
            Condition condition = new Condition();
            if (StringUtils.isNotEmpty(companyNo)){
                condition.eq("company_no",companyNo);
            }
            return selectOne(condition);
        }

}
