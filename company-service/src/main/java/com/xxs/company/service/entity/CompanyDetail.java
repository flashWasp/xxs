package com.xxs.company.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung123
 * @since 2018-12-11
 */
public class CompanyDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 创始人Id
     */
    private String founderNo;
    /**
     * 网址
     */
    private String url;
    /**
     * 地点
     */
    private String address;
    /**
     * 营业开始时间
     */
    private Long businessStartTime;
    /**
     * 营业结束时间
     */
    private Long businessEndTime;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 全名
     */
    private String fullName;
    /**
     * 创建时间
     */
    private Long establishTime;
    /**
     * 法定代表人
     */
    private String legalRepresentative;
    /**
     * 公司类型
     */
    private String paidupCapital;
    /**
     * 内容
     */
    private String content;
    /**
     * 优势
     */
    private String advantage;
    /**
     * 历史记录
     */
    private String history;
    /**
     * 团结介绍
     */
    private String team;
    /**
     * 详情图片介绍
     */
    private String picNos;
    private String companyNo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFounderNo() {
        return founderNo;
    }

    public void setFounderNo(String founderNo) {
        this.founderNo = founderNo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getBusinessStartTime() {
        return businessStartTime;
    }

    public void setBusinessStartTime(Long businessStartTime) {
        this.businessStartTime = businessStartTime;
    }

    public Long getBusinessEndTime() {
        return businessEndTime;
    }

    public void setBusinessEndTime(Long businessEndTime) {
        this.businessEndTime = businessEndTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getEstablishTime() {
        return establishTime;
    }

    public void setEstablishTime(Long establishTime) {
        this.establishTime = establishTime;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }

    public String getPaidupCapital() {
        return paidupCapital;
    }

    public void setPaidupCapital(String paidupCapital) {
        this.paidupCapital = paidupCapital;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAdvantage() {
        return advantage;
    }

    public void setAdvantage(String advantage) {
        this.advantage = advantage;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getPicNos() {
        return picNos;
    }

    public void setPicNos(String picNos) {
        this.picNos = picNos;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    @Override
    public String toString() {
        return "CompanyDetail{" +
        ", id=" + id +
        ", founderNo=" + founderNo +
        ", url=" + url +
        ", address=" + address +
        ", businessStartTime=" + businessStartTime +
        ", businessEndTime=" + businessEndTime +
        ", phone=" + phone +
        ", fullName=" + fullName +
        ", establishTime=" + establishTime +
        ", legalRepresentative=" + legalRepresentative +
        ", paidupCapital=" + paidupCapital +
        ", content=" + content +
        ", advantage=" + advantage +
        ", history=" + history +
        ", team=" + team +
        ", picNos=" + picNos +
        ", companyNo=" + companyNo +
        "}";
    }
}
