package com.xxs.web.entity.req;

/**
 * @author Administrator
 * @Title: CompanySearchReq
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1420:49
 */
public class CompanySearchReq {

    /**
     * 关键字
     */
    private String keyWord;

    /**
     * 分类Id
     */
    private String sortId;

    /**
     * 地域Id
     */
    private String regionId;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 页大小
     */
    private Integer rows;

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
}
