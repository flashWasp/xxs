package com.xxs.web.entity.req;

import io.swagger.models.auth.In;

/**
 * 用户信息补充对象
 */
public class MemberSupportReq {

    /**
     * 编号
     */
    private String memberNo;

    /**
     * 用户名
     */
    private String name;

    /**
     * qq
     */
    private String qqNo;

    /**
     * 备注
     */
    private String remark;

    /**
     * 昵称
     */
    private String nickName;
    // 性别
    private Integer sex;

    /**
     * 图片
     */
    private String picImg;

    /**
     * 微信
     */
    private String wechat;

    private String address;

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQqNo() {
        return qqNo;
    }

    public void setQqNo(String qqNo) {
        this.qqNo = qqNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPicImg() {
        return picImg;
    }

    public void setPicImg(String picImg) {
        this.picImg = picImg;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
}
