package com.xxs.web.entity.req;

/**
 * 评论创建对象
 */
public class CommentCreateReq {

    /**
     * 评论发送者
     */
    private String senderNo;

    /**
     * 评论接受者
     */
    private String receiverNo;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 父级评论Id
     */
    private String parentCommentNo;

    /**
     * 文章编号
     */
    private String articleNo;

    /**
     * 点赞量
     */
    private Integer like;

    /**
     * 贬低量
     */
    private Integer down;

    /**
     * 发送者名称
     */
    private String senderName;

    /**
     * 接受者名称
     */
    private String receiverName;

    private String targetCommentNo;



    public String getSenderNo() {
        return senderNo;
    }

    public void setSenderNo(String senderNo) {
        this.senderNo = senderNo;
    }

    public String getReceiverNo() {
        return receiverNo;
    }

    public void setReceiverNo(String receiverNo) {
        this.receiverNo = receiverNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getParentCommentNo() {
        return parentCommentNo;
    }

    public void setParentCommentNo(String parentCommentNo) {
        this.parentCommentNo = parentCommentNo;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public Integer getDown() {
        return down;
    }

    public void setDown(Integer down) {
        this.down = down;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTargetCommentNo() {
        return targetCommentNo;
    }

    public void setTargetCommentNo(String targetCommentNo) {
        this.targetCommentNo = targetCommentNo;
    }
}
