package com.xxs.web.entity.req;

/**
 * @author Administrator
 * @Title: CompanyRegionSearchReq
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1421:27
 */
public class CompanyRegionSearchReq {
    /**
     * 地域名称
     */
    private String name;

    /**
     * 地域状态
     */
    private String state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
