package com.xxs.web.service;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.NewsArticleSearchReq;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "news-service")
public interface ArticleService {

    /**
     * 获取最新的文章(分页)
     *
     * @return
     */
    @RequestMapping(value = "/xxs-news-server/newsArticle/pagePassArticle", method = RequestMethod.POST)
    AjaxResult page(@RequestBody NewsArticleSearchReq req);

    /**
     * 获取最热的文章
     *
     * @param size
     * @return
     */
    @RequestMapping(value = "/xxs-news-server/newsArticle/hotWebArticle", method = RequestMethod.GET)
    AjaxResult hot(@RequestParam("size") Integer size);

    /**
     * 获取文章内容
     *
     * @param articleNo
     * @return
     */
    @RequestMapping(value = "/xxs-news-server/newsArticle/getWebArticle")
    AjaxResult get(@RequestParam("articleNo") String articleNo);


    /**
     * 点赞
     *
     * @param articleNo
     */
    @RequestMapping(value = "/like/doLike", method = RequestMethod.GET)
    AjaxResult like(@RequestParam("articleNo") String articleNo);

    /**
     * 取消点赞
     *
     * @param articleNo
     */
    @RequestMapping(value = "/like/notLike", method = RequestMethod.GET)
    AjaxResult disLike(@RequestParam("articleNo") String articleNo);

    /**
     * 收藏
     *
     * @param articleNo
     */
    @RequestMapping(value = "/fav/add", method = RequestMethod.GET)
    AjaxResult fav(@RequestParam("articleNo") String articleNo);

    /**
     * 取消收藏
     *
     * @param articleNo
     */
    @RequestMapping(value = "/fav/del", method = RequestMethod.GET)
    AjaxResult disFav(@RequestParam("articleNo") String articleNo);

    @RequestMapping(value = "/xxs-news-server/newsArticle/search",method = RequestMethod.GET)
    AjaxResult search(@RequestParam("content") String conentet,@RequestParam("pageNo")Integer pageNo,
                      @RequestParam("pageSize")Integer pageSize);
}
