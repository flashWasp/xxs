package com.xxs.web.service.impl;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.NewsArticleSearchReq;
import com.xxs.web.service.ArticleService;


public class ArticleServiceImpl implements ArticleService {

    @Override
    public AjaxResult page(NewsArticleSearchReq req) {
        return null;
    }

    @Override
    public AjaxResult hot(Integer size) {
        return null;
    }

    @Override
    public AjaxResult get(String articleNo) {
        return null;
    }

    @Override
    public AjaxResult like(String articleNo) {
        return null;
    }

    @Override
    public AjaxResult disLike(String articleNo) {
        return null;
    }

    @Override
    public AjaxResult fav(String articleNo) {
        return null;
    }

    @Override
    public AjaxResult disFav(String articleNo) {
        return null;
    }

    @Override
    public AjaxResult search(String conente, Integer pageNo, Integer pageSize) {
        return null;
    }
}
