package com.xxs.web.service;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.CommentCreateReq;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Get请求需要 @RequestParam
 */
@FeignClient(name = "news-service")
public interface CommentService {

    @ApiOperation("根据文章获取其所有评论")
    @RequestMapping(value = "/xxs-news-server/newsArticleComment/pageCommentByArticleNo",method = RequestMethod.GET)
    AjaxResult pageCommentByArticleNo(@RequestParam(value = "articleNo")String articleNo, @RequestParam(value = "pageNo") Integer pageNo, @RequestParam(value = "pageSize") Integer pageSize);

    /**
     * 根据父级评论获取子级评论对象（分页）
     * @param parentId
     * @return
     */
    @ApiOperation("根据父级评论获取子级评论对象")
    @RequestMapping(value = "/xxs-news-server/newsArticleComment/pageCommentByParentId",method = RequestMethod.GET)
    AjaxResult pageCommentByParentId(@RequestParam(value = "parentId") String parentId);

    /**
     * 创建评论对象
     * @return
     */
    @ApiOperation("创建评论对象")
    @RequestMapping(value = "/xxs-news-server/newsArticleComment/createComment",method = RequestMethod.POST)
    AjaxResult createComment(@RequestBody CommentCreateReq req);

    @RequestMapping(value = "/xxs-news-server/newsArticleComment/get",method = RequestMethod.GET)
    AjaxResult get(String commentNo);

}
