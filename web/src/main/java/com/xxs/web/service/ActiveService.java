package com.xxs.web.service;

import com.xxs.common.service.model.AjaxResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Administrator
 * @Title: ActiveService
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1514:55
 */
@FeignClient("active-service")
public interface ActiveService {

    /**
     * 根据分页获取活动内容
     * @param page
     * @param rows
     * @return
     */
    @GetMapping("/active/pagePass")
    AjaxResult page(@RequestParam("page") Integer page,
                    @RequestParam("rows") Integer rows);

    /**
     * 根据活动编号 获取活动内容
     * @param no
     * @return
     */
    @GetMapping("/active/get")
    AjaxResult get(@RequestParam("no") String no);

    /**
     * 关注活动
     * @param activeNo
     * @return
     */
    @GetMapping("/active/follow")
    AjaxResult follow(@RequestParam("activeNo") String activeNo);

    /**
     * 取消活动
     * @param activeNo
     * @return
     */
    @GetMapping("/active/disFollow")
    AjaxResult disFollow(@RequestParam("activeNo") String activeNo);

    @GetMapping("/active/pageSelfFollowing")
    AjaxResult pageSelfFollowing(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows);
}
