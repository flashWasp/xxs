package com.xxs.web.service.impl;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.web.entity.req.CommentCreateReq;
import com.xxs.web.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class CommentServiceImpl implements CommentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommentServiceImpl.class);

    @Override
    public AjaxResult pageCommentByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        LOGGER.info("调用{}失败","pageCommentByArticleNo");
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @Override
    public AjaxResult pageCommentByParentId(String parentId) {
        LOGGER.info("调用{}失败","pageCommentByParentId");
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @Override
    public AjaxResult createComment(CommentCreateReq req) {
        LOGGER.info("调用{}失败","createComment");
        return AjaxResult.getError(ResultCode.ParamException);
    }
}
