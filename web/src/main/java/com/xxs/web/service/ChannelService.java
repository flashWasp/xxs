package com.xxs.web.service;

import com.xxs.common.service.model.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "news-service")
public interface ChannelService {

    /**
     * 获取所有的频道对象
     * @return
     */
    @RequestMapping(value = "/newsChannel/listFirstNewsChannel",method = RequestMethod.GET)
    AjaxResult listChannel();

    /**
     * 获取频道详情
     * @param
     * @return
     */
    @RequestMapping(value = "/newsChannel/detail",method = RequestMethod.GET)
    AjaxResult detail(@RequestParam("id") Integer  id);

}
