package com.xxs.web.service;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.CompanyRegionSearchReq;

import com.xxs.web.entity.req.CompanySearchReq;
import com.xxs.web.entity.req.CompanySortSearchReq;
import org.bouncycastle.cert.ocsp.Req;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Administrator
 * @Title: CompanyService
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1420:44
 */
@FeignClient(name = "company-service")
public interface CompanyService {

    /**
     * 获取所有地区信息
     * @return
     */
    @PostMapping(value = "/region/list")
    AjaxResult listRegion(CompanyRegionSearchReq req);

    /**
     * 获取所有分类
     * @return
     */
    @PostMapping(value = "/sort/page")
    AjaxResult listSort(Map<String,Object> req);

    /**
     * 分页获取公司内容
     * @return
     */
    @PostMapping("/company/page")
    AjaxResult pageCompany(CompanySearchReq req);

    /**
     * 获取公司基本信息
     * @param companyNo
     * @return
     */
    @RequestMapping(value = "/company/get",method = RequestMethod.GET)
    AjaxResult getCompany(@RequestParam("companyNo")String companyNo);

    /**
     * 获取公司详情
     * @param companyNo
     * @return
     */
    @RequestMapping(value = "/company/detail",method = RequestMethod.GET)
    AjaxResult getCompanyDetail(@RequestParam("companyNo")String companyNo);
}
