package com.xxs.web.service;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.MemberSupportReq;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author Administrator
 * @Title: MemberService
 * @ProjectName master
 * @Description: 平台用户服务
 * @date 2018/12/1515:32
 */
@FeignClient("member-service")
public interface MemberService {

    /**
     * 注册用户信息 :
     * 版本1 :
     * 1）生成用户信息
     * 2）利用用户信息获取验证码 发送到邮箱
     * 3）验证用户信息
     * @param params
     * @return
     */
    @PostMapping("/member/registered")
    AjaxResult registered(@RequestBody  Map<String,Object> params);

    /**
     * 验证用户信息
     * @param account
     * @param validCode
     * @return
     */
    @GetMapping("/member/valid")
    AjaxResult valid(@RequestParam("account") String account,
                     @RequestParam("validCode") String validCode);

    /**
     * 补充用户信息
     * @return
     */
    @PostMapping("/member/support")
    AjaxResult support(@RequestBody MemberSupportReq req);

    /**
     * 根据用户编码获取用户基本信息
     * @param no
     * @return
     */
    @GetMapping("/member/get")
    AjaxResult get(@RequestParam("no") String no);

    /**
     * 根据用户编号获取用户详细信息
     * @param no
     * @return
     */
    @GetMapping("/member/detail")
    AjaxResult detail(@RequestParam("no")String no);



}
