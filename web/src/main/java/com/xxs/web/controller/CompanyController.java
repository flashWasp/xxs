package com.xxs.web.controller;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.CompanyRegionSearchReq;
import com.xxs.web.entity.req.CompanySearchReq;
import com.xxs.web.entity.req.CompanySortSearchReq;
import com.xxs.web.service.CompanyService;
import io.swagger.annotations.ApiOperation;
import org.bouncycastle.cert.ocsp.Req;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @Title: CompanyController
 * @ProjectName master
 * @Description: 公司模块 Map做参数
 * @date 2018/12/140:28
 */
@RestController
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @ApiOperation("获取所有分类")
    @GetMapping("/sort/list")
    public AjaxResult listSort(String name){
        Map<String, Object> req = new HashMap<>();
        req.put("name",name);
        return companyService.listSort(req);
    }

    @ApiOperation("获取所有地区信息")
    @PostMapping("/region/list")
    public AjaxResult listRegion(@RequestBody CompanyRegionSearchReq req){
        return companyService.listRegion(req);
    }

    @ApiOperation("分页获取公司内容")
    @PostMapping("/company/page")
    public AjaxResult page(@RequestBody CompanySearchReq req){
        return  companyService.pageCompany(req);
    }

    @ApiOperation("获取公司基本信息")
    @GetMapping("/company/get")
    public AjaxResult get(String companyNo){
        return companyService.getCompany(companyNo);
    }

    @ApiOperation("获取公司详情")
    @GetMapping("/company/detail")
    public AjaxResult detail(String companyNo){
        return companyService.getCompanyDetail(companyNo);
    }



}
