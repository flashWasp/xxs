package com.xxs.web.controller;


import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.entity.req.NewsArticleSearchReq;
import com.xxs.web.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 频道 文章 API
 */
@RestController
@Api("频道对象")
@RequestMapping("/web/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    /**
     * 分页查询文章（最新）
     * @return
     */
    @ApiOperation("分页查询文章")
    @PostMapping("/page")
    public AjaxResult pageArticle(@RequestBody NewsArticleSearchReq req) {
        return articleService.page(req);
    }

    /**
     * 获取最热文章集合
     *
     * @return
     */
    @ApiOperation("获取最热文章集合")
    @GetMapping("/hot")
    public AjaxResult hot(@RequestParam("size") Integer size) {
        return articleService.hot(size);
    }

    /**
     * 根据编号获取文章内容
     * @param articleNo
     * @return
     */
    @ApiOperation("根据编号获取文章内容")
    @GetMapping("/view")
    public AjaxResult view(@RequestParam("articleNo") String articleNo) {
        // 查询文章
        return articleService.get(articleNo);
    }

    /**
     * 点赞文章
     *
     * @param articleNo
     * @return
     */
    @ApiOperation("点赞文章")
    @GetMapping("/like")
    public AjaxResult like(@RequestParam("article") String articleNo) {
        return articleService.like(articleNo);
    }

    /**
     * 取消点赞文章
     *
     * @param articleNo
     * @return
     */
    @ApiOperation("取消点赞文章")
    @GetMapping("/disLike")
    public AjaxResult disLike(@RequestParam("articleNo") String articleNo) {
        return articleService.disLike(articleNo);
    }

    /**
     * 收藏文章
     *
     * @param articleNo
     * @return
     */
    @ApiOperation("文章收藏")
    @GetMapping("/fav")
    public AjaxResult fav(@RequestParam("articleNo")String articleNo) {
        // 文章收藏
        return articleService.fav(articleNo);
    }

    /**
     * 取消收藏文章
     *
     * @return
     */
    @ApiOperation("取消文章收藏")
    @GetMapping("/disFav")
    public AjaxResult disFav(@RequestParam("articleNo")String articleNo) {
        return articleService.disFav(articleNo);
    }


    @ApiOperation("模糊搜索")
    @GetMapping("/search")
    public AjaxResult search(String q,Integer pageNo,Integer pageSize){
        return articleService.search(q,pageNo,pageSize);
    }

}
