package com.xxs.web.controller;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.service.ActiveService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 * @Title: ActiveController
 * @ProjectName master
 * @Description: 活动模块
 * @date 2018/12/14 23:42
 */
@RestController
@RequestMapping("/active")
public class ActiveController {

    @Autowired
    private ActiveService activeService;

    /*===========  web页面操作  ==================*/
    @ApiOperation("分页获取活动内容")
    @GetMapping("/page")
    public AjaxResult page(@RequestParam("page")Integer page,
                           @RequestParam("rows")Integer rows) {
        return AjaxResult.getOK(activeService.page(page, rows));
    }

    @ApiOperation("获取具体活动")
    @GetMapping("/get")
    public AjaxResult get(String activeNo){
        return AjaxResult.getOK(activeService.get(activeNo));
    }

    @ApiOperation("关注活动")
    @GetMapping("/follow")
    public AjaxResult follow(String activeNo){
        return AjaxResult.getOK(activeService.follow(activeNo));
    }

    @ApiOperation("取消活动关注")
    @GetMapping("/disFollow")
    public AjaxResult disFollow(String activeNo){
        return AjaxResult.getOK(activeService.disFollow(activeNo));
    }

    @ApiOperation("/获取个人关注列表")
    @GetMapping("/pageSelfFollowing")
    public AjaxResult pageSelfFollowing(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows){
        return AjaxResult.getOK(activeService.pageSelfFollowing(page,rows));
    }
}
