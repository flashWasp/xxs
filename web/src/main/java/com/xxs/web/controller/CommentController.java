package com.xxs.web.controller;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.web.entity.req.CommentCreateReq;
import com.xxs.web.service.CommentService;
import com.xxs.web.service.MemberService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 评论api
 */
@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private MemberService memberService;

    /**
     * 创建评论对象
     * @return
     */
    @ApiOperation("创建评论对象")
    @PostMapping("/createComment")
    public AjaxResult createComment(@RequestBody CommentCreateReq req){
        String uid = TokenRedisRepository.getCurUserUUID();
        Map<String,String> senderMap = (HashMap<String, String>) memberService.get(uid).getData();
        String senderName  = senderMap.get("nickName").toString();
        // 发送者等于接受者Id 自己发送给自己
        if (!uid.equals(req.getReceiverNo()) && !req.getParentCommentNo().equals(req.getTargetCommentNo())){
            Map<String,String> receiverMap = (HashMap<String, String>) memberService.get(req.getReceiverNo()).getData();
            String receiverName= receiverMap.get("nickName").toString();
            req.setReceiverName(receiverName);
        }
        req.setSenderName(senderName);
        return commentService.createComment(req);
    }

    /**
     * 根据文章获取一级评论
     * @return
     */
    @ApiOperation("分页获取评论")
    @GetMapping("/pageByArticleNo")
    public AjaxResult pageByArticleNo(@RequestParam(value = "articleNo")String articleNo, @RequestParam(value = "pageNo") Integer pageNo, @RequestParam(value = "pageSize") Integer pageSize){
        AjaxResult ajaxResult = commentService.pageCommentByArticleNo(articleNo, pageNo, pageSize);
        return ajaxResult;
    }

    /**
     * 根据父级评论获取子级评论对象（分页）
     * @param parentId
     * @return
     */
    @ApiOperation("根据父级评论获取子级评论对象")
    @GetMapping("/pageByParentId")
    public AjaxResult pageByParentId(@RequestParam("parentId") String parentId){
        return commentService.pageCommentByParentId(parentId);
    }
}
