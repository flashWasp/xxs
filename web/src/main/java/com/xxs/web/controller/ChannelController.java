package com.xxs.web.controller;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.web.service.ChannelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/web/channel")
public class ChannelController {
    @Autowired
    private ChannelService channelService;

    /**
     * 获取所有的频道对象
     *
     * @return
     */
    @ApiOperation("获取所有的频道对象")
    @GetMapping("/list")
    public AjaxResult list() {
        return channelService.listChannel();
    }

    /**
     * 获取单条分类对象详情
     *
     * @return
     */
    @ApiOperation("获取单条分类对象详情")
    @GetMapping("/detail")
    public AjaxResult detail(@RequestParam("id") Integer id) {
        return channelService.detail(id);
    }

}
