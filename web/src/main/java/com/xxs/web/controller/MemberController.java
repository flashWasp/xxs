package com.xxs.web.controller;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.web.entity.req.MemberSupportReq;
import com.xxs.web.service.MemberService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @Title: MemberController
 * @ProjectName master
 * @Description: 用户 -- 个人中心
 * @date 2018/12/1515:26
 */
@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    @GetMapping("/registered")
    public AjaxResult registered(
            @RequestParam("email") String email,
            @RequestParam(value = "mobile", required = false) String mobile,
            @RequestParam(value = "account", required = false) String account,
            @RequestParam("pwd") String pwd) {
        Map<String, Object> params = new HashMap<>();
        params.put("email",email);
        params.put("mobile",mobile);
        params.put("account",account);
        params.put("pwd",pwd);
        return AjaxResult.getOK(memberService.registered(params));
    }

    /**
     * 验证
     * @param account
     * @param validCode
     * @return
     */
    @GetMapping("/valid")
    public AjaxResult valid(@RequestParam("account") String account,
                            @RequestParam("validCode") String validCode){
        return memberService.valid(account, validCode);
    }

    /**
     * 根据用户获取基本信息
     * @param no
     * @return
     */
    @GetMapping("/get")
    public AjaxResult get(String no){
        return  memberService.get(no);
    }

    @GetMapping("/getUserInfo")
    public AjaxResult getUserInfo(){
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        return memberService.get(curUserUUID);
    }


    /**
     * 根据用户编号获取用户详细信息
     * @param no
     * @return
     */
    @GetMapping("/member/detail")
    public AjaxResult detail(String no){
        return memberService.detail(no);
    }

    @ApiOperation("补充用户信息")
    @PostMapping("/member/support")
    public AjaxResult update(@RequestBody MemberSupportReq req){
        return  memberService.support(req);
    }

}
