package com.xxs.sso.service.token.mgt;

import com.xxs.sso.service.token.Token;
import org.apache.shiro.session.InvalidSessionException;

/**
 * 验证Token 拓展token特性
 */
public interface ValidatingToken extends Token{

    boolean isValid();

    void validate() ;
}

