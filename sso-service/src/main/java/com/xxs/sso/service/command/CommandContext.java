package com.xxs.sso.service.command;

/** 命令上下文 */
public class CommandContext {

/** 命令对象 */
private Command command;

public CommandContext(Command command) {
	this.command = command;
}

public Command getCommand() {
	return command;
}
}
