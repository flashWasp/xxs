package com.xxs.sso.service.token.mgt.eis;

import com.xxs.sso.service.token.Token;

import java.io.Serializable;
import java.util.Collection;

/**
 * Token数据访问
 */
public interface TokenDao {

    void update(Token token);

    void delete(Token token);

    Serializable create(Token token);

    Token readToken(Serializable tokenId);

    Collection<Token> getActiveTokens();


}
