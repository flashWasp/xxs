package com.xxs.sso.service.service;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.sso.service.app.SSOConstant;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * 验证码服务
 */
@Service
public class VerificationCodeService {


    /**
     * 获取登陆验证失败次数
     */
    public Integer countErrorLoginTime(HttpServletRequest request){
        return (Integer) TokenRedisRepository.getSession().getAttribute(SSOConstant.LOGINERRORCOUNT);
    }

    /**
     * 创建后端验证码
     * @return
     */
    public void getBgVerificationCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BufferedImage bi = createBufferedImage(request, response);
        ImageIO.write(bi, "JPG", response.getOutputStream());
    }

    /**
     * 添加验证码错误次数
     * @param request
     */
    public void addErrorLoginTime(HttpServletRequest request){
        Integer loginErrorTime = (Integer) TokenRedisRepository.getSession().getAttribute(SSOConstant.LOGINERRORCOUNT);
        loginErrorTime += 1;
        TokenRedisRepository.getSession().setAttribute(SSOConstant.LOGINERRORCOUNT,loginErrorTime);
    }

    /**
     * 重置验证码错误次数
     */
    public void resetErrorLoginTime(){
        TokenRedisRepository.getSession().setAttribute(SSOConstant.LOGINERRORCOUNT,Integer.valueOf(0));
    }




    /**
     * 生成后端验证码
     * @param request
     * @param response
     * @return
     */
    private BufferedImage createBufferedImage(HttpServletRequest request, HttpServletResponse response) {
        // 创建图片的buff内存缓存区
        BufferedImage bi = new BufferedImage(78, 43, BufferedImage.TYPE_INT_RGB);
        fillBackground(bi);
        // 获取图像的处理接口
        Graphics GImage = bi.getGraphics();
        // 创建图片背景色
        Color color = new Color(250, 251, 252);
        // 设置图片颜色
//		GImage.setColor(color);
        // 与canvas 差不多 用来 用当前颜色填充 矩形
//		GImage.fillRect(0, 0, 68, 43);
        char[] ca = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        Random r = new Random(); // 随机默认当前系统时间对应的相对时间有关的数字作为种子数:
        int len = ca.length;
        int index;
        StringBuilder sb = new StringBuilder();
        Font f = new Font("宋体", Font.BOLD, 24);
        GImage.setFont(f);
        for (int i = 0; i < 4; i++) { // 随机生成四个数字
            index = r.nextInt(len);
            GImage.setColor(new Color(r.nextInt(88), r.nextInt(188), r.nextInt(255)));
            if (i == 2) {
                Graphics2D g2d = (Graphics2D) GImage;
                g2d.rotate(0.02 * Math.PI);// 旋转

            }
            GImage.drawString(ca[index] + "", i * 15 + 10, 30); // 设置每个字符的位置
            sb.append(ca[index]);
        }
        request.getSession().setAttribute(SSOConstant.BG_VERIFICATIONCODE, sb.toString().toUpperCase());
        return bi;
    }

    /**
     * 设置背景
     *
     * @param image
     */
    private void fillBackground(BufferedImage image) {
        Graphics graphics = image.getGraphics();
        Random rand = new Random();

        /** 噪点数量 */
        int noises = 20;
        // 验证码图片的宽高
        int imgWidth = image.getWidth();
        int imgHeight = image.getHeight();

        // 填充为白色背景
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, imgWidth, imgHeight);

        /**
         * 画100个噪点(颜色及位置随机)
         */
        for (int i = 0; i < noises; i++) {
            // 随机颜色
            int rInt = rand.nextInt(255);
            int gInt = rand.nextInt(255);
            int bInt = rand.nextInt(255);

            graphics.setColor(new Color(rInt, gInt, bInt));

            // 随机位置
            int xInt = rand.nextInt(imgWidth - 3);
            int yInt = rand.nextInt(imgHeight - 2);

            // 随机旋转角度
            int sAngleInt = rand.nextInt(60);
            int eAngleInt = rand.nextInt(360);

            // 随机大小
            int wInt = rand.nextInt(6);
            int hInt = rand.nextInt(6);

            graphics.fillArc(xInt, yInt, wInt, hInt, sAngleInt, eAngleInt);

            // 画5条干扰线
            if (i % 20 == 0) {
                int xInt2 = rand.nextInt(imgWidth);
                int yInt2 = rand.nextInt(imgHeight);
                graphics.drawLine(xInt, yInt, xInt2, yInt2);
            }
        }
    }




}
