package com.xxs.sso.service.filter;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.system.SystemConfig;
import com.xxs.common.service.util.CookieUtils;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.app.TokenManager;
import com.xxs.sso.service.command.cmd.RemoveDomainCookieCmd;
import com.xxs.sso.service.command.excutor.CommandExecutor;
import com.xxs.sso.service.module.LoginUser;
import com.xxs.sso.service.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * 登录过滤器
 */
@WebFilter
public class LoginFilter implements Filter {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private CommandExecutor commandExecutor;

    // SSO接收用户对象
    private LoginUser loginUser;

    /**
     * 目标Url
     */
    private String targetUrl;

    /**
     * token管理对象
     */
    private TokenManager tokenManager;


    private UserService userService;


    /**
     * 初始化数据内容
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext context = filterConfig.getServletContext();
        ApplicationContext applicationContext =
                WebApplicationContextUtils.getWebApplicationContext(context);
        tokenManager = (TokenManager) applicationContext.getBean(TokenManager.class);
        commandExecutor = (CommandExecutor) applicationContext.getBean(CommandExecutor.class);
        userService = (UserService) applicationContext.getBean(UserService.class);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        request = (HttpServletRequest) req;
        response = (HttpServletResponse) res;
        // 获取目标登录系统
        this.targetUrl = request.getParameter(SSOConstant.TARGETURL) + "";
        // 获取具体命令
        String cmd = request.getParameter(SSOConstant.CMD);
        // 清理记住我的Cookie
        if (StringUtils.isNotBlank(cmd) && cmd.equals(SSOConstant.CMD_CLEAR)) {
            commandExecutor.execute(new RemoveDomainCookieCmd(request, response, SSOConstant.CMD_REMEMBERME));
        }
        // 根据cookie获取当前用户
        loginUser = userService.getLoginUser();
        // 请求的url
        String servletPath = request.getServletPath();

        // 配置是否白名单 合法IP(登陆SSO和登陆子系统的IP)

        // 用户没登陆
        if (loginUser == null) {
            System.out.println("当前无登陆对象");
            // 重定向回登陆页面(添加sessionId)
            // 选择性放行
            if(servletPath.contains("/login.action")){
                chain.doFilter(req,res);
            }
        }else{
            chain.doFilter(req,res);
        }
    }

    @Override
    public void destroy() {
    }

    /**
     * 跳转到登录页
     */
    private void gotoLogin() {
        try {
            request.getRequestDispatcher(SSOConstant.LOGIN_PAGE).forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 授权回调的url
     *
     * @param backUrl
     * @return
     */
    private void gotoBackUrl(HttpServletResponse response, String backUrl) {
        try {
            response.sendRedirect(backUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到默认系统
     */
    private void gotoITService() {
        try {
            if (request.getHeader("X-Requested-With") != null) {
                // 异步请求
                response.addHeader("sessionTimeOut", "0");
                OutputStream out = response.getOutputStream();
                out.flush();
                out.close();
            } else {
                response.sendRedirect(SystemConfig.getValue("defaultServiceServiceUrl"));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }





}
