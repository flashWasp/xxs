package com.xxs.sso.service.token.mgt;

import com.xxs.sso.service.SSOException;
import com.xxs.sso.service.token.Token;
import org.apache.shiro.session.InvalidSessionException;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.*;

public class SimpleToken implements ValidatingToken,Token,Serializable {
    /**
     * id
     */
    private Serializable id;
    /**
     * 创建时机
     */
    private Date createTimestamp;

    private Date stopTimestamp;

    /**
     * 主机host
     */
    private String host;

    /**
     * 过期标记位
     */
    private boolean expired;

    private long timeout;

    private Date lastAccessTime;



    /**
     * token携带的属性集合
     */
    private final Map<String, Object> attributes;

    public SimpleToken() {
        this.createTimestamp = new Date();
        this.attributes = new LinkedHashMap();
    }

    @Override
    public Serializable getId() {
        return this.id;
    }

    public void setId(Serializable id) {
        this.id = id;
    }

    @Override
    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long maxIdleTimeInMillis) {
        this.timeout = maxIdleTimeInMillis;
    }

    public Date getStopTimestamp() {
        return stopTimestamp;
    }

    public void setStopTimestamp(Date stopTimestamp) {
        this.stopTimestamp = stopTimestamp;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public Date getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(Date lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public Object getAttribute(Object key) {
        return this.attributes.get(key);
    }

    /**
     * 根据名字获取value
     *
     * @param name
     * @return
     */
    public Object getValue(String name) {
        return this.getAttribute(name);
    }

    @Override
    public void setAttribute(String key, Object value) {
        if (value != null) {
            this.attributes.put(key, value);
        } else {
            this.removeAttribute(key);
        }
    }

    @Override
    public Object removeAttribute(String key) {
        return this.attributes.remove(key);
    }

    @Override
    public void touch() {
        this.lastAccessTime = new Date();
    }

    @Override
    public void stop() {
        if (this.stopTimestamp == null) {
            this.stopTimestamp = new Date();
        }
    }

    protected boolean isStopped() {
        return getStopTimestamp() != null;
    }

    protected void expire() {
        stop();
        this.expired = true;
    }

    public boolean isValid() {
        return !isStopped() && !isExpired();
    }

    /**
     * 判断是否超时
     *
     * @return
     */
    protected boolean isTimedOut() {
        if (isExpired()) {
            return true;
        }
        long timeout = getTimeout();
        if (timeout >= 01) {
            Date lastAccessTime = getLastAccessTime();
            if (lastAccessTime == null) {
                throw new IllegalStateException("token has no lastAccessTime");
            }
            long expireTimeMillis = System.currentTimeMillis() - timeout;
            Date expireTime = new Date(expireTimeMillis);
            return lastAccessTime.before(expireTime);

        }
        return false;
    }

    /**
     * 移除属性
     *
     * @param name
     */
    public void removeValue(String name) {
        this.removeAttribute(name);
    }

    public void setHost(String host) {
        this.host = host;
    }

    /**
     * 获取所有的属性Key集合
     *
     * @return
     * @throws InvalidSessionException
     */
    public Collection<Object> getAttributeKeys() throws InvalidSessionException {
        try {
            Enumeration namesEnum = Collections.enumeration(new LinkedHashSet(this.attributes.keySet()));
            Collection<Object> keys = null;
            if (namesEnum != null) {
                keys = new ArrayList<Object>();
                while (namesEnum.hasMoreElements()) {
                    keys.add(namesEnum.nextElement());
                }
            }
            return keys;
        } catch (Exception e) {
            throw new InvalidSessionException(e);
        }
    }

    public void putValue(String name, Object value) {
        this.setAttribute(name, value);
    }

    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(new LinkedHashSet(this.attributes.keySet()));
    }

    /**
     * 验证token
     */

    public void validate() {
        if (isStopped()) {
            throw new SSOException("token is stopped");
        }
        if (isTimedOut()) {
            expire();
            Date lastAccessTime = getLastAccessTime();
            long timeout = getTimeout();
            Serializable tokenId = getId();
            DateFormat df = DateFormat.getInstance();
            throw new SSOException("token has expired");
        }
    }
}
