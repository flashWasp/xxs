package com.xxs.sso.service.command;

/**
 * 命令上下文工厂
 */
public class CommandContextFactory {

public static CommandContext createCommandContext(Command command) {
	return new CommandContext(command);
}
}
