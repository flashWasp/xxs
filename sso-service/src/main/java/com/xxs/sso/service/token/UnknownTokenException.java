package com.xxs.sso.service.token;

public class UnknownTokenException extends  InvalidTokenException {

    /**
     * Creates a new UnknownTokenException.
     */
    public UnknownTokenException() {
        super();
    }

    /**
     * Constructs a new UnknownTokenException.
     *
     * @param message the reason for the exception
     */
    public UnknownTokenException(String message) {
        super(message);
    }

    /**
     * Constructs a new UnknownTokenException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public UnknownTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new UnknownTokenException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public UnknownTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
