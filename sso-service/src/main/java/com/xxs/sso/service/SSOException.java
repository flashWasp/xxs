package com.xxs.sso.service;

/**
 * SSO异常处理框架
 */
public class SSOException extends  RuntimeException {

    /**
     * Creates a new SSOException.
     */
    public SSOException() {
        super();
    }

    /**
     * Constructs a new SSOException.
     *
     * @param message the reason for the exception
     */
    public SSOException(String message) {
        super(message);
    }

    /**
     * Constructs a new SSOException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public SSOException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new SSOException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public SSOException(String message, Throwable cause) {
        super(message, cause);
    }


}
