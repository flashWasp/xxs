package com.xxs.sso.service.token;

import com.xxs.sso.service.SSOException;

public class TokenException extends SSOException {

    /**
     * Creates a new TokenException.
     */
    public TokenException() {
        super();
    }

    /**
     * Constructs a new TokenException.
     *
     * @param message the reason for the exception
     */
    public TokenException(String message) {
        super(message);
    }

    /**
     * Constructs a new TokenException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public TokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new TokenException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

}
