package com.xxs.sso.service.command.excutor;

import com.xxs.sso.service.command.Command;
import com.xxs.sso.service.command.CommandContext;
import com.xxs.sso.service.command.CommandContextFactory;
import org.springframework.stereotype.Service;

/** 命令执行器实现 */
@Service
public class CommandExecutorImpl implements CommandExecutor {
public Object execute(Command command) {
	return executeCmd(command);
}

//	通过命令拦截器再执行命令
private Object executeCmd(Command command) {
	CommandContext commandContext = CommandContextFactory.createCommandContext(command);
	Object result = commandContext.getCommand().excute(commandContext);
	return result;
}
}
