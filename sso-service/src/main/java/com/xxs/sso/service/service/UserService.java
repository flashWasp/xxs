package com.xxs.sso.service.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xxs.common.service.constant.CommonConstant;
import com.xxs.common.service.exception.SystemException;
import com.xxs.common.service.model.ResultMsg;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.CookieUtils;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.app.TicketManager;
import com.xxs.sso.service.module.LoginUser;
import com.xxs.sso.service.module.RemoteUserResponse;
import com.xxs.sso.service.token.Token;
import com.xxs.sso.service.token.mgt.DefaultTokenManager;
import com.xxs.sso.service.token.mgt.eis.TokenDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** 用户服务 */
@Service
public class UserService {
@Autowired
private DefaultTokenManager tokenManager;

@Autowired private TokenDao tokenDao;
private static Logger log = LoggerFactory.getLogger(TicketManager.class);
/**
 * 获取登录对象
 *
 * @return
 */
public LoginUser getLoginUser() {
	if (null == TokenRedisRepository.getRequest()) {
		return null;
	}
	String tokenId = CookieUtils.getCookie(TokenRedisRepository.getRequest(), SSOConstant.TOKENID);
	LoginUser loginUser = validateToken(tokenId);
	return loginUser;
}

/**
 * 验证token对象
 *
 * @param tokenId
 * @return
 */
public LoginUser validateToken(String tokenId) {
	if (tokenId != null) {
		// 根据tokenKey获取token
		Token token = tokenManager.getToken(tokenId);
		if (token != null) {
			log.info("当前用户登录Token:" + token.getId());
			token.touch();
			// 正在登录的用户对象
			LoginUser loginUser = (LoginUser) token.getAttribute(SSOConstant.SHIRO_CURRENT_USER);
			log.info("当前登录用户的TokenId：{}", tokenId);
			// 验证loginUser
			if (loginUser != null) {
				log.info("当前登录用户的名称：{}", loginUser.getAccount());
				return loginUser;
			} else {
				log.info(" token 发生异常：token失效");
			}
		} else {
			log.info("当前用户登录Token已经失效");
		}
	}
	return null;
}

	/**
	 * 根据用户系统获取登陆用户对象,本地登陆后生成TOKEN
	 * @return
	 */
	public LoginUser getUserByRometeSys(String account,String password){
		// 创建默认的httpClient实例.
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String url = "http://localhost:8081/user/findUserByAccount.action";
		HttpPost httpPost = new HttpPost(url);
		// 封装参数
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		// 根据账户获取账号密码
		nvps.add(new BasicNameValuePair("account",account));
		//nvps.add(new BasicNameValuePair("password",password));
		CloseableHttpResponse response = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			response = httpClient.execute(httpPost);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				return null;
			}
			HttpEntity entity = response.getEntity();
			String result = EntityUtils.toString(entity, "utf-8");
			if (StringUtils.isBlank(result)) {
				return null;
			}
			RemoteUserResponse remoteUserResponse = JSON.parseObject(result, RemoteUserResponse.class);
			if(remoteUserResponse != null){
				Integer statusCode = remoteUserResponse.getStatusCode();
				if(statusCode.equals(200)){
					return remoteUserResponse.getData();
				}
			}
		} catch (IOException e) {
			log.error(new DateUtils()+ " 退出子系统发生异常：" + e.toString());
			throw new SystemException(ResultMsg.SYSTEM_EXCEPTION);
		} finally {
			if (null != response) {
				try {
					response.close();
				} catch (IOException e) {
					throw new SystemException(ResultMsg.SYSTEM_EXCEPTION_IO);
				}
			}
		}
		return null;
	}





}
