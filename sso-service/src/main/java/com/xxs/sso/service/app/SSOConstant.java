package com.xxs.sso.service.app;

public class SSOConstant{
  // 回调地址
  public static final String TARGETURL = "targetUrl";

  // 具体命令
  public static final String CMD = "cmd";

  // 具体指令 -- 清理
  public static final String CMD_CLEAR ="clear";

  // 具体命令 -- 记住我
  public static final String CMD_REMEMBERME = "rememberMe";

  // 登陆错误次数
  public static final String LOGINERRORCOUNT = "loginErrorCount";

  // 当前需要登录对象
  public static final String LOGINUSER = "loginUser";

  /** shiro中保存的对象 */
  public static final String SHIRO_CURRENT_USER = "shiroCurrentUser";

  // 服务名
  public static final String SERVICENAME = "serviceName";

  // 后端验证码
  public static final String BG_VERIFICATIONCODE = "bg_VerificationCode";

  // 前端验证码
  public static final String FRONT_VERIFICATIONCODE = "front_VerificationCode";

  // 验证码错误key
  public static final String ERRORKEY_VERIFICATIONCODE = "vvcError";

  /** 验证码错误value */
  public static final String ERRORVALUE_VERIFICATIONCODE = "验证码错误！";



  /** shiro登录失败*/
  public static final String SHIRO_LOGIN_FAILURE = "shiroLoginFailure";

  /**
   * 用户账号或密码错误
   */
  public static final String SHIRO_UNKNOWACCOUNT = "unknowAccount";

  public static final String SHIRO_UNKNOWACCOUNT_VALUE = "用户名或密码错误";

  /**
   * 账号被锁定
   */
  public static final String SHIRO_LOCKEDACCOUNT = "lockedAccount";

  public static final String SHIRO_LOCKEDACCOUNT_VALUE = "账号被锁定";
  /**
   * 验证失败
   */
  public static final String SHIRO_AUTHENTICATION = "authentication";

  public static final String SHIRO_AUTHENTICATION_VALUE = "验证失败";

  /**
   * 未知：验证失败
   */
  public static final String SHIRO_CLASSNAMEERROR = "error";

  /**
   * shiro未知错误
   */
  public static final String SHIRO_CLASSNAMEERROR_VALIUE = "未知错误:" + "验证失败";

  /**
   * 登录页
   */
  public static final String LOGIN_PAGE ="login";

  /**
   * Shrio当前用户下密码
   */
  public static final String SHIRO_CURRENTUSERPASSWORD = "shiroCurrentUserPassword";

  /**
   * 设置域
   */
  public static final String DOMAINNAME ="domainName";

  /**
   * 凭证
   */
  public static final String TOKEN = "token";


  /**
   * 凭证Id
   */
  public static final String TOKENID = "ssoTokenId";
}
