package com.xxs.sso.service.token.mgt.eis;

import com.xxs.sso.service.token.Token;

import java.io.Serializable;

public interface TokenIdGenerator {
    Serializable generateId(Token token);
}
