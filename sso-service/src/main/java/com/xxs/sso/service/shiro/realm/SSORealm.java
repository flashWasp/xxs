package com.xxs.sso.service.shiro.realm;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.module.LoginUser;
import com.xxs.sso.service.service.UserService;
import com.xxs.sso.service.token.mgt.DefaultTokenManager;
import com.xxs.sso.service.token.mgt.SimpleToken;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 实现AuthorizingRealm接口用户  : 用户认证接口
 */

public class SSORealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;
    @Autowired
    private DefaultTokenManager tokenManager;


    public static final Logger logger = (Logger) LoggerFactory.getLogger(SSORealm.class);

    /**
     * 获取用户相关信息
     *
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 获取用户登录名
        String userName = (String) principals.getPrimaryPrincipal();
        // 获取shiro的session
        Session session = SecurityUtils.getSubject().getSession();
        // 获取Shrio  session中的用户对象
        LoginUser loginUser = (LoginUser) session.getAttribute(SSOConstant.SHIRO_CURRENT_USER);
        // 获取用户登录名
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        return simpleAuthorizationInfo;
    }

    /**
     * 身份认证(账号密码)
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String account = (String) token.getPrincipal().toString();
        String password = (String) token.getCredentials().toString();
        // 远程查询
        LoginUser loginUser = userService.getUserByRometeSys(account, password);
        if (account == null) {
            throw new UnknownAccountException();// 没找到帐号
        }
        if (loginUser != null) {
            SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(loginUser.getAccount(),loginUser.getPassword(),ByteSource.Util.bytes(loginUser.getSalt()), this.getName());
            SimpleToken ssoToken = new SimpleToken();
            ssoToken.setAttribute(SSOConstant.SHIRO_CURRENT_USER, loginUser);
            Serializable tokenId = tokenManager.createToken(ssoToken);
            // 注册ticket进cookie
            addTokenIdToCookie(tokenId, TokenRedisRepository.getRequest(),  ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse());
            //tokenManager.createToken(ssoToken);
            return info;
        }
        return null;
    }

    private void addTokenIdToCookie(
            Serializable tokenId, HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = new Cookie(SSOConstant.TOKENID, tokenId.toString());
        cookie.setPath("/");
        // request.getScheme() 返回当前链接使用的协议
        if ("https".equals(request.getScheme())) {
            cookie.setSecure(true);
        }
        response.addCookie(cookie);
    }
}
