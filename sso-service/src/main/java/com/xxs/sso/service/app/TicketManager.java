package com.xxs.sso.service.app;


import com.xxs.sso.service.module.LoginUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 凭证管理中心
 */
@Component
public class TicketManager {

    /**
     * Session持久层读取 shiro
     */
    @Autowired
    private SessionDAO sessionDAO;

    // <ticket,sessionId>
    private static final Map<String, String> ticketMap = new ConcurrentHashMap<String, String>();

    /**
     * 返回全局唯一凭证存储对象Map
     *
     * @return
     */
    public static Map<String, String> getTicketMap() {
        return ticketMap;
    }


    /**
     * 添加凭证
     *
     * @param ticket
     * @param sessionId
     */
    public void registerTicket(String ticket, String sessionId) {
        if (StringUtils.isNotBlank(ticket) && StringUtils.isNotBlank(sessionId)) {
            ticketMap.put(ticket, sessionId);
        }
    }

    /**
     * 删除 ticket
     *
     * @param ticket
     */
    public void unregisterTicket(String ticket) {
        if (StringUtils.isNotBlank(ticket)) {
            Session session = sessionDAO.readSession(ticketMap.get(ticket));
            if (null != session) {
                sessionDAO.delete(session);
            }
        }
    }

    /**
     * 清空过期的键值对
     */
    public void emptyExpire() {
        for (Map.Entry<String, String> entry : ticketMap.entrySet()) {
            String sessionId = entry.getValue();
            Session session = sessionDAO.readSession(sessionId);

        }
    }

    /**
     * 根据凭证返回sessionId
     *
     * @param ticket
     * @return
     */
    public String getSessionIdByTicket(String ticket) {
        if (StringUtils.isNotBlank(ticket)) {
            return ticketMap.get(ticket);
        }
        return null;
    }

    /**
     * 校验Ticket 成功：返回loginuser
     *
     * @param ticket
     * @return
     */
    public LoginUser validateTicket(String ticket) {
        if (StringUtils.isNotBlank(ticket)) {
            String sessionId = ticketMap.get(ticket);
            if (StringUtils.isNotBlank(sessionId)) {
                Session session = sessionDAO.readSession(sessionId);
                if (null == session) {
                    ticketMap.remove(ticket);
                    return null;
                }
                LoginUser loginUser = (LoginUser) session.getAttribute("loginUser");
                if (null != loginUser) {
                    // 账号有没被锁定
                    return loginUser;
                }
            }

        }
        return null;

    }
    /**
     * 校验Ticket。成功：返回loginuser<br>
     * （不检查账号是否被锁定）
     *
     * @param ticket
     * @return
     */
    public LoginUser validateTicketWithoutLocked(String ticket) {
        if (StringUtils.isNotBlank(ticket)) {
            String sessionId = ticketMap.get(ticket);
            if (StringUtils.isNotBlank(sessionId)) {
                try {
                    Session session = sessionDAO.readSession(sessionId);
                    if (null == session) {
                        ticketMap.remove(ticket);
                        return null;
                    }
                    LoginUser loginUser = (LoginUser) session.getAttribute(SSOConstant.LOGINUSER);
                    return loginUser;
                } catch (Exception exception) {
                    ticketMap.remove(ticket);
                }
            }
            unregisterTicket(ticket);
        }
        return null;
    }
}
