package com.xxs.sso.service.servlet;


import com.xxs.common.service.system.TokenRedisRepository;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * System servlet listener.
 * 
 * @author Fung
 * @date 2018年3月19日  
 *
 */
@WebListener
public class XXSServletListener implements ServletContextListener, HttpSessionListener, ServletRequestListener {
	
	/**
     * Logger.
     */
	private static final Logger LOGGER = Logger.getLogger(XXSServletListener.class);

	/**
     * Servlet context.
     */
    private static ServletContext servletContext;
    
    
    /**
     * Gets the servlet context.
     *
     * @return the servlet context
     */
    public static ServletContext getServletContext() {
        if (null == servletContext) {
            throw new IllegalStateException("Initializes the servlet context first!");
        }
        return servletContext;
    }

	@Override
	public void contextInitialized(final ServletContextEvent servletContextEvent) {
		servletContext = servletContextEvent.getServletContext();
		TokenRedisRepository.startApplication();
		LOGGER.info("Initializing the context....");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		TokenRedisRepository.endApplication();
		LOGGER.info("Destroying the context....");
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		final HttpSession session = se.getSession();
		String sessionId = session.getId();
		sessionDestroyed(se);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		TokenRedisRepository.endSession();
	}

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
		TokenRedisRepository.setRequest(request);

    }



	
}
