package com.xxs.sso.service.service;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.system.SystemConfig;
import com.xxs.common.service.util.CookieUtils;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.module.LoginUser;
import com.xxs.sso.service.token.Token;
import com.xxs.sso.service.token.mgt.DefaultTokenManager;
import com.xxs.sso.service.token.mgt.SimpleToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class LoginService {

    @Autowired
    private DefaultTokenManager tokenManager;

    /**
     * 登录成功 url : 登录的系统地址
     *
     * @return
     */
    public Object login(String account,String password) {
        // 返回的系统名称
      //  String serviceName = getServiceName(url);
        // 请求目的系统url，获取用户信息
        UsernamePasswordToken token = new UsernamePasswordToken(account, password);
        // 使用Shiro对象
        Subject subject = SecurityUtils.getSubject();
        // 未登陆而且验证码正确
        if (!subject.isAuthenticated()) {
            // 成功登陆，并且赋值给token对象
            subject.login(token);
            // 清空输入错误次数  登录成功后清洗输入次数
            resetCountByLoginError();
            // 返回本来登录的子系统
        }
           return null;

    }



    /**
     * 根据backUrl获取登录的系统名（这里对url做了限定 如何来拓展出来需要思考）
     *
     * @param backurl
     * @return
     */
    private String getServiceName(String backurl) {
          if (StringUtils.isNotBlank(backurl)) {
            String[] strs = backurl.split("/");
            if (strs.length > 3) {
                return strs[2];
            }
        }
        return null;
    }

//    // 获取用户登录对象
//    public LoginUser getLoginUser() {
//        if (null == TokenRedisRepository.getRequest()) {
//            return null;
//        }
//        String ticket = CookieUtils.getCookie(TokenRedisRepository.getRequest(), SSOConstant.TOKEN);
//        LoginUser loginUser = ticketManager.validateTicketWithoutLocked(ticket);
//
//        return loginUser;
//
//    }


    /**
     * 重置登陆失败次数
     */
    private void resetCountByLoginError() {
        Subject subject = SecurityUtils.getSubject();
        subject.getSession().setAttribute("errorLoginCount", Integer.valueOf(0));
    }

    /**
     * 添加ticket到cookie中
     */

//
//    /**
//     * 子系统关联网站,创建serviceTicket，用于跨域单点登陆
//     *
//     * @return
//     */
//    private String addServiceTicket(String backUrl, String ticket) {
//        String[] serviceName = SystemConfig.getValue(SSOConstant.SERVICENAME).split(",");
//        List<String> serviceNameList = Arrays.asList(serviceName);
//        if (StringUtils.isNotBlank(backUrl)) {
//            for (String temp : serviceNameList) {
//                // 假如返回的url里面包含了关联网站生成ticket
//                if (backUrl.contains(temp)) {
//                    String serviceTicket = "t-" + UUID.randomUUID();
//                    ticketManager.registerTicket(serviceTicket, ticket);
//                    backUrl = backUrl + "?serviceTicket=" + serviceTicket;
//                    break;
//                }
//            }
//        }
//        return backUrl;
//    }

    /**
     * 重定向返回backUrl
     *
     * @param backUrl
     * @return
     */
    public String gotoBackUrl(String backUrl) {
        return "redirect:" + backUrl;
    }

    /**
     * 校验验证码
     *
     * @param request
     * @return
     */
    public Boolean validateCode(HttpServletRequest request) {
        Integer loginErronNum =
                (Integer) TokenRedisRepository.getSession().getAttribute(SSOConstant.LOGINERRORCOUNT);
        // 当错误次数大于3次时判断验证码
        if (null == loginErronNum || 0 == loginErronNum) {
            TokenRedisRepository.getSession().setAttribute("errorLoginCount", Integer.valueOf(1));
            return true;
        } else {
            if (loginErronNum >= 3) {
                String bg_code =
                        (String) TokenRedisRepository.getSession().getAttribute(SSOConstant.BG_VERIFICATIONCODE);
                String front_code = TokenRedisRepository.getRequest().getParameter(SSOConstant.FRONT_VERIFICATIONCODE);
                if (front_code != null && front_code.toUpperCase().equals(bg_code)) {
                    // 验证码正确,修正验证码错误次数
                    TokenRedisRepository.getSession().setAttribute(SSOConstant.LOGINERRORCOUNT, Integer.valueOf(0));
                    return true;
                } else {
                    TokenRedisRepository.getRequest().setAttribute(SSOConstant.ERRORKEY_VERIFICATIONCODE, "验证码错误");
                    // 错误次数+1
                    TokenRedisRepository.getSession().setAttribute(SSOConstant.LOGINERRORCOUNT, ++loginErronNum);
                    return false;
                }
            }
        }
        return false;
    }

    public void loginFailed() {
        // 获取登录错误信息
        String errorClassName =
                (String) TokenRedisRepository.getRequest().getAttribute(SSOConstant.SHIRO_LOGIN_FAILURE);
        noteDownErrorMessage(errorClassName);
    }

    /**
     * 根据shiro登录错误信息对错误信息进行记录
     *
     * @param e
     */
    private void noteDownErrorMessage(String e) {
        HttpServletRequest request = TokenRedisRepository.getRequest();
        if (UnknownAccountException.class.getName().equals(e) || IncorrectCredentialsException.class.getName().equals(e)) {
            request.setAttribute(SSOConstant.SHIRO_UNKNOWACCOUNT, SSOConstant.SHIRO_UNKNOWACCOUNT_VALUE);
        } else if (LockedAccountException.class.getName().equals(e)) {
            request.setAttribute(SSOConstant.SHIRO_LOCKEDACCOUNT, SSOConstant.SHIRO_LOCKEDACCOUNT_VALUE);
        } else if (AuthenticationException.class.getName().equals(e)) {
            request.setAttribute(SSOConstant.SHIRO_AUTHENTICATION, SSOConstant.SHIRO_AUTHENTICATION_VALUE);
        } else if (e != null) {
            request.setAttribute(SSOConstant.SHIRO_CLASSNAMEERROR, SSOConstant.SHIRO_CLASSNAMEERROR_VALIUE);
        }
    }


}
