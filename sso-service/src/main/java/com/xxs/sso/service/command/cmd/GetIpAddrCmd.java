package com.xxs.sso.service.command.cmd;

import com.xxs.sso.service.command.Command;
import com.xxs.sso.service.command.CommandContext;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 获取IP地址
 */
public class GetIpAddrCmd implements Command {
HttpServletRequest request;

public GetIpAddrCmd(HttpServletRequest request){
	this.request = request;
}

@Override
public Object excute(CommandContext commandContext) {
	if (request != null) {
		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		
		// 多个路由时，取第一个非unknown的ip
		final String[] arr = ip.split(",");
		for (final String str : arr) {
			if (!"unknown".equalsIgnoreCase(str)) {
				ip = str;
				break;
			}
		}
		return ip;
	}
	return null;
}
}
