package com.xxs.sso.service.command.excutor;

import com.xxs.sso.service.command.Command;

public interface CommandExecutor {
public Object execute(Command command);
}
