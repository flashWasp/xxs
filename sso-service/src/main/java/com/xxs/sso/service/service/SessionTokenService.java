package com.xxs.sso.service.service;

import com.xxs.sso.service.token.mgt.SimpleToken;
import com.xxs.sso.service.token.mgt.eis.TokenDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Token Session关联对象服务 */
@Service
public class SessionTokenService {
@Autowired private SessionDAO sessionDAO;

@Autowired private TokenDao tokenDao;

/**
 * 删除token相关Session
 *
 * @param tokenId
 */
public void removeToken(String tokenId) {
	if (StringUtils.isNotBlank(tokenId)) {
		SimpleToken token = (SimpleToken) tokenDao.readToken(tokenId);
		if (token != null) {
			Session session = sessionDAO.readSession(token);
			if (session != null) {
				sessionDAO.delete(session);
			}
		}
	}
}

public void addToken(){

}

public void getTokenId(){

}

}
