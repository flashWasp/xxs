package com.xxs.sso.service.command.cmd;

import com.xxs.common.service.system.SystemConfig;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.command.Command;
import com.xxs.sso.service.command.CommandContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 清理某个域下的Cookie对象
 */
public class RemoveDomainCookieCmd implements Command {
HttpServletRequest request;
HttpServletResponse response;
String domain;
String cookieName;

public RemoveDomainCookieCmd(
		HttpServletRequest request, HttpServletResponse response, String cookieName) {
	this.request = request;
	this.response = response;
	this.domain = SystemConfig.getValue(SSOConstant.DOMAINNAME);
	this.cookieName = cookieName;
}

@Override
public Object excute(CommandContext commandContext) {
	Cookie[] cookies = request.getCookies();
	if (null != cookies) {
		for (Cookie cookie : cookies) {
			if (cookieName.equals(cookie.getName())) {
				cookie.setMaxAge(0);
				cookie.setDomain(domain);
				cookie.setValue(null);
				cookie.setPath("/");
				response.addCookie(cookie);
			}
		}
	}
	return true;
}
}
