package com.xxs.sso.service.token.mgt.eis;

import com.xxs.sso.service.token.Token;
import org.apache.shiro.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/** 内存token数据访问层 */
@Service
public class MemoryTokenDao extends AbstractTokenDao {

  private static final Logger log = LoggerFactory.getLogger(MemoryTokenDao.class);

  /** tokens */
  private static ConcurrentHashMap<Serializable, Token> tokens;

  public MemoryTokenDao() {
    this.tokens = new ConcurrentHashMap<Serializable, Token>();
  }

  /**
   * 创建token
   *
   * @param token
   * @return
   */
  protected Serializable doCreate(Token token) {
    Serializable tokenId = generateTokenId(token);
    assignTokenId(token, tokenId);
    storeToken(tokenId, token);
    return tokenId;
  }

  /**
   * 存储token进入内存
   *
   * @param id
   * @param token
   * @return
   */
  protected Token storeToken(Serializable id, Token token) {
    if (id == null) {
      throw new NullPointerException("id argument cannot be null.");
    }
    return tokens.putIfAbsent(id, token);
  }

  /**
   * 根据ID获取token对象
   *
   * @param tokenId
   * @return
   */
  protected Token doReadToken(Serializable tokenId) {
    return tokens.get(tokenId);
  }

  /**
   * 更新token对象
   *
   * @param token
   */
  public void update(Token token) {
    storeToken(token.getId(), token);
  }

  /**
   * 删除token对象
   *
   * @param token
   */
  public void delete(Token token) {
    if (token == null) {
      throw new NullPointerException("session argument cannot be null.");
    }
    Serializable id = token.getId();
    if (id != null) {
      tokens.remove(id);
    }
  }

  /**
   * 获取活跃的token对象集合
   *
   * @return
   */
  public Collection<Token> getActiveTokens() {
    Collection<Token> values = tokens.values();
    if (CollectionUtils.isEmpty(values)) {
      return Collections.emptySet();
    } else {
      return Collections.unmodifiableCollection(values);
    }
  }
}
