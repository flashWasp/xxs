package com.xxs.sso.service.web;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.service.LoginService;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录模块
 */
@Controller
public class SSOLoginController {
//	@Autowired
//	private LoginService loginService;
//
//	@Autowired
//	private
//	@RequestMapping("/login.action")
//	public Object login(HttpServletResponse response, HttpServletRequest request){
//		Subject subject = SecurityUtils.getSubject();
//		// 获取用户账号
//		String account = (String) subject.getPrincipal();
//		// 回调的URL
//		String backUrl = (String) TokenRedisRepository.getSession().getAttribute(SSOConstant.TARGETURL);
//		// 校验验证码
//		Boolean isTrueCode = loginService.validateCode(request);
//		// 已登录且验证码正确才放行
//		if(subject.isAuthenticated() && isTrueCode){
//			// 保存用户信息到session
//			UserDto userDto = userService.findUserDtoByAccount(account);
//			subject.getSession().setAttribute(SSOConstant.SHIRO_CURRENT_USER,userDto);
//			return loginService.loginSuccess(backUrl);
//		}else{
//			if(isTrueCode){
//				// 验证码正确，但是登录失败
//				loginService.loginFailed();
//			}
//
//		}
//		return gotoLoginPage();
//	}

	/**
	 * 返回登录页
	 *
	 * @return
	 */
	private String gotoLoginPage() {
		return SSOConstant.LOGIN_PAGE;
	}

}
