package com.xxs.sso.service.web;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.sso.service.app.SSOConstant;
import com.xxs.sso.service.command.cmd.GetIpAddrCmd;
import com.xxs.sso.service.command.excutor.CommandExecutor;
import com.xxs.sso.service.module.LoginUser;
import com.xxs.sso.service.service.LoginService;
import com.xxs.sso.service.service.UserService;
import com.xxs.sso.service.service.VerificationCodeService;
import com.xxs.sso.service.token.Token;
import com.xxs.sso.service.token.mgt.DefaultTokenManager;
import com.xxs.sso.service.token.mgt.SimpleToken;
import org.apache.catalina.security.SecurityUtil;
import org.apache.http.HttpResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

@Controller
public class LoginController {
    @Autowired
    private CommandExecutor commandExecutor;

    @Autowired
    private VerificationCodeService verificationCodeService;

    @Autowired
	private UserService userService;

    @Autowired
    private DefaultTokenManager tokenManager;

    @Autowired
    private LoginService loginService;

    private static Logger log = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping("/login.action")
    @ResponseBody
    public Object login(HttpServletRequest request, HttpServletResponse response) {
        // 获取登陆账号和密码
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String ip = (String) commandExecutor.execute(new GetIpAddrCmd(request));
        // 验证验证码
       // boolean isTrueVerficationCode = verifyVerficationCode(request);
        // 获取目标系统Url
        // String targetUrl = (String) TokenRedisRepository.getSession().getAttribute(SSOConstant.TARGETURL);
        Object login = loginService.login(account, password);
        if(login != null ){
            Cookie cookie = new Cookie(SSOConstant.TOKENID,login + "");
            response.addCookie(cookie);
        }
        return "true";
    }
    @RequestMapping("/test.action")
    public Object test(){
        return "haha";
    }

    /**
     * 检验验证码
     *
     * @return
     */
    private boolean verifyVerficationCode(HttpServletRequest request) {
        Integer errorLoginTime = verificationCodeService.countErrorLoginTime(request);
        // 业务需求：当账户密码输入错误3次，使用验证码
        if (errorLoginTime > 3) {
            String bgVerificationCode = (String) TokenRedisRepository.getSession().getAttribute(SSOConstant.BG_VERIFICATIONCODE);
            String frontVerificationCoe = TokenRedisRepository.getRequest().getParameter(SSOConstant.FRONT_VERIFICATIONCODE);
            // 验证码正确
            if (frontVerificationCoe != null && bgVerificationCode != null && frontVerificationCoe.toUpperCase().equals(bgVerificationCode)) {
                return true;
                // 记录验证码错误:防止恶意请求注入:看具体策略
            } else {
                TokenRedisRepository.getRequest().setAttribute(SSOConstant.ERRORKEY_VERIFICATIONCODE, SSOConstant.ERRORVALUE_VERIFICATIONCODE);
                return false;
            }
        }
        return false;
    }


}
