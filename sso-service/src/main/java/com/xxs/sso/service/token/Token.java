package com.xxs.sso.service.token;

import java.io.Serializable;
import java.util.Date;

public interface Token {

    /**
     * 获取token的标记位
     *
     * @return
     */
    Serializable getId();

    /**
     * token创建时间
     *
     * @return
     */
    Date getCreateTimestamp();

    /**
     * 获取访问主机对象
     */
    String getHost();

    /**
     * 获取token中的属性
     *
     * @param key
     * @return
     */
    Object getAttribute(Object key);

    /**
     * 设置token属性
     *
     * @param key
     * @param value
     */
    void setAttribute(String key, Object value);

    /**
     * 移除token中的属性
     *
     * @param key
     * @return
     */
    Object removeAttribute(String key);

    void touch();

    void stop();

    void setTimeout(long maxIdleTimeInMillis);
}
