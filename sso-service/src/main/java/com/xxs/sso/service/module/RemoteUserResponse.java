package com.xxs.sso.service.module;

/**
 * 用户返回对象
 */
public class RemoteUserResponse {
    private String msg;
    private LoginUser data;
    private Integer statusCode;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LoginUser getData() {
        return data;
    }

    public void setData(LoginUser data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "RemoteUserResponse{" +
                "msg='" + msg + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                '}';
    }
}
