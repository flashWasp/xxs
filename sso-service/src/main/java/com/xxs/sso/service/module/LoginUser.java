package com.xxs.sso.service.module;

import java.io.Serializable;

/**
 * 登录用户对象
 */
public class LoginUser implements Serializable {
    /**
     * 用户Id
     */
    private String aid;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 用户手机账户
     */
    private String mobile;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 登陆IP
     */
    private String loginIp;

    /**
     * 登录时间
     */
    private String loginTime;

    /**
     * token
     */
    private String tokenId;

    /**
     * salt
     */
    private String salt ;


    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "aid='" + aid + '\'' +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", loginIp='" + loginIp + '\'' +
                ", loginTime='" + loginTime + '\'' +
                ", tokenId='" + tokenId + '\'' +
                ", salt='" + salt + '\'' +
                '}';
    }
}
