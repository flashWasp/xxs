package com.xxs.sso.service.token.mgt.eis;

import com.xxs.sso.service.token.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Random;

/**
 * 随机Id生成器
 */
public class RandomTokenIdGenerator implements TokenIdGenerator {

    private static final Logger log = LoggerFactory.getLogger(RandomTokenIdGenerator.class);
    private static final String RANDOM_NUM_GENERATOR_ALGORITHM_NAME = "SHA1PRNG";
    private Random random;

    public RandomTokenIdGenerator() {
        try {
            this.random = java.security.SecureRandom.getInstance(RANDOM_NUM_GENERATOR_ALGORITHM_NAME);
        } catch (java.security.NoSuchAlgorithmException e) {
            log.debug(
                    "The SecureRandom SHA1PRNG algorithm is not available on the current platform.  Using the "
                            + "platform's default SecureRandom algorithm.",
                    e);
            this.random = new java.security.SecureRandom();
        }
    }

    public Random getRandom() {
        return this.random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public Serializable generateId(Token token) {
        // ignore the argument - just call the Random:
        return Long.toString(getRandom().nextLong());
    }
}
