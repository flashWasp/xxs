package com.xxs.sso.service.app;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.Map;

/**
 * sso定时器类
 */
@Resource
public class SSOScheduled {

    @Autowired
    private TicketManager tickerManager;

    /**
     *
     */
    @Autowired
    private SessionDAO sessionDAO;

    @Scheduled(cron = "0 0 * * * ?")
    public void emptyExpireTicket() {
        Map<String, String> ticketMap = TicketManager.getTicketMap();
        for (Map.Entry<String, String> entry : ticketMap.entrySet()) {
            String sessionId = entry.getValue();
            Session session = sessionDAO.readSession(sessionId);
            if(session == null){
                ticketMap.remove(entry.getKey());
            }
        }
    }
}
