package com.xxs.sso.service.token.mgt;

import com.xxs.sso.service.token.Token;

public interface   TokenManager {

    Token getToken(String tokenId) ;

}
