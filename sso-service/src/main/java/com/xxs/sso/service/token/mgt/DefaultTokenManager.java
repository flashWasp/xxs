package com.xxs.sso.service.token.mgt;

import com.xxs.common.service.system.SystemConfig;
import com.xxs.sso.service.SSOException;
import com.xxs.sso.service.token.Token;
import com.xxs.sso.service.token.mgt.eis.MemoryTokenDao;
import com.xxs.sso.service.token.mgt.eis.TokenDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.annotation.XmlElementDecl;
import java.io.Serializable;

/**
 * token管理对象
 */
@Service
public class DefaultTokenManager implements  TokenManager {
    protected static final long MILLIS_PER_SECOND = 1000;
    protected static final long MILLIS_PER_MINUTE = 60 * MILLIS_PER_SECOND;
    protected static final long MILLIS_PER_HOUR = 60 * MILLIS_PER_MINUTE;
    public static final long DEFAULT_GLOBAL_TOKEN_TIMEOUT = 30 * MILLIS_PER_MINUTE;
    public static final String GLOBAL_TOKEN_TIMEOUT="globalSessionTimeout";
    private long globalTokenTimeout = DEFAULT_GLOBAL_TOKEN_TIMEOUT;


    @Autowired
    private MemoryTokenDao tokenDao;

    /**
     * log
     */
    private static final Logger log = LoggerFactory.getLogger(DefaultTokenManager.class);
    @Override
    public Token getToken(String tokenId) {
        if(tokenId == null){
            log.debug("Unable to resolve token ID from tokenId [{}].  Returning null to indicate a " +
                    "token could not be found.", tokenId);
            return null;
        }
    return tokenDao.readToken(tokenId);
    }

    /**
     * 创建 token
     * @param token
     * @return
     */
    public Serializable createToken(Token token){
        String maxIdleTimeInMillis = SystemConfig.getValue(GLOBAL_TOKEN_TIMEOUT);
        // 设置默认的过期时间
        if(maxIdleTimeInMillis != null ){
            Long timeout = Long.valueOf(maxIdleTimeInMillis);
            token.setTimeout(timeout);
        }else{
            token.setTimeout(globalTokenTimeout);
        }
        token.touch();
        return tokenDao.create(token);
    }

    
    protected  void  validate(Token token,String tokenId){
        doValidate(token);
    }

    private void doValidate(Token token) {
        if(token instanceof  ValidatingToken){
            ((ValidatingToken) token).validate();
        }else{
            throw new SSOException("token validate is fail");
        }
    }
}
