package com.xxs.sso.service.token.mgt.eis;

import com.xxs.sso.service.token.Token;
import com.xxs.sso.service.token.mgt.SimpleToken;

import java.io.Serializable;
import java.util.Collection;

/**
 * Token数据源抽象层
 */
public abstract class AbstractTokenDao implements TokenDao {

    /**
     * tokenId生成对象
     */
    private TokenIdGenerator tokenIdGenerator;

    /**
     * 构造方法生成 初始化tokenId生成对象
     */
    public AbstractTokenDao() {
        this.tokenIdGenerator = new RandomTokenIdGenerator();
    }

    public TokenIdGenerator getTokenIdGenerator() {
        return tokenIdGenerator;
    }

    public void setTokenIdGenerator(TokenIdGenerator tokenIdGenerator) {
        this.tokenIdGenerator = tokenIdGenerator;
    }

    protected Serializable generateTokenId(Token token) {
        if (this.tokenIdGenerator == null) {
            String msg = "tokenIdGenerator attribute has not been configured.";
            throw new IllegalStateException(msg);
        }
        return this.tokenIdGenerator.generateId(token);
    }

    /**
     * 创建token
     *
     * @param token
     */
    @Override
    public Serializable create(Token token) {
        Serializable tokenId = doCreate(token);
        verifyTokenId(tokenId);
        return tokenId;
    }

    /**
     * 认证tokenId是否存在
     *
     * @param tokenId
     */
    private void verifyTokenId(Serializable tokenId) {
        if (tokenId == null) {
            String msg =
                    "tokenId returned from doCreate implementation is null.  Please verify the implementation.";
            throw new IllegalStateException(msg);
        }
    }

    /**
     * 根据tokenId查看Token对象
     *
     * @param tokenId
     * @return
     */
    @Override
    public Token readToken(Serializable tokenId) {
        Token token = doReadToken(tokenId);
        return token;
    }

    /**
     * 获取活跃的Token对象集合
     *
     * @return
     */
    @Override
    public Collection<Token> getActiveTokens() {
        return null;
    }

    protected void assignTokenId(Token token, Serializable tokenId) {
        ((SimpleToken) token).setId(tokenId);
    }

    /**
     * 实现创建 子类实现
     *
     * @param token
     * @return
     */
    protected abstract Serializable doCreate(Token token);

    /**
     * 实现查看 子类实现
     *
     * @param tokenId
     * @return
     */
    protected abstract Token doReadToken(Serializable tokenId);
}
