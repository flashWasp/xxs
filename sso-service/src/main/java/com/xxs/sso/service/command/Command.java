package com.xxs.sso.service.command;

/** 命令接口 */
public interface Command {
public Object excute(CommandContext commandContext);
}
