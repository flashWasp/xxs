package com.xxs.msg.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.sun.corba.se.impl.protocol.giopmsgheaders.MessageBase;
import com.xxs.msg.service.entity.MsgBase;
import com.xxs.msg.service.entity.MsgSort;

import java.io.IOException;
import java.util.List;

public interface MsgService {

    /*============ 消息分类服务 ==============*/

    /**
     * 添加信息分类对象
     * @param name
     * @return
     */
    Boolean saveMsgSort(String name);

    /**
     * 根据Id获取消息分类
     * @param id
     * @return
     */
    MsgSort getById(Long id);

    /**
     * 更新信息分类对象
     * @param id
     * @return
     */
    Boolean updateMsgSort(Long id,String name);

    /**
     * 删除信息分类对象
     * @param id
     * @param isAll 关联删除
     * @return
     */
    Boolean delMsgSort(Long id,Boolean isAll);

    /**
     * 分类获取分类对象
     * @param page
     * @param rows
     * @return
     */
    Page<MsgSort> pageMsgSort(Integer page, Integer rows);

    /**
     * 获取分类对象集合
     * @return
     */
    List<MsgSort> listMsgSort();


    /*============ 消息服务 ==============*/

    Boolean saveMsg(String title,
                    Long sortId,
                    Integer isStar,
                    String from,
                    List<String> toSet,
                    String cmd,
                    String content,
                    String msgType,
                    String extras
                    );

    /**
     * 根据分类获取msgNo
     * @return
     */
    List<String> listMsgNo(Long sortId,
                           Integer isStart,
                           String state
                           );

    /**
     * 根据Id获取所有分类对象
     * @param msgNos
     * @return
     */
    List<MsgBase> listMsgBase(List<String> msgNos);


    /**
     * 分页获取信息对象
     * @param msgNos 信息编码集合
     * @param from  来源Id
     * @param to 目的地Id
     * @param createStartTime 创建开始时间
     * @param createEndTime 创建结束时间
     * @param page 页码
     * @param rows 页大小
     * @return
     */
    Page<MsgBase> pageMsg(List<String> msgNos,
                          String from,
                          String to,
                          Long createStartTime,
                          Long createEndTime,
                          Integer page,
                          Integer rows
                          );

    /**
     * 根据编号获取信息基本对象
     * @param msgNo
     * @return
     */
    MsgBase getByMsgNo(String msgNo);

    /**
     * 修改消息为已读
     * @param msgNo
     * @return
     */
    Boolean isRead(String msgNo);

    /**
     * 批量修改消息为已读
     * @param msgNos
     * @return
     */
    Boolean isReadBatch(List<String> msgNos);



    /**
     * 批量删除消息
     * @param msgNos
     * @return
     */
    Boolean delMsgBatch(List<String> msgNos);

    /**
     * 统计获取
     * @param state
     * @param to
     * @return
     */
    Integer count(String state,Long sort ,String to);

    /*============= webScoket =================*/
    void sendMsgWebocket(String message) throws IOException;

}
