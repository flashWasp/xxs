package com.xxs.msg.service.dao;

import com.xxs.msg.service.entity.MsgSort;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public interface MsgSortDao extends BaseMapper<MsgSort> {

}
