package com.xxs.msg.service.service.impl;

import com.google.common.collect.Sets;
import com.xxs.common.service.util.EmptyUtil;
import com.xxs.common.service.util.MatchUtil;
import com.xxs.msg.service.entity.mail.MailEntity;
import com.xxs.msg.service.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.sql.rowset.serial.SerialException;
import java.util.Set;

@Service("emailService")
public class MailServiceImpl implements MailService {
    @Resource
    private TaskExecutor taskExecutor;

    @Resource
    private JavaMailSender mailSender;

    private Logger LOG = LoggerFactory.getLogger(this.getClass());
    /**
     * 发送者
     */
    private static  final  String from = "yezhifung@163.com";

    @Override
    public int sendSimpleMail(String subject, String text, Set<String> to) {
        LOG.info("sendSimpleMail - 发送简单邮件. subject={}, text={}, to={}", subject, text, to);
        int result = 1;
        try {
            SimpleMailMessage message = MailEntity.createSimpleMailMessage(subject, text, to);
            message.setFrom(from);
            taskExecutor.execute(()->mailSender.send(message));
        }catch (Exception e){
            LOG.info("sendSimpleMail [FAIL] ex={}", e.getMessage(), e);
            result = 0;
        }
        return result;
    }

    @Override
    public int sendTemplateMail(String subject, String text, Set<String> to) {
        LOG.info("sendTemplateMail - 发送模板邮件. subject={}, text={}, to={}", subject, text, to);
        int result = 1;
        try {
            MimeMessage mimeMessage = getMimeMessage(subject, text, to);
            taskExecutor.execute(() -> mailSender.send(mimeMessage));
        } catch (Exception e) {
            LOG.info("sendTemplateMail -- MimeMessage  [FAIL] ex={}", e.getMessage(), e);
            result = 0;
        }
        return result;
    }

    private MimeMessage getMimeMessage(String subject, String text, Set<String> to) throws SerialException{
        String[] toArray = setToArray(to);
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
            helper.setFrom(from);
            helper.setTo(toArray);
            helper.setSubject(subject);
            helper.setText(text, true);
        } catch (MessagingException e) {
            LOG.error("生成邮件消息体, 出现异常={}", e.getMessage(), e);
            throw new SerialException("生成邮件消息体失败");

        }

        return mimeMessage;
    }

    private String[] setToArray(Set<String> to) {
        Set<String> toSet = Sets.newHashSet();
        for (String toStr : to) {
            toStr = toStr.trim();
            if (MatchUtil.isEmail(toStr)) {
                toSet.add(toStr);
            }
        }
        if (EmptyUtil.isEmpty(toSet)) {
            return null;
        }
        return toSet.toArray(new String[toSet.size()]);
    }

}
