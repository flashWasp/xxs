package com.xxs.msg.service.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.support.converter.MessageConverter;

import java.io.IOException;

public interface Consumer {

    void handler(Message message) throws IOException, ClassNotFoundException;
}
