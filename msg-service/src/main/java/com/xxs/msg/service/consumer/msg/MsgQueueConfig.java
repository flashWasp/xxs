package com.xxs.msg.service.consumer.msg;

import com.xxs.common.service.constant.MQConstants;
import com.xxs.msg.service.AbstractMessageListener;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MsgQueueConfig {

    /**
     * 1 首先声明要使用哪个交换机
     */
    @Bean
    public DirectExchange msgExchange() {
        return new DirectExchange(MQConstants.MSG_EXCHANGE);
    }

    /**
     * 2 queue的名称bizQueue，以及一些参数配置
     */
    @Bean
    public Queue msgQueue() {
        Map<String, Object> arguments = new HashMap<String, Object>();
        /**配置的死信队列*/
        arguments.put("x-dead-letter-exchange", MQConstants.DLX_EXCHANGE);
        arguments.put("x-dead-letter-routing-key", MQConstants.DLX_ROUTING_KEY);
        /**消息被确认前的最大等待时间*/
        arguments.put("x-message-ttl", 60000);
        /**消息队列的最大大长度*/
        arguments.put("x-max-length", 300);
        return new Queue(MQConstants.MSG_QUEUE,true,false,false,arguments);
    }

    /**
     * 3 绑定bizQueue到相应的key
     *
     */
    @Bean
    public Binding msgBinding() {
        return BindingBuilder.bind(msgQueue()).to(msgExchange())
                .with(MQConstants.MSG_KEY);
    }

    /**
     * 4 最后声明一个listener，用来监听
     */
    @Bean
    public SimpleMessageListenerContainer msgListenerContainer(ConnectionFactory connectionFactory,
                                                               AbstractMessageListener messageListener) {

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueues(msgQueue());
        container.setExposeListenerChannel(true);
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.setMessageListener(messageListener);
        /** 设置消费者能处理消息的最大个数 */
        container.setPrefetchCount(100);
        return container;
    }


}
