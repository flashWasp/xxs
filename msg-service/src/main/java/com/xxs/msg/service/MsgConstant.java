package com.xxs.msg.service;

public class MsgConstant {

    /**
     * 新
     */
    public final static String MSG_STATE_ISNEW = "isNew";

    /**
     * 已读
     */
    public final static String MSG_STATE_ISREAD = "isRead";
}
