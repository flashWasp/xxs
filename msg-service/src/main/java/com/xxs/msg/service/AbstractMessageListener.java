package com.xxs.msg.service;

import com.rabbitmq.client.Channel;
import com.xxs.common.service.constant.MQConstants;
import com.xxs.msg.service.consumer.Consumer;
import com.xxs.msg.service.consumer.mail.MailConsumer;
import com.xxs.msg.service.consumer.msg.MsgConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class AbstractMessageListener implements ChannelAwareMessageListener {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 消费次数
     */
    private Integer maxConsumerCount = 3;

    @Autowired
    private RedisTemplate redisTemplate;

    private Consumer consumer;

    @Autowired
    private MailConsumer mailConsumer;

    @Autowired
    private MsgConsumer msgConsumer;


    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        MessageProperties messageProperties = message.getMessageProperties();
        Long deliveryTag = messageProperties.getDeliveryTag();
        Long consumerCount = redisTemplate.opsForHash().increment(MQConstants.MQ_CONSUMER_RETRY_COUNT_KEY,
                messageProperties.getMessageId(), 1);
        // 消费者队列
        String consumerQueue = messageProperties.getConsumerQueue();
        logger.info("当前消息ID:{} 消费次数：{}", messageProperties.getMessageId(), consumerCount);
        try {
            if (consumerQueue.equals(MQConstants.EMAIL_QUEUE)){
                consumer =  mailConsumer;
            }
            if (consumerQueue.equals(MQConstants.MSG_QUEUE)){
                consumer = msgConsumer;
            }
            consumer.handler(message);
            // 成功的回执
            channel.basicAck(deliveryTag, false);
            // 如果消费成功，将Redis中统计消息消费次数的缓存删除
            redisTemplate.opsForHash().delete(MQConstants.MQ_CONSUMER_RETRY_COUNT_KEY,
                    messageProperties.getMessageId());
        } catch (Exception e) {
            logger.error("RabbitMQ 消息消费失败，" + e.getMessage(), e);
            if (consumerCount >= maxConsumerCount) {
                // 入死信队列
                channel.basicReject(deliveryTag, false);
            } else {
                // 重回到队列，重新消费
                channel.basicNack(deliveryTag, false, true);
            }
        }
    }
    public Integer getMaxConsumerCount() {
        return maxConsumerCount;
    }

    public void setMaxConsumerCount(Integer maxConsumerCount) {
        this.maxConsumerCount = maxConsumerCount;
    }
}
