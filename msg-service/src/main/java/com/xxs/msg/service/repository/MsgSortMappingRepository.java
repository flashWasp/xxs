package com.xxs.msg.service.repository;

import com.xxs.msg.service.entity.MsgSortMapping;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public interface MsgSortMappingRepository extends IService<MsgSortMapping> {

    List<String> selectList(Long sortId, Integer isStar, String state);
}
