package com.xxs.msg.service.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.netflix.hystrix.contrib.javanica.utils.CommonUtils;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.msg.service.MsgConstant;
import com.xxs.msg.service.WebSocketServer;
import com.xxs.msg.service.entity.MsgBase;
import com.xxs.msg.service.entity.MsgSort;
import com.xxs.msg.service.service.MsgService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/msg")
public class MsgController {

    @Autowired
    private MsgService msgService;

    /**
     * 获取所有分类对象
     *
     * @return
     */
    @ApiOperation("获取所有分类对象")
    @RequestMapping("/listMsgSort")
    public AjaxResult listMsgSort() {
        List<MsgSort> msgSorts = msgService.listMsgSort();
        return AjaxResult.getOK(msgSorts);
    }

    /**
     * 根据用户编号统计 接收的新消息
     *
     * @param userNo
     * @return
     */
    @ApiOperation("根据用户编号统计 接收的新消息")
    @RequestMapping("/countNewMsg")
    public AjaxResult countNewMsg(String userNo, Long sortId) {
        String state = MsgConstant.MSG_STATE_ISNEW;
        Integer count = msgService.count(state, sortId, userNo);
        return AjaxResult.getOK(count);
    }

    /**
     * 获取用户消息
     *
     * @return
     */
    @ApiOperation("/分页获取用户信息")
    @RequestMapping("/pageNewsMsg")
    public AjaxResult pageNewMsg(String userNo, Long sortId, Integer page, Integer rows) {
        String state = MsgConstant.MSG_STATE_ISNEW;
        List<String> msgNos = msgService.listMsgNo(sortId, null, state);
        Page<MsgBase> msgBasePage = msgService.pageMsg(msgNos,
                null, userNo,
                null, null, page, rows);
        return AjaxResult.getOK(msgBasePage);
    }

    /**
     * 更新所有为已读
     * @return
     */
    @ApiOperation("更新全部消息为已读")
    @RequestMapping("/updateAllIsRead")
    public AjaxResult updateAllIsRead() {
        List<String> msgNos = msgService.listMsgNo(null, null, MsgConstant.MSG_STATE_ISNEW);
        Boolean flag = msgService.isReadBatch(msgNos);
        if (flag != null) {
            return AjaxResult.getOK();
        }
        return  AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("更新单个消息为已读")
    @RequestMapping("/updateIsRead")
    public AjaxResult updateIsRead(String msgNo){
        Boolean read = msgService.isRead(msgNo);
        if (read){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @RequestMapping(value="/pushVideoListToWeb")
    public Map<String,Object> pushVideoListToWeb() {
        Map<String,Object> result =new HashMap<String,Object>();
        try {
            WebSocketServer.sendInfo("有新客户呼入,sltAccountId:");
            result.put("operationResult", true);
        }catch (IOException e) {
            result.put("operationResult", true);
        }
        return result;
    }



}
