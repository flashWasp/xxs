package com.xxs.msg.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.msg.service.MsgConstant;
import com.xxs.msg.service.dao.MsgBaseDao;
import com.xxs.msg.service.entity.MsgBase;
import com.xxs.msg.service.repository.MsgBaseRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
@Service
public class MsgBaseRepositoryImpl extends ServiceImpl<MsgBaseDao, MsgBase> implements MsgBaseRepository {

    @Override
    public Page<MsgBase> selectPage(List<String> msgNos,
                                    String from, String to,
                                    Long createStartTime, Long createEndTime,
                                    Integer page, Integer rows) {
        Page<MsgBase> msgBasePage = new Page<>(page, rows);
        Condition condition = new Condition();
        if (!CollectionUtils.isEmpty(msgNos)) {
            condition.in("msg_no", msgNos);
        }
        if (StringUtils.isNotEmpty(from)) {
            condition.eq("from", from);
        }
        if (StringUtils.isNotEmpty(to)) {
            condition.eq("to", from);
        }
        if (null != createStartTime & null != createEndTime) {
            condition.between("create_time", createStartTime, createEndTime);
        }
        return selectPage(msgBasePage, condition);
    }

    @Override
    public MsgBase getByMsgNo(String msgNo) {
        return selectOne(new Condition().eq("msg_no", msgNo));
    }

    @Override
    public Boolean isRead(String msgNo) {
        MsgBase msgBase = getByMsgNo(msgNo);
        if (msgBase != null) {
            msgBase.setState(MsgConstant.MSG_STATE_ISREAD);
            baseMapper.updateAllColumnById(msgBase);
            return true;
        }
        return false;
    }

    @Override
    public Boolean isReadBatch(List<String> msgNos) {
        List<MsgBase> msgBases = selectList(msgNos);
        if (!CollectionUtils.isEmpty(msgBases)){
            for (MsgBase msg:msgBases) {
                if (msg != null){
                    msg.setState(MsgConstant.MSG_STATE_ISREAD);
                    baseMapper.updateAllColumnById(msg);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<MsgBase> selectList(List<String> msgNos) {
        return baseMapper.selectList(new Condition().in("msg_no",msgNos));
    }

    @Override
    public Integer selectCount(String state,Long sortId ,String to) {
        Condition condition = new Condition();
        if (StringUtils.isNotEmpty(state)){
           condition.eq("state",state);
        }
        if (StringUtils.isNotEmpty(to)){
            condition.eq("to",to);
        }
        if (sortId != null){
            condition.eq("sort_id",sortId);
        }
        return selectCount(condition);
    }
}
