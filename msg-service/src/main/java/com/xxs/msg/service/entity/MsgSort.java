package com.xxs.msg.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public class MsgSort implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String sortName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    @Override
    public String toString() {
        return "MsgSort{" +
        ", id=" + id +
        ", sortName=" + sortName +
        "}";
    }
}
