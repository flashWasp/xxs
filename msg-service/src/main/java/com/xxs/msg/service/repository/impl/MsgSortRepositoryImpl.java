package com.xxs.msg.service.repository.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.msg.service.entity.MsgSort;
import com.xxs.msg.service.dao.MsgSortDao;
import com.xxs.msg.service.repository.MsgSortRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
@Service
public class MsgSortRepositoryImpl extends ServiceImpl<MsgSortDao, MsgSort> implements MsgSortRepository {

    @Override
    public Page<MsgSort> pageMsgSort(Integer page, Integer rows) {
        Page<MsgSort> msgSortPage = new Page<>();
        return selectPage(msgSortPage, null);
    }
}
