package com.xxs.msg.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.xxs.msg.service.entity.MsgSortMapping;
import com.xxs.msg.service.dao.MsgSortMappingDao;
import com.xxs.msg.service.repository.MsgSortMappingRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
@Service
public class MsgSortMappingRepositoryImpl extends ServiceImpl<MsgSortMappingDao, MsgSortMapping> implements MsgSortMappingRepository {

    @Override
    public List<String> selectList(Long sortId, Integer isStar, String state) {
        Condition condition = new Condition();
        if (sortId != null){
            condition.eq("sort_id",sortId);
        }
        if (isStar != null){
            condition.eq("is_star",isStar);
        }
        if (state != null){
            condition.eq("state",state);
        }
        return selectList(condition);
    }
}
