package com.xxs.msg.service.service;

import javax.sql.rowset.serial.SerialException;
import java.util.Map;
import java.util.Set;

/**
 * 邮件发送服务
 */
public interface MailService {

    /**
     * 发送简单Email
     * @param subject
     * @param text
     * @param to
     * @return
     */
    int sendSimpleMail(String subject, String text, Set<String> to);

    /**
     * 发送模板邮件
     * @param subject
     * @param text
     * @param to
     * @return
     */
    int sendTemplateMail(String subject, String text, Set<String> to) throws SerialException;


}
