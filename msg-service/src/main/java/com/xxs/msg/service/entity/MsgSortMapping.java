package com.xxs.msg.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public class MsgSortMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 信息_编号
     */
    private String msgNo;
    /**
     * 分类_id
     */
    private Long sortId;

    /**
     * 重要的
     */
    private Integer isStar;

    /**
     * 状态
     */
    private String state;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsgNo() {
        return msgNo;
    }

    public void setMsgNo(String msgNo) {
        this.msgNo = msgNo;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public Integer getIsStar() {
        return isStar;
    }

    public void setIsStar(Integer isStar) {
        this.isStar = isStar;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "MsgSortMapping{" +
        ", id=" + id +
        ", msgNo=" + msgNo +
        ", sortId=" + sortId +
        ", state=" + state +
        ", isStar=" + isStar +
        "}";
    }
}
