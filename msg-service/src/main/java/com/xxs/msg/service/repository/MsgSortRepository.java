package com.xxs.msg.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.msg.service.entity.MsgSort;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public interface MsgSortRepository extends IService<MsgSort> {

    Page<MsgSort> pageMsgSort(Integer page, Integer rows);

}
