package com.xxs.msg.service.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.msg.service.MsgConstant;
import com.xxs.msg.service.WebSocketServer;
import com.xxs.msg.service.entity.MsgBase;
import com.xxs.msg.service.entity.MsgSort;
import com.xxs.msg.service.entity.MsgSortMapping;
import com.xxs.msg.service.repository.MsgBaseRepository;
import com.xxs.msg.service.repository.MsgSortMappingRepository;
import com.xxs.msg.service.repository.MsgSortRepository;
import com.xxs.msg.service.service.MsgService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("msgService")
public class MsgServiceImpl implements MsgService {
    @Autowired
    private MsgSortRepository msgSortRepository;

    @Autowired
    private MsgBaseRepository msgBaseRepository;

    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private MsgSortMappingRepository msgSortMappingRepository;

    @Override
    public Boolean saveMsgSort(String name) {
        MsgSort msgSort = new MsgSort();
        msgSort.setSortName(name);
        return msgSortRepository.insert(msgSort);
    }

    @Override
    public MsgSort getById(Long id) {
        return msgSortRepository.selectById(id);
    }

    @Override
    public Boolean updateMsgSort(Long id, String name) {
        MsgSort msgSort = getById(id);
        if (null != msgSort) {
            msgSort.setSortName(name);
            return msgSortRepository.updateAllColumnById(msgSort);
        }
        return false;
    }

    @Override
    public Boolean delMsgSort(Long id, Boolean isAll) {
        // 删除分类对象
        boolean flag = msgSortRepository.deleteById(id);
        if (flag) {
            // 级联删除
            if (isAll) {
                //
                List<String> msgNos = listMsgNo(id, null, null);
                return delMsgBatch(msgNos);
            }
        }
        return false;
    }

    @Override
    public Page<MsgSort> pageMsgSort(Integer page, Integer rows) {
        return msgSortRepository.pageMsgSort(page, rows);
    }

    @Override
    public List<MsgSort> listMsgSort() {
        return msgSortRepository.selectList(null);
    }

    @Override
    public Boolean saveMsg(String title,
                           Long sortId, Integer isStar,
                           String from, List<String> toSet,
                           String cmd, String content,
                           String msgType, String extras) {
        List<MsgBase> msgBases = new ArrayList<>();
        List<String> msgBaseNos = new ArrayList<>();
        // 生成MSG
        MsgBase msgBase = new MsgBase();
        Boolean flag = false;
        msgBase.setTitle(title);
        msgBase.setIsStar(isStar);
        msgBase.setMsgType(msgType);
        msgBase.setCmd(cmd);
        msgBase.setContent(content);
        msgBase.setCrateTime(System.currentTimeMillis());
        msgBase.setExtras(extras);
        msgBase.setState(MsgConstant.MSG_STATE_ISNEW);
        for (String to : toSet) {
            if (StringUtils.isNotBlank(to)) {
                msgBase.setFrom(TokenRedisRepository.getCurUserUUID());
                msgBase.setMsgNo(KeyUtil.getUniqueKey());
                msgBase.setTo(to);
                msgBases.add(msgBase);
                msgBaseNos.add(msgBase.getMsgNo());
            }
        }
        flag = msgBaseRepository.insertBatch(msgBases);

        ArrayList<MsgSortMapping> msgSortMappings = new ArrayList<>();
        MsgSortMapping msgSortMapping = new MsgSortMapping();
        msgSortMapping.setSortId(sortId);
        if (!CollectionUtils.isEmpty(msgBaseNos)) {
            for (String msgBaseNo : msgBaseNos) {
                msgSortMapping.setMsgNo(msgBaseNo);
                msgSortMappings.add(msgSortMapping);
            }
        }
        flag = msgSortMappingRepository.insertBatch(msgSortMappings);
        return flag;
    }

    @Override
    public List<String> listMsgNo(Long sortId, Integer isStar, String state) {
        return msgSortMappingRepository.selectList(sortId,isStar,state);
    }

    @Override
    public List<MsgBase> listMsgBase(List<String> msgNos) {
        return msgBaseRepository.selectList(msgNos);
    }

    @Override
    public Page<MsgBase> pageMsg(List<String> msgNos, String from, String to,
                                 Long createStartTime, Long createEndTime,
                                 Integer page, Integer rows) {
        return msgBaseRepository.selectPage(msgNos,
                from,to,createStartTime,createEndTime,page,rows);
    }

    @Override
    public MsgBase getByMsgNo(String msgNo) {
        return msgBaseRepository.getByMsgNo(msgNo);
    }

    @Override
    public Boolean isRead(String msgNo) {
        return msgBaseRepository.isRead(msgNo);
    }

    @Override
    public Boolean isReadBatch(List<String> msgNos) {
        return msgBaseRepository.isReadBatch(msgNos);
    }

    @Override
    public Boolean delMsgBatch(List<String> msgNos) {
        List<MsgBase> msgBases = listMsgBase(msgNos);
        if (msgBases != null){
            List<Long> ids = msgBases.stream().map(MsgBase::getId).distinct().collect(Collectors.toList());
            return msgBaseRepository.deleteBatchIds(ids);
        }
        return false;
    }

    @Override
    public Integer count(String state,Long sortId ,String to) {
        return msgBaseRepository.selectCount(state,sortId,to);
    }

    @Override
    public void sendMsgWebocket(String message) throws IOException {
        Integer isNew = msgBaseRepository.selectCount("isNew", null, TokenRedisRepository.getCurUserUUID());
        // 封装Msg对象
        webSocketServer.sendInfo(isNew + "");
    }
}
