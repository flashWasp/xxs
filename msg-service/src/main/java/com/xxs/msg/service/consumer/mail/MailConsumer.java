package com.xxs.msg.service.consumer.mail;


import com.alibaba.fastjson.JSONObject;
import com.xxs.common.service.model.template.MailTemplate;
import com.xxs.msg.service.consumer.Consumer;
import com.xxs.msg.service.service.MailService;
import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 邮件消费者
 */
@Component
public class MailConsumer implements Consumer {
    @Autowired
    private MailService mailService;

    @Override
    public void handler(Message message) throws IOException {
        String encoded = new String(message.getBody(), "UTF-8");
        String s = StringEscapeUtils.unescapeJava(encoded);
        String substring = s.substring(1, s.length() - 1);
        MailTemplate mailTemplate = JSONObject.parseObject(substring, MailTemplate.class);
        mailService.sendSimpleMail(mailTemplate.getSubject(), mailTemplate.getText(), mailTemplate.getTo());
    }

}
