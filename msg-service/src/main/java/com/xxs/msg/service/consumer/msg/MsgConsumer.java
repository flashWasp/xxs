package com.xxs.msg.service.consumer.msg;

import com.alibaba.fastjson.JSONObject;
import com.xxs.common.service.model.template.MsgTemplate;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.msg.service.consumer.Consumer;
import com.xxs.msg.service.service.MsgService;
import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;

/**
 * 系统消息
 */
@Component
public class MsgConsumer implements Consumer{

    @Autowired
    private MsgService msgService;
    /**
     * 处理消息通知
     * @param message
     * @throws IOException
     * @throws ClassNotFoundException
     */

    @Override
    public void handler(Message message) throws IOException, ClassNotFoundException {
        String encoded = new String(message.getBody(), "UTF-8");
        String s = StringEscapeUtils.unescapeJava(encoded);
        String substring = s.substring(1, s.length() - 1);
        MsgTemplate msgTemplate = JSONObject.parseObject(substring, MsgTemplate.class);
        msgService.saveMsg(
                msgTemplate.getSubject(),
                msgTemplate.getSortId(),
                msgTemplate.getIsStar(),
                TokenRedisRepository.getCurUserUUID(),
                new ArrayList<>(msgTemplate.getTo()),
                msgTemplate.getCmd(),
                msgTemplate.getText(),
                msgTemplate.getMsgType(),
                msgTemplate.getExtras()
        );
        // 发送websocket
        msgService.sendMsgWebocket(msgTemplate.getText());
    }
}
