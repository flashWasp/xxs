package com.xxs.msg.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.msg.service.entity.MsgBase;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public interface MsgBaseRepository extends IService<MsgBase> {

    Page<MsgBase> selectPage(List<String> msgNos, String from, String to, Long createStartTime, Long createEndTime, Integer page, Integer rows);

    MsgBase getByMsgNo(String msgNo);

    Boolean isRead(String msgNo);

    Boolean isReadBatch(List<String> msgNos);

    List<MsgBase> selectList(List<String> msgNos);

    Integer selectCount(String state,Long sortId ,String to);

}
