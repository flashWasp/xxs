package com.xxs.msg.service.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-03
 */
public class MsgBase implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 标题
     */
    private String title;
    /**
     * 具体动作
     */
    private String msgNo;
    /**
     * 来源Id
     */
    private String from;
    /**
     * 目标对象Id
     */
    private String to;
    /**
     * 命令码
     */
    private String cmd;
    /**
     * isNew表示新 isOld代表否
     */
    private String state;

    /**
     * 0 不重要 1重要
     */
    private Integer isStar;

    /**
     * 内容
     */
    private String content;
    /**
     * 0:text、1:image、2:voice、3:vedio、4:music、5:news
     */

    @TableField("msgType")
    private String msgType;
    /**
     * "扩展字段,JSON对象格式如：{'扩展字段名称':'扩展字段value'}"
     */
    private String extras;


    @TableField("crateTime")
    private Long crateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsgNo() {
        return msgNo;
    }

    public void setMsgNo(String msgNo) {
        this.msgNo = msgNo;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getIsStar() {
        return isStar;
    }

    public void setIsStar(Integer isStar) {
        this.isStar = isStar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCrateTime() {
        return crateTime;
    }

    public void setCrateTime(Long crateTime) {
        this.crateTime = crateTime;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "MsgBase{" +
        ", id=" + id +
        ", msgNo=" + msgNo +
        ", from=" + from +
        ", to=" + to +
        ", cmd=" + cmd +
        ", state=" + state +
        ", isStar=" + isStar +
        ", content=" + content +
        ", crateTime=" + crateTime +
        ", msgType=" + msgType +
        ", extras=" + extras +
        "}";
    }
}
