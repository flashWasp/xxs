

-- ========================   2018-11-07 ========================
CREATE TABLE `member` (
  `id` bigint(11) NOT NULL,
  `account` varchar(20) NOT NULL DEFAULT '' COMMENT '登录账户',
  `password` varchar(50) DEFAULT '' COMMENT '登录密码',
  `name` varchar(24) NOT NULL DEFAULT '' COMMENT '姓名',
  `mobile_no` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '性别',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '用户邮箱',
  `qq_no` varchar(50) NOT NULL DEFAULT '' COMMENT 'QQ号码',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注说明',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '账号状态 1正常 2禁用',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL,
  `member_no` varchar(128) NOT NULL DEFAULT '' COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_article` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `article_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章编号',
  `channelId` int(11) NOT NULL COMMENT '频道Id',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `views` int(11) NOT NULL DEFAULT '0' COMMENT '观察量 ',
  `favorites` int(11) NOT NULL DEFAULT '0' COMMENT '收藏数',
  `image_no` varchar(64) NOT NULL COMMENT '封面',
  `version` varchar(64) NOT NULL DEFAULT '' COMMENT '版本 最低是1',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` varchar(24) NOT NULL DEFAULT 'draft' COMMENT '状态 draft   reviewing  pass',
  `content` longtext NOT NULL COMMENT '文章内容',
  `summary` longtext COMMENT '文章简介',
  PRIMARY KEY (`id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_article_comment_history` (
  `id` bigint(32) NOT NULL,
  `comment_no` varchar(64) NOT NULL DEFAULT '' COMMENT '评论编码',
  `article_no` varchar(64) NOT NULL DEFAULT '' COMMENT '文章编码',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '文章内容',
  `sender_no` varchar(64) NOT NULL DEFAULT '' COMMENT '发送者编码',
  `recevier_no` varchar(64) NOT NULL DEFAULT '' COMMENT '接受者编码',
  `parent_comment_no` varchar(64) NOT NULL DEFAULT '' COMMENT '父评论编码',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻频道--评论记录';

CREATE TABLE `news_article_fav_history` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `article_no` varchar(64) NOT NULL DEFAULT '' COMMENT '文章编码',
  `user_no` varchar(64) NOT NULL DEFAULT '' COMMENT '用户编码',
  `map_no` varchar(64) NOT NULL DEFAULT '' COMMENT '关系表编码',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻模块--收藏记录';

CREATE TABLE `news_article_info` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `article_no` varchar(64) NOT NULL DEFAULT '' COMMENT '文章编号',
  `content` longtext NOT NULL COMMENT '文章内容',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_article_version` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `article_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文章编号',
  `channelId` int(11) NOT NULL COMMENT '频道Id',
  `version` varchar(64) NOT NULL DEFAULT '' COMMENT '版本 最低是1',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` varchar(24) NOT NULL DEFAULT 'draft' COMMENT '状态 reject  pass',
  `content` longtext NOT NULL COMMENT '文章内容',
  PRIMARY KEY (`id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) NOT NULL DEFAULT '0' COMMENT '父Id',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT '简介',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序字段，数值越大优先级越高',
  `parentIds` varchar(64) NOT NULL DEFAULT '0',
  `first_parent_Id` int(2) NOT NULL DEFAULT '0',
  `level` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='新闻模块--频道';

CREATE TABLE `news_label` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `label_no` varchar(64) NOT NULL DEFAULT '' COMMENT '标签编码',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名字',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻模块-标签';

CREATE TABLE `news_view_history` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT,
  `article_no` varchar(64) NOT NULL DEFAULT '' COMMENT '文章编码',
  `viewer_no` varchar(64) NOT NULL DEFAULT '' COMMENT '浏览者编码',
  `map_no` varchar(64) NOT NULL DEFAULT '' COMMENT '关系表编码',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻模块-文章浏览记录';

CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sys_ablum` (
  `id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `image_no` varchar(64) NOT NULL DEFAULT '' COMMENT '图片编号',
  `image_name` varchar(64) NOT NULL DEFAULT '' COMMENT '图片名称',
  `bulket` varchar(100) NOT NULL DEFAULT '' COMMENT '文件存储空间(Oss服务器则为其bulket)',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统模块--图片服务';

CREATE TABLE `sys_permission_base` (
  `id` bigint(32) NOT NULL,
  `name` varchar(16) NOT NULL DEFAULT '' COMMENT '名称',
  `resourceType` varchar(16) NOT NULL DEFAULT '' COMMENT '资源类型',
  `url` varchar(64) NOT NULL DEFAULT '' COMMENT '资源路径',
  `permission` varchar(32) NOT NULL DEFAULT '' COMMENT '权限字符串',
  `parentId` bigint(32) NOT NULL COMMENT '父编号',
  `parentIds` varchar(64) NOT NULL DEFAULT '' COMMENT '父编号列表',
  `available` tinyint(1) NOT NULL COMMENT '有效判断 0:可用 1:不可用',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `updateUserUuid` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `createUserUuid` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未删除 1:已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限基本表';

CREATE TABLE `sys_role_base` (
  `id` bigint(32) NOT NULL,
  `role` varchar(16) NOT NULL DEFAULT '' COMMENT '用户描述',
  `description` varchar(64) NOT NULL DEFAULT '' COMMENT '角色简介',
  `available` tinyint(1) NOT NULL DEFAULT '1' COMMENT '有效判断 0:可用 1:不可用(默认)',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限角色表';

CREATE TABLE `sys_role_permission` (
  `id` bigint(32) NOT NULL,
  `roleId` bigint(32) NOT NULL COMMENT '角色权限Id',
  `permissionId` bigint(32) NOT NULL COMMENT '权限Id',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` varchar(255) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限中间表';

CREATE TABLE `sys_user_base` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '默认主键',
  `uuid` varchar(128) NOT NULL DEFAULT '' COMMENT 'uuid内部主键',
  `account` varchar(16) NOT NULL DEFAULT '' COMMENT '账号',
  `userName` varchar(16) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(64) NOT NULL DEFAULT '' COMMENT '加密盐',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态  0:创建未认证 1:被删除 2:正常状态 3:被锁定',
  `createUserUuId` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUID',
  `updateUserUuId` varchar(32) NOT NULL DEFAULT '' COMMENT '更新人UUId',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户基本表';

CREATE TABLE `sys_user_info` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT,
  `userUuid` varchar(128) NOT NULL DEFAULT '' COMMENT '用户uuid',
  `mobile` varchar(16) NOT NULL DEFAULT '' COMMENT '用户移动手机',
  `email` varchar(32) NOT NULL DEFAULT '' COMMENT '用户邮箱',
  `loginCount` int(128) NOT NULL DEFAULT '0' COMMENT '用户总登陆次数',
  `ip` varchar(255) NOT NULL DEFAULT '' COMMENT 'IP地址',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `lastLoginTime` timestamp NOT NULL COMMENT '用户最后一次登陆时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` varchar(255) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  `gender` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 表示男性 2 表示女性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户信息表';

CREATE TABLE `sys_user_role` (
  `id` bigint(32) NOT NULL,
  `roleId` bigint(32) NOT NULL COMMENT '角色Id',
  `userUuid` varchar(128) NOT NULL DEFAULT '' COMMENT '用户UUId',
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `createTime` timestamp NOT NULL COMMENT '创建时间',
  `createUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者UUId',
  `updateUserUuid` varchar(32) NOT NULL DEFAULT '' COMMENT '更新者UUID',
  `state` varchar(255) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:被删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色中间表';

-- ========================   2018-11-07  end========================

-- ========================   2018-12-03  start========================
CREATE TABLE `msg_base` (
  `id` bigint(11) NOT NULL,
  `msg_no` varchar(255) DEFAULT NULL COMMENT '具体动作',
  `from` varchar(255) DEFAULT NULL COMMENT '来源Id',
  `to` varchar(255) DEFAULT NULL,
  `cmd` varchar(64) DEFAULT NULL COMMENT '命令码',
  `is_new` tinyint(1) DEFAULT NULL COMMENT '0 表示新 1代表否',
  `is_read` tinyint(1) DEFAULT NULL,
  `is_star` tinyint(1) DEFAULT NULL COMMENT '0 不重要 1重要',
  `content` varchar(255) DEFAULT NULL,
  `crateTime` bigint(11) DEFAULT NULL COMMENT '0:text、1:image、2:voice、3:vedio、4:music、5:news',
  `msgType` varchar(255) DEFAULT NULL,
  `extras` longtext COMMENT '"扩展字段,JSON对象格式如：{''扩展字段名称'':''扩展字段value''}"',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `msg_sort` (
  `id` bigint(11) NOT NULL,
  `sort_id` tinyint(11) DEFAULT NULL,
  `sort_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `msg_sort_mapping` (
  `id` bigint(11) NOT NULL,
  `msg_no` varchar(128) DEFAULT NULL COMMENT '信息_编号',
  `sort_id` tinyint(1) DEFAULT NULL COMMENT '分类_id',
  `is_news` tinyint(1) DEFAULT NULL COMMENT '新的',
  `is_star` tinyint(1) DEFAULT NULL COMMENT '重要的',
  `is_read` tinyint(1) DEFAULT NULL COMMENT '可见的',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ========================   2018-12-03  end========================
