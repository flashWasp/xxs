package com.xxs.common.service.model.template;

import java.io.Serializable;
import java.util.Set;

public class MailTemplate implements Serializable {

    private  String subject;
    private String text;
    private Set<String> to;

    public MailTemplate(String subject, String text, Set<String> to) {
        this.subject = subject;
        this.text = text;
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<String> getTo() {
        return to;
    }

    public void setTo(Set<String> to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "MailTemplate{" +
                "subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", to=" + to +
                '}';
    }
}
