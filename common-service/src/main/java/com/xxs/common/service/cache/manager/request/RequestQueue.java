package com.xxs.common.service.cache.manager.request;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 请求内存队列
 */
public class RequestQueue {
    /**
     * 内存队列
     */
    private List<ArrayBlockingQueue<CacheRequest>> queues =
            new ArrayList<ArrayBlockingQueue<CacheRequest>>();

    /**
     * 单位对象更新Flag
     */
    private ConcurrentHashMap<String,Boolean> eleFlag= new ConcurrentHashMap<String,Boolean>();

    /**
     * 单例
     *
     * @author Administrator
     *
     */
    private static class Singleton {

        private static RequestQueue instance;

        static {
            instance = new RequestQueue();
        }

        public static RequestQueue getInstance() {
            return instance;
        }

    }

    /**
     * jvm的机制去保证多线程并发安全
     *
     * 内部类的初始化，一定只会发生一次，不管多少个线程并发去初始化
     *
     * @return
     */
    public static RequestQueue getInstance() {
        return Singleton.getInstance();
    }

    /**
     * 添加一个内存队列
     * @param queue
     */
    public void addQueue(ArrayBlockingQueue<CacheRequest> queue) {
        this.queues.add(queue);
    }

    /**
     * 获取内存队列的数量
     * @return
     */
    public int queueSize() {
        return queues.size();
    }

    /**
     * 获取内存队列
     * @param index
     * @return
     */
    public ArrayBlockingQueue<CacheRequest> getQueue(int index) {
        return queues.get(index);
    }

    public ConcurrentHashMap getEleFlag() {
        return eleFlag;
    }
}
