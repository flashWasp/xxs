package com.xxs.common.service.exception;

import com.xxs.common.service.model.ResultCode;





/**
 * 参数验证异常
 *
 *
 *         2018年2月1日
 */
public class ParamException extends ServiceException {

	/**
	 *
	 */
	private static final long serialVersionUID = 8004975722028133384L;

	public ParamException(String message) {
		super(message);
		this.code = ResultCode.ParamException; // 参数验证异常
	}

	public ParamException(String message, Throwable cause) {
		super(message, cause);
		this.code = ResultCode.ParamException; // 参数验证异常
	}
}
