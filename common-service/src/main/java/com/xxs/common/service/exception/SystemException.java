package com.xxs.common.service.exception;



import com.xxs.common.service.model.ResultCode;

/**
 * 系统异常
 *
 *
 *         2018年1月31日
 */
public class SystemException extends BaseException {

	/**
	 *
	 */
	private static final long serialVersionUID = -6246281755627739978L;

	public SystemException(String message) {
		super(message);
		this.code = ResultCode.SystemException;// 系统异常
	}

	public SystemException(String message, Throwable cause) {
		super(message, cause);
		this.code = ResultCode.SystemException; // 系统异常
	}

	public SystemException(ResultCode code, String message) {
		super(code, message);
	}

	public SystemException(ResultCode code, String message, Throwable cause) {
		super(code, message, cause);
	}
}
