package com.xxs.common.service.util.io;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageCut {
	
	/**
	 * 图片切割
	 * @param imagePath 原图地址
	 * @param x 目标切片坐标 X轴起点
	 * @param y 目标切片坐标 Y轴起点
	 * @param w 目标切片 宽度
	 * @param h 目标切片 高度
	 * @param proportion 
	 */
	public void cutImage(String imagePath, int x, int y, int w, int h, Double proportion) {
		try {
			
			// 定义图片和过滤器
			Image img;
			ImageFilter cropFilter;
			
			// 读取源图像
			BufferedImage bi = ImageIO.read(new File(imagePath));
			int srcWidth = bi.getWidth(); // 源图宽度
			int srcHeight = bi.getHeight(); // 源图高度
			
			//图片宽度大于高度
			if(srcWidth >= srcHeight){
				// 若原图大小大于切片大小，则进行切割
				if (srcWidth >= w && srcHeight >= h) {
					Image image = bi.getScaledInstance(srcWidth, srcHeight,
							Image.SCALE_DEFAULT);
					int x1 = (int) (x * (1/proportion));//起点横坐标
					int y1 = (int) (y * (1/proportion));//起点纵坐标
					int w1 = (int) (w * (1/proportion));//图片宽
					int h1 = (int) (h * (1/proportion));//图片长
					cropFilter = new CropImageFilter(x1, y1, w1, h1);
					img = Toolkit.getDefaultToolkit().createImage(
							new FilteredImageSource(image.getSource(), cropFilter));
					BufferedImage tag = new BufferedImage(h1, h1,
							BufferedImage.TYPE_INT_RGB);
					Graphics g = tag.getGraphics();
					g.drawImage(img, 0, 0, null); // 绘制缩小后的图
					g.dispose();
					// 输出为文件
					ImageIO.write(tag, "JPEG", new File(imagePath));
				}else{
					// 若原图大小大于切片大小，则进行切割，则保存为矩形
					Image image = bi.getScaledInstance(srcWidth, srcHeight,
							Image.SCALE_DEFAULT);
					cropFilter = new CropImageFilter(0, 0, srcWidth, srcHeight);
					img = Toolkit.getDefaultToolkit().createImage(
							new FilteredImageSource(image.getSource(), cropFilter));
					BufferedImage tag = new BufferedImage(srcHeight, srcHeight,
							BufferedImage.TYPE_INT_RGB);
					Graphics g = tag.getGraphics();
					g.drawImage(img, 0, 0, null); // 绘制缩小后的图
					g.dispose();
					// 输出为文件
					ImageIO.write(tag, "JPEG", new File(imagePath));
				}
			}else{
				// 若原图大小大于切片大小，则进行切割
				if (srcWidth >= w && srcHeight >= h) {
					Image image = bi.getScaledInstance(srcWidth, srcHeight,Image.SCALE_DEFAULT);
					int x1 = (int) (x * (1/proportion));//起点横坐标
					int y1 = (int) (y * (1/proportion));//起点纵坐标
					int w1 = (int) (w * (1/proportion));//图片宽
					int h1 = (int) (h * (1/proportion));//图片长
					cropFilter = new CropImageFilter(x1, y1, w1, h1);
					img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), cropFilter));
					BufferedImage tag = new BufferedImage(w1, w1,BufferedImage.TYPE_INT_RGB);
					Graphics g = tag.getGraphics();
					g.drawImage(img, 0, 0, null); // 绘制缩小后的图
					g.dispose();
					// 输出为文件
					ImageIO.write(tag, "JPEG", new File(imagePath));
				}else{
					// 若原图大小大于切片大小，则进行切割，则保存为矩形
					Image image = bi.getScaledInstance(srcWidth, srcHeight,Image.SCALE_DEFAULT);
					cropFilter = new CropImageFilter(0, 0, srcWidth, srcHeight);
					img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), cropFilter));
					BufferedImage tag = new BufferedImage(srcWidth, srcWidth, BufferedImage.TYPE_INT_RGB);
					Graphics g = tag.getGraphics();
					g.drawImage(img, 0, 0, null); // 绘制缩小后的图
					g.dispose();
					// 输出为文件
					ImageIO.write(tag, "JPEG", new File(imagePath));
				}
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 自定义复制图片类
	 * @param realPath
	 * @param savePath
	 * @throws IOException
	 */
	public void cpPic(String realPath,String savePath) throws IOException{
        
		FileInputStream fis =null;
        FileOutputStream fos =null;
        fis= new FileInputStream(realPath);
        fos= new FileOutputStream(savePath);
        byte[] buf =new byte[1024];
        int ch= 0;
        while((ch=fis.read (buf ))!=-1 ){
             fos. write( buf, 0, ch);
        }
        fis. close();
        fos. close();
 }

}
