package com.xxs.common.service.model;

/**
 * AjaxResult通用message
 *
 *         2017年12月1日
 */
public class ResultMsg {
	
	public static final String FAILURE = "执行失败!";
	
	public static final String SUCCESS = "执行成功!";

	public static final String SYSTEM_EXCEPTION = "系统异常!";

	public static final String SERVICE_EXCEPTION = "业务异常!";

	public static final String SYSTEM_EXCEPTION_DB = "数据库操作异常!";

	public static final String SYSTEM_EXCEPTION_IO = "IO操作异常!";

	public static final String SYSTEM_EXCEPTION_NULL_POINT = "空指针异常!";
	
	public static final String PARAMETER_EXCEPTION = "参数错误!";

	public static final String QUARTZ_EXCEPTION = "定时器异常!";
	
	public static final String LISTENER_EXCEPTION = "监听器异常!";
	
	public static final String INVALID_EVENT_TYPE = "非法的事件类型!";
	
	public static final String EVENT_CANNOT_NULL = "事件不能为空!";
	
	public static final String LISTENER_ABNORMAL_EXCEPTION = "监听器异常记录异常!";
	
	public static final String NOPERMISSION = "无操作权限！";

}
