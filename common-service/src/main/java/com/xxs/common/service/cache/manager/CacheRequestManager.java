package com.xxs.common.service.cache.manager;


import com.xxs.common.service.cache.CacheFactory;
import com.xxs.common.service.cache.CacheManager;
import com.xxs.common.service.cache.CacheObj;
import com.xxs.common.service.cache.manager.request.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存请求处理器
 */
public class CacheRequestManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheRequestManager.class);

    /**
     * 缓存请求
     */
    private CacheRequest cacheRequest;

    public CacheRequestManager(CacheRequest cacheRequest) {
        this.cacheRequest = cacheRequest;
    }

    private void asynProcess() {
        RequestQueue requestQueue = RequestQueue.getInstance();
        // 获取单个对象更新标记位
        ConcurrentHashMap<String, Boolean> eleFlag = requestQueue.getEleFlag();
        // 做请求的路由，根据每个请求的id，路由到对应的内存队列中去
        ArrayBlockingQueue<CacheRequest> queue = getRoutingQueue(cacheRequest.getId().toString());
        // 缓存更新请求
//        if (cacheRequest instanceof UpdateCacheRequest ||
//            cacheRequest instanceof DelCacheRequest) {
//            // true 表示队列中该对象存在
//            eleFlag.put(cacheRequest.getkey(), true);
//        } else
        if (cacheRequest instanceof QueryCacheRequest) {
            Boolean flag = eleFlag.get(cacheRequest.getkey());
            // 如果flag是null,无请求
            if (flag == null) {
                eleFlag.put(cacheRequest.getkey(), false);
            }
            // 如果是缓存刷新的请求，那么就判断，如果标识不为空，而且是true，就说明之前有一个对象缓存请求更新
            if (flag != null && flag) {
                eleFlag.put(cacheRequest.getkey(), false);
            }
            // 数据请求去重
            if (flag != null && !flag) {
                return;
            }
        }
        // 将请求放入对应的队列中，完成路由操作
        try {
            LOGGER.info(cacheRequest.toString() + "路由进入队列" + queue.toString());
            queue.put(cacheRequest);
            LOGGER.info("queue 添加对象 queue Size :" + queue.size() );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取路由到的内存队列
     *
     * @param
     * @return 内存队列
     */
    private ArrayBlockingQueue<CacheRequest> getRoutingQueue(String key) {
        RequestQueue requestQueue = RequestQueue.getInstance();
        LOGGER.info(requestQueue.queueSize() + "");
//        Integer code = key.hashCode();
//        int h;
//        int hash = (code == null) ? 0 : (h = code.hashCode()) ^ (h >>> 16);
//        // 对hash值取模，将hash值路由到指定的内存队列中
//        int index = (requestQueue.queueSize() - 1) & hash;
        //  System.out.println("===========日志===========: 路由内存队列，id =" + code + ", 队列索引=" + index);
        int hashCode = 0;
        int size = requestQueue.queueSize();
        for (int i = 0; i < key.length(); i++) { // 从字符串的左边开始计算
            boolean digit = Character.isDigit(key.charAt(i));
            int letterValue = 0;
            if (digit) {
                letterValue = key.charAt(i);
            } else {
                letterValue = key.charAt(i) - 96;// 将获取到的字符串转换成数字，比如a的码值是97，则97-96=1
            }
            // 就代表a的值，同理b=2；
            hashCode = ((hashCode << 5) + letterValue) % size;// 防止编码溢出，对每步结果都进行取模运算
        }
        return requestQueue.getQueue(hashCode);
    }

    /**
     * 更新操作
     *
     * @return
     */
    public Boolean update() {
        asynProcess();
        return true;
    }

    public Boolean del() {
        asynProcess();
        return true;
    }

    public CacheObj read() {
        asynProcess();
        long startTime = System.currentTimeMillis();
        long endTime = 0L;
        long waitTime = 0L;
        CacheObj cacheObj;
        try {
            while (true) {
                if (waitTime > 100) {
                    break;
                }
                CacheFactory cacheFactory = new CacheFactory();
                CacheManager cacheManager = cacheFactory.newCacheManager();
                cacheObj = cacheManager.get(cacheRequest.getRegion(), cacheRequest.getId());
                if (cacheObj != null) {
                    return cacheObj;
                } else {
                    // 等待更新操作完成
                    Thread.sleep(20);
                    endTime = System.currentTimeMillis();
                    waitTime = endTime - startTime;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
