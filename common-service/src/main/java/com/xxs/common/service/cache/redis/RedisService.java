package com.xxs.common.service.cache.redis;

import redis.clients.jedis.BinaryClient.LIST_POSITION;
import redis.clients.jedis.*;
import redis.clients.jedis.params.sortedset.ZAddParams;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 备注：默认使用DataBase1
 *
 */
public interface RedisService {

	
	/*----------------------------String数据类型的DAO操作---------------------------------*/

	void set(int database, final byte[] key, final byte[] value);

	byte[] get(int database, byte[] key);

	void del(int database, String...key);

	/**
	 * 设置String类型的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 */
	void set(int database, String key, String value);
	
	
	/**
	 * 得到值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	String get(int database, String key);
	
	
	/**
	 * 返回 key 中字符串值的子字符
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		开始
	 * @param end		结束
	 * @return			子字符窜
	 */
	String getrange(int database, String key, long start, long end);
	

	/**
	 * 将给定 key 的值设为 value ，并返回 key 的旧值(old value)。
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		新值
	 * @return			key的旧值
	 */
	String getset(int database, String key, String value);
	
	/**
	 * 
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param offset	偏移值
	 * @return
	 */
	Boolean getbit(int database, String key, long offset);
	
	/**
	 * 获取所有(一个或多个)给定 key 的值。
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	List<String> mget(int database, String... key);
	
	/**
	 * 对 key 所储存的字符串值，设置或清除指定偏移量上的位(bit)
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param offset	偏移量
	 * @param value		值
	 * @return
	 */
	Boolean setbit(int database, String key, long offset, String value);
		
	/**
	 * 将值 value 关联到 key ，并将 key 的过期时间设为 seconds (以秒为单位)
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param seconds	过期时间
	 * @param value		值
	 * @return
	 */
	String setex(int database, String key, int seconds, String value);
	
	/**
	 * 只有在 key不存在时设置 key的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 * @return
	 */
	Long setnx(int database, String key, String value);
	
	/**
	 * 用value参数覆写给定key所储存的字符串值，从偏移量offset开始
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param offset	偏移量
	 * @param value		值
	 * @return
	 */
	Long setrange(int database, String key, long offset, String value);
	
	/**
	 * 返回 key 所储存的字符串值的长度
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long strlen(int database, String key);
	
	/**
	 * 同时设置一个或多个 key-value 对
	 * @param database	数据库索引号
	 * @param keysvalues 键值对
	 * @return
	 */
	String mset(int database, String... keysvalues);
	
	/**
	 * 同时设置一个或多个 key-value 对，当且仅当所有给定 key 都不存在
	 * @param database	数据库索引号
	 * @param keysvalues 键值对
	 * @return
	 */
	Long msetnx(int database, String... keysvalues);
	
	/**
	 * 这个命令和 SETEX 命令相似，但它以毫秒为单位设置 key 的生存时间，而不是像 SETEX 命令那样，以秒为单位
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param milliseconds	毫秒
	 * @param value		值
	 * @return
	 */
	@Deprecated
	String psetex(int database, String key, int milliseconds, String value);
	
	/**
	 * 将 key 中储存的数字值增一
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long incr(int database, String key);

	/**
	 * 将key所储存的值加上给定的增量值(increment)
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param increment	增量
	 * @return
	 */
	Long incrby(int database, String key, long increment);

	/**
	 * 将key所储存的值加上给定的浮点增量值(increment)
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 * @return
	 */
	Double incrByFloat(int database, String key, double value);
	
	/**
	 * 将key中储存的数字值减一
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long decr(int database, String key);
	
	/**
	 * key所储存的值减去给定的减量值(decrement)
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param decrement	减量值
	 * @return
	 */
	Long decrby(int database, String key, long decrement);

	/**
	 * 如果key已经存在并且是一个字符串,APPEND命令将指定的value追加到该key原来值(value)的末尾
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 * @return
	 */
	Long decrby(int database, String key, String value);
	
	/**
	 * 删除key
	 * @param database	数据库索引号
	 * @param key		键值
	 */
	void del(int database, String key);
	
	
	/*----------------------------字典数据类型的DAO操作---------------------------------*/
	
	
	/**
	 * 设置字典类型的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param hash		hash值
	 */
	void hmset(int database, String key, Map<String, String> hash);
	
	/**
	 * 得到字典的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param field		hash键值
	 * @return
	 */
	String hget(int database, String key, String field);
	
	/**
	 * 删除字典的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param fields	hash键值
	 * @return
	 */
	Long hdel(int database, String key, String... fields);
	
	/**
	 * 查看哈希 key中，指定的字段是否存在
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param field		域
	 * @return	
	 */
	Boolean hexists(int database, String key, String field);
	
	/**
	 * 获取在哈希表中指定 key 的所有字段和值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Map<String, String> hgetall(int database, String key);
	
	/**
	 * 为哈希表 key 中的指定字段的整数值加上增量increment
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param field		域
	 * @param increment	增量
	 * @return
	 */
	Long hincrby(int database, String key, String field, long increment);
	
	/**
	 * 为哈希表 key 中的指定字段的浮点数值加上增量 increment
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param field		域
	 * @param increment	增量
	 * @return
	 */
	Double hincrByFloat(int database, String key, String field, double increment);
	
	/**
	 * 获取所有哈希表中的字段
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Set<String> hkeys(int database, String key);
	
	/**
	 * 获取哈希表中字段的数量
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long hlen(int database, String key);
	
	/**
	 * 获取所有给定字段的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param fields	域
	 * @return
	 */
	List<String> hmget(int database, String key, String... fields);
	
	/**
	 * 将哈希表key中的字段field的值设为 value
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param field		域
	 * @param value		值
	 * @return
	 */
	Long hset(int database, String key, String field, String value);
	
	/**
	 * 只有在字段field不存在时，设置哈希表字段的值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param field		域
	 * @param value		值
	 * @return
	 */
	Long hsetnx(int database, String key, String field, String value);
	
	/**
	 * 获取哈希表中所有值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	List<String> hvals(int database, String key);
	
	/**
	 * 迭代哈希表中的键值对
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param cursor	游标
	 * @return
	 */
	ScanResult<Map.Entry<String, String>> hscan(int database, String key, int cursor);

	
	/*------------------------------集合操作-----------------------------------*/
	
	
	/**
	 * 向集合添加一个或多个成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param members	成员
	 * @return
	 */
	Long sadd(int database, String key, String... members);
	
	/**
	 * 获取集合的成员数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long scard(int database, String key);
	
	/**
	 * 返回给定所有集合的差集
	 * @param database	数据库索引号
	 * @param keys		键值数组
	 * @return
	 */
	Set<String> sdiff(int database, String... keys);
	
	/**
	 * 返回给定所有集合的差集并存储在 destination 中
	 * @param database	数据库索引号
	 * @param dstkey	目标键值
	 * @param keys		键值数组
	 * @return
	 */
	Long sdiffstore(int database, String dstkey, String... keys);
	
	/**
	 * 返回给定所有集合的交集
	 * @param database	数据库索引号
	 * @param keys		键值数组
	 * @return
	 */
	Set<String> sinter(int database, String... keys);
	
	/**
	 * 返回给定所有集合的交集并存储在destination中
	 * @param database	数据库索引号
	 * @param dstKey	目标键值
	 * @param keys		键值数组
	 * @return
	 */
	Long sinterstore(int database, String dstKey, String keys);
	
	/**
	 * 判member元素是否是集合key的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param member	元素值
	 * @return
	 */
	Boolean sismember(int database, String key, String member);

	/**
	 * 返回集合中的所有成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Set<String> smembers(int database, String key);
	
	/**
	 * 将member元素从srckey集合移动到dstkey集合
	 * @param database	数据库索引号
	 * @param srckey	键值
	 * @param dstkey	键值
	 * @param member	成员
	 * @return
	 */
	Long smove(int database, String srckey, String dstkey, String member);

	/**
	 * 移除并返回集合中的一个随机元素
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	String spop(int database, String key);
	
	/**
	 * 返回集合中一个随机数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	String srandmember(int database, String key);
	
	/**
	 * 返回集合中多个随机数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	List<String> srandmember(int database, String key, int count);

	/**
	 * 移除集合中一个或多个成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param members	成员
	 * @return
	 */
	Long srem(int database, String key, String... members);
	
	/**
	 * 返回所有给定集合的并集
	 * @param database	数据库索引号
	 * @param keys		键值
	 * @return
	 */
	Set<String> sunion(int database, String... keys);

	/**
	 * 所有给定集合的并集存储在destination集合中
	 * @param database	数据库索引号
	 * @param dstkey	目标键值
	 * @param keys		给定的键值数组
	 * @return
	 */
	Long sunionstore(int database, String dstkey, String... keys);

	/**
	 * 迭代集合中的元素
	 * @param database	数据库索引号
	 * @param key		目标键值
	 * @param cursor	给定的键值数组
	 * @return
	 */
	ScanResult<String> sscan(int database, String key, String cursor);
	
	/**
	 * 迭代集合中的元素
	 * @param database	数据库索引号
	 * @param key		目标键值
	 * @param cursor	给定的键值数组
	 * @param scanParams pattern和count的复合对象
	 * @return
	 */
	ScanResult<String> sscan(int database, String key, String cursor, ScanParams scanParams);
	
	
	/*-----------------------------有序集合操作----------------------------------*/
	
	
	/**
	 * 向有序集合添加一个或多个成员，或者更新已存在成员的分数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param score		分数
	 * @param member	值
	 * @return
	 */
	Long zadd(int database, String key, double score, String member);
	
	/**
	 * 向有序集合添加一个或多个成员，或者更新已存在成员的分数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param scoreMembers	分数/值
	 * @return
	 */
	Long zadd(int database, String key, Map<String, Double> scoreMembers);
	
	/**
	 * 向有序集合添加一个或多个成员，或者更新已存在成员的分数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param scoreMembers	分数/值
	 * @param params	参数
	 * @return
	 */
	Long zadd(int database, String key, Map<String, Double> scoreMembers, ZAddParams params);

	/**
	 * 获取有序集合的成员数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long zcard(int database, String key);
	
	/**
	 * 计算在有序集合中指定区间分数的成员数
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		分数值开始
	 * @param max		分数值结束
	 * @return
	 */
	Long zcount(int database, String key, double min, double max);
	
	/**
	 * 有序集合中对指定成员的分数加上增量 increment
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param score		分数
	 * @param member	值
	 * @return
	 */
	Double zincrby(int database, String key, double score, String member);
	
	/**
	 * 计算给定的一个或多个有序集的交集并将结果集存储在新的有序集合key中
	 * @param database	数据库索引号
	 * @param dstKey	键值
	 * @param sets		键值
	 * @return
	 */
	Long zinterstore(int database, String dstKey, String... sets);
	
	/**
	 * 计算给定的一个或多个有序集的交集并将结果集存储在新的有序集合key中
	 * @param database	数据库索引号
	 * @param dstKey	键值
	 * @param params	复合对象
	 * @param sets		键值
	 * @return
	 */
	Long zinterstore(int database, String dstKey, ZParams params, String... sets);
	
	/**
	 * 在有序集合中计算指定字典区间内成员数量
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		区间开始
	 * @param max		区间结束
	 * @return
	 */
	Long zlexcount(int database, String key, String min, String max);
	
	/**
	 * 通过索引区间返回有序集合成指定区间内的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param stop		区间结束
	 * @return
	 */
	Set<String> zrange(int database, String key, long start, long stop);
	
	/**
	 * 通过字典区间返回有序集合的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param stop		区间结束
	 * @return
	 */
	Set<String> zrangeByLex(int database, String key, String start, String stop);
	
	/**
	 * 通过字典区间返回有序集合的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param stop		区间结束
	 * @param offset	偏移量（相当于Mysql LIMIT 0,10中的0）
	 * @param count		数量（相当于Mysql LIMIT 0,10中的10）
	 * @return
	 */
	Set<String> zrangeByLex(int database, String key, String start, String stop, int offset, int count);
	
	/**
	 * 通过分数返回有序集合指定区间内的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		分数开始
	 * @param max		分数结束
	 * @return
	 */
	Set<String> zrangeByScore(int database, String key, double min, double max);
	
	/**
	 * 通过分数返回有序集合指定区间内的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		分数开始
	 * @param max		分数结束
	 * @return
	 */
	Set<String> zrangeByScore(int database, String key, String min, String max);
	
	/**
	 * 通过分数返回有序集合指定区间内的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		分数开始
	 * @param max		分数结束
	 * @param offset	偏移量（相当于Mysql LIMIT 0,10中的0）
	 * @param count		数量（相当于Mysql LIMIT 0,10中的10）
	 * @return
	 */
	Set<String> zrangeByScore(int database, String key, double min, double max, int offset, final int count);
	
	/**
	 * 通过分数返回有序集合指定区间内的成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		分数开始
	 * @param max		分数结束
	 * @param offset	偏移量（相当于Mysql LIMIT 0,10中的0）
	 * @param count		数量（相当于Mysql LIMIT 0,10中的10）
	 * @return
	 */
	Set<String> zrangeByScore(int database, String key, String min, String max, int offset, final int count);
	
	/**
	 * 返回有序集合中指定成员的索引(其实就是返回分数)
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param member	成员
	 * @return
	 */
	Long zrank(int database, String key, String member);
	
	/**
	 * 移除有序集合中的一个或多个成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param members	成员
	 * @return
	 */
	Long zrem(int database, String key, String... members);
	
	/**
	 * 移除有序集合中给定的字典区间的所有成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param min		区间开始
	 * @param max		区间结束
	 * @return
	 */
	Long zremrangeByLex(int database, String key, String min, String max);
	
	/**
	 * 移除有序集合中给定的排名区间的所有成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param stop		区间结束
	 * @return
	 */
	Long zremrangeByRank(int database, String key, long start, long stop);
	
	/**
	 * 移除有序集合中给定的分数区间的所有成员
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param end		区间结束
	 * @return
	 */
	Long zremrangeByScore(int database, String key, String start, String end);
	
	/**
	 * 返回有序集中指定区间内的成员，通过索引，分数从高到底
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param end		区间结束
	 * @return
	 */
	Set<String> zrevrange(int database, String key, long start, long end);

	/**
	 * 返回有序集中指定分数区间内的成员，分数从高到低排序
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param max		区间开始
	 * @param min		区间结束
	 * @return
	 */
	Set<String> zrevrangeByScore(int database, String key, double max, double min);
	
	/**
	 * 返回有序集合中指定成员的排名，有序集成员按分数值递减(从大到小)排序
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param member	成员
	 * @return
	 */
	Long zrevrank(int database, String key, String member);
	
	/**
	 * 返回有序集中，成员的分数值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param member	成员
	 * @return
	 */
	Double zscore(int database, String key, String member);
	
	/**
	 * 计算给定的一个或多个有序集的并集，并存储在新的key中
	 * @param database	数据库索引号
	 * @param dstKey	键值
	 * @param sets		键值数组
	 * @return
	 */
	Long zunionstore(int database, String dstKey, String... sets);
	
	/**
	 * 迭代有序集合中的元素（包括元素成员和元素分值）
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param cursor	游标
	 * @return
	 */
	ScanResult<Tuple> zscan(int database, String key, String cursor);
	
	/**
	 * 迭代有序集合中的元素（包括元素成员和元素分值）
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param cursor	游标
	 * @param scanParams 复合对象
	 * @return
	 */
	ScanResult<Tuple> zscan(int database, String key, String cursor, ScanParams scanParams);
	
	
	/*----------------------------通用的列表操作---------------------------------*/
	
	
	/**
	 * 将一个值插入到已存在的列表头部
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 */
	void lpushx(int database, String key, String... value);
	
	/**
	 * 将一个值插入到已存在的列表头部
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 */
	void lpush(int database, String key, String... value);
	
	/**
	 * 将一个值插入到已存在的列表头部
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 */
	void lpush(int database, String key, List<String> value);
	
	/**
	 * 为已存在的列表添加值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 */
	void rpushx(int database, String key, String... value);
	
	/**
	 * 在列表中添加一个或多个值
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param value		值
	 */
	void rpush(int database, String key, String... value);
	
	/**
	 * 返回列表中指定区间内的元素
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param end		区间结束
	 */
	List<String> lrange(int database, String key, int start, int end);
	
	/**
	 * 移出并获取列表的第一个元素， 如果列表没有元素会阻塞列表直到等待超时或发现可弹出元素为止。
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param timeout	超时时间
	 */
	List<String> blpop(int database, String key, int timeout);
	
	/**
	 * 移出并获取列表的最后一个元素， 如果列表没有元素会阻塞列表直到等待超时或发现可弹出元素为止。
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param timeout	超时时间
	 */
	List<String> brpop(int database, String key, int timeout);
	
	/**
	 * 从列表中弹出一个值，将弹出的元素插入到另外一个列表中并返回它； 如果列表没有元素会阻塞列表直到等待超时或发现可弹出元素为止。
	 * @param database	数据库索引号
	 * @param source	源键值
	 * @param destination 目标键值
	 * @param timeout	超时时间
	 * @return
	 */
	String brpoplpush(int database, String source, String destination, int timeout);
	
	/**
	 * 通过索引获取列表中的元素
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param index		索引值（下标）
	 * @return
	 */
	String lindex(int database, String key, int index);
	
//	/**
//	 * 在列表的元素前或者后插入元素
//	 * @param database	数据库索引号
//	 * @param key		键值
//	 * @param where		enum值，指示在前还是后插入
//	 * @param pivot		元素值
//	 * @param value		值
//	 * @return
//	 */
//	Long linsert(int database, String key, LIST_POSITION where, String pivot, String value);
//
	/**
	 * 获取列表长度
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	Long llen(int database, String key);
	
	/**
	 * 移出并获取列表的第一个元素
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	String lpop(int database, String key);
	
	/**
	 * 移除列表元素
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param count		移除的数量(重复的数据量)
	 * @param value		被移出的值
	 * @return
	 */
	Long lrem(int database, String key, Long count, String value);
	
	/**
	 * 通过索引设置列表元素的值	
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param index		索引
	 * @param value		值
	 * @return
	 */
	String lset(int database, String key, Long index, String value);
	
	/**
	 * 对一个列表进行修剪(trim)，就是说，让列表只保留指定区间内的元素，不在指定区间之内的元素都将被删除。
	 * @param database	数据库索引号
	 * @param key		键值
	 * @param start		区间开始
	 * @param stop		区间结束
	 * @return
	 */
	String ltrim(int database, String key, long start, long stop);
	
	/**
	 * 移除并获取列表最后一个元素
	 * @param database	数据库索引号
	 * @param key		键值
	 * @return
	 */
	String rpop(int database, String key);
	
	/**
	 * 移除列表的最后一个元素，并将该元素添加到另一个列表并返回
	 * @param database	数据库索引号
	 * @param source	源键值
	 * @param destination 目标键值
	 * @return
	 */
	String rpoplpush(int database, String source, String destination);
	
	
	/*----------------------------通用的DAO操作---------------------------------*/

	
	/**
	 * 返回一个操作实例
	 * @return
	 */
	Jedis getJeditInstance();
	
	/**
	 * 返回redis全部Key值
	 * @param database	数据库索引号
	 * @return
	 */
	Set<String> getAllKey(int database);
	
	/**
	 * 根据database删除对应数据库全部数据
	 * @param database	数据库索引号
	 */
	void flushdb(int database);
	
	/**
	 * 删除数据库全部数据
	 */
	void flushall();
	
	/**
	 * 关闭
	 * @param jedis
	 */
	void close(Jedis jedis);


	void close();
	
}
