package com.xxs.common.service.cache;

/**
 * 缓存处理对象
 */
public class CacheManager {

    private CacheConfig cacheConfig;

    private CacheHolder cacheHolder;

    public CacheManager(){
        this.cacheConfig = cacheConfig;
        this.cacheHolder = new CacheHolder();
    }

    /**
     * 读取缓存
     * @param region
     * @param key
     * @return
     */
    public CacheObj get(String region,String key){
        /**
         * 请求获取的一个缓存对象
         * TODO 一二级缓存可以在这里设置同步刷新
         */
        CacheObj obj = new CacheObj(region,key,CacheObj.LEVEL_2);
        obj.setValue(cacheHolder.getLevel2Cache(region).get(key));
        if(obj.rawValue() != null) {
            return obj;
        }
        return null;
    }


    /**
     * Write data to Cache
     * @param region
     * @param key
     * @param value
     */
    public void set(String region,String key,Object value){
        L2Cache level2Cache = cacheHolder.getLevel2Cache(region);
        level2Cache.put(key,value);
    }

    /**
     * del data from Cache
     * @param region
     * @param key
     */
    public void del(String region,String key){
        L2Cache level2Cache = cacheHolder.getLevel2Cache(region);
        level2Cache.evict(key);
    }

}
