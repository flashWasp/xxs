package com.xxs.common.service.exception;


import com.xxs.common.service.model.ResultCode;

/**
 * IO异常
 *
 *
 *         2018年3月7日
 */
public class SystemIOException extends SystemException {

	/**
	 *
	 */
	private static final long serialVersionUID = -6584988732793226221L;

	public SystemIOException(String message) {
		super(message);
		this.code = ResultCode.SystemIOException; // IO异常
	}

	public SystemIOException(String message, Throwable cause) {
		super(message, cause);
		this.code = ResultCode.SystemIOException; // IO异常
	}

}
