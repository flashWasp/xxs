package com.xxs.common.service.util.io;


import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ImageUtils {

    /**
     * 裁剪图片
     *
     * @param bili 比例
     * @param x
     * @param y
     * @param w
     * @param h    2017年11月17日 @author Fung
     */

    public static OutputStream cutImage(Double bili, Double x, Double y, Double w, Double h, InputStream is) throws Exception {
        OutputStream os = new ByteArrayOutputStream();
        int x1 = (int) (x * (1 / bili));
        int y1 = (int) (y * (1 / bili));
        int w1 = (int) (w * (1 / bili));
        int h1 = (int) (h * (1 / bili));
        Thumbnails.of(is).sourceRegion(x1, y1, w1, h1).scale(w / h).toOutputStream(os);
        return os;
    }


    /**
     * 压缩图片 --宽高
     *
     * @param quality        压缩质量 0~1
     * @param compressWidth  压缩宽度
     * @param compressHeight 压缩高度
     * @param isKeepRatio    是否保持宽高比
     * @throws Exception 2017年11月17日 @author Fung
     */
    public static OutputStream compressImageForWH(InputStream is, Float quality, Integer compressWidth, Integer compressHeight, Boolean isKeepRatio) throws Exception {
        OutputStream os = new ByteArrayOutputStream();
        Thumbnails.of(is).size(compressWidth, compressHeight).keepAspectRatio(isKeepRatio).outputQuality(1f).toOutputStream(os);
        return os;
    }

    /**
     * 压缩图片  --比例
     *
     * @param quality       图片质量 0~1
     * @param compressScale 压缩比例
     * @throws Exception 2017年11月17日 @author Fung
     */
    public static OutputStream compressImageForScale(InputStream is, Float quality, Float compressScale) throws Exception {
        OutputStream os = new ByteArrayOutputStream();
        // 图片尺寸不变， 压缩文件大小
        Thumbnails.of(is).scale(compressScale).outputQuality(quality).toOutputStream(os);
        return os;
    }

    /**
     * 根据给定宽度压缩图片 --宽高
     *
     * @param
     * @param compressWidth 压缩后的宽度
     * @param isKeepRatio   是否保持宽高比
     * @throws Exception
     */
    public static OutputStream compressImageForWHAccordingW(InputStream is,
                                                    Integer compressWidth, Boolean isKeepRatio, Integer originalWidth, Integer originalHeight) throws Exception {
        OutputStream os = new ByteArrayOutputStream();
        Integer newWidth = originalWidth;
        Integer newHeight = originalHeight;
        if (originalWidth > compressWidth) {
            newWidth = compressWidth;
            newHeight = (int) (new Integer(compressWidth).doubleValue() / originalWidth * originalHeight);
        }
        Thumbnails.of(is).size(newWidth, newHeight).keepAspectRatio(isKeepRatio).toOutputStream(os);
        return os;
    }

    /**
     * 检验图片大小和类型
     *
     * @param img
     * @param photoSize 单位是M
     * @return
     * @throws IOException
     */
    public static Map<String, Object> checkingPhoto(MultipartFile img, Integer photoSize) throws IOException {
        Map<String, Object> map = new HashMap<String, Object>(2);
        map.put(CasterUtilConstant.STR, CasterUtilConstant.STR_FALSE);
        if (!isImage(img.getInputStream())) {
            map.put("message", "不是图片类型");
            return map;
        }
        if (photoSize != null && img.getSize() > photoSize * 1024 * 1024) {
            map.put("message", "图片过大，请尽量别超过2M");
            return map;
        }
        map.put(CasterUtilConstant.STR, CasterUtilConstant.STR_TRUE);
        return map;
    }


    /**
     * 判断是否为图片
     *
     * @param imageFile 图片的文件流
     * @return
     */
    public static boolean isImage(InputStream imageFile) {
        Image img = null;
        try {
            img = ImageIO.read(imageFile);
            if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//
//    /**
//     * 根据原图占用内存大小 或者 指定目标大小压缩图片
//     *
//     * @param srcPath      经过压缩宽高后的图片地址
//     * @param desPath      目标图片地址
//     * @param desFileSize  目标大小，单位kb
//     * @param originalPath 没经过压缩后的源图片地址
//     * @return
//     */
//    public static String compressPicForScale(String srcPath, String desPath,
//                                             Long desFileSize, String originalPath) {
//        if (StringUtils.isEmpty(srcPath) || StringUtils.isEmpty(desPath)) {
//            return null;
//        }
//        if (!new File(originalPath).exists()) {
//            return null;
//        }
//        try {
//            Thumbnails.of(srcPath).scale(1f).toFile(desPath);
//            // 递归压缩，直到目标文件大小小于desFileSize
//            ImageUtils.commpressPicCycle(originalPath, desPath, desFileSize);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//        return desPath;
//    }
//
//    /**
//     * 根据原图内存大小递归缩小宽高
//     *
//     * @param originalImgPath 原图路径
//     * @param desPath         经过压缩宽高之后的路径（也是目标路径）
//     * @param desFileSize     有目标大小优先，没有则按照大小比例
//     * @throws IOException
//     */
//    public static void commpressPicCycle(String originalImgPath, String desPath, Long desFileSize) throws IOException {
//        File srcFileJPG = new File(desPath);
//        long srcFileSizeJPG = srcFileJPG.length();
//        File originalImgFileJPG = new File(originalImgPath);
//        long originalSizeImg = originalImgFileJPG.length();
//        double accuracy = 0.8;
//        if (desFileSize != null && desFileSize > 1L) {
//            if (srcFileSizeJPG <= desFileSize * 1024) {
//                return;
//            }
//        } else {
//            /*
//             * 原图内存大小  > 3M , 压缩宽高后临界大小为 800kb  —— 压缩宽高后小于800kb， 不压缩；压缩图片大于800kb，则按0.9的比例递归;
//             * 1M < 原图内存大小  < 3M , 压缩宽高后临界大小为 500kb（同上）;
//             * 原图内存大小  < 1M , 压缩宽高后临界大小为  300kb;
//             */
//            if (originalSizeImg > 1025 * 1024 * 3) {
//                if (srcFileSizeJPG <= 801 * 1024) {
//                    return;
//                }
//            } else if (originalSizeImg > 1025 * 1024) {
//                if (srcFileSizeJPG <= 501 * 1024) {
//                    return;
//                } else {
//                    accuracy = 0.9;
//                }
//            } else {
//                if (srcFileSizeJPG <= 301 * 1024) {
//                    return;
//                } else {
//                    accuracy = 0.95;
//                }
//            }
//        }
//        // 计算宽高
//        BufferedImage bim = ImageIO.read(srcFileJPG);
//        int srcWdith = bim.getWidth();
//        int srcHeigth = bim.getHeight();
//        int desWidth = new BigDecimal(srcWdith).multiply(new BigDecimal(accuracy)).intValue();
//        int desHeight = new BigDecimal(srcHeigth).multiply(new BigDecimal(accuracy)).intValue();
//        Thumbnails.of(desPath).size(desWidth, desHeight).keepAspectRatio(true).toFile(desPath);
//        ImageUtils.commpressPicCycle(originalImgPath, desPath, desFileSize);
//    }


}
