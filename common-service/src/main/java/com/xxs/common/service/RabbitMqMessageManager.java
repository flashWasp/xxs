package com.xxs.common.service;

import com.alibaba.fastjson.JSONObject;
import com.xxs.common.service.constant.MQConstants;
import com.xxs.common.service.model.RabbitMetaMessage;
import com.xxs.common.service.model.template.MailTemplate;
import com.xxs.common.service.model.template.MsgTemplate;

public class RabbitMqMessageManager {

    /**
     * 获取邮件信息对象
     * @param mailTemplate
     * @return
     */
   public static RabbitMetaMessage newMailMessageInstance(MailTemplate mailTemplate){
       /** 生成一个发送对象 */
       RabbitMetaMessage rabbitMetaMessage = new RabbitMetaMessage();
       /**设置交换机 */
       rabbitMetaMessage.setExchange(MQConstants.EMAIL_EXCHANGE);
       /**指定routing key */
       rabbitMetaMessage.setRoutingKey(MQConstants.EMAIL_KEY);
       String json = JSONObject.toJSONString(mailTemplate);
       rabbitMetaMessage.setPayload(json);
       return rabbitMetaMessage;
   }

    /**
     * 获取信息对象
     * @param msgTemplate
     * @return
     */
   public static RabbitMetaMessage newMsgMessageInstance(MsgTemplate msgTemplate){
       /** 生成一个发送对象 */
       RabbitMetaMessage rabbitMetaMessage = new RabbitMetaMessage();
       /**设置交换机 */
       rabbitMetaMessage.setExchange(MQConstants.MSG_EXCHANGE);
       /**指定routing key */
       rabbitMetaMessage.setRoutingKey(MQConstants.MSG_KEY);
       String json = JSONObject.toJSONString(msgTemplate);
       rabbitMetaMessage.setPayload(json);
       return rabbitMetaMessage;
   }
}
