package com.xxs.common.service.util;

import com.xxs.common.service.exception.ServiceException;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {

    public static Map<String, Object> whatDayENMap = new HashMap<>();
    public static Map<Object, Object> whatDayNumMap = new HashMap<>();
    static {
        whatDayENMap.put("星期日", "Sunday");
        whatDayENMap.put("星期一", "Monday");
        whatDayENMap.put("星期二", "Tuesday");
        whatDayENMap.put("星期三", "Wednesday");
        whatDayENMap.put("星期四", "Thursday");
        whatDayENMap.put("星期五", "Friday");
        whatDayENMap.put("星期六", "Saturday");

        whatDayNumMap.put("星期日", 7);
        whatDayNumMap.put("星期一", 1);
        whatDayNumMap.put("星期二", 2);
        whatDayNumMap.put("星期三", 3);
        whatDayNumMap.put("星期四", 4);
        whatDayNumMap.put("星期五", 5);
        whatDayNumMap.put("星期六", 6);
    }

    /**
     * yyyy年MM月dd日
     */
    public static String YYYYMMDD_CN = "yyyy年MM月dd日";

    /**
     * MM月dd日
     */
    public static String MMDD_CN = "MM月dd日";

    /**
     * dd号
     */
    public static String DD_CN = "dd号";

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static String YYYYMMDDHHMMSS_ = "yyyy-MM-dd HH:mm:ss";

    /**
     * yyyy-MM-dd
     */
    public static String YYYYMMDD_ = "yyyy-MM-dd";

    /**
     * yyyy-MM-dd HH:mm
     */
    public static String YYYYMMDDHHMM_ = "yyyy-MM-dd HH:mm";

    /**
     * yyyy-MM
     */
    public static String YYYYMM_ = "yyyy-MM";

    /**
     * yyyy-MM-dd HH
     */
    public static String YYYYMMDDHH_ = "yyyy-MM-dd HH";

    /**
     * yyyyMMddHHmmss
     */
    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    /**
     * yyyyMMddHHmm
     */
    public static String YYYYMMDDHHMM = "yyyyMMddHHmm";

    /**
     * yyyyMMdd
     */
    public static String YYYYMMDD = "yyyyMMdd";

    /**
     * yyyy
     */
    public static String YYYY = "yyyy";

    /**
     * MM
     */
    public static String MM = "MM";

    /**
     * dd
     */
    public static String DD = "dd";

    /**
     * mm
     */
    public static String mm = "mm";

    /**
     * ss
     */
    public static String ss = "ss";

    /**
     * date转为字符串
     *
     * @param date 日期对象
     * @param pattern 匹配模式
     * @return
     *
     * @author fung
     * @date 2018年6月28日
     */
    public static String format(Date date, String pattern) {
        return DateFormatUtils.format(date, pattern);
    }

    /**
     * 字符串转为Date </br>
     * 系统默认按以下的格式转换</br>
     * yyyy-MM-dd HH:mm:ss</br>
     * yyyy-MM-dd</br>
     * yyyy-MM</br>
     * yyyyMMddHHmmss</br>
     * yyyyMMdd</br>
     * yyyyMM</br>
     * yyyy/MM/dd HH:mm:ss</br>
     * yyyy/MM/dd</br>
     * yyyy/MM</br>
     *
     * @param str 字符串
     * @return
     *
     * @author fung
     * @date 2018年6月28日
     */
    public static Date parse(String str) {
        try {
            return DateUtils.parseDate(str, new String[] { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "yyyy-MM", "yyyyMMddHHmmss", "yyyyMMdd", "yyyyMM",
                    "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd", "yyyy/MM" });
        } catch (ParseException e) {
            throw new ServiceException(UtilMsg.DATE_PARSE_ERROR);
        }
    }

    /**
     * 字符串转为Date
     *
     * @param str 日期字符串
     * @param parttern 匹配模式
     * @return
     *
     * @author fung
     * @date 2018年6月28日
     */
    public static Date parse(String str, String parttern) {
        String[] partterns = new String[1];
        partterns[0] = parttern;
        try {
            return DateUtils.parseDate(str, partterns);
        } catch (ParseException e) {
            throw new ServiceException(UtilMsg.DATE_PARSE_ERROR);
        }
    }

    /**
     * 得到两个日期相差的天数
     *
     * @param date1
     * @param date2
     * @return int
     * @throw
     * @Date 2018/06/28
     */
    public static int getBetweenDay(Date date1, Date date2) {
        return Integer.parseInt(DurationFormatUtils.formatPeriod(date1.getTime(), date2.getTime(), "d"));
    }

    /**
     * 获取当前时间（yyyy-MM-dd HH:mm:ss）
     *
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String getCurrentTimeForYYYYMMDDHHMMSS_() {
        return format(new Date(), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 获取当前时间(yyyy-MM-dd HH:mm）
     *
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String getCurrentTimeForYYYYMMDDHHMM_() {
        return format(new Date(), DateUtil.YYYYMMDDHHMM_);
    }

    /**
     * 获取当前时间（yyyy-MM-dd）
     *
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String getCurrentTimeForYYYYMMDD_() {
        return format(new Date(), DateUtil.YYYYMMDD_);
    }

    /**
     * 毫秒:添加 删减操作<br>
     *
     * @param date 时间 正数:添加 负数:删减
     * @param
     * @param parttern 匹配模式
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addMilliseconds(Date date, int milliSeconds, String parttern) {
        return format(DateUtils.addMilliseconds(date, milliSeconds), parttern);
    }

    /**
     * 毫秒:添加 删减操作<br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间 正数:添加 负数:删减
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addMilliseconds(Date date, int milliSeconds) {
        return format(DateUtils.addMilliseconds(date, milliSeconds), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 秒:添加 删减操作<br>
     *
     * @param date 时间 正数:添加 负数:删减
     * @param seconds 秒
     * @param parttern 匹配模式
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addSeconds(Date date, int seconds, String parttern) {
        return format(DateUtils.addSeconds(date, seconds), parttern);
    }

    /**
     * 秒:添加 删减操作<br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间 正数:添加 负数:删减
     * @param seconds 秒
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addSeconds(Date date, int seconds) {
        return format(DateUtils.addSeconds(date, seconds), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 分钟:添加 删减操作 <br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间
     * @param minutes 分钟
     * @param parttern 匹配模式
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addMinutes(Date date, int minutes, String parttern) {
        return format(DateUtils.addMinutes(date, minutes), parttern);
    }

    /**
     * 分钟:添加 删减操作
     *
     * @param date 时间
     * @param minutes 分钟
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addMinutes(Date date, int minutes) {
        return format(DateUtils.addMinutes(date, minutes), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 分钟:添加 删减操作 <br>
     *
     * @param date 时间
     * @param  分钟
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addHours(Date date, int hours, String parttern) {
        return format(DateUtils.addHours(date, hours), parttern);
    }

    /**
     * 分钟:添加 删减操作 <br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addHours(Date date, int hours) {
        return format(DateUtils.addHours(date, hours), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 天:添加 删减操作 <br>
     *
     * @param date 时间
     * @param days 天数
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addDays(Date date, int days, String parttern) {
        return format(DateUtils.addDays(date, days), parttern);
    }

    /**
     * 天数:添加 删减操作 <br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间
     * @param days 天数
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addDays(Date date, int days) {
        return format(DateUtils.addDays(date, days), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 周:添加 删减操作 <br>
     *
     * @param date 时间
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addWeeks(Date date, int weeks, String parttern) {
        return format(DateUtils.addWeeks(date, weeks), parttern);
    }

    /**
     * 周:添加 删减操作 <br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addWeeks(Date date, int weeks) {
        return format(DateUtils.addWeeks(date, weeks), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 分钟:添加 删减操作 <br>
     *
     * @param
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addMonths(Date date, int months, String parttern) {
        return format(DateUtils.addDays(date, months), parttern);
    }

    /**
     * 月:添加 删减操作 <br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间
     * @param
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addMonths(Date date, int months) {
        return format(DateUtils.addDays(date, months), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 年:添加 删减操作 <br>
     *
     * @param date 时间
     * @param years 年
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addYears(Date date, int years, String parttern) {
        return format(DateUtils.addDays(date, years), parttern);
    }

    /**
     * 年:添加 删减操作 <br>
     * 默认模式(yyyy-MM-dd HH:mm:ss)
     *
     * @param date 时间
     * @param years 年
     * @return java.lang.String
     * @throw
     * @Date 2018/06/28
     */
    public static String addYears(Date date, int years) {
        return format(DateUtils.addDays(date, years), DateUtil.YYYYMMDDHHMMSS_);
    }

    /**
     * 比较两个日期的大小（=:0;<:-1;>1）
     *
     * @param
     * @param
     * @return
     * @throws
     * @Date 2018/06/28
     */
    public static int compareDate(Date date1, Date date2) {
        return date1.compareTo(date2);
    }

    /**
     * Java将Unix时间戳转换成指定格式日期字符串
     *
     * @param
     *
     * @param
     *
     *
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static String UnixTimeStamp2Date(String timestampStr, String parttern) {
        Long timestamp = Long.parseLong(timestampStr) * 1000;
        String date = new SimpleDateFormat(parttern, Locale.CHINA).format(new Date(timestamp));
        return date;
    }

    /**
     * 返回年份
     *
     * @param date
     * @return
     */
    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.CHINA);
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 返回月
     *
     * @param date
     * @return
     */
    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.CHINA);
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 返回的是本月的第几天
     *
     * @param date
     * @return
     */
    public static long getDayFromMonth(Date date) {
        return DateUtils.getFragmentInDays(date, Calendar.MONTH);
    }

    /**
     * 返回的是本年的第几天
     *
     * @param date
     * @return
     */
    public static long getDayFromYear(Date date) {
        return DateUtils.getFragmentInDays(date, Calendar.YEAR);
    }

    /**
     * 返回的是本日的第几个小时
     *
     * @param date
     * @return
     */
    public static long getHourFromDay(Date date) {
        return DateUtils.getFragmentInHours(date, Calendar.DATE);
    }

    /**
     * 根据日期返回星期几(中文)
     * eg. 星期一 星期二
     *
     * @param date
     * @return
     *
     * @author fung
     * @date 2018年6月29日
     */
    public static String getWhatday2ZH(Date date) {
        return DateFormatUtils.format(date, "EEEE");
    }

    /**
     * 根据日期返回星期几(英文)
     * eg. Sunday Monday
     *
     * @param date
     * @return
     *
     * @author fung
     * @date 2018年6月29日
     */
    public static String getWhatday2EN(Date date) {
        return whatDayNumMap.get(getWhatday2ZH(date)).toString();
    }

    /**
     * 根据日期返回星期几(数字)
     * eg. 1 2 3
     *
     * @param date
     * @return
     *
     * @author fung
     * @date 2018年6月29日
     */
    public static int getWhatday2Num(Date date) {
        return (int) (whatDayENMap.get(getWhatday2ZH(date)));
    }

    class UtilMsg {
        public static final String DATE_PARSE_ERROR = "日期分析错误";
    }

}
