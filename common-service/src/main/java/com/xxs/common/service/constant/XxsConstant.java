package com.xxs.common.service.constant;

public class XxsConstant {
    /**
     * 正常状态
     */
    public final static String XXS_STATE_ISNORMAL = "isNormal";

    /**
     * 被删除状态
     */
    public final static String XXS_STATE_ISDELETE = "isDelete";

}
