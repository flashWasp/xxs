package com.xxs.common.service.util;

import java.util.Collection;

/**
 * 匹配工具类
 */
public class MatchUtil {

    /**
     * The constant STRING_NULL.
     */
    private final static String STRING_NULL = "-";
    /**
     * 匹配手机号码, 支持+86和86开头
     */
    private static final String REGX_MOBILENUM = "^((\\+86)|(86))?(13|15|17|18)\\d{9}$";

    /**
     * 匹配邮箱帐号
     */
    private static final String REGX_EMAIL = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

    /**
     * 匹配手机号码（先支持13, 15, 17, 18开头的手机号码）.
     *
     * @param inputStr the input str
     * @return the boolean
     */
    public static Boolean isMobileNumber(String inputStr) {
        return !isNull(inputStr) && inputStr.matches(REGX_MOBILENUM);
    }

    /**
     * 判断一个或多个对象是否为空
     *
     * @param values 可变参数, 要判断的一个或多个对象
     * @return 只有要判断的一个对象都为空则返回true, 否则返回false boolean
     */
    public static boolean isNull(Object... values) {
        if (!isNotNullAndNotEmpty(values)) {
            return true;
        }
        for (Object value : values) {
            if (value instanceof Object[]) {
                return !isNotNullAndNotEmpty((Object[]) value);
            }
            if (value instanceof Collection<?>) {
                return !isNotNullAndNotEmpty((Collection<?>) value);
            }
            if (value instanceof String) {
                return isOEmptyOrNull(value);
            }
            return (null == value);
        }
        return false;
    }

    /**
     * Is o empty or null boolean.
     *
     * @param o the o
     * @return boolean boolean
     */
    private static boolean isOEmptyOrNull(Object o) {
        return o == null || isSEmptyOrNull(o.toString());
    }

    /**
     * Is s empty or null boolean.
     *
     * @param s the s
     * @return boolean boolean
     */
    private static boolean isSEmptyOrNull(String s) {
        return trimAndNullAsEmpty(s).length() <= 0;
    }

    /**
     * Trim and null as empty string.
     *
     * @param s the s
     * @return java.lang.String string
     */
    private static String trimAndNullAsEmpty(String s) {
        if (s != null && !s.trim().equals(STRING_NULL)) {
            return s.trim();
        }
            return "";
        // return s == null ? "" : s.trim();
    }

    /**
     * 判断对象数组是否为空并且数量大于0
     *
     * @param value the value
     * @return boolean
     */
    private static Boolean isNotNullAndNotEmpty(Object[] value) {
        if (null != value && 0 < value.length) {
           return true;
        }
        return false;
    }

    /**
     * 判断对象集合（List,Set）是否为空并且数量大于0
     *
     * @param value the value
     * @return boolean
     */
    private static Boolean isNotNullAndNotEmpty(Collection<?> value) {
        if (null != value && !value.isEmpty()) {
            return  true;
        }
        return false;
    }

    /**
     * Is email boolean.
     *
     * @param str the str
     * @return the boolean
     */
    public static boolean isEmail(String str) {
        if (isSEmptyOrNull(str) || !str.matches(REGX_EMAIL)) {
           return false;
        }
        return true;
    }

}
