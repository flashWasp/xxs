package com.xxs.common.service.cache.redis;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.*;
import redis.clients.jedis.params.sortedset.ZAddParams;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class RedisServiceImpl implements RedisService{

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisServiceImpl.class);

	/*----------------------------String数据类型的DAO操作---------------------------------*/


	@Override
	public void set(int database,byte[] key, byte[] value) {
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(0);
			jedis.set(key,value);
			LOGGER.info("Redis set : {key :" + new String(key) +"}"  );
		} catch (Exception e) {

		} finally{
			close(jedis);
		}

	}

	@Override
	public byte[] get(int database,byte[] key) {
		Jedis jedis = null;
	    try{
			jedis = getJeditInstance();
			jedis.select(database);
			return jedis.get(key);
		}catch (Exception e){
	    	return null;
		}finally {
	    	close(jedis);
		}
	}

	@Override
	public void del(int database, String... key) {
		Jedis jedis = null;
		try{
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.del(key);
			LOGGER.info("Redis del : {key :" + new String(key[1]) +"}"  );
		}catch (Exception e){

		}finally {
			close(jedis);
		}

	}


	@Override
	public void set(int database,String key,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.set(key,value);
		} catch (Exception e) {

		} finally{
			close(jedis);
		}
	}

	@Override
	public String get(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String value = jedis.get(key);
			return value;
		} catch (Exception e) {
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String getrange(int database,String key,long start,long end){

		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.getrange(key, start, end);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}

	}

	@Override
	public String getset(int database,String key,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.getSet(key, value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Boolean getbit(int database, String key, long offset) {
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Boolean data = jedis.getbit(key, offset);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> mget(int database, String... keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.mget(keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Boolean setbit(int database,String key,long offset,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Boolean data = jedis.setbit(key, offset, value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String setex(int database,String key,int seconds,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.setex(key, seconds, value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long setnx(int database,String key,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.setnx(key,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long setrange(int database,String key,long offset,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.setrange(key, offset, value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long strlen(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.strlen(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String mset(int database,String... keysvalues){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.mset(keysvalues);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long msetnx(int database,String... keysvalues){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.msetnx(keysvalues);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}
	@SuppressWarnings("deprecation")
	@Override
	public String psetex(int database,String key,int milliseconds,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.psetex(key,milliseconds,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long incr(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.incr(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long incrby(int database,String key,long increment){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.incrBy(key,increment);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Double incrByFloat(int database,String key,double value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Double data = jedis.incrByFloat(key,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long decr(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.decr(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long decrby(int database,String key,long decrement){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.decrBy(key,decrement);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long decrby(int database,String key,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.append(key,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public void del(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.del(key);
			LOGGER.info("Redis del : {key :" + key +"}"  );
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}


	/*----------------------------字典数据类型的DAO操作---------------------------------*/

	@Override
	public void hmset(int database,String key, Map<String,String> hash) {
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.hmset(key, hash);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public String hget(int database,String key,String field){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String value = jedis.hget(key, field);
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long hdel(int database,String key,String... fields){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long value = jedis.hdel(key,fields);
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Boolean hexists(int database,String key,String field){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Boolean value = jedis.hexists(key,field);
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Map<String, String> hgetall(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Map<String, String> data = jedis.hgetAll(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long hincrby(int database,String key,String field,long increment){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.hincrBy(key, field, increment);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Double hincrByFloat(int database,String key,String field,double increment){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Double data = jedis.hincrByFloat(key, field, increment);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> hkeys(int database, String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.hkeys(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long hlen(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.hlen(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> hmget(int database,String key,String... fields){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.hmget(key,fields);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long hset(int database,String key,String field,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.hset(key,field,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long hsetnx(int database,String key,String field,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.hsetnx(key,field,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> hvals(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.hvals(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public ScanResult<Map.Entry<String, String>> hscan(int database, String key, int cursor){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			ScanResult<Map.Entry<String, String>> data = jedis.hscan(key,cursor);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	/*----------------------------通用的列表操作---------------------------------*/


	@Override
	public void lpushx(int database,String key,String... value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.lpushx(key,value);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public void lpush(int database,String key,String... value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.lpush(key,value);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public void lpush(int database,String key,List<String> value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			for(String val:value){
				jedis.lpush(key,val);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public void rpushx(int database,String key,String... value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.rpushx(key,value);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public void rpush(int database,String key,String... value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.rpush(key,value);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> lrange(int database,String key,int start,int end){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.lrange(key, start, end);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> blpop(int database,String key,int timeout){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.blpop(timeout, key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> brpop(int database,String key,int timeout){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.brpop(timeout, key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String brpoplpush(int database,String source,String destination,int timeout){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.brpoplpush(source, destination,timeout);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String lindex(int database,String key,int index){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.lindex(key,index);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

//	@Override
//	public Long linsert(int database,String key,LIST_POSITION where,String pivot,String value){
//		Jedis jedis = null;
//		try {
//			jedis = getJeditInstance();
//			jedis.select(database);
//			Long data = jedis.linsert(key, LIST_POSITION.BEFORE, pivot, value);
//			return data;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		} finally{
//			close(jedis);
//		}
//	}

	@Override
	public Long llen(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.llen(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String lpop(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.lpop(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long lrem(int database,String key,Long count,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.lrem(key,count,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String lset(int database,String key,Long index,String value){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.lset(key,index,value);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String ltrim(int database,String key,long start,long stop){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.ltrim(key,start,stop);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String rpop(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.rpop(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String rpoplpush(int database,String source,String destination){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.rpoplpush(source,destination);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}


	/*------------------------------集合操作-----------------------------------*/

	@Override
	public Long sadd(int database,String key,String... members){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.sadd(key,members);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long scard(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.scard(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> sdiff(int database,String... keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.sdiff(keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long sdiffstore(int database,String dstkey,String... keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.sdiffstore(dstkey,keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> sinter(int database,String... keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.sinter(keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long sinterstore(int database,String dstKey,String keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.sinterstore(dstKey,keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Boolean sismember(int database,String key,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Boolean data = jedis.sismember(key, member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> smembers(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.smembers(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long smove(int database,String srckey,String dstkey,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.smove(srckey, dstkey, member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String spop(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.spop(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public String srandmember(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			String data = jedis.srandmember(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public List<String> srandmember(int database,String key,int count){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			List<String> data = jedis.srandmember(key,count);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long srem(int database,String key,String... members){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.srem(key,members);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> sunion(int database,String... keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.sunion(keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long sunionstore(int database,String dstkey,String... keys){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.sunionstore(dstkey,keys);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public ScanResult<String> sscan(int database,String key,String cursor){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			ScanResult<String> data = jedis.sscan(key,cursor);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public ScanResult<String> sscan(int database,String key,String cursor,ScanParams scanParams){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			ScanResult<String> data = jedis.sscan(key,cursor,scanParams);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}


	/*-----------------------------有序集合操作----------------------------------*/


	@Override
	public Long zadd(int database,String key,double score,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zadd(key, score, member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zadd(int database,String key,Map<String, Double> scoreMembers){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zadd(key,scoreMembers);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zadd(int database,String key,Map<String, Double> scoreMembers,ZAddParams params){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zadd(key,scoreMembers,params);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}


	@Override
	public Long zcard(int database,String key){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zcard(key);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zcount(int database,String key,double min,double max){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zcount(key,min,max);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Double zincrby(int database,String key,double score,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Double data = jedis.zincrby(key,score,member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zinterstore(int database,String dstKey,String... sets){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zinterstore(dstKey,sets);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zinterstore(int database, String dstKey, ZParams params, String... sets){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zinterstore(dstKey,params,sets);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zlexcount(int database,String key,String min,String max){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zlexcount(key,min,max);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrange(int database,String key,long start,long stop){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrange(key,start,stop);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrangeByLex(int database,String key,String start,String stop){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrangeByLex(key,start,stop);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrangeByLex(int database,String key,String start,String stop,int offset,int count){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrangeByLex(key,start,stop,offset,count);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrangeByScore(int database,String key,double min,double max){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrangeByScore(key,min,max);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrangeByScore(int database,String key,String min,String max){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrangeByScore(key,min,max);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrangeByScore(int database,String key,double min,double max,int offset,int count){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrangeByScore(key,min,max,offset,count);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrangeByScore(int database,String key,String min,String max,int offset,int count){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrangeByScore(key,min,max,offset,count);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zrank(int database,String key,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zrank(key,member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zrem(int database,String key,String... members){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zrem(key,members);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zremrangeByLex(int database,String key,String min,String max){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zremrangeByLex(key,min,max);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zremrangeByRank(int database,String key,long start,long stop){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zremrangeByRank(key,start,stop);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zremrangeByScore(int database,String key,String start,String end){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zremrangeByScore(key,start,end);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrevrange(int database,String key,long start,long end){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrevrange(key,start,end);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Set<String> zrevrangeByScore(int database,String key,double max,double min){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> data = jedis.zrevrangeByScore(key,max,min);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zrevrank(int database,String key,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zrevrank(key,member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Double zscore(int database,String key,String member){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Double data = jedis.zscore(key,member);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public Long zunionstore(int database,String dstKey,String... sets){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Long data = jedis.zunionstore(dstKey,sets);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public ScanResult<Tuple> zscan(int database,String key,String cursor){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			ScanResult<Tuple> data = jedis.zscan(key,cursor);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public ScanResult<Tuple> zscan(int database,String key,String cursor,ScanParams scanParams){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			ScanResult<Tuple> data = jedis.zscan(key,cursor,scanParams);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	/*----------------------------通用的DAO操作---------------------------------*/

	@Override
	public Jedis getJeditInstance(){
//		ApplicationContext applicationContext = Lifecycle.getApplicationContext();
//		JedisPool pool = (JedisPool) applicationContext.getBean("jedisPool");
        JedisPool pool = null;
        if(pool == null){
            pool = getJedisPool();
        }
		Jedis jedis = pool.getResource();

		return jedis;
	}

	@Override
	public Set<String> getAllKey(int database){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			Set<String> value = jedis.keys("*");
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally{
			close(jedis);
		}
	}

	@Override
	public void flushdb(int database){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.select(database);
			jedis.flushDB();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public void flushall(){
		Jedis jedis = null;
		try {
			jedis = getJeditInstance();
			jedis.flushAll();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			close(jedis);
		}
	}

	@Override
	public void close(Jedis jedis){
		jedis.close();
	}

	@Override
	public void close() {
		getJeditInstance().close();
	}


	public JedisPool getJedisPool(){
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        config.setMaxIdle(100);
        config.setMinIdle(5);
        config.setMaxWaitMillis(100);
        config.setTestOnBorrow(true);
        config.setTestOnReturn(true);
        config.setTestWhileIdle(true);
        String host = "127.0.0.1";
        String port = "6379";
        String timeOut = "0";

        return new JedisPool(config, host,6379,0);

	}

}
