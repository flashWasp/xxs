package com.xxs.common.service.constant;

/**
 * Author : Fung
 * Date : 2018/12/4 0004 下午 12:24
 * Desc : MQ常量配置
 */
public class MQConstants {

    /**邮件交换机名称*/
    public static final String EMAIL_EXCHANGE = "email.exchange";
    /**邮件队列名称*/
    public static final String EMAIL_QUEUE = "email.queue";
    /**邮件业务key*/
    public static final String EMAIL_KEY = "email.key";

    /**
     * 消息交换器
     */
    public static final String MSG_EXCHANGE ="msg.exchange";

    /**
     * 消息队列
     */
    public static final String MSG_QUEUE = "msg.queue";

    /**
     * 消息Key
     */
    public static final String MSG_KEY = "msg_key";

    /**
     * 生产者 重试key
     */
    public static final String MQ_PRODUCER_RETRY_KEY = "mq.producer.retry.key";

    /**
     * 消费者 统计重试key
     */
    public static final String MQ_CONSUMER_RETRY_COUNT_KEY = "mq.consumer.retry.count.key";


    /**死信队列配置*/
    public static final String DLX_EXCHANGE = "dlx.exchange";
    public static final String DLX_QUEUE = "dlx.queue";
    public static final String DLX_ROUTING_KEY = "dlx.routing.key";
}
