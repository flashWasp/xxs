package com.xxs.common.service.util;

import java.util.UUID;

/**
 * 生成唯一主键工具类
 * @author fung
 *
 */
public class KeyUtil {
    /**
     * 生成唯一的主键
     * 格式: 时间+UUID
     * @return
     */
    public static synchronized String getUniqueKey(){
        return System.currentTimeMillis() + UUID.randomUUID().toString().replace("-", "");
    }
}
