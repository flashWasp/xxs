package com.xxs.common.service.system;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/** 系统配置类 */
public class SystemConfig {
  /** Sys properties (msg.properties). */
  private static final Properties SYS_PROPS = new Properties();

  private SystemConfig() {}

  static {
    final InputStream systemConfigStream =
        SystemConfig.class.getResourceAsStream("/xxs/xxs-dev.properties");
    if (null != systemConfigStream) {
      try {
        SYS_PROPS.load(systemConfigStream);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
  /**
   * 获取配置文件 根据key获取value
   *
   * @param key
   * @return
   */
  public static String getValue(String key) {
    if (StringUtils.isNotBlank(key)) {
      return SYS_PROPS.getProperty(key);
    }
    return null;
  }
}
