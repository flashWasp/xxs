package com.xxs.common.service.cache.manager.request;

import com.xxs.common.service.cache.CacheFactory;
import com.xxs.common.service.cache.CacheManager;
import com.xxs.common.service.cache.CacheObj;

/**
 * @author Administrator
 * @Title: DelCacheRequest
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/248:34
 */
public class DelCacheRequest implements CacheRequest {
    /**
     * id key
     */
    private String id;

    /**
     * 域
     */
    private String region;

    public DelCacheRequest(String region, String id) {
        this.id = id;
        this.region = region;
    }

    @Override
    public CacheObj process() {
        // 删除就有缓存
        CacheFactory cacheFactory = new CacheFactory();
        CacheManager cacheManager = cacheFactory.newCacheManager();
        cacheManager.del(region, id);
        return null;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRegion() {
        return region;
    }
}
