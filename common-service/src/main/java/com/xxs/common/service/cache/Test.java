package com.xxs.common.service.cache;

import com.xxs.common.service.cache.manager.CacheRequestManager;
import com.xxs.common.service.cache.manager.request.UpdateCacheRequest;
import com.xxs.common.service.cache.manager.thread.RequestProcessorThreadPool;

import java.util.Date;
import java.util.Random;

public class Test {

    private Date after3;

    public static void main(String[] args) throws InterruptedException {
//        CacheFactory cacheFactory = new CacheFactory();
//
//
//        System.out.println();
//        CacheManager cacheManager = cacheFactory.newCacheManager();
//        cacheManager.set("user","123","123");
//        Date before = new Date();
//        CacheObj obj = cacheManager.get("user", "123");
//        Object value = obj.getValue();
//        System.out.println(after.getTime()-before.getTime());
//         初始化工作线程池和内存队列

//        CacheFactory cacheFactory = new CacheFactory();
//        CacheManager cacheManager = cacheFactory.newCacheManager();
//
//
//
//
//        //System.out.println(obj.getValue());

        //  不启动cache框架机制时间()
//        Date before1 = new Date();
//        CacheFactory cacheFactory = new CacheFactory();
//        CacheManager cacheManager = cacheFactory.newCacheManager();
//        cacheManager.set("user","556","123");
//        Date after1 = new Date();
//        System.out.println("不启动ITcache机制进行更新 花费时间:  " + (after1.getTime()-before1.getTime()) + "ms");
//
        // 启动框架机制时间()
        Date before2 = new Date();
        RequestProcessorThreadPool.init();
        UpdateCacheRequest user = new UpdateCacheRequest("user", "243", "12367");
        CacheRequestManager cacheRequestManager = new CacheRequestManager(user);
        cacheRequestManager.update();
        Date after2 = new Date();
        System.out.println("启动ITcache机制进行更新 花费时间:  " + (after2.getTime()-before2.getTime()) + "ms");

//
//
//        Date before3 = new Date();
//        CacheObj obj = cacheManager.get("user", "12345");
//        Date after3 = new Date();
//        System.out.println("不启动ITcache机制进行查询 花费时间:  " + (after3.getTime()-before3.getTime()) + "ms");
//
//        Date before4 = new Date();
//        QueryCacheRequest user1 = new QueryCacheRequest("user", 123L);
//        CacheRequestManager cacheRequestManager2 = new CacheRequestManager(user1);
//        CacheObj read = cacheRequestManager2.read(new UserRoleRepositoryImpl());
//        System.out.println(read.getValue());
//        Date after4 = new Date();
//        System.out.println("启动ITcache机制进行查询 花费时间:  " + (after4.getTime()-before4.getTime()) + "ms");

//        CacheFactory cacheFactory = new CacheFactory();
//        CacheManager cacheManager = cacheFactory.newCacheManager();
//        RequestProcessorThreadPool.init();
//
//        Thread updateTread = new UpdateTread();
//        updateTread.start();

//        for (int i = 0; i < 1000; i++) {
//            Thread testReadThread = new TestReadThread();
//            testReadThread.start();
//        }
//    }
//
//class TestThread extends  Thread{
//        new FlowTaskServiceImpl();
//    @Override
//    public void run() {
//
//    }
//}


    }

//    }
//
//
//}
//
static class UpdateTread extends  Thread{

    @Override
    public void run() {
        Date before2 = new Date();
        Random random = new Random();
        int i = random.nextInt();
        UpdateCacheRequest user = new UpdateCacheRequest("user", "231", i + "");
        CacheRequestManager cacheRequestManager = new CacheRequestManager(user);
        cacheRequestManager.update();
        Date after2 = new Date();
        System.out.println("启动Fcache框架机制进行更新 花费时间:  " + (after2.getTime()-before2.getTime()) + "ms,值是" + i  );
    }
}
//
//class TestReadThread extends Thread{
//    @Override
//    public void run() {
//        Date before4 = new Date();
//        QueryCacheRequest user1 = new QueryCacheRequest("user", 123L);
//        CacheRequestManager cacheRequestManager2 = new CacheRequestManager(user1);
//        CacheObj read = cacheRequestManager2.read(new UserRoleRepositoryImpl());
//        System.out.println(read.getValue());
//        Date after4 = new Date();
//        System.out.println(new AtomicInteger().incrementAndGet());
//        System.out.println("启动Fcache框架机制进行查询 花费时间:  " + (after4.getTime()-before4.getTime()) + "ms");
//    }
}
