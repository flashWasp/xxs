package com.xxs.common.service.util.io;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 简单的图片工具处理类  头像、封面等
 * @author Fung 
 * 
 * 2018年2月27日
 */

public class SimpleUploadImgManager  extends AbstractUploadImgManager {
	
	public SimpleUploadImgManager(UploadImageBuilder uploadImageBuilder) {
		super(uploadImageBuilder);
	}
	public InputStream uploadImage() throws Exception {
//			copyInputStreamToFile(img, originalPath + imageName, is);
			OutputStream os = cutImage(bili, x, y, w, h, is);
			is = ConvertUtil.parse(os);
			os = compressImage(bili, x, y, w, h, is, compressWidth, compressHeight, quality, compressScale);
			is = ConvertUtil.parse(os);
			return is;
	}
	
}
