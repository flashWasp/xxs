package com.xxs.common.service.cache;

/**
 * 缓存提供器
 */
public interface CacheProvider  {

    /**
     * 缓存标记 redis ehcache
     * @return
     */
    String name();

    /**
     * 缓存层级
     * @return
     */
    int level();


    Cache buildCache(String regionName);


    Cache buildCache(String regionName, long timeToLiveInSeconds);

    default void removeCache(String region){

    }

    void stop();
}
