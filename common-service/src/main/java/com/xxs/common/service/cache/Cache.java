package com.xxs.common.service.cache;


/**
 * 缓存对象接口
 */
public interface Cache {

    /**
     * 获取单个缓存对象
     * @param key
     * @return
     */
    Object get(String key);

//    /**
//     * 批量获取缓存对象
//     * @param keys cache keys
//     * @return return key-value objects
//     */
//    Map<String, Object> get(Collection<String> keys);

    /**
     * 判断缓存是否存在
     * @return
     */
    default Boolean exists(String key){
        return get(key) != null;
    }

    /**
     * 插入数据
     * @param key
     * @param value
     */
    void put(String key, Object value);


//    /**
//     * 批量插入数据
//     * @param elements
//     */
//    void put(Map<String,Object> elements);


//    /**
//     * 返回键的集合
//     * @return
//     */
//    Collection<String> keys();

    /**
     * 删除缓存对象
     */
    void evict(String... keys);

    /**
     * 清空缓存
     */
    void clear();
}
