package com.xxs.common.service.model;

import com.alibaba.fastjson.JSON;

import java.lang.reflect.Type;

/**
 * 统一api数据结构
 *
 * 2018年2月1日
 */
public class AjaxResult {

	/**
	 * 状态码
	 */
	private int statusCode = ResultCode.SUCCESS.getCode();

	/**
	 * 请求返回信息
	 */
	private String message = ResultMsg.FAILURE;

	/**
	 * 请求结果
	 */
	private Object data = null;

	/**
	 * Instantiates a new Ajax result.
	 */
	private AjaxResult() {
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 获取正确结果模板
	 *
	 * @return AjaxResult
	 */
	public static AjaxResult getOK() {
		return getOK(ResultMsg.SUCCESS, null);
	}


	/**
	 * 获取正确结果模板
	 *
	 * @param obj
	 *            请求结果
	 * @return AjaxResult
	 */
	public static AjaxResult getOK(Object obj) {
		AjaxResult result = new AjaxResult();
		result.setMessage(ResultMsg.SUCCESS);
		result.setData(obj);
		return result;
	}

	/**
	 * 获取正确结果模板
	 *
	 * @param message
	 *            请求返回信息
	 * @param obj
	 *            请求结果
	 * @return AjaxResult
	 */
	public static AjaxResult getOK(String message, Object obj) {
		AjaxResult result = new AjaxResult();
		result.setMessage(message);
		result.setData(obj);
		return result;
	}

	/**
	 * 获取错误结果模板
	 *
	 * @return AjaxResult
	 */
	public static AjaxResult getError(ResultCode resultCode) {
		return getError(resultCode, resultCode.getMsg(), null);
	}
	
	/**
	 * 获取错误结果模板
	 *
	 * @param message
	 *            请求返回信息
	 * @param obj
	 *            请求结果
	 * @return AjaxResult
	 */
	public static AjaxResult getError(ResultCode statusCode, String message, Object obj) {
		AjaxResult result = new AjaxResult();
		result.setStatusCode(statusCode.getCode());
		result.setMessage(message);
		result.setData(obj);
		return result;
	}

	@Override
	public String toString() {
		return "AjaxResult [statusCode=" + statusCode + ", message=" + message + ", data=" + data + "]";
	}

	/**
	 * 解析Ajax
	 * @param ajaxResult
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	public static <T> T parseAjax(AjaxResult ajaxResult, Class<T> clazz){
		Object data = ajaxResult.getData();
		return  JSON.parseObject(JSON.toJSONString(data), (Type) clazz);
	}
}
