package com.xxs.common.service.cache.manager.request;


import com.xxs.common.service.cache.CacheFactory;
import com.xxs.common.service.cache.CacheManager;
import com.xxs.common.service.cache.CacheObj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryCacheRequest implements CacheRequest {

    /**
     * id key
     */
    private String id;

    /**
     * 域
     */
    private String region;

    public QueryCacheRequest(String region ,String id){
        this.id = id;
        this.region = region;
    }

    @Override
    public CacheObj process() {
        CacheFactory cacheFactory = new CacheFactory();
        CacheManager cacheManager = cacheFactory.newCacheManager();
        return cacheManager.get(region,id.toString());
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRegion() {
        return region;
    }


}
