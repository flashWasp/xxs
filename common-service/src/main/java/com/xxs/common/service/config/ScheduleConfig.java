package com.xxs.common.service.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

/**
 * 多线程执行定时任务
 * 定时任务的配置中设定了一个SingleThreadScheduledExecutor,在ScheduledTaskRegistrar（定时任务注册类）中的ScheduleTasks中
 * taskScheduler为空,那么就给定时任务做了一个单线程的线程池，所以我们要给其赋值设置一个定时任务线程池
 * @author fung
 * 2018年8月18日
 */
@Configuration
public class ScheduleConfig implements SchedulingConfigurer {

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
	  //设定一个长度10的定时任务线程池
	  taskRegistrar.setScheduler(Executors.newScheduledThreadPool(10));
  }
}
