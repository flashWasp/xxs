package com.xxs.common.service.exception;


import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.util.DateUtil;
import org.omg.CORBA.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 异常处理对象
 */

@ControllerAdvice
public class GlobalExceptionHandler {
    @Autowired
    private JavaMailSender mailSender;
    ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10, 200, TimeUnit.MILLISECONDS,
            new ArrayBlockingQueue<Runnable>(5));

    /**
     * 错误记录日志 邮件方式
     * @param req
     * @param e
     * @return
     */

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Object handle(HttpServletRequest req, Exception e) {
        if (e instanceof ServiceException) {
            StringBuilder sb = new StringBuilder();
            ServiceException serviceException = (ServiceException) e;
            String message = serviceException.getMessage();
            // 当前时间
            sb.append("系统时间: " + DateUtil.format(new Date(),DateUtil.YYYYMMDDHHMMSS_) + "<br/>");
            String method = req.getMethod();
            sb.append("请求方法: " + method + "<br/>");
            Enumeration em = req.getParameterNames();
            sb.append("请求参数 : ");
            // 请求参数处理
            ConcurrentHashMap<String,Object> params = new ConcurrentHashMap<String,Object>();
            while (em.hasMoreElements()) {
                String name = (String) em.nextElement();
                String value = req.getParameter(name);
                params.put(name,value);
                sb.append(value);
            }
            sb.append("错误信息: "+ message);
           // EmailTask emailTask = new EmailTask(mailSender, sb.toString());
           // executor.submit(emailTask);
            return AjaxResult.getError(serviceException.getCode(), message, null);
        }
        if (e instanceof SystemException) {
            e.getCause();
            return AjaxResult.getError(ResultCode.SystemException, "系统异常", null);
        }

        return AjaxResult.getError(ResultCode.UnknownException, "未知异常:" + e.toString(), null);
    }

}
//
//class EmailTask implements Runnable{
//    private JavaMailSender mailSender;
//    private String msg;
//    public EmailTask(JavaMailSender mailSender,String msg){
//        this.mailSender = mailSender;
//        this.msg = msg;
//    }
//    @Override
//    public void run() {
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setFrom("yezhifung@163.com");
//        message.setTo("yezhifung@163.com");  //接受者邮箱
//        message.setSubject("主题：系统报错"); //邮件主题
//        message.setText(msg); //邮件内容
//        mailSender.send(message);
//    }
//}
