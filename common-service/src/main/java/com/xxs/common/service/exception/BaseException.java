package com.xxs.common.service.exception;

import com.xxs.common.service.model.ResultCode;



/**
 * 异常基类
 *
 *
 *         2018年1月31日
 */
public class BaseException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -6640843483099484882L;
	protected ResultCode code; // 异常错误码
	protected String message; // 异常错误信息

	public BaseException(String message) {
		super(message);
		this.code = ResultCode.UnknownException;// 默认未知错误
		this.message = message;
	}

	/**
	 *
	 * @param message
	 * @param cause
	 *            源错误信息
	 */
	public BaseException(String message, Throwable cause) {
		super(message, cause);
		this.code = ResultCode.UnknownException;// 默认未知错误
	}

	public BaseException(ResultCode code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public BaseException(ResultCode code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public ResultCode getCode() {
		return code;
	}

	public void setCode(ResultCode code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
