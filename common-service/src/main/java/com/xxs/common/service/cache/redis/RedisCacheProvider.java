package com.xxs.common.service.cache.redis;


import com.xxs.common.service.cache.Cache;
import com.xxs.common.service.cache.CacheObj;
import com.xxs.common.service.cache.CacheProvider;
import com.xxs.common.service.cache.L2Cache;

import java.util.concurrent.ConcurrentHashMap;

public class RedisCacheProvider implements CacheProvider {

    /**
     * redis客户端对象
     */
    private RedisClient redisClient;

    /**
     *
     */
    private String namespace;


    private String storage;

    private RedisService redisService = new RedisServiceImpl();

    private static final ConcurrentHashMap<String, L2Cache> regions = new ConcurrentHashMap();

    private String name = "redis";

    public RedisCacheProvider() {
        this.redisClient = new RedisClient(redisService,0);
    }
//    public RedisCacheProvider{
//
//    }
//    public RedisCacheProvider{
//
//    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public int level() {
        return CacheObj.LEVEL_2;
    }

    @Override
    public Cache buildCache(String region) {
        /**
         * CAS锁比较
         */
        return new RedisCache(this.namespace, region, redisClient);
//        L2Cache L2Cache = regions.computeIfAbsent(region,
//                v -> "hash".equalsIgnoreCase(this.storage) ? new RedisCache(this.namespace, region, redisClient) :
//                        );
//        return L2Cache;
    }

    @Override
    public Cache buildCache(String regionName, long timeToLiveInSeconds) {
        return buildCache(regionName);
    }

    @Override
    public void stop() {
        redisClient.stop();
    }

    public RedisClient getRedisClient(){
        return redisClient;
    }


}
