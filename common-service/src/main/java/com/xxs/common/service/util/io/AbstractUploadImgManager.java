package com.xxs.common.service.util.io;


import com.xxs.common.service.exception.ServiceException;
import com.xxs.common.service.util.io.CasterUtilConstant;
import com.xxs.common.service.util.io.ImageUtils;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public abstract class AbstractUploadImgManager {
	// 必要参数
	protected MultipartFile img; // 图片对象
	protected InputStream is;	// 流对象
	// 非必要参数
	protected Integer photoSize; // 图片限制大小
	protected Double bili;
	protected Double x;				// 左上角横坐标
	protected Double y;				// 左上角纵坐标
	protected Double w;				// 宽
	protected Double h;				// 高
	protected String originalPath;	// 原图路径
	protected String compressPath;	// 压缩图路径
	protected Integer compressWidth;	// 压缩宽度
	protected Integer compressHeight;	// 压缩高度
	protected Float quality;		// 压缩质量（0-1）
	protected Float compressScale;	// 压缩比例
	// 生成参数
	protected String imageName; // 图片名称
	public AbstractUploadImgManager(UploadImageBuilder uploadImageBuilder) {
		super();
		img = uploadImageBuilder.img;
		photoSize = uploadImageBuilder.photoSize;
		checkIsImage(img, photoSize);
		this.imageName = getImageName(img);
		is = uploadImageBuilder.is;
		bili = uploadImageBuilder.bili;
		x = uploadImageBuilder.x;
		y = uploadImageBuilder.y;
		w = uploadImageBuilder.w;
		h = uploadImageBuilder.h;
		originalPath = uploadImageBuilder.originalPath;
		compressPath = uploadImageBuilder.compressPath;
		compressWidth = uploadImageBuilder.compressWidth;
		compressHeight = uploadImageBuilder.compressHeight;
		quality = uploadImageBuilder.quality;
		compressScale = uploadImageBuilder.compressScale;
	}
	public abstract InputStream uploadImage() throws Exception;



	/**
	 * 构造图片上传参数对象
	 * @author Fung
	 *
	 * 2018年2月27日
	 */
	public static class UploadImageBuilder{
		// 必要参数
		private final MultipartFile img; // 图片对象
		// 非必要参数
		private InputStream is;
		private Double bili;
		private Double x;
		private Double y;
		private Double w;
		private Double h;
		private String originalPath;
		private String compressPath;
		private Integer compressWidth;
		private Integer compressHeight;
		private Float quality;
		private Float compressScale;
		private  Integer photoSize; // 图片限制大小

		public UploadImageBuilder(MultipartFile img,InputStream is){
			this.is = is;
			this.img = img;
		}

		public UploadImageBuilder(MultipartFile img, Integer photoSize, InputStream is, Double bili, Double x,
								  Double y, Double w, Double h) {
			this.img = img;
			this.photoSize = photoSize;
			this.is = is;
			this.bili = bili;
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}
		public UploadImageBuilder setIs(InputStream is) {
			this.is = is;
			return this;
		}
		public UploadImageBuilder setBili(Double bili) {
			this.bili = bili;
			return this;
		}
		public UploadImageBuilder setX(Double x) {
			this.x = x;
			return this;
		}
		public UploadImageBuilder setY(Double y) {
			this.y = y;
			return this;
		}
		public UploadImageBuilder setW(Double w) {
			this.w = w;
			return this;
		}
		public UploadImageBuilder setH(Double h) {
			this.h = h;
			return this;
		}
		public UploadImageBuilder setOriginalPath(String originalPath) {
			this.originalPath = originalPath;
			return this;
		}
		public UploadImageBuilder setCompressPath(String compressPath) {
			this.compressPath = compressPath;
			return this;
		}
		public UploadImageBuilder setCompressWidth(Integer compressWidth) {
			this.compressWidth = compressWidth;
			return this;
		}
		public UploadImageBuilder setCompressHeight(Integer compressHeight) {
			this.compressHeight = compressHeight;
			return this;
		}
		public UploadImageBuilder setQuality(Float quality) {
			this.quality = quality;
			return this;
		}
		public UploadImageBuilder setCompressScale(Float compressScale) {
			this.compressScale = compressScale;
			return this;
		}

	}

	/**
	 * 检查图片
	 * @param img
	 * @param photoSize
	 * @throws Exception
	 *
	 *  2018年1月21日 @author Fung
	 */
	private void checkIsImage(MultipartFile img,Integer photoSize) {
		try {
			ImageUtils.checkingPhoto(img, photoSize);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new ServiceException(CasterUtilConstant.JudgeImage_ERROR);
		}
	}

	/**
	 * 创建图片名称
	 * @param img
	 * @return
	 *
	 *  2018年2月27日 @author Fung
	 */
	private String getImageName(MultipartFile img){
		return img.getOriginalFilename();
	}

	/**
	 * 上传图片
	 * @param img
	 * @param imageOriginalPath
	 * @param is
	 *
	 *  2018年1月21日 @author Fung
	 */

	protected  void copyInputStreamToFile(MultipartFile img,String imageOriginalPath,InputStream is ){
		try {
			is = img.getInputStream();
			FileUtils.copyInputStreamToFile(is, new File(imageOriginalPath));
			if(is!=null) {
				is.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new ServiceException(CasterUtilConstant.UploadImage_ERROR);

		}
	}

	/**
	 * 读取图片
	 * @param originalPath
	 * @param imageName
	 * @return
	 */
	protected BufferedImage readImage(String originalPath,String imageName) {
		try {
			BufferedImage image = ImageIO.read(new FileInputStream(new File(originalPath + imageName)));
			return image;
		} catch (IOException e) {
			throw new ServiceException(CasterUtilConstant.ReadImage_ERROR);
		}
	}
	/**
	 * 裁剪图片
	 * @throws Exception
	 */
	protected OutputStream cutImage(Double bili, Double x, Double y, Double w, Double h, InputStream is){
		try {
			return ImageUtils.cutImage(bili, x, y, w, h, is);
		} catch (Exception e) {
			throw new ServiceException(CasterUtilConstant.CutImage_ERROR);

		}
	}

	/**
	 * 压缩图片
	 * @param bili
	 * @param x
	 * @param y
	 * @param w
	 * @param h

	 * 先压宽高再压质量比例
	 * 2018年1月21日 @author Fung
	 * @return
	 */
	protected  OutputStream  compressImage(Double bili, Double x, Double y, Double w, Double h, InputStream is
			,Integer compressWidth,Integer compressHeight,Float quality,Float compressScale){
		OutputStream os = new ByteArrayOutputStream();
		try {
			if(compressScale == null  ){
				os = (ByteArrayOutputStream) ImageUtils.compressImageForWH(is,1f,compressWidth , compressHeight, true);
			}else if(compressHeight == null){
				Thumbnails.of(is).scale(compressScale).outputQuality(quality).toOutputStream(os);
			}else{
				ImageUtils.compressImageForWH(is, 1f,compressWidth , compressHeight, true);
				Thumbnails.of(is).scale(compressScale).outputQuality(quality).toOutputStream(os);
			}
		} catch (Exception e) {
			throw new ServiceException(CasterUtilConstant.CompressImage_ERROR);
		}
		return os;
	}

}
