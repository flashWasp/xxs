package com.xxs.common.service.cache.manager.request;


import com.xxs.common.service.cache.CacheObj;

/**
 * 缓存请求接口
 */
public interface CacheRequest {

    /**
     * 缓存请求处理实现
     */
     CacheObj process();

    /**
     *
     * @return
     */
    String getId();

    /**
     *
     * @return
     */
    String getRegion();

    default String getkey(){
        return getRegion() + ":" +getId() ;
    }

}
