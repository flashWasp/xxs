package com.xxs.common.service.cache.manager.thread;


import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.cache.CacheObj;
import com.xxs.common.service.cache.manager.request.CacheRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
/**
 * 执行请求的工作线程
 */
public class RequestProcessorThread  implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestProcessorThread.class);

    /**
     * 自己监控的内存队列 是否可以使用furture
     */
    private ArrayBlockingQueue<CacheRequest> queue;

    public RequestProcessorThread(ArrayBlockingQueue<CacheRequest> queue) {
        LOGGER.info("初始化queue ：" + queue.size());
        this.queue = queue;
    }

//    @Override
//    public CacheObj call()  {
//        LOGGER.info("队列剩余长度 ：" + queue.size());
//        try {
//            while(true) {
//                LOGGER.info("队列剩余长度 ：" + queue.size());
//                CacheRequest request = queue.take();
//                LOGGER.info("队列消费请求 ：" + request);
//                LOGGER.info("队列剩余长度 ：" + queue.size());
//                synchronized (this){
//                    return request.process();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Override
    public void run() {
        try {
            while(true) {
                LOGGER.info("队列剩余长度 ：" + queue.size());
                CacheRequest request = queue.take();
                LOGGER.info("队列消费请求 ：" + request);
                LOGGER.info("队列剩余长度 ：" + queue.size());
                synchronized (this){
                     request.process();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
