package com.xxs.common.service.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * cookie工具类
 * 
 * @author akid
 *
 *         2017年8月21日
 */
public class CookieUtils {
	private CookieUtils() {
	}

	/**
	 * 根据name获取cookie的值
	 * 
	 * @param request
	 * @param name
	 * @return
	 * @author akid
	 *
	 *         2017年8月21日
	 */
	public static String getCookie(HttpServletRequest request, String name) {
		Cookie[] cookies = request.getCookies();
		if (null == cookies || StringUtils.isBlank(name)) {
			return null;
		}
		for (Cookie cookie : cookies) {
			if (name.equals(cookie.getName())) {
				return cookie.getValue();
			}
		}
		return null;
	}

	/**
	 * 删除请求中的cookie（把生命值设为0）
	 * 
	 * @param 、
	 * @author akid
	 *
	 *         2017年8月23日
	 */
	public static void removeAllCookies(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				cookie.setMaxAge(0);
			}
		}
	}

	/**
	 * 删除请求中的指定cookie（把生命值设为0）
	 * 
	 * @param request
	 * @param response
	 * @param cookieName
	 * @param domainName
	 */
	public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String domainName) {

		Cookie[] cookies = request.getCookies();
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					cookie.setMaxAge(0);
					cookie.setDomain(domainName);
					cookie.setValue(null);
					response.addCookie(cookie);
				}
			}
		}
	}
}
