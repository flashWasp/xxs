package com.xxs.common.service.cache.redis;

/**
 * 可以设置封装统一接口的redisClient,此处结合项目直接引用redisService工具类
 */
public class RedisClient {

    /**
     * Service服务
     */
    private RedisService redisService;

    private Integer databaseCode = 0;

    public RedisClient(RedisService redisService,Integer databaseCode){
        this.redisService = redisService;
        this.databaseCode = databaseCode;
    }

    /**
     * 设置缓存对象
     * @param key
     * @param value
     */
    public void setCache( byte[] key,byte[] value){
      redisService.set(databaseCode,key,value);
    }

    /**
     * 获取缓存对象
     * @param key
     * @return
     */
    public byte[] getCache(final byte[] key){
        return redisService.get(databaseCode,key);
    }
//        public List<byte[]> getBytes(Collection<String> keys) {
//        return  null;
//    }
//
//
//    public void setBytes(String key, byte[] bytes) {
//        redisService.mset(key,bytes);
//    }
//
//
//    public void setBytes(Map<String, byte[]> bytes) {
//
//    }
//
//
//    public Collection<String> keys() {
//        return null;
//    }


    public void evict(String key) {
        redisService.del(databaseCode,key);
    }

    public void evict(String... keys) {
        redisService.del(databaseCode,keys);
    }

    public void clear() {
        redisService.flushdb(databaseCode);
    }


    public void stop(){
        redisService.close();
    }

}
