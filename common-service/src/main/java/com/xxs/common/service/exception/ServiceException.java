package com.xxs.common.service.exception;



import com.xxs.common.service.model.ResultCode;

/**
 * 业务错误
 *
 *
 *         2018年1月31日
 */
public class ServiceException extends BaseException {

	/**
	 *
	 */
	private static final long serialVersionUID = -6246281755627739978L;

	public ServiceException(String message) {
		super(message);
		this.code = ResultCode.ServiceException; // 业务错误
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		this.code = ResultCode.ServiceException; // 业务错误
	}

	public ServiceException(ResultCode code, String message) {
		super(code, message);
	}

	public ServiceException(ResultCode code, String message, Throwable cause) {
		super(code, message, cause);
	}

}
