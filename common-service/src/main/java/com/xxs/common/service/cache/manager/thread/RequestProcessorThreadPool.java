package com.xxs.common.service.cache.manager.thread;


import com.xxs.common.service.cache.manager.request.CacheRequest;
import com.xxs.common.service.cache.manager.request.RequestQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 缓存请求处理线程池
 * @author Administrator
 *
 */
public class RequestProcessorThreadPool {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestProcessorThreadPool.class);

    /**
     * 线程池
     */
    private ExecutorService threadPool = Executors.newFixedThreadPool(10);

    public RequestProcessorThreadPool()  {
        RequestQueue requestQueue = RequestQueue.getInstance();
        for(int i = 0; i < 10; i++) {
            ArrayBlockingQueue<CacheRequest> queue = new ArrayBlockingQueue<CacheRequest>(100);
            requestQueue.addQueue(queue);
            LOGGER.info(threadPool.toString());
            threadPool.submit(new RequestProcessorThread(queue));
        }
    }

    /**
     * 单例
     *
     * @author Administrator
     *
     */
    private static class Singleton {

        private static RequestProcessorThreadPool instance;

        static {
            instance = new RequestProcessorThreadPool();
        }

        public static RequestProcessorThreadPool getInstance() {
            return instance;
        }

    }

    public static RequestProcessorThreadPool getInstance() {
        return Singleton.getInstance();
    }

    /**
     * 初始化的便捷方法
     */
    public static void init() {
        getInstance();
    }



}
