package com.xxs.common.service.cache;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 缓存配置文件
 */
public class CacheConfig {

    private Properties properties = new Properties();

    private Properties l1CacheProperties = new Properties();

    private Properties l2CacheProperties = new Properties();

    private String l1CacheName;

    private String l2CacheName;

    private String serialization;

    public final static CacheConfig initConfig(String configResource) throws IOException {
        try (InputStream stream = getConfigStream(configResource)) {
            return initConfig(stream);
        }
    }

    public final static CacheConfig initConfig(InputStream stream) throws IOException {
        CacheConfig config = new CacheConfig();
        config.properties.load(stream);

        // 定义一级缓存提供者 EhCache 系统暂时不做
        config.l1CacheName = trim(config.properties.getProperty("cache.L1.provider"));

        // 定义二级缓存提供者 默认应该由redis实现
        config.l2CacheName = trim(config.properties.getProperty("cache.L2.provider"));

        config.serialization = trim(config.properties.getProperty("cache.serialization"));

        config.l1CacheProperties = config.getSubProperties(config.l1CacheName);

        config.l2CacheProperties = config.getSubProperties(config.l2CacheName);

        return  config;
    }

    public Properties getSubProperties(String i_prefix) {
        Properties props = new Properties();
        final String prefix = i_prefix + '.';
        properties.forEach((k,v) -> {
            String key = (String)k;
            if(key.startsWith(prefix))
                props.setProperty(key.substring(prefix.length()), trim((String)v));
        });
        return props;
    }




    private static String trim(String str) {
        return (str != null) ? str.trim() : null;
    }

    private static InputStream getConfigStream(String resource) {
        InputStream configStream = Cache.class.getResourceAsStream(resource);
        if(configStream == null)
            configStream = Cache.class.getClassLoader().getParent().getResourceAsStream(resource);
        if(configStream == null)
            throw new CacheException("Cannot find " + resource + " !!!");
        return configStream;
    }


    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public Properties getL1CacheProperties() {
        return l1CacheProperties;
    }

    public void setL1CacheProperties(Properties l1CacheProperties) {
        this.l1CacheProperties = l1CacheProperties;
    }

    public Properties getL2CacheProperties() {
        return l2CacheProperties;
    }

    public void setL2CacheProperties(Properties l2CacheProperties) {
        this.l2CacheProperties = l2CacheProperties;
    }

    public String getL1CacheName() {
        return l1CacheName;
    }

    public void setL1CacheName(String l1CacheName) {
        this.l1CacheName = l1CacheName;
    }

    public String getL2CacheName() {
        return l2CacheName;
    }

    public void setL2CacheName(String l2CacheName) {
        this.l2CacheName = l2CacheName;
    }

    public String getSerialization() {
        return serialization;
    }

    public void setSerialization(String serialization) {
        this.serialization = serialization;
    }

    @Override
    public String toString() {
        return "CacheConfig{" +
                "properties=" + properties +
                ", l1CacheProperties=" + l1CacheProperties +
                ", l2CacheProperties=" + l2CacheProperties +
                ", l1CacheName='" + l1CacheName + '\'' +
                ", l2CacheName='" + l2CacheName + '\'' +
                ", serialization='" + serialization + '\'' +
                '}';
    }
}
