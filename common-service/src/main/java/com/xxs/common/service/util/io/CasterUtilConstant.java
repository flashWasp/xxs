package com.xxs.common.service.util.io;

public class CasterUtilConstant {
	public static final String  CompressImage_ERROR= "压缩图片失败";
	public static final String  CutImage_ERROR= "裁剪图片失败";
	public static final String  JudgeImage_ERROR= "非图片";
	public static final String  UploadImage_ERROR= "上传图片失败";
	public static final String  ReadImage_ERROR= "读取图片失败";

	public static final String STR = "str";

	public static final String STR_FALSE = "false";

	public static final String STR_TRUE = "true";
}
