package com.xxs.common.service.cache.redis;



import com.xxs.common.service.cache.L2Cache;

import java.io.UnsupportedEncodingException;

public class RedisCache implements L2Cache {
    /**
     * 命名空间 ： 项目:模块名称
     */
    private String namespace;

    /**
     * 域: 对象
     */
    private String region;

    /**
     * 数据库ID : redis
     */
    private Integer databaseCode;

    private RedisClient client;

    public RedisCache(String namesspace,String region,RedisClient redisClient){
        this.client = redisClient;
        this.namespace = namespace;
        this.region = _regionName(region);
    }

    /**
     * key转换
     * @param key
     * @return
     */
    private byte[] _key(String key) {
        try {
            return (this.region + ":" + key).getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            return (this.region + ":" + key).getBytes();
        }
    }

    private String _keyStr(String key){
        return (this.region + ":" + key);

    }

    /**
     * 获取Key地域
     * @param region
     * @return
     */
    private String _regionName(String region) {
        if (namespace != null && !namespace.trim().isEmpty())
            region = namespace + ":" + region;
        return region;
    }

    @Override
    public byte[] getBytes(String key) {
        return client.getCache(_key(key));
    }

    @Override
    public void setBytes(String key, byte[] bytes) {
        client.setCache(_key(key),bytes);
    }

//    @Override
//    public List<byte[]> getBytes(Collection<String> keys) {
////        byte[][] bytes = keys.stream().map(k -> _key(k)).toArray(byte[][]::new);
////        clien.get
////
////
////        return keys.stream().map(k -> getBytes(k)).collect(Collectors.toList());
//        return  null;
//    }
//

//
//    @Override
//    public void setBytes(Map<String, byte[]> bytes) {
//
//    }

//    @Override
//    public Collection<String> keys() {
//        return null;
//    }

    @Override
    public void evict(String  key) {
        client.evict(new String(_key(key)));
    }

//    @Override
//    public Collection<String> keys() {
//        return null;
//    }

    @Override
    public void evict(String... keys) {
        client.evict(keys);
    }

    @Override
    public void clear() {
        client.clear();
    }
}
