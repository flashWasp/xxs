package com.xxs.common.service.cache;


import com.xxs.common.service.cache.redis.RedisCacheProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 缓存管理器
 */
public class CacheHolder {

    private final static Logger log = LoggerFactory.getLogger(CacheHolder.class);

    private static CacheProvider L1CacheProvider;

    private static CacheProvider L2CacheProvider;

    private static String redisCacheName = "redis";


    public CacheHolder(){
        L2CacheProvider = loadProviderInstace(redisCacheName);
    }

    /**
     * 创建缓存提供其对象
     * @return
     */
    private  CacheProvider loadProviderInstace(String cacheName){
        cacheName = "redis";
        if(redisCacheName.equals(cacheName)){
            return new RedisCacheProvider();
        }
        return null;
    }


    /**
     * 二级缓存实例
     */
    public  L2Cache getLevel2Cache(String region) {
        return (L2Cache)L2CacheProvider.buildCache(region);
    }




}
