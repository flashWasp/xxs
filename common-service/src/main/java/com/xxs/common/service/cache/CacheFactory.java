package com.xxs.common.service.cache;

/**
 * 缓存总入口
 */
public class CacheFactory{

    private final static String CONFIG_FILE = "/cache.properties";

    private final static CacheBuilder builder ;

    static{
        try {
//            CacheConfig config = CacheConfig.initConfig(CONFIG_FILE);
            CacheConfig config = new CacheConfig();

            builder = CacheBuilder.init(config);
        } catch (Exception e) {
            throw new CacheException("Failed to load cache configuration " + CONFIG_FILE, e);
        }
    }

    public static CacheManager newCacheManager(){
        return builder.getCacheManager();
    }







}
