package com.xxs.common.service.constant;

public class CommonConstant {

    public final static String STATUSCODE = "statusCode";

    public final static String MESSAGE = "message";

    public final static String DATA = "data";

    /**
     * 当前用户
     */
    public static final String CURRENT_USER = "current_user";

    public static final String CURRENT_USER_ID = "current_user_id";
}
