package com.xxs.common.service.util.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FileUtil {

	public static void delete(String path) {
		File delete = new File(path);
		if (null != delete && delete.exists()) {
			delete.delete();
		}
	}

	/**
	 * 复制单个文件
	 *
	 * @return boolean
	 */
	public static void copyFile(String strOldpath, String strNewPath) throws Exception {

		File fOldFile = new File(strOldpath);
		if (fOldFile.exists()) {
			int byteread = 0;
			InputStream inputStream = new FileInputStream(fOldFile);
			FileOutputStream fileOutputStream = new FileOutputStream(strNewPath);
			byte[] buffer = new byte[1444];
			while ((byteread = inputStream.read(buffer)) != -1) {
				fileOutputStream.write(buffer, 0, byteread);// 三个参数，第一个参数是写的内容，
				// 第二个参数是从什么地方开始写，第三个参数是需要写的大小
			}
			inputStream.close();
			fileOutputStream.close();
		}

	}

}
