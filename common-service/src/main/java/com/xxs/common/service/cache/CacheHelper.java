package com.xxs.common.service.cache;

import com.xxs.common.service.cache.manager.CacheRequestManager;
import com.xxs.common.service.cache.manager.request.DelCacheRequest;
import com.xxs.common.service.cache.manager.request.QueryCacheRequest;
import com.xxs.common.service.cache.manager.request.UpdateCacheRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Administrator
 * @Title: CacheHelper
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/257:48
 */
public class CacheHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheHelper.class);

    /**
     * 更新缓存
     */
    public static void update(String region,String key, String value) {
        UpdateCacheRequest req = new UpdateCacheRequest(region, key, value);
        CacheRequestManager cacheRequestManager = new CacheRequestManager(req);
        cacheRequestManager.update();
        LOGGER.info("Redis Cache update :  {key : "
                + region + ":" + key + "}");
    }

    /**
     * 删除缓存
     * @param region
     * @param key
     */
    public static void del(String region,String key) {
        DelCacheRequest req = new DelCacheRequest(region, key);
        CacheRequestManager manager = new CacheRequestManager(req);
        manager.del();
        LOGGER.info("Redis Cache del :  {key : "
                + region + ":" + key + "}");
    }

    public static Object read(String region,String key){
        QueryCacheRequest queryCacheRequest = new QueryCacheRequest(region, key);
        CacheRequestManager query = new CacheRequestManager(queryCacheRequest);
        CacheObj cacheObj = query.read();
        LOGGER.info("Redis Cache read :  {key : "
                + region + ":" + key + "}");
        if (cacheObj != null){
            return cacheObj.getValue();
        }
        return cacheObj;

    }
}
