package com.xxs.common.service.cache;


import com.xxs.common.service.cache.util.SerializationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自定义配置构建Cache
 */
public class CacheBuilder {

    private final static Logger log = LoggerFactory.getLogger(CacheBuilder.class);

    private CacheConfig  config;

    private CacheManager cacheManager;

    private CacheBuilder(){
        this.config = config;
        this.cacheManager = new CacheManager();
    }

    /**
     * 初始化 Cache
     */
    public static CacheBuilder init(CacheConfig config) {
//        SerializationUtil.init(null);

        return new CacheBuilder();
    }

    /**
     * 可以添加工厂配置属性
     * @return
     */
    public CacheManager getCacheManager(){
//        this.cacheManager = new CacheManager(config);
        return this.cacheManager;
    }

}
