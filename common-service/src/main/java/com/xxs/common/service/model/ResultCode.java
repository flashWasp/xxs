package com.xxs.common.service.model;


/**
 * Ajax请求状态码枚举类
 *
 * 2017年12月1日
 */
public enum ResultCode {
	/**
	 * 未知异常. ErrorCode : 100
	 */
	UnknownException(100, "未知异常"),
	/**
	 * 成功. ErrorCode : 200
	 */
	SUCCESS(200, "成功"),
	/**
	 * 失败. ErrorCode : 300
	 */
	FAILURE(300, "失败"),
	/**
	 * 系统异常. ErrorCode : 400
	 */
	SystemException(400, "系统异常"),
	/**
	 * 业务错误. ErrorCode : 500
	 */
	ServiceException(500, "业务错误"),
	/**
	 * 失败:未验证身份 ErrorCode : 301
	 */
	NoAuthentication (301, "未验证身份"),
	/**
	 * 空指针异常. ErrorCode : 401
	 */
	SystemNullPointerException(401, "空指针异常"),
	/**
	 * IO异常. ErrorCode : 402
	 */
	SystemIOException(402, "IO异常"),

	/**
	 * 参数验证错误. ErrorCode : 501
	 */
	ParamException(501, "参数验证错误"),

	/**
	 * 数据库数据对象为空
	 */
	NullPointerException(204,"返回对象为空");
	private int _code;
	private String _msg;

	private ResultCode(int code, String msg) {
		_code = code;
		_msg = msg;
	}

	public int getCode() {
		return _code;
	}

	public String getMsg() {
		return _msg;
	}

	public static ResultCode getByCode(int code) {
		for (ResultCode ec : ResultCode.values()) {
			if (ec.getCode() == (code)) {
				return ec;
			}
		}

		return null;
	}
}
