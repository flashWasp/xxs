package com.xxs.common.service.util.io;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * FileUtil 附件 文件上传工具类
 * 基本原理：
 * SpringMvc的上传API
 * 
 * @author Fung
 * 
 *         2018年2月8日
 */
public class FileUpLoadUtil {

	// 必须参数
	private final MultipartFile file; // 文件
	private final String filePath; // 文件路径

	// 非必须参数
	private Integer maxSize; // 最大值
	private Integer minSize; // 最小值
	private Integer fileNameLength; // 文件名长度限制

	private FileUpLoadUtil(FileUploadBuilder builder) {
		file = builder.file;
		filePath = builder.filePath;
		fileNameLength = builder.fileNameLength;
		maxSize = builder.maxSize;
		minSize = builder.minSize;
	}

	public static class FileUploadBuilder {
		// 必备参数
		private final MultipartFile file;
		private final String filePath;

		// 非必须参数
		private int maxSize = Integer.MAX_VALUE;
		private Integer minSize = 0;
		private Integer fileNameLength = Integer.MAX_VALUE;

		public FileUploadBuilder(MultipartFile file, String filePath) {
			this.file = file;
			this.filePath = filePath;
		}

		public FileUploadBuilder setMaxSize(int maxSizeVal) {
			maxSize = maxSizeVal;
			return this;
		}

		public FileUploadBuilder setMinSize(Integer minSizeVal) {
			minSize = minSizeVal;
			return this;
		}

		public FileUploadBuilder setFileNameLength(Integer fileNameLengthVal) {
			fileNameLength = fileNameLengthVal;
			return this;
		}

		public FileUpLoadUtil build() {
			return new FileUpLoadUtil(this);

		}
	}

	/**
	 * 上传文件
	 * 
	 * @param file
	 * @param maxSize
	 * @return
	 * 
	 *         2018年2月7日 @author Fung
	 * @throws Exception
	 */
	public String uploadFile() throws Exception {
		String fileName = createFileName(file);
		if (file != null) {
			if (maxSize != null && file.getSize() > maxSize) {
				throw new Exception("文件大小过大");
			}
			if (minSize != null && file.getSize() < minSize) {
				throw new Exception("文件大小过小");
			}
			if (fileNameLength != null && StringUtils.length(file.getOriginalFilename()) > fileNameLength) {
				throw new Exception("文件名过长");
			}

			try {
				File targetFile = new File(filePath + fileName);
				if (!targetFile.exists()) {
					targetFile.mkdirs();
				}
				file.transferTo(targetFile);
			} catch (Exception e) {
				throw new Exception("文件上传失败");
			}
			return fileName;
		}
		return null;
	}

	/**
	 * 下载文件
	 * 
	 * @param filePath 文件路径(不包括文件名)
	 * @param fileName 文件名
	 * @return
	 * 
	 *         2018年2月8日 @author Fung
	 */

	public static void downloadFile(String filePath, String fileName, HttpServletResponse response) {
		byte[] b = new byte[2048];
		int len;
		try {
			// 文件的存放路径
			InputStream inStream = new FileInputStream(filePath + "/" + fileName);
			String originalName = new String(getOriginalName(fileName).getBytes("UTF-8"), "ISO-8859-1");
			// 设置返回头
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename=\"" + originalName + "\"");
			response.setContentType("application/octet-stream");

			// 将ContentType、Header等需要的设置放进 HttpServletResponse传回前端
			while ((len = inStream.read(b)) > 0) {
				response.getOutputStream().write(b, 0, len);
			}
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * 包括文件名的路径
	 * 
	 * @param filePath
	 * @param fileName
	 * @return
	 * 
	 *         2018年3月2日 @author Fung
	 */
	public static void downloadFileFullPath(String fileNamePath, HttpServletResponse response) {
		File file = new File(fileNamePath);
		try {
			String path = file.getParent();
			downloadFile(path, file.getName(), response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 生成文件名-- 用于存数据库
	 * 
	 * @param attachment
	 * @return
	 */
	private static String createFileName(MultipartFile file) {
		// 前缀
		String prefix = file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf("."));
		// 后缀
		String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		return prefix + "-" + UUID.randomUUID().toString() + suffix;
	}

	/**
	 * 根据数据库名获取文件原名
	 * 
	 * @param dbAttachmentName
	 *            数据库存储的attachmentName
	 * @return
	 */
	public static String getOriginalName(String dbFileName) {
		String fineprefix = "";
		String[] fix = dbFileName.split("-");
		if (null != fix && fix.length > 6) {
			for (int i = 0; i < fix.length - 5; ++i) {
				fineprefix += (fix[i] + "-");
			}
			fineprefix = fineprefix.substring(0, fineprefix.lastIndexOf("-"));
		} else {
			fineprefix = fix[0];
		}
		String suffix = dbFileName.substring(dbFileName.lastIndexOf("."));
		return fineprefix + suffix;
	}
}
