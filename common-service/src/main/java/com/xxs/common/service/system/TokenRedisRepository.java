package com.xxs.common.service.system;


import com.xxs.common.service.util.KeyUtil;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 系统生命周期配置
 */

public class TokenRedisRepository {
    private final static String HOST = "127.0.0.1";

    private final static Integer PORT = 6379;

    private final static String COOKIE_PATH = "/";
    // 3天
    private final static Integer COOKIE_MAXAGE = 3 * 24 * 3600;

    /**
     * 获取当前Session的用户对象
     *
     * @return
     */
    public static void setXxsAdminToken(String uuid) {
        setToken("XxsAdminToken", uuid);

    }

    public static void setXxsWebToken(String uuid) {
        setToken("XxsWebToken", uuid);
    }


    /**
     * 获取当前session的用户UUID
     *
     * @return
     */
    public static String getCurUserUUID() {
        Jedis jedis = jedis();
        HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Cookie[] cookies = req.getCookies();
        String xxsClientType = req.getHeader("xxs-client-type");
        ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();
        map.put("web", "XxsWebToken");
        map.put("admin", "XxsAdminToken");
        String s = map.get(xxsClientType);
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(s)) {
                    return jedis.get(cookie.getValue());
                }
            }

        }
        return "1";
    }

    public static Jedis jedis() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        String host = TokenRedisRepository.HOST;
        int port = TokenRedisRepository.PORT;
        Integer timeout = 0;
        jedisPoolConfig.setMaxIdle(8);
        jedisPoolConfig.setMaxWaitMillis(-1);
        jedisPoolConfig.setMaxTotal(200);
        jedisPoolConfig.setMinIdle(0);
        JedisPool jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, null);
        return jedisPool.getResource();
    }

    private static void setToken(String tokenName, String tokenValue) {
        Jedis jedis = jedis();
        String uniqueKey = KeyUtil.getUniqueKey();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        Cookie cookie = new Cookie(tokenName, uniqueKey);
        cookie.setPath(TokenRedisRepository.COOKIE_PATH);
        cookie.setMaxAge(TokenRedisRepository.COOKIE_MAXAGE);//过期时间为1小时
        //  cookie.setHttpOnly(true);
        response.addCookie(cookie);
        jedis.set(uniqueKey, tokenValue);
    }
}
