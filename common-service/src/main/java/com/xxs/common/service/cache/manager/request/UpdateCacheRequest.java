package com.xxs.common.service.cache.manager.request;


import com.xxs.common.service.cache.CacheFactory;
import com.xxs.common.service.cache.CacheManager;
import com.xxs.common.service.cache.CacheObj;

public class UpdateCacheRequest implements CacheRequest {

    /**
     * id key
     */
    private String id;

    /**
     * 域
     */
    private String region;

    /**
     * 目标值
     */
    private String value;

    public UpdateCacheRequest(String region,String id, String value){
        this.region = region;
        this.value = value;
        this.id = id;
    }

    @Override
    public CacheObj process() {
        CacheFactory cacheFactory = new CacheFactory();
        CacheManager cacheManager = cacheFactory.newCacheManager();
        // 删除就有缓存
       // cacheManager.del(region,id);
        // 添加新缓存
        cacheManager.set(region,id,value);
        return  cacheManager.get(region, id.toString());
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRegion() {
        return region;
    }


}
