package com.xxs.member.service.entity;

/**
 * 平台用户创建请求
 */
public class MemberRegisterReq {
    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String mobileNo;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String pwd;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
