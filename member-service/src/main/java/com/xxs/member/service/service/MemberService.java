package com.xxs.member.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.member.service.entity.MemberBase;
import com.xxs.member.service.entity.MemberSearchReq;
import com.xxs.member.service.entity.MemberSupportReq;

import java.lang.reflect.Member;
import java.util.List;

/**
 * 功能:
 * 1) 注册账号
 *      业务逻辑:
 *      手机
 *      验证码
 *      邮箱
 *      验证码
 *      第三方账号登陆
 *
 * 2) 补充账号信息(更新)
 *
 * 3) 查询账号信息(基础信息)
 *
 * 4) 查询账号密码
 *
 * 5) 获取用户列表
 *
 * 6) 删除用户
 *
 *
 */
public interface MemberService {


    /**
     * 添加用户对象
     * @return
     */
    MemberBase register(String email, String mobileNo, String account, String pwd) throws JsonProcessingException;

    /**
     * 根据no获取用户对象
     * @param no
     * @return
     */
    MemberBase getMember(String  no);

    /**
     * 获取用户密码
     * @return
     */
    String getPwd(String no);

    /**
     * 根据用户账号获取密码
     * @param account
     * @return
     */
    String getPwdByAccount(String account);

    /**
     * 根据用户账号获取用户对象
     * @param account
     * @return
     */
    MemberBase detailByAccount(String account);

    /**
     * 根据用户状态获取分页用户列表
     * @param state
     * @return
     */
    Page<MemberBase> pageMemberByState(String state,Integer page,Integer rows);

    /**
     * 根据用户状态获取用户集合
     * @param state
     * @return
     */
    List<MemberBase> listMemberByState(String state);

    /**
     * 根据用户状态统计用户数量
     * @param state
     * @return
     */
    Integer countMemberByState(String state);

    /**
     * 更新用户对象
     * @param member
     * @return
     */
    MemberBase updateMember(MemberBase member);

    /**
     * 删除Member
     * @param memberNo
     * @return
     */
    Boolean delMember(String memberNo);

    /**
     * 验证用户对象
     * @param account
     * @param validCode
     * @return
     */
    Boolean valid(String account, String validCode);

    /**
     * 分页获取用户对象
     * @param req
     * @return
     */
    Page<MemberBase> pageMember(MemberSearchReq req);

    /**
     * 填充用户信息
     * @param req
     * @return
     */
    Boolean support(MemberSupportReq req);
}
