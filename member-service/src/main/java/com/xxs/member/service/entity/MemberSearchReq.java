package com.xxs.member.service.entity;

import java.util.List;

/**
 * 用户搜索请求
 */
public class MemberSearchReq {

    private List<Long> ids;

    /**
     * 登录账户
     */
    private String account;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号码
     */
    private String mobileNo;

    /**
     * 性别 1男 2女
     */
    private Integer sex;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * QQ号码
     */
    private String qqNo;

    /**
     * 账号状态 未开通：nonactivated 正常：normal 禁用：disable
     */
    private String state;

    /**
     * 更新时间(前)
     */
    private Long updateStartTime;

    /**
     * 更新时间(后)
     */
    private Long updateEndTime;

    /**
     * 创建时间(前)
     */
    private Long createStartTime;

    /**
     * 创建时间(后)
     */
    private Long createEndTime;

    /**
     * 用户编号
     */
    private List<String> memberNos;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 登录时间(前)
     */
    private Long loginStartTime;

    /**
     * 登录时间(后)
     */
    private Long loginEndTime;

    /**
     * 登录Ip
     */
    private String loginIp;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 微信
     */
    private String wechat;

    private Integer page;

    private Integer rows;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQqNo() {
        return qqNo;
    }

    public void setQqNo(String qqNo) {
        this.qqNo = qqNo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getUpdateStartTime() {
        return updateStartTime;
    }

    public void setUpdateStartTime(Long updateStartTime) {
        this.updateStartTime = updateStartTime;
    }

    public Long getUpdateEndTime() {
        return updateEndTime;
    }

    public void setUpdateEndTime(Long updateEndTime) {
        this.updateEndTime = updateEndTime;
    }

    public Long getCreateStartTime() {
        return createStartTime;
    }

    public void setCreateStartTime(Long createStartTime) {
        this.createStartTime = createStartTime;
    }

    public Long getCreateEndTime() {
        return createEndTime;
    }

    public void setCreateEndTime(Long createEndTime) {
        this.createEndTime = createEndTime;
    }

    public List<String> getMemberNos() {
        return memberNos;
    }

    public void setMemberNos(List<String> memberNos) {
        this.memberNos = memberNos;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getLoginStartTime() {
        return loginStartTime;
    }

    public void setLoginStartTime(Long loginStartTime) {
        this.loginStartTime = loginStartTime;
    }

    public Long getLoginEndTime() {
        return loginEndTime;
    }

    public void setLoginEndTime(Long loginEndTime) {
        this.loginEndTime = loginEndTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
}
