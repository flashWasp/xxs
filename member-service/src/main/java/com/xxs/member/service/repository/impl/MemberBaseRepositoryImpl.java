package com.xxs.member.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.xxs.member.service.MemberConstant;
import com.xxs.member.service.entity.MemberBase;
import com.xxs.member.service.dao.MemberBaseDao;
import com.xxs.member.service.repository.MemberBaseRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-11-29
 */
@Service
public class MemberBaseRepositoryImpl extends ServiceImpl<MemberBaseDao, MemberBase> implements MemberBaseRepository {

    @Override
    public MemberBase selectByNo(String no) {
        return selectOne(new Condition().eq("member_no",no));
    }

    @Override
    public MemberBase selectByAccount(String account) {
        return selectOne(new Condition().eq("account",account));
    }

    @Override
    public Page<MemberBase> pageMemberByState(String state, Integer page, Integer rows) {
        Page<MemberBase> memberBasePage = new Page<>(page, rows);
        return selectPage(memberBasePage, new Condition().eq("state", state));
    }

    @Override
    public Integer countMemberByState(String state) {
        return selectCount(new Condition().eq("state",state));
    }

    @Override
    public Boolean delOne(String memberNo) {
        MemberBase memberBase = selectOne(new Condition().eq("memberNo", memberNo));
        if (null != memberBase){
            memberBase.setState(MemberConstant.STATE_DISABLE);
            baseMapper.updateAllColumnById(memberBase);
            return  true;
        }
        return false;
    }

    @Override
    public List<MemberBase> listMemberByState(String state) {
        return selectList(new Condition().eq("state", state));
    }

    @Override
    public Page<MemberBase> pageMember(List<Long> ids,
                                       String account, String name,
                                       String mobileNo, Integer sex,
                                       String email, String qqNo,
                                       String state, Long updateStartTime,
                                       Long updateEndTime, Long createStartTime,
                                       Long createEndTime, List<String> memberNos,
                                       String nickName, Long loginStartTime,
                                       Long loginEndTime, String loginIp,
                                       String address, String wechat,
                                       Integer page, Integer rows) {
        Page<MemberBase> memberBasePage = new Page<>(page,rows);
        Condition condition = new Condition();
        if (!CollectionUtils.isEmpty(ids)){
            condition.in("id",ids);
        }
        if (sex != null){
            condition.eq("sex",sex);
        }
        if (state != null){
            condition.eq("state",state);
        }
        condition.like("account",account);
        condition.like("mobile_no",mobileNo);
        condition.like("email",email);

        condition.like("qq_no",qqNo);
        condition.like("nick_name",nickName);
        condition.like("login_ip",loginIp);
        condition.like("address",address);
        condition.like("wechat",wechat);
        if (updateStartTime != null & updateEndTime != null ){
            condition.between("update_time",updateStartTime,updateEndTime);
        }
        if (loginStartTime != null & loginEndTime != null){
            condition.between("login_time",createStartTime,createEndTime);
        }
        if (createStartTime != null & createEndTime != null){
            condition.between("create_time",createStartTime,createEndTime);
        }
        if (!CollectionUtils.isEmpty(memberNos)){
            condition.in("member_no",memberNos);
        }
        return selectPage(memberBasePage,condition);
    }
}
