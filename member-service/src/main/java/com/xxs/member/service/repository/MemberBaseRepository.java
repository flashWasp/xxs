package com.xxs.member.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.member.service.entity.MemberBase;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-11-29
 */
public interface MemberBaseRepository extends IService<MemberBase> {

    /**
     * 根据编号获取对象
     * @param no
     * @return
     */
    MemberBase selectByNo(String no);

    /**
     * 根据账号获取对象
     * @param account
     * @return
     */
    MemberBase selectByAccount(String account);

    /**
     * 根据状态分页获取对象
     * @param state
     * @param page
     * @param rows
     * @return
     */
    Page<MemberBase> pageMemberByState(String state, Integer page, Integer rows);

    /**
     * 根据状态统计对象
     * @param state
     * @return
     */
    Integer countMemberByState(String state);

    /**
     * 删除对象
     * @param memberNo
     * @return
     */
    Boolean delOne(String memberNo);

    /**
     * 根据状态获取所有对象
     * @param state
     * @return
     */
    List<MemberBase> listMemberByState(String state);

    /**
     * 根据条件分页获取对象
     * @param ids
     * @param account
     * @param name
     * @param mobileNo
     * @param sex
     * @param email
     * @param qqNo
     * @param state
     * @param updateStartTime
     * @param updateEndTime
     * @param createStartTime
     * @param createEndTime
     * @param memberNos
     * @param nickName
     * @param loginStartTime
     * @param loginEndTime
     * @param loginIp
     * @param address
     * @param wechat
     * @param page
     * @param rows
     * @return
     */
    Page<MemberBase> pageMember(List<Long> ids, String account, String name, String mobileNo, Integer sex, String email, String qqNo, String state, Long updateStartTime, Long updateEndTime, Long createStartTime, Long createEndTime, List<String> memberNos, String nickName, Long loginStartTime, Long loginEndTime, String loginIp, String address, String wechat, Integer page, Integer rows);
}
