package com.xxs.member.service.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.member.service.entity.MemberBase;
import com.xxs.member.service.entity.MemberRegisterReq;
import com.xxs.member.service.entity.MemberSearchReq;
import com.xxs.member.service.entity.MemberSupportReq;
import com.xxs.member.service.service.MemberService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-10-29
 */
@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    /**
     * 注册：
     * 第一版本 使用邮箱进行注册通知 MQ引入
     *
     * @return
     */
    @ApiOperation("注册")
    @PostMapping("/registered")
    public AjaxResult registered(@RequestBody MemberRegisterReq req) throws JsonProcessingException {
        MemberBase registerMember = memberService.register(req.getEmail(),
                req.getMobileNo(),
                req.getAccount(),
                req.getPwd());
        if (null != registerMember) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 验证
     *
     * @param account   账号
     * @param validCode 验证码
     * @return
     */
    @ApiOperation("验证")
    @GetMapping("/valid")
    public AjaxResult valid(String account, String validCode) {
        Boolean flag = memberService.valid(account, validCode);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 补充用户信息
     * @param req
     * @return
     */
    @ApiOperation("补充用户信息")
    @RequestMapping("/support")
    public AjaxResult support(@RequestBody MemberSupportReq req){
        Boolean support = memberService.support(req);
        if (support){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 根据账号获取用户密码
     *
     * @param account
     * @return
     */
    @ApiOperation("根据账号获取用户密码")
    @GetMapping("/getPwdByAccount")
    public AjaxResult getPwdByAccount(@RequestParam("account") String account) {
        return AjaxResult.getOK(memberService.getPwdByAccount(account));
    }

    /**
     * 根据用户编号获取用户对象简单信息
     *
     * @return
     */
    @ApiOperation("根据用户简单信息")
    @GetMapping("/get")
    public AjaxResult getMemberInfo(@RequestParam("no") String no) {
        MemberBase member = memberService.getMember(no);
        if (null != member) {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("nickName", member.getNickName());
            result.put("picImg", member.getPicImg());
            result.put("email", member.getEmail());
            result.put("sex", member.getSex());
            result.put("state", member.getState());
            result.put("no", member.getMemberNo());
            return AjaxResult.getOK(result);
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 获取用户详细信息
     *
     * @param no
     * @return
     */
    @ApiOperation("获取用户详情")
    @GetMapping("/detail")
    public AjaxResult getMemberDetail(@RequestParam("no") String no) {
        MemberBase member = memberService.getMember(no);
        if (null != member) {
            member.setPassword(null);
            return AjaxResult.getOK(member);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 根据用户账号获取用户详细信息
     *
     * @param account
     * @return
     */
    @ApiOperation("根据用户账号获取用户详情")
    @GetMapping("/detailByAccount")
    public AjaxResult detailByAccount(@RequestParam("account")String account){
        MemberBase member = memberService.detailByAccount(account);
        if (null != member) {
            return AjaxResult.getOK(member);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


    /**
     * 分页获取用户对象
     * @param req
     * @return
     */
    @ApiOperation("分页获取用户对象")
    @GetMapping("/pageMember")
    public AjaxResult pageMember(@RequestBody MemberSearchReq req) {
        Page<MemberBase> memberBasePage = memberService.pageMember(req);
        if (null != memberBasePage) {
            return AjaxResult.getOK(memberBasePage);
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    @ApiOperation("获取用户状态")
    @GetMapping("/getState")
    public AjaxResult getState(String no){
        MemberBase member = memberService.getMember(no);
        if (null != member){
            return AjaxResult.getOK(member.getState());
        }
        return  AjaxResult.getError(ResultCode.FAILURE);
    }

}

