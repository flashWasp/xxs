package com.xxs.member.service;

public class MemberConstant {

    /**
     * 未开通
     */
    public final static String STATE_NON_ACTIVATED = "nonactivated";

    /**
     * 正常
     */
    public final static String STATE_NORMAL = "normal";

    /**
     * 禁用
     */
    public final static String STATE_DISABLE = "disable";

    /**
     * 验证码 : 账号
     */
    public final static String VALIDCODE_ACCOUNT_KEY = "vaildCode:account";


    /*=============== Redis_Key_Constant  =================*/






}
