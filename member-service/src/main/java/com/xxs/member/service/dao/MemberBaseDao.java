package com.xxs.member.service.dao;

import com.xxs.member.service.entity.MemberBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-11-29
 */
public interface MemberBaseDao extends BaseMapper<MemberBase> {

}
