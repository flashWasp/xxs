package com.xxs.member.service.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.common.service.RabbitMqMessageManager;
import com.xxs.common.service.RabbitSender;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.constant.MQConstants;
import com.xxs.common.service.exception.ServiceException;
import com.xxs.common.service.model.RabbitMetaMessage;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.model.template.MailTemplate;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.member.service.MemberConstant;
import com.xxs.member.service.entity.MemberBase;
import com.xxs.member.service.entity.MemberSearchReq;
import com.xxs.member.service.entity.MemberSupportReq;
import com.xxs.member.service.repository.MemberBaseRepository;
import com.xxs.member.service.service.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * 用户对象服务
 */
@Service("memberService")
public class MemberServiceImpl implements MemberService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MemberServiceImpl.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;
    private static final String Member_BASE = "member:base";
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MemberBaseRepository memberBaseRepository;

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public MemberBase register(String email, String mobileNo, String account, String pwd) throws JsonProcessingException {
        // 注册逻辑
        MemberBase member = new MemberBase();
        member.setMemberNo(KeyUtil.getUniqueKey());
        member.setPassword(passwordEncoder.encode(pwd));
        member.setCreateTime(System.currentTimeMillis());
        member.setUpdateTime(System.currentTimeMillis());
        member.setState(MemberConstant.STATE_NON_ACTIVATED);
        // 邮箱注册
        if (StringUtils.isNotBlank(email)) {
            account = email;
            member.setEmail(email);
            member.setAccount(email);
            sendMail(email,account);
        }
        // 手机注册
        if (StringUtils.isNotBlank(mobileNo)){
            member.setMobileNo(mobileNo);
        }
        //定义账号
        if (StringUtils.isNotBlank(account)){
            member.setAccount(account);
        }
        memberBaseRepository.insert(member);
        return member;
    }

    @Override
    public MemberBase getMember(String no) {
//        Object cacheObj = CacheHelper.read(Member_BASE,no);
//        if (cacheObj != null){
//            return JSONObject.parseObject(cacheObj.toString(),MemberBase.class);
//        }
        MemberBase member =memberBaseRepository.selectByNo(no);
//        if (null != member) {
//            CacheHelper.update(Member_BASE,no,JSONObject.toJSONString(member));
            return member;
//        }
//        return null;
    }

    @Override
    public String getPwd(String no) {
        MemberBase member = memberBaseRepository.selectByNo(no);
        if (null != member){
            return member.getMemberNo();
        }
        return null;
    }

    @Override
    public String getPwdByAccount(String account) {
        MemberBase memberBase = memberBaseRepository.selectByAccount(account);
        if (null != memberBase){
            return memberBase.getPassword();
        }
        return null;
    }

    @Override
    public MemberBase detailByAccount(String account) {
        return memberBaseRepository.selectByAccount(account);
    }

    @Override
    public Page<MemberBase> pageMemberByState(String state,Integer page,Integer rows) {
        return memberBaseRepository.pageMemberByState(state,page,rows);
    }

    @Override
    public List<MemberBase> listMemberByState(String state) {
        return memberBaseRepository.listMemberByState(state);
    }

    @Override
    public Integer countMemberByState(String state) {
        return memberBaseRepository.countMemberByState(state);
    }

    @Override
    public MemberBase updateMember(MemberBase member) {
        boolean  flag = memberBaseRepository.updateAllColumnById(member);
        if (flag){
            CacheHelper.del(Member_BASE,member.getMemberNo());
            return member;
        }
        return null;
    }

    @Override
    public Boolean delMember(String memberNo) {
        Boolean b = memberBaseRepository.delOne(memberNo);
        if (b){
            CacheHelper.del(Member_BASE,memberNo);
        }
        return b;
    }

    @Override
    public Boolean valid(String account, String validCode) {
        String redisValidCode = redisTemplate.opsForHash().get(MemberConstant.VALIDCODE_ACCOUNT_KEY, account).toString();
        if (validCode.equals(redisValidCode)){
            MemberBase memberBase = memberBaseRepository.selectByAccount(account);
            if (memberBase != null){
                memberBase.setState(MemberConstant.STATE_NORMAL);
                return memberBaseRepository.updateAllColumnById(memberBase);
            }
        }
        return Boolean.FALSE;
    }

    @Override
    public Page<MemberBase> pageMember(MemberSearchReq req) {
        List<Long> ids = req.getIds();
        String account = req.getAccount();
        String name = req.getName();
        String mobileNo = req.getMobileNo();
        Integer sex = req.getSex();
        String email = req.getEmail();
        String qqNo = req.getQqNo();
        String state = req.getState();
        Long updateStartTime = req.getUpdateStartTime();
        Long updateEndTime = req.getUpdateEndTime();
        Long createStartTime = req.getCreateStartTime();
        Long createEndTime = req.getCreateEndTime();
        List<String> memberNos = req.getMemberNos();
        String nickName = req.getNickName();
        Long loginStartTime = req.getLoginStartTime();
        Long loginEndTime = req.getLoginEndTime();
        String loginIp = req.getLoginIp();
        String address = req.getAddress();
        String wechat = req.getWechat();
        Integer page = req.getPage();
        Integer rows = req.getRows();
        return  memberBaseRepository.pageMember(
                ids,account,name,mobileNo,sex,email,qqNo,state,
                updateStartTime,updateEndTime,createStartTime,createEndTime,
                memberNos,nickName,loginStartTime,loginEndTime,loginIp,address,
                wechat,page,rows
        );
    }

    @Override
    public Boolean support(MemberSupportReq req) {
        String memberNo = req.getMemberNo();
        String name = req.getName();
        String nickName = req.getNickName();
        String picImg = req.getPicImg();
        String qqNo = req.getQqNo();
        String remark = req.getRemark();
        String wechat = req.getWechat();
        String address = req.getAddress();
        Integer sex = req.getSex();
        MemberBase member = getMember(memberNo);
        if (null != member){
            member.setName(name);
            member.setNickName(nickName);
            member.setPicImg(picImg);
            member.setQqNo(qqNo);
            member.setRemark(remark);
            member.setWechat(wechat);
            member.setAddress(address);
            member.setSex(sex);
            return memberBaseRepository.updateAllColumnById(member);
        }
        return false;
    }


    /**
     * 给账户发送验证码信息
     * @param email
     * @param account
     * @throws JsonProcessingException
     */
    private void sendMail(String email,String account) throws JsonProcessingException {
        /** 设置需要传递的消息体 */
        Set<String> set = new HashSet<String>();
        set.add(email);
        String subject = "用户注册邮件通知";
        String vaildCode = (int) ((Math.random() * 9 + 1) * 100000) + "";
        String content = "验证码:" + vaildCode  ;
        redisTemplate.opsForHash().put(MemberConstant.VALIDCODE_ACCOUNT_KEY, account, vaildCode);
        MailTemplate mailTemplate = new MailTemplate(subject, content, set);
        RabbitMetaMessage rabbitMetaMessage = RabbitMqMessageManager.newMailMessageInstance(mailTemplate);
        /** 发送消息 */
        RabbitSender.send(rabbitMetaMessage, rabbitTemplate, LOGGER);
    }
}
