package com.xxs.member.service;

import ch.qos.logback.core.encoder.EchoEncoder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.common.service.RabbitMqMessageManager;
import com.xxs.common.service.RabbitSender;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.RabbitMetaMessage;
import com.xxs.common.service.model.template.MailTemplate;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.CookieUtils;
import com.xxs.common.service.util.DateUtil;
import com.xxs.member.service.service.impl.MemberServiceImpl;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@ControllerAdvice
public class MemberControllerAdvice {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private RedisTemplate redisTemplate;

    private Logger LOG = LoggerFactory.getLogger(MemberServiceImpl.class);
    /**
     * 全局异常捕捉
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public AjaxResult exceptionHandler(HttpServletRequest req, Exception e) throws JsonProcessingException {
        // 获取当前用户ID
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        // 获取当前时间
        String curDate = DateUtil.getCurrentTimeForYYYYMMDDHHMMSS_();
        // 处理参数
        Map<String, String[]> parameterMap = req.getParameterMap();

        StringBuffer params = new StringBuffer();
        Set<String> keys = parameterMap.keySet();
        for (String key:keys) {
            String[] values = parameterMap.get(key);
            params.append("<p>" + key + ":"+ values[0] + "</p>");
            params.toString();
        }
        StringBuffer errorMsg = new StringBuffer();
        errorMsg.append("<p>用户id：" + curUserUUID + "</p>");
        errorMsg.append("<p>系统时间：" + curDate + "</p>");
        errorMsg.append("<p>请求参数：" + params + "</p>");
        errorMsg.append("<p>异常信息：" + e.getMessage()+ "</p>");
        String content = errorMsg.toString();
        /** 设置需要传递的消息体 */
        Set<String> set = new HashSet<String>();
        set.add("yezhifung@163.com");
        String subject = "异常邮件通知";
        MailTemplate mailTemplate = new MailTemplate(subject, content, set);
        RabbitMetaMessage rabbitMetaMessage = RabbitMqMessageManager.newMailMessageInstance(mailTemplate);
        /** 发送消息 */
        RabbitSender.send(rabbitMetaMessage, rabbitTemplate, LOG);
        return AjaxResult.getOK("完成异常邮件通知");
    }
}
