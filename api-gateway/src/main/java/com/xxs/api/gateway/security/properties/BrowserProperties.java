package com.xxs.api.gateway.security.properties;

/**
 * Author : Fung
 * Date : 2018/11/14 0014 下午 1:25
 * Desc : 浏览器登录配置属性
 */
public class BrowserProperties {

    private SessionProperties sessionProperties = new SessionProperties();



    /**
     * 默认登录页
     */
    private String loginPage = "/login.html";
    /**
     * 记住我登录秒数 1小时
     */
    private int remmemberMeSeconds = 3600 ;

    public SessionProperties getSessionProperties() {
        return sessionProperties;
    }

    public void setSessionProperties(SessionProperties sessionProperties) {
        this.sessionProperties = sessionProperties;
    }

    public int getRemmemberMeSeconds() {
        return remmemberMeSeconds;
    }

    public void setRemmemberMeSeconds(int remmemberMeSeconds) {
        this.remmemberMeSeconds = remmemberMeSeconds;
    }

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }
}
