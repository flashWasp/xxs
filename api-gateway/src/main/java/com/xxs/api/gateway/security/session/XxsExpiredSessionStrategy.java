package com.xxs.api.gateway.security.session;

import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Author : Fung
 * Date : 2018/11/15 0015 下午 12:24
 * Desc : session由超时失效
 */
public class XxsExpiredSessionStrategy  extends AbstractSessionStrategy implements SessionInformationExpiredStrategy {

    public XxsExpiredSessionStrategy(String invalidSessionUrl) {
        super(invalidSessionUrl);
    }

    @Override
    public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException, ServletException {
        onSessionInvalid(event.getRequest(), event.getResponse());
    }

    @Override
    protected boolean isConcurrency() {
        return true;
    }


}
