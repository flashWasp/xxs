package com.xxs.api.gateway.security.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 *  系统基础的安全配置
 *
 */
public  class AbstractSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    /**
     * 登录页
     */
    private String DEFAULT_UNAUTHENTICATION_URL = "/account/loginFilter";

    /**
     * 登录认证接口
     */
    private String DEFAULT_LOGIN_PROCESSING_URL_FORM = "/account/login";

    /**
     * 密码服务
     * @param http
     * @throws Exception
     */
    protected void applyPasswordAuthenticationConfig(HttpSecurity http) throws Exception {
        http.formLogin()
                // 自定义登陆所需url
                .loginPage(DEFAULT_UNAUTHENTICATION_URL)
//                // 自定义登陆接口
                .loginProcessingUrl(DEFAULT_LOGIN_PROCESSING_URL_FORM)
               .successHandler(authenticationSuccessHandler)
               .failureHandler(authenticationFailureHandler);
    }






}
