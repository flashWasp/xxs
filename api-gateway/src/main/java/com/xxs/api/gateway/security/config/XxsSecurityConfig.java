package com.xxs.api.gateway.security.config;

import com.xxs.api.gateway.XxsUserDetailsService;
import com.xxs.api.gateway.security.authentication.AuthorizeConfigManager;
import com.xxs.api.gateway.security.config.AbstractSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.session.InvalidSessionStrategy;

import javax.sql.DataSource;

//@EnableWebSecurity
@Configuration
public class XxsSecurityConfig extends AbstractSecurityConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private XxsUserDetailsService userDetailsService;

    @Autowired
    private InvalidSessionStrategy invalidSessionStrategy;

    @Autowired
    private AuthorizeConfigManager authorizeConfigManager;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 封装登陆跳转服务
        applyPasswordAuthenticationConfig(http);
        http.sessionManagement()
                .invalidSessionStrategy(invalidSessionStrategy)
                // 最大并发session
                .maximumSessions(1)
                // 是否阻止新的登录
                .maxSessionsPreventsLogin(true)
                .and()
                .and()
                .rememberMe()
                .tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(3600)
                .userDetailsService(userDetailsService)
                .and()
                .authorizeRequests()        // 定义不需要被保护的权限
                .antMatchers("/login.html",
                        "/account/loginFilter","/account/login",
                        "/web/**","/index.html"
                        ).permitAll()
                .anyRequest()               // 任何请求,登录后可以访问
                .authenticated()
                .and()
                .csrf().disable(); // 关闭csrf防护
        // 添加认证处理
      authorizeConfigManager.config(http.authorizeRequests());
    }

    /**
     * 定义加密解密器（自带盐）
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 配置 记住我 功能
     * 配置TokenRepository
     * @return
     */
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        InMemoryTokenRepositoryImpl inMemoryTokenRepository = new InMemoryTokenRepositoryImpl();
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        // 配置数据源
        jdbcTokenRepository.setDataSource(dataSource);
        // 第一次启动的时候自动建表（可以不用这句话，自己手动建表，源码中有语句的）
        // jdbcTokenRepository.setCreateTableOnStartup(true);
        return jdbcTokenRepository;
    }

}
