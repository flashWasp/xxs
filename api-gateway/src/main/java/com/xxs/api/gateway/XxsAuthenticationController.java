package com.xxs.api.gateway;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/account")
public class XxsAuthenticationController {

    /**
     * 日志打印工具
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(XxsAuthenticationController.class);

    private RequestCache requestCache = new HttpSessionRequestCache();

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private final static String XXS_LOGIN_PAGE = "/login.html";


    @RequestMapping("/loginFilter")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public AjaxResult authenfilter(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest != null) {
            LOGGER.info("转发发生，转发目标为:" + XXS_LOGIN_PAGE  );
            redirectStrategy.sendRedirect(request, response, XXS_LOGIN_PAGE);
        }
        return AjaxResult.getError(ResultCode.NoAuthentication);
    }

    @PostMapping("/login")
    @ResponseBody
    public void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // 不做任何事
    }





}
