package com.xxs.api.gateway.security.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Author : Fung
 * Date : 2018/11/14 0014 下午 1:22
 * Desc :  门关配置对象
 */
@Configuration
@PropertySource("classpath:gateway.properties")
@ConfigurationProperties(prefix = "gateWay")
public class GateWayProperties {
    /**
     * PC端属性配置对象
     */
    private BrowserProperties browser = new BrowserProperties();

    public BrowserProperties getBrowser() {
        return browser;
    }

    public void setBrowser(BrowserProperties browser) {
        this.browser = browser;
    }

}
