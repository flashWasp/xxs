package com.xxs.api.gateway.security.config;

import com.xxs.api.gateway.security.properties.GateWayProperties;
import com.xxs.api.gateway.security.session.XxsExpiredSessionStrategy;
import com.xxs.api.gateway.security.session.XxsInvalidSessionStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

/**
 * Author : Fung
 * Date : 2018/11/15 0015 下午 12:26
 * Desc :
 */
@Configuration
public class XxsSessionStrategyConfig {

    @Autowired
    private GateWayProperties gateWayProperties;

    @Bean
    @ConditionalOnMissingBean(InvalidSessionStrategy.class)
    public InvalidSessionStrategy invalidSessionStrategy(){
        return new XxsInvalidSessionStrategy(gateWayProperties.getBrowser().getSessionProperties().getSessionInvalidUrl());
    }

    @Bean
    @ConditionalOnMissingBean(SessionInformationExpiredStrategy.class)
    public SessionInformationExpiredStrategy sessionInformationExpiredStrategy(){
        return new XxsExpiredSessionStrategy(gateWayProperties.getBrowser().getSessionProperties().getSessionInvalidUrl());
    }

}
