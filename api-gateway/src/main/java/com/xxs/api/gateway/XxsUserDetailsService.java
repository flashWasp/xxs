package com.xxs.api.gateway;

import com.xxs.api.gateway.service.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Component;

/**
 * Caster 用户服务框架
 */
@Component
public class XxsUserDetailsService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberService memberService;

    /**
     * 完成用户认证逻辑
     * @param
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("用户名 : " + username);
        String memberPwd = memberService.getPwdByAccount(username);
        if (StringUtils.isNotEmpty(memberPwd)){
            return new User(username,memberPwd,AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
        }
        return null;
    }
}
