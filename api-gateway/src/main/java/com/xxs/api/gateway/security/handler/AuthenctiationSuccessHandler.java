package com.xxs.api.gateway.security.handler;

import com.xxs.api.gateway.entity.MemberDto;
import com.xxs.api.gateway.entity.UserDto;
import com.xxs.api.gateway.service.MemberService;
import com.xxs.common.service.system.TokenRedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登陆成功的回调处理
 */
@Component("authenctiationSuccessHandler")
public class AuthenctiationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberService memberService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        System.out.println("登录成功操作");
        String redirectUrl = "/index.html";
        SavedRequest savedRequest = (SavedRequest) request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        if(savedRequest != null) {
            redirectUrl =   savedRequest.getRedirectUrl();
            request.getSession().removeAttribute("SPRING_SECURITY_SAVED_REQUEST");
        }
        User principal = (User) authentication.getPrincipal();
        MemberDto memberDto = memberService.detailByAccount(principal.getUsername());
        if (memberDto != null){
            TokenRedisRepository.setXxsWebToken(memberDto.getMemberNo());
        }
        redirectStrategy.sendRedirect(request, response, redirectUrl);
    }
}

