package com.xxs.api.gateway.service;

import com.xxs.api.gateway.entity.MemberDto;
import com.xxs.api.gateway.service.feign.MemberServiceFegin;
import com.xxs.common.service.model.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @Title: MemberService
 * @ProjectName master
 * @Description: 平台用户 服务
 * @date 2018/12/1520:27
 */
@Service("memberService")
public class MemberService {

    @Autowired
    private MemberServiceFegin memberServiceFegin;
    /**
     * 获取用户信息
     * @param memberNo
     * @return
     */
    public MemberDto get(String memberNo){
        AjaxResult ajaxResult = memberServiceFegin.get(memberNo);
        if (ajaxResult != null){
            return  AjaxResult.parseAjax(ajaxResult,MemberDto.class);
        }
        return null;
    }

    /**
     * 根据账号获取用户密码
     * @param account
     * @return
     */
    public String getPwdByAccount(String account) {
        AjaxResult ajaxResult = memberServiceFegin.getPwdByAccount(account);
        if (ajaxResult != null) {
            return AjaxResult.parseAjax(ajaxResult, String.class);
        }
        return null;
    }

    /**
     * 根据账号获取用户详情
     * @param account
     * @return
     */
    public MemberDto detailByAccount(String account){
        AjaxResult ajaxResult = memberServiceFegin.detailByAccount(account);
        if (ajaxResult != null){
            return AjaxResult.parseAjax(ajaxResult,MemberDto.class);
        }
        return null;
    }


}
