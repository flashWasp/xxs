package com.xxs.api.gateway.security.session;

import org.springframework.security.web.session.InvalidSessionStrategy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Author : Fung
 * Date : 2018/11/15 0015 下午 12:23
 * Desc : Session由并发失效
 */
public class XxsInvalidSessionStrategy extends AbstractSessionStrategy implements InvalidSessionStrategy {

    public XxsInvalidSessionStrategy(String invalidSessionUrl) {
        super(invalidSessionUrl);
    }

    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        onSessionInvalid(request, response);
    }

}
