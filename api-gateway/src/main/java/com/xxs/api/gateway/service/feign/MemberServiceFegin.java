package com.xxs.api.gateway.service.feign;

import com.xxs.common.service.model.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Administrator
 * @Title: MemberServiceFegin
 * @ProjectName master
 * @Description:  平台服务
 * @date 2018/12/15 20:26
 */
@FeignClient("member-service")
public interface MemberServiceFegin {
    @GetMapping("/member/detail")
    AjaxResult get(@RequestParam("no")String no);

    @ApiOperation("根据用户账号获取用户密码")
    @GetMapping("/member/getPwdByAccount")
    AjaxResult getPwdByAccount(@RequestParam("account")String account);

    @ApiOperation("根据用户账号获取用户对象")
    @GetMapping("/member/detailByAccount")
    AjaxResult detailByAccount(@RequestParam("account")String account);
}
