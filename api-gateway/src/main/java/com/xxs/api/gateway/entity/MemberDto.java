package com.xxs.api.gateway.entity;

/**
 * @author Administrator
 * @Title: MemberDto
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1520:28
 */
public class MemberDto {

    private Long id;
    /**
     * 登录账户
     */
    private String account;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 姓名
     */
    private String name;
    /**
     * 手机号码
     */
    private String mobileNo;
    /**
     * 性别 1男 2女
     */
    private Integer sex;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * QQ号码
     */
    private String qqNo;
    /**
     * 备注说明
     */
    private String remark;
    /**
     * 账号状态 未开通：nonactivated 正常：normal 禁用：disable
     */
    private String state;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 用户编号
     */
    private String memberNo;
    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像图片路径
     */
    private String picImg;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 登录Ip
     */
    private String loginIp;
    /**
     * 密码错误登录次数
     */
    private Integer loginErrorNum;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 微信
     */
    private String wechat;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQqNo() {
        return qqNo;
    }

    public void setQqNo(String qqNo) {
        this.qqNo = qqNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPicImg() {
        return picImg;
    }

    public void setPicImg(String picImg) {
        this.picImg = picImg;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Integer getLoginErrorNum() {
        return loginErrorNum;
    }

    public void setLoginErrorNum(Integer loginErrorNum) {
        this.loginErrorNum = loginErrorNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    @Override
    public String toString() {
        return "MemberBase{" +
                ", id=" + id +
                ", account=" + account +
                ", password=" + password +
                ", name=" + name +
                ", mobileNo=" + mobileNo +
                ", sex=" + sex +
                ", email=" + email +
                ", qqNo=" + qqNo +
                ", remark=" + remark +
                ", state=" + state +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                ", memberNo=" + memberNo +
                ", nickName=" + nickName +
                ", picImg=" + picImg +
                ", loginTime=" + loginTime +
                ", loginIp=" + loginIp +
                ", loginErrorNum=" + loginErrorNum +
                ", address=" + address +
                ", wechat=" + wechat +
                "}";
    }
}
