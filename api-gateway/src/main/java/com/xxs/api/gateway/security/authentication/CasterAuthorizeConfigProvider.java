package com.xxs.api.gateway.security.authentication;

import com.xxs.api.gateway.security.properties.GateWayProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * Author : Fung
 * Date : 2018/11/14 0014 下午 6:30
 * Desc :
 */
@Component
@Order(Integer.MIN_VALUE)
public class CasterAuthorizeConfigProvider implements  AuthorizeConfigProvider {

    @Autowired
    private GateWayProperties gateWayProperties;

    /**
     * 默认可以全权限可通过
     * @param config
     */
    @Override
    public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        config.antMatchers("/authentication/require").permitAll();
    }
}
