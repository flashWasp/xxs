package com.xxs.news.service.oss;

import java.util.List;

/**
 * oss 批量查询
 */
public class OssBatchQueryFileRequest {

    private List<Long> objects;

    public OssBatchQueryFileRequest(List<Long> objects){
        this.objects = objects;
    }
    public List<Long> getObjects() {
        return objects;
    }

    public void setObjects(List<Long> objects) {
        this.objects = objects;
    }

    @Override
    public String toString() {
        return "OssBatchQueryFileRequest{" +
                "objects=" + objects +
                '}';
    }
}
