package com.xxs.news.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.dao.NewsArticleDao;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.repository.NewsArticleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Service
public class NewsArticleRepositoryImpl extends ServiceImpl<NewsArticleDao, NewsArticle> implements NewsArticleRepository {


    @Override
    public NewsArticle getArticleByNo(String articleNo) {
        NewsArticle newsArticle = new NewsArticle();
        newsArticle.setArticleNo(articleNo);
        return baseMapper.selectOne(newsArticle);
    }


    @Override
    public Page<NewsArticle> listArticleVersionByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        NewsArticle articleCondition = new NewsArticle();
        articleCondition.setArticleNo(articleNo);
        Page<NewsArticle> newsArticlePage = new Page<NewsArticle>(pageNo,pageSize);
        EntityWrapper<NewsArticle> entity = new EntityWrapper<>(articleCondition);
        entity.setSqlSelect("id, article_no, channelId AS channelId, title, views, favorites, image_no, updateTime AS updateTime, createTime AS createTime, createUserUuid AS createUserUuid, updateUserUuid AS updateUserUuid, state,summary,likes AS likes");
        return newsArticlePage.setRecords(baseMapper.selectPage(newsArticlePage,entity));
    }

    @Override
    public Page<NewsArticle> listNewsArticlePage(Integer channelNo, String createUserNo, String state, Integer pageNo, Integer pageSize, String startTime, String endTime, String title) {
        NewsArticle article = new NewsArticle();
        article.setChannelId(channelNo);
        Wrapper<NewsArticle> entity = new EntityWrapper<>(article);
        if (startTime != null && endTime != null){
           entity.between("createTime",startTime,endTime);
        }
        if (title != null){
            entity.like("title",title);
        }
        entity.eq("state",state);
        entity.setSqlSelect("id, article_no, channelId AS channelId, title, views, favorites, image_no, updateTime AS updateTime, createTime AS createTime, createUserUuid AS createUserUuid, updateUserUuid AS updateUserUuid,content, state,summary,likes AS likes");
        entity.orderBy("updateTime",false);
        Page<NewsArticle> newsArticlePage = new Page<>();
        newsArticlePage.setCurrent(pageNo);
        newsArticlePage.setSize(pageSize);
        return selectPage(newsArticlePage,entity);
    }

    @Override
    public List<NewsArticle> listArticleByViewAsc(Integer size) {
        return selectList(new Condition().eq("state",NewsContants.STATUS_PASS).orderBy("views",false).setSqlSelect("id, article_no, channelId AS channelId, title, views, favorites, image_no, updateTime AS updateTime, createTime AS createTime, createUserUuid AS createUserUuid, updateUserUuid AS updateUserUuid, state,summary,likes AS likes"));
    }


}
