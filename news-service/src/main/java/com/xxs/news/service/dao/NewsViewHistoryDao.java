package com.xxs.news.service.dao;

import com.xxs.news.service.entity.NewsViewHistory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 新闻模块-文章浏览记录 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
public interface NewsViewHistoryDao extends BaseMapper<NewsViewHistory> {

}
