package com.xxs.news.service.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 新闻模块-标签
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
@TableName("news_label")
public class NewsLabel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 标签编码
     */
    @TableField(value = "label_no")
    private String labelNo;
    /**
     * 名字
     */
    private String name;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 创建者UUId
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 更新者UUID
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;
    /**
     * 状态 0:正常 1:被删除
     */
    private Integer state;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelNo() {
        return labelNo;
    }

    public void setLabelNo(String labelNo) {
        this.labelNo = labelNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "NewsLabel{" +
        ", id=" + id +
        ", labelNo=" + labelNo +
        ", name=" + name +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", createUserUuid=" + createUserUuid +
        ", updateUserUuid=" + updateUserUuid +
        ", state=" + state +
        "}";
    }
}
