package com.xxs.news.service.oss;

import java.util.List;

/**
 * OOS 文件删除成功返回对象
 */
public class OssDeleteFileResponse {
    /**
     * 删除失败文件
     */
    public List<String> failDeleteList;

    /**
     * 是否成功 1全部成功 2部分成功 0不成功
     */
    public Integer respCode;

    public List<String> getFailDeleteList() {
        return failDeleteList;
    }

    public void setFailDeleteList(List<String> failDeleteList) {
        this.failDeleteList = failDeleteList;
    }

    public Integer getRespCode() {
        return respCode;
    }

    public void setRespCode(Integer respCode) {
        this.respCode = respCode;
    }

}
