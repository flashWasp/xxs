package com.xxs.news.service.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.common.service.RabbitMqMessageManager;
import com.xxs.common.service.RabbitSender;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.exception.ServiceException;
import com.xxs.common.service.model.RabbitMetaMessage;
import com.xxs.common.service.model.template.MailTemplate;
import com.xxs.common.service.model.template.MsgTemplate;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsArticleVersion;
import com.xxs.news.service.entity.request.NewsArticleReq;
import com.xxs.news.service.entity.request.NewsArticleSearchReq;
import com.xxs.news.service.repository.NewsArticleRepository;
import com.xxs.news.service.repository.NewsArticleVersionRepository;
import com.xxs.news.service.service.NewsArticleESService;
import com.xxs.news.service.service.NewsArticleService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service("newArticleService")
public class NewsArticleServiceImpl implements NewsArticleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsArticleServiceImpl.class);
    private static final String NEWSARTICLE_BASE = "newsArticle:base";

    private static final String NEWSARTICLE_HOTVIEW = "newsArticle:hotView";

    @Autowired
    private NewsArticleRepository newsArticleRepository;

    @Autowired
    private NewsArticleVersionRepository newsArticleVersionRepository;

    @Autowired
    private NewsArticleESService newsArticleESService;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    /**
     * 创建（设为草稿）
     */
    private static final String HANDLER_CREATE = "create";

    /**
     * 自动保存
     */
    private static final String HANDLER_UPDATE = "update";

    /**
     * 根据文章编号获取文章
     *
     * @return
     */
    @Override
    public NewsArticle findOne(String aticleNo) {
        return newsArticleRepository.getArticleByNo(aticleNo);
    }

    /**
     * 草稿操作
     *
     * @param request
     * @return
     */
    @Transactional
    @Override
    public Boolean saveOrUpdateArticle(NewsArticleReq request) {
        if (request != null) {
            String articleNo = request.getId();
            String cmd = request.getCmd();
            // 获取数据库中Article对象
            NewsArticle dbArticle = null;
            if (StringUtils.isNotBlank(articleNo)) {
                dbArticle = newsArticleRepository.getArticleByNo(articleNo);
            }
            switch (cmd) {
                case HANDLER_CREATE:
                    if (dbArticle == null) {
                        NewsArticle newsArticle = packInitArticle(request);
                        newsArticleRepository.insert(newsArticle);
                        newsArticleESService.saveNewArticle(newsArticle);
                        CacheHelper.update(NEWSARTICLE_BASE, newsArticle.getArticleNo(), JSONObject.toJSONString(newsArticle));
                        LOGGER.info("添加文章 id:" + newsArticle.getArticleNo());
                        break;
                    }
                    // 自动保存
                case HANDLER_UPDATE:
                    if (dbArticle != null) {
                        NewsArticle article = updateArticle(request, dbArticle);
                        CacheHelper.del(NEWSARTICLE_BASE, articleNo);
                        LOGGER.info("更新文章 id:" + article.getArticleNo());
                        break;
                    }
            }
            return true;
        }
        return false;
    }

    /**
     * 发布文章 状态由草稿变为审核中
     */
    @Override
    public Boolean issueArticle(String id) {
        NewsArticle newsArticle = newsArticleRepository.getArticleByNo(id);
        if (newsArticle != null && newsArticle.getState().equals(NewsContants.STATUS_DRAFT)) {
            // 更新文章状态为审核中
            newsArticle.setState(NewsContants.STATUS_REVIEWING);
            boolean b = newsArticleRepository.updateAllColumnById(newsArticle);
            LOGGER.info("发布文章 id:" + newsArticle.getArticleNo());
            if (b) {
                CacheHelper.del(NEWSARTICLE_BASE, id);
            }
            return b;
        }
        return false;
    }

    /**
     * 审核文章：拒绝或者通过
     * 状态结果为： pass
     * draft
     *
     * @param articleNo
     * @return
     */
    @Transactional
    @Override
    public Boolean reviewArticle(String articleNo, String state) throws JsonProcessingException {
        // 根据文章No获取正在审核的文章对象
        NewsArticle article = newsArticleRepository.getArticleByNo(articleNo);
        if (article != null && article.getState().equals(NewsContants.STATUS_REVIEWING)) {
            // 更新文章状态
            if(state.equals(NewsContants.STATUS_PASS)){
                article.setState(state);
                boolean b1 = newsArticleRepository.updateAllColumnById(article);
                if (b1) {
                    CacheHelper.del(NEWSARTICLE_BASE, articleNo);
                    CacheHelper.del(NEWSARTICLE_HOTVIEW,NEWSARTICLE_HOTVIEW);
                    LOGGER.info("审批文章草稿 id:" + article.getArticleNo());
                }
                // 生成版本
                NewsArticleVersion newsArticleVersion = new NewsArticleVersion();
                BeanUtils.copyProperties(article, newsArticleVersion);
                newsArticleVersion.setState(state);
                // 时间戳为版本号
                newsArticleVersion.setVersion(new Date().toString());
                boolean b2 = newsArticleVersionRepository.insert(newsArticleVersion);
                LOGGER.info("文章审批完成 id:" + newsArticleVersion.getArticleNo() + "生成版本 version: " + newsArticleVersion.getVersion());
                if (b1 && b2) {
                    sendMsg(articleNo,article.getCreateUserUuid());
                    return true;
                }
                throw new ServiceException("文章审批失败 id:" + newsArticleVersion.getArticleNo());
            }
        }
        return false;
    }

    @Override
    public Boolean delete(String articleNo) {
        NewsArticle draft = newsArticleRepository.getArticleByNo(articleNo);
        if (draft != null) {
            draft.setState(NewsContants.STATUS_ISDELETE);
            boolean b = newsArticleRepository.updateAllColumnById(draft);
            if (b) {
                CacheHelper.del(NEWSARTICLE_BASE, articleNo);
                LOGGER.info("审批文章草稿 id:" + articleNo);
            }
            LOGGER.info("删除文章 id:" + draft.getArticleNo());
            return b;
        }
        return false;
    }

    @Override
    public Boolean update(NewsArticle newsArticle) {
        boolean b = newsArticleRepository.updateById(newsArticle);
        if (b) {
            CacheHelper.del(NEWSARTICLE_BASE, newsArticle.getArticleNo());
            CacheHelper.del(NEWSARTICLE_HOTVIEW,NEWSARTICLE_HOTVIEW);
            LOGGER.info("更新文章 id:" + newsArticle.getArticleNo());
        }
        return b;
    }

    /**
     * 根据文章编号分页获取文章版本
     *
     * @param articleNo
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public Page<NewsArticleVersion> pageVersionByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        return newsArticleVersionRepository.listVersionPageByArticleNo(articleNo, pageNo, pageSize);
    }

    /**
     * 多条件查询文章内容
     *
     * @param req
     * @param state
     * @return
     */
    @Override
    public Page<NewsArticle> listNewsArticlePageByReq(NewsArticleSearchReq req, String state) {
        if (req != null) {
            return newsArticleRepository.listNewsArticlePage(
                    req.getChannelId(),
                    "1",
                    state,
                    req.getPage(),
                    req.getRows(),
                    req.getStartTime(),
                    req.getEndTime(),
                    req.getTitle()
            );
        }
        return null;
    }

    @Override
    public List<NewsArticle> hotWebArticle(Integer size) {
        Object cacheObj = CacheHelper.read(NEWSARTICLE_HOTVIEW, NEWSARTICLE_HOTVIEW);
        if (cacheObj != null){
            return JSONObject.parseObject(cacheObj.toString(),List.class);
        }
        List<NewsArticle> newsArticles = newsArticleRepository.listArticleByViewAsc(size);
        if (null != newsArticles) {
            CacheHelper.update(NEWSARTICLE_HOTVIEW,NEWSARTICLE_HOTVIEW,JSONObject.toJSONString(newsArticles));
            return newsArticles;
        }
        return null;
    }

    /**
     * 保存Article
     *
     * @param dbArticle
     * @return
     */
    private NewsArticle updateArticle(NewsArticleReq req, NewsArticle dbArticle) {
        dbArticle.setSummary(req.getSummary());
        dbArticle.setContent(req.getContent());
        dbArticle.setTitle(req.getTitle());
        dbArticle.setImageNo(req.getImageNo());
        dbArticle.setChannelId(req.getChannelId());
        dbArticle.setUpdateTime(new Date());
        System.out.println(TokenRedisRepository.getCurUserUUID());
        dbArticle.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        boolean b = newsArticleRepository.updateById(dbArticle);
        if (b) {
            CacheHelper.del(NEWSARTICLE_BASE, dbArticle.getArticleNo());
            LOGGER.info("更新文章 id:" + dbArticle.getArticleNo());
        }
        return dbArticle;
    }

    /**
     * 封装新建文章的属性
     *
     * @param
     * @return
     */
    private NewsArticle packInitArticle(NewsArticleReq req) {
        NewsArticle article = new NewsArticle();
        article.setChannelId(req.getChannelId());
        article.setArticleNo(req.getId());
        article.setImageNo(req.getImageNo());
        article.setTitle(req.getTitle());
        article.setContent(req.getContent());
        article.setSummary(req.getSummary());
        article.setViews(0);
        article.setArticleNo(KeyUtil.getUniqueKey());
        article.setState(NewsContants.STATUS_DRAFT);
        article.setFavorites(0);
        article.setCreateTime(new Date());
        article.setUpdateTime(new Date());
        article.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        article.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        return article;
    }

    private void sendMsg(String articleNo,String uid) throws JsonProcessingException {
        /** 设置需要传递的消息体 */
        Set<String> set = new HashSet<String>();
        set.add(uid);
        String subject = "文章审核通过";
        String content = "恭喜你！文章编号" + articleNo + "通过审核";
        MsgTemplate msgTemplate = new MsgTemplate();
        msgTemplate.setSubject(subject);
        msgTemplate.setText(content);
        msgTemplate.setTo(set);
        msgTemplate.setMsgType("passArticle");
        msgTemplate.setCmd("edit");
        RabbitMetaMessage rabbitMetaMessage = RabbitMqMessageManager.newMsgMessageInstance(msgTemplate);
        /** 发送消息 */
        RabbitSender.send(rabbitMetaMessage, rabbitTemplate, LOGGER);
    }
}

