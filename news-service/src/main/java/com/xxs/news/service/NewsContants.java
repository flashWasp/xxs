package com.xxs.news.service;

public class NewsContants {
    /**
     * 状态：草稿
     */
    public static final String STATUS_DRAFT = "draft";

    /**
     * 状态：审核中
     */
    public static final String STATUS_REVIEWING = "reviewing";

    /**
     * 状态: 已上线
     */
    public static final String STATUS_PASS = "pass";


    /**
     * 文章删除状态标记位
     */

    public final  static Integer NEWS_NODELETE = 0;
    public final  static Integer NEWS_ISDELETE = 1;
    public final  static String STATUS_ISDELETE = "isDelete";
    public final static String STATUS_NODELETE = "noDelete";


}
