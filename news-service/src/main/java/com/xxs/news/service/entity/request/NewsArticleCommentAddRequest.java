package com.xxs.news.service.entity.request;

/**
 * 文章评论添加请求对象
 */
public class NewsArticleCommentAddRequest {
    /**
     * 文章编码
     */
    private String articleNo;

    /**
     * 文章内容
     */
    private String content;

    /**
     * 接受者编码
     */
    private String receiverNo;

    /**
     * 父评论编码
     */
    private String parentCommentNo;

    /**
     * 发送者名称
     */
    private String senderName;

    /**
     * 接受者名称
     */
    private String receiverName;

    /**
     * 目标回复的评论编号
     */
    private String targetCommentNo;

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReceiverNo() {
        return receiverNo;
    }

    public void setReceiverNo(String receiverNo) {
        this.receiverNo = receiverNo;
    }

    public String getParentCommentNo() {
        return parentCommentNo;
    }

    public void setParentCommentNo(String parentCommentNo) {
        this.parentCommentNo = parentCommentNo;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTargetCommentNo() {
        return targetCommentNo;
    }

    public void setTargetCommentNo(String targetCommentNo) {
        this.targetCommentNo = targetCommentNo;
    }

    @Override
    public String toString() {
        return "NewsArticleCommentAddRequest{" +
                "articleNo='" + articleNo + '\'' +
                ", content='" + content + '\'' +
                ", receiverNo='" + receiverNo + '\'' +
                ", parentCommentNo='" + parentCommentNo + '\'' +
                ", senderName='" + senderName + '\'' +
                ", receiverName='" + receiverName + '\'' +
                '}';
    }
}
