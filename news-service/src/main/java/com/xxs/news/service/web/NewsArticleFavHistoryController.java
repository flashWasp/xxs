package com.xxs.news.service.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.news.service.entity.NewsArticleFavHistory;
import com.xxs.news.service.service.NewsArticleFavHistoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 新闻模块--收藏记录 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@RestController
@RequestMapping("/fav")
public class NewsArticleFavHistoryController {

    @Autowired
    private NewsArticleFavHistoryService newsArticleFavHistoryService;

    /**
     * 添加个人收藏
     * @return
     */
    @ApiOperation("添加个人收藏")
    @GetMapping(value = "/add")
    public AjaxResult addFav(@RequestParam("articleNo") String articleNo){
        String no = newsArticleFavHistoryService.add(articleNo);
        if (no != null && !no.isEmpty()){
            return AjaxResult.getOK(no);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 根据文章编号获取收藏对象
     * @param mapNo
     * @return
     */
    @ApiOperation("根据文章编号获取收藏对象")
    @GetMapping(value = "/get")
    public AjaxResult get(@RequestParam("mapNo") String mapNo){
        NewsArticleFavHistory newsArticleFavHistory = newsArticleFavHistoryService.get(mapNo);
        if (null != newsArticleFavHistory){
            return AjaxResult.getOK(newsArticleFavHistory);
        }
        return AjaxResult.getError(ResultCode.SystemNullPointerException);
    }

    /**
     * 分页获取个人收藏列表
     * @param userNo
     * @param page
     * @param rows
     * @return
     */
    @ApiOperation("分页获取个人收藏列表")
    @GetMapping(value = "/pageFav")
    public AjaxResult pageFav(@RequestParam("userNo") String userNo,
                              @RequestParam("page") Integer page,
                              @RequestParam("rows") Integer rows){
        Page<NewsArticleFavHistory> newsArticleFavHistoryPage = newsArticleFavHistoryService.pageFav(userNo, page, rows);
        if (newsArticleFavHistoryPage != null){
            return AjaxResult.getOK(newsArticleFavHistoryPage);
        }
        return  AjaxResult.getError(ResultCode.SystemNullPointerException);
    }

    /**
     * 根据文章删除收藏
     * @param
     * @return
     */
    @ApiOperation("根据文章删除收藏")
    @GetMapping(value = "/del")
    public AjaxResult del(@RequestParam("articleNo")String articleNo){
        Boolean flag = newsArticleFavHistoryService.del(articleNo);
        if (flag){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }
}

