package com.xxs.news.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsArticleLikeHistory;

/**
 * Author : Fung
 * Date : 2018/11/24 0024 下午 4:13
 * Desc :
 */
public interface NewsArticleLikeHistoryService {

    /**
     * 添加当前点赞
     * @return
     */
    String add(String articleNo);

    /**
     * 获取点赞对象
     * @param mapNo
     * @return
     */
    NewsArticleLikeHistory get(String mapNo);

    /**
     * 分页获取收藏对象
     * @param userNo
     * @param current
     * @param rows
     * @return
     */
    Page<NewsArticleLikeHistory> pageFav(String userNo, Integer current, Integer rows);

    /**
     * 删除当前点赞
     * @return
     */
    Boolean del(String mapNo);

    /**
     * 统计文章点赞个数
     * @param articleNo
     * @return
     */
    Integer countByArticleNo(String articleNo);

    /**
     * 统计个人点赞个数
     * @param userNo
     * @return
     */
    Integer countByUserNo(String userNo);

    /**
     * 根据用户编号 文章标号获取点赞
     * @param userNo
     * @param articleNo
     * @return
     */
    NewsArticleLikeHistory get(String userNo,String articleNo);




}
