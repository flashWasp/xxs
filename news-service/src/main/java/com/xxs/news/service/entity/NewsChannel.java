package com.xxs.news.service.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 新闻模块--频道
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
@TableName("news_channel")
public class NewsChannel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 父Id
     */
    @TableField("parentId")
    private Integer parentId;
    /**
     * 父Id集合 [顶点0，第一层，第二层]
     */
    @TableField("parentIds")
    private String parentIds;
    /**
     * 简介
     */
    private String description;
    /**
     * 名称
     */
    private String name;
    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;
    /**
     * 层级：默认第一层
     */
    private Integer level;
    /**
     * 顶级父类Id
     */
    @TableField("first_parent_Id")
    private Integer firstParentId;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 创建者UUId
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 更新者UUID
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;
    /**
     * 状态 0:正常 1:被删除
     */
    private Integer state;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getFirstParentId() {
        return firstParentId;
    }

    public void setFirstParentId(Integer firstParentId) {
        this.firstParentId = firstParentId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "NewsChannel{" +
        ", id=" + id +
        ", parentId=" + parentId +
        ", parentIds=" + parentIds +
        ", description=" + description +
        ", name=" + name +
        ", level=" + level +
        ", firstParentId=" + firstParentId +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", createUserUuid=" + createUserUuid +
        ", updateUserUuid=" + updateUserUuid +
        ", state=" + state +
        "}";
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
