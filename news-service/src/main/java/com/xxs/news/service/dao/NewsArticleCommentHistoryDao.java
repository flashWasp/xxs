package com.xxs.news.service.dao;

import com.xxs.news.service.entity.NewsArticleCommentHistory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 新闻频道--评论记录 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
public interface NewsArticleCommentHistoryDao extends BaseMapper<NewsArticleCommentHistory> {

}
