package com.xxs.news.service.entity.request;

public class NewsChannelSearchReq extends PageParams {
    /**
     * 频道父级Id
     */
    private Integer parentId;

    private Integer level;

    /**
     * 开始时间
     */
    private String name;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
