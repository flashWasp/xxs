package com.xxs.news.service.repository.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.dao.NewsArticleCommentHistoryDao;
import com.xxs.news.service.entity.NewsArticleCommentHistory;
import com.xxs.news.service.repository.NewsArticleCommentHistoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 新闻频道--评论记录 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Service
public class NewsArticleCommentHistoryRepositoryImpl extends ServiceImpl<NewsArticleCommentHistoryDao, NewsArticleCommentHistory> implements NewsArticleCommentHistoryRepository {

    @Override
    public Page<NewsArticleCommentHistory> pageArticleLevel1CommentHistoryByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        NewsArticleCommentHistory req = new NewsArticleCommentHistory();
        req.setArticleNo(articleNo);
        req.setState(0);
        Page<NewsArticleCommentHistory> page = new Page<NewsArticleCommentHistory>(pageNo, pageSize);
        return (selectPage(page, new EntityWrapper<NewsArticleCommentHistory>(req).isNull("parent_comment_no")));
    }

    @Override
    public Page<NewsArticleCommentHistory> pageByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        NewsArticleCommentHistory req = new NewsArticleCommentHistory();
        req.setArticleNo(articleNo);
        req.setState(0);
        Page<NewsArticleCommentHistory> page = new Page<NewsArticleCommentHistory>(pageNo, pageSize);
        return  selectPage(page,new EntityWrapper<NewsArticleCommentHistory>(req).isNull("parent_comment_no"));
    }

    @Override
    public List<NewsArticleCommentHistory> listByCommentNo(String parentNo) {
        NewsArticleCommentHistory req = new NewsArticleCommentHistory();
        req.setParentCommentNo(parentNo);
        req.setState(0);
        return  selectList(new EntityWrapper<NewsArticleCommentHistory>(req));
    }

    @Override
    public Page<NewsArticleCommentHistory> listArticleCommentHistoryByParentNo(String parentCommentNo, Integer pageNo, Integer pageSize) {
        NewsArticleCommentHistory req = new NewsArticleCommentHistory();
        req.setParentCommentNo(parentCommentNo);
        req.setState(0);
        Page<NewsArticleCommentHistory> page = new Page<NewsArticleCommentHistory>(pageNo, pageSize);
        return selectPage(page,new EntityWrapper<NewsArticleCommentHistory>(req));
    }

    @Override
    public List<NewsArticleCommentHistory> listArticleCommentHistoryByParentNo(String parentCommentNo) {
        NewsArticleCommentHistory req = new NewsArticleCommentHistory();
        req.setParentCommentNo(parentCommentNo);
        req.setState(0);
        return baseMapper.selectList(new EntityWrapper<>(req));
    }

    @Override
    public NewsArticleCommentHistory selectByComentId(String commentNo) {
        NewsArticleCommentHistory req = new NewsArticleCommentHistory();
        req.setCommentNo(commentNo);
        return selectOne(new EntityWrapper<>(req));
    }

}
