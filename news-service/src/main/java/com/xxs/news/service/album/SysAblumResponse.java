package com.xxs.news.service.album;

import com.xxs.news.service.entity.SysAblum;

/**
 * 系统相册返回值
 */
public class SysAblumResponse extends SysAblum {
    /**
     * 文件对象
     */
    private byte[] fileIns;

    public byte[] getFileIns() {
        return fileIns;
    }

    public void setFileIns(byte[] fileIns) {
        this.fileIns = fileIns;
    }
}
