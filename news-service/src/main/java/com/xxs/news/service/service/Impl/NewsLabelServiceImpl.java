package com.xxs.news.service.service.Impl;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.news.service.service.NewsLabelService;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.entity.NewsLabel;
import com.xxs.news.service.repository.NewsLabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 *  标签管理服务
 */
@Service("newsLabelService")
public class NewsLabelServiceImpl implements NewsLabelService {
    @Autowired
    private NewsLabelRepository newsLabelRepository;

    /**
     * 创建标签对象
     * @param name
     * @return
     */
    public String createLabel(String name){
        NewsLabel newsLabel = new NewsLabel();
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        newsLabel.setCreateUserUuid(curUserUUID);
        newsLabel.setName(name);
        newsLabel.setUpdateUserUuid(curUserUUID);
        newsLabel.setLabelNo(KeyUtil.getUniqueKey());
        newsLabel.setState(0);
        newsLabelRepository.insertAllColumn(newsLabel);
        return newsLabel.getLabelNo();
    }
    /**
     * 根据编码获取标签对象
     * @param labelNo
     * @return
     */
    public NewsLabel findLabelByNo(String labelNo){
        return newsLabelRepository.findLabelByNo(labelNo);
    }


    /**
     * 更新标签对象
     * @param labelNo
     * @param name
     * @return
     */
    public Boolean updateLabel(String labelNo,String name){
        NewsLabel newsLabel = newsLabelRepository.findLabelByNo(labelNo);
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        if(newsLabel != null){
            newsLabel.setName(name);
            newsLabel.setUpdateUserUuid(curUserUUID);
            newsLabel.setUpdateTime(new Date());
            return  newsLabelRepository.updateAllColumnById(newsLabel);
        }
        return false;
    }


    /**
     * 删除标签
     * @param labelNo
     * @return
     */
    public Boolean deleteLabelByNo(String labelNo){
        NewsLabel label = newsLabelRepository.findLabelByNo(labelNo);
        if(label != null){
            label.setState(NewsContants.NEWS_ISDELETE);
            newsLabelRepository.updateAllColumnById(label);
            return true;
        }
        return false;
    }

    /**
     * 依据名称去查询Label对象
     * @param name
     * @return
     */
    public List<NewsLabel> listLabelByName(String name){
        return newsLabelRepository.listLabelByName(name);
    }

    /**
     * 获取所有的Label对象
     * @return
     */
    public List<NewsLabel> listAll(){
        return newsLabelRepository.listAll();
    }







}
