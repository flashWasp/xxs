package com.xxs.news.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsArticleCommentHistory;

import java.util.List;

public interface NewsArticleCommentHistorySerivce {

    /**
     * 添加评论记录
     * @param articleNo
     * @param content
     * @param senderNo
     * @param receiverNo
     * @param parentCommentNo
     * @return
     */
    NewsArticleCommentHistory addArticleCommentHistory(String articleNo,String content,
                                                       String senderNo,String receiverNo,
                                                       String parentCommentNo,String senderName,
                                                       String receiverName,String targetCommentNo);


    /**
     * 获取评论对象
     * @param commentNo
     * @return
     */
    NewsArticleCommentHistory findOne(String commentNo);


    /**
     * 删除评论记录
     * @param commentId
     * @return
     */
    Boolean deleteArticleCommentHistory(String commentId);

    /**
     * 根据父类评论获取子类评论（分页）
     * @param parentCommentNo
     * @return
     */
    Page<NewsArticleCommentHistory> listArticleCommentHistoryByParentNo(String parentCommentNo,Integer pageNo,Integer pageSize);

    /**
     * 根据文章Id获取评论集合（分页）
     * @param articleNo
     * @return
     */
    Page<NewsArticleCommentHistory> pageByArticleNo(String articleNo, Integer pageNo, Integer pageSize);

    /**
     *
     * @param parentNo
     * @return
     */
    List<NewsArticleCommentHistory> listByCommentNo(String parentNo);
}
