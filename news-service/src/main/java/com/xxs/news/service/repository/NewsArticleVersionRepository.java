package com.xxs.news.service.repository;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.news.service.entity.NewsArticleVersion;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-11-02
 */
public interface NewsArticleVersionRepository extends IService<NewsArticleVersion> {

    Page<NewsArticleVersion> listVersionPageByArticleNo(String articleNo, Integer pageNo, Integer pageSize);
}
