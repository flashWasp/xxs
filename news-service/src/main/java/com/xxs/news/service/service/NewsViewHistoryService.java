package com.xxs.news.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsViewHistory;

import java.util.List;

/**
 * 浏览历史记录
 */
public interface NewsViewHistoryService {

    /**
     * 添加浏览对象
     * @return
     */
    Boolean addView(String articleNo,String viewerNo);

    /**
     * 根据浏览者分页获取浏览记录
     * @return
     */
    Page<NewsViewHistory> pageViews(String viewer,Integer current,Integer rows);

    /**
     * 根据映射编码删除对象
     * @param mapNo
     * @return
     */
    Boolean del(String mapNo);

    /**
     * 根据编码获取对象
     * @param mapNo
     * @return
     */
    NewsViewHistory get(String mapNo);

}
