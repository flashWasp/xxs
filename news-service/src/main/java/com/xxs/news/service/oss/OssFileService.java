package com.xxs.news.service.oss;

import java.util.List;

/**
 *  Object Storage Service 对象存储服务
 */
public interface OssFileService {

    /**
     * 文件上传
     * @param ossUploadFileRequest
     * @return
     */
    Long uploadObject(OssUploadFileRequest ossUploadFileRequest);

    /**
     *  删除文件
     * @return
     */
    OssDeleteFileResponse deleteObject(OssQueryFileRequest ossQueryFileRequest);

    /**
     * 批量删除文件
     * @param ossBatchQueryFileRequest
     * @return
     */
    OssDeleteFileResponse batchDeleteObject(OssBatchQueryFileRequest ossBatchQueryFileRequest);


    OssQueryFileResponse getObject(OssQueryFileRequest ossQueryFileRequest);


    List<OssQueryFileResponse> getObjects(OssQueryFileRequest ossQueryFileRequest);

}
