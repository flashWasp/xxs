package com.xxs.news.service.entity.request;

/**
 * 请求对象 -- 新闻文章创建/更新请求
 */
public class NewsArticleReq {

    /**
     * 文章名
     */
    private String title;

    /**
     * 文章内容
     */
    private String content;

    /**
     * 文章编号
     */
    private String id;

    /**
     * 文章封面图片no
     */
    private String imageNo;

    /**
     * 简介
     */
    private String  summary;


    /**
     * 频道Id
     */
    private Integer channelId;


    /**
     * 处理方式
     * 保存 ： save
     * 提交 ： publish
     */
    private String cmd;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }


    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }


}
