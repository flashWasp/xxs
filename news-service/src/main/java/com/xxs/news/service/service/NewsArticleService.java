package com.xxs.news.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsArticleVersion;
import com.xxs.news.service.entity.request.NewsArticleReq;
import com.xxs.news.service.entity.request.NewsArticleSearchReq;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface NewsArticleService {

    /**
     * 根据文章编号获取文章
     *
     * @return
     */
    public NewsArticle findOne(String aticleNo);

    /**
     * 草稿操作
     *
     * @param request
     * @return
     */
    @Transactional
    public Boolean saveOrUpdateArticle(NewsArticleReq request);

    /**
     * 发布文章 状态由草稿变为审核中
     */
    public Boolean issueArticle(String id) ;

    /**
     * 审核文章：拒绝或者通过
     * 状态结果为： pass
     *             draft
     * @param articleNo
     * @return
     */
    public Boolean reviewArticle(String articleNo, String state) throws JsonProcessingException;

    Boolean delete(String articleNo);


    Boolean update(NewsArticle newsArticle);

    /**
     * 根据文章编号分页获取文章版本
     * @param articleNo
     * @param pageNo
     * @param pageSize
     * @return
     */
     Page<NewsArticleVersion> pageVersionByArticleNo(String articleNo, Integer pageNo, Integer pageSize);

    /**
     * 多条件查询文章内容
     * @param req
     * @param state
     * @return
     */
    Page<NewsArticle> listNewsArticlePageByReq(NewsArticleSearchReq req, String state);

    /**
     * 获取最热的文章集合
     * @param size
     * @return
     */
    List<NewsArticle> hotWebArticle(Integer size);
}
