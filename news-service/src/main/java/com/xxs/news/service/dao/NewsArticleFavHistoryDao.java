package com.xxs.news.service.dao;

import com.xxs.news.service.entity.NewsArticleFavHistory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 新闻模块--收藏记录 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
public interface NewsArticleFavHistoryDao extends BaseMapper<NewsArticleFavHistory> {

}
