package com.xxs.news.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsChannel;
import com.xxs.news.service.entity.request.NewsArticleSearchReq;
import com.xxs.news.service.entity.request.NewsChannelSearchReq;

import java.util.List;

public interface NewsChannelService {

    /**
     * 创建新频道
     *
     * @param name
     * @param description
     * @param parentId
     * @return
     */
    Integer createNewChannel(String name, String description, Integer parentId, Integer sort);

    /**
     * 更新频道
     *
     * @param name
     * @param description
     * @param parentId
     * @return
     */
    Boolean updateNewsChannel(Integer id, String name, String description, Integer parentId, Integer sort);

    /**
     * 根据Id获取新闻频道对象
     *
     * @param id
     * @return
     */
    NewsChannel findNewsChannelById(Integer id);

    /**
     * 根据Id获取父级Id集合
     *
     * @param id
     * @return
     */
    String findParentIdsStrById(Integer id);

    /**
     * 获取所有一级新闻频道对象
     *
     * @return
     */
    List<NewsChannel> listFirstLevelNewsChannel();

    /**
     * 根据父级Id获取子级新闻频道对象
     *
     * @param id
     * @return
     */
    List<NewsChannel> listChildNewsChannelById(Integer id);

    /**
     * 根据父级Id获取子级Id集合
     * @param id
     * @return
     */
    List<Integer> listChildIdsById(Integer id);

    /**
     * 根据父级Id获取子级新闻频道列表
     *
     * @param parentId
     * @return
     */
    List<NewsChannel> listChannel(Integer parentId);



    /**
     * 删除频道对象
     * @param id
     * @return
     */

    Boolean deleteNewChannel(Integer id);

    /**
     * 分页查询
     * @param req
     * @return
     */
    Page<NewsChannel> pageChannel(NewsChannelSearchReq req);

}
