package com.xxs.news.service.web;

import com.xxs.news.service.album.SysAblumResponse;
import com.xxs.news.service.service.SysAblumService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@RestController
@Api("图片接口")
public class SysAblumController {

    //  暂时写死
    private final static String URL_PREFIX = "http://localhost:8081/xxs-news-server/findSysAblum?imageNo=";

    @Autowired
    private SysAblumService sysAblumService;

    @Autowired
    private MongoTemplate mongoTemplate;
    @RequestMapping("/uploadSysAblum")
    public Object uploadSysAblum(MultipartFile file) throws Exception {
        HashMap<String,String> map = new HashMap<>();
        String imageNo = sysAblumService.addAblum(file, "1", 1d, 0d, 0d, 100D, 100D);
        map.put("imageNo",imageNo);
        map.put("imageUrl",URL_PREFIX + imageNo);
        return  map;
    }

    @RequestMapping("/findSysAblum")
    public void findSysAblum(String imageNo, HttpServletResponse response) throws Exception {
        byte[] b = new byte[2048];
        int len;
        try {
            SysAblumResponse ablum = sysAblumService.findAblum(imageNo);
            response.reset();
            response.setContentType("image/jpeg");
            response.setDateHeader("expries",-1);
            response.setHeader("Cache-Control","no-cache");
            response.setHeader("Pragma","no-cache");
            response.getOutputStream().write(ablum.getFileIns());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
