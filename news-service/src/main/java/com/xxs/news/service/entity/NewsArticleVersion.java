package com.xxs.news.service.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-11-02
 */

public class NewsArticleVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 文章编号
     */
    private String articleNo;
    /**
     * 频道Id
     */
    @TableField("channelId")
    private Integer channelId;
    /**
     * 版本 最低是1
     */
    private String version;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 创建者UUId
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 更新者UUID
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;
    /**
     * 状态 reject  pass
     */
    private String state;
    /**
     * 文章内容
     */
    private String content;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "NewsArticleVersion{" +
        ", id=" + id +
        ", articleNo=" + articleNo +
        ", channelId=" + channelId +
        ", version=" + version +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", createUserUuid=" + createUserUuid +
        ", updateUserUuid=" + updateUserUuid +
        ", state=" + state +
        ", content=" + content +
        "}";
    }
}
