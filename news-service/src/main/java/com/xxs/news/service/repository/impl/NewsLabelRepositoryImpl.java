package com.xxs.news.service.repository.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.xxs.news.service.entity.NewsLabel;
import com.xxs.news.service.dao.NewsLabelDao;
import com.xxs.news.service.repository.NewsLabelRepository;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 新闻模块-标签 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Service
public class NewsLabelRepositoryImpl extends ServiceImpl<NewsLabelDao, NewsLabel> implements NewsLabelRepository {

    @Override
    public NewsLabel findLabelByNo(String labelNo) {
        NewsLabel newsLabelCondition = new NewsLabel();
        newsLabelCondition.setLabelNo(labelNo);
        return selectOne(new EntityWrapper<NewsLabel>(newsLabelCondition));
    }

    @Override
    public List<NewsLabel> listLabelByName(String name) {
        return selectList(new EntityWrapper<NewsLabel>(new NewsLabel()).like("name",name));
    }

    @Override
    public List<NewsLabel> listAll() {
        return selectList(new EntityWrapper<NewsLabel>(new NewsLabel()));
    }
}
