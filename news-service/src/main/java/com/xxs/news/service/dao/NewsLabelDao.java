package com.xxs.news.service.dao;

import com.xxs.news.service.entity.NewsLabel;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 新闻模块-标签 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
public interface NewsLabelDao extends BaseMapper<NewsLabel> {

}
