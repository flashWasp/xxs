package com.xxs.news.service.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.news.service.entity.NewsViewHistory;
import com.xxs.news.service.repository.NewsViewHistoryRepository;
import com.xxs.news.service.service.NewsViewHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("newsViewHistoryService")
public class NewsViewHistoryServiceImpl implements NewsViewHistoryService {
    @Autowired
    private NewsViewHistoryRepository newsViewHistoryRepository;

    @Override
    public Boolean addView(String articleNo, String viewerNo) {
        NewsViewHistory newsViewHistory = new NewsViewHistory();
        newsViewHistory.setArticleNo(articleNo);
        newsViewHistory.setViewerNo(viewerNo);
        newsViewHistory.setMapNo(KeyUtil.getUniqueKey());
        newsViewHistory.setCreateTime(new Date());
        newsViewHistory.setUpdateTime(new Date());
        newsViewHistory.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        newsViewHistory.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        return newsViewHistoryRepository.insert(newsViewHistory);
    }

    @Override
    public Page<NewsViewHistory> pageViews(String viewer, Integer current, Integer rows) {
        return newsViewHistoryRepository.pageViews(viewer,current,rows) ;
    }

    @Override
    public Boolean del(String mapNo) {
        return newsViewHistoryRepository.del(mapNo);
    }

    @Override
    public NewsViewHistory get(String mapNo) {
        return newsViewHistoryRepository.get(mapNo);
    }
}
