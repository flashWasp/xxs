package com.xxs.news.service.repository;

import com.xxs.news.service.entity.NewsArticle;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author Administrator
 * @Title: NewsArticleESRepository
 * @ProjectName master
 * @Description:  ES搜索下的文章存储服务
 * @date 2018/12/1722:20
 */
public interface NewsArticleESRepository extends ElasticsearchRepository<NewsArticle,Long> {



}
