package com.xxs.news.service.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.news.service.entity.NewsChannel;
import com.xxs.news.service.entity.request.NewsChannelCreateReq;
import com.xxs.news.service.entity.request.NewsChannelSearchReq;
import com.xxs.news.service.entity.request.NewsChannelUpdateReq;
import com.xxs.news.service.service.Impl.NewsChannelServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 新闻模块--频道 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Api("新闻频道管理")
@RestController
@RequestMapping("/newsChannel")
public class NewsChannelController {
    @Autowired
    private NewsChannelServiceImpl newsChannelServiceImpl;

    /**
     * 创建新频道
     *
     * @return
     */
    @ApiOperation(value = "新建新闻频道")
    @RequestMapping(value = "/createChannel",method=RequestMethod.POST)
    public Object createNewsChannel(@RequestBody NewsChannelCreateReq  channelCreateReq) {
        Integer newChannelId = newsChannelServiceImpl.createNewChannel(
                channelCreateReq.getName(),
                channelCreateReq.getDescription(),
                channelCreateReq.getParentId(),
                channelCreateReq.getSort());
        if (newChannelId != null) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


    /**
     * 获取所有一级频道对象
     *
     * @return
     */
    @ApiOperation(value = "获取所有一级频道对象")
    @GetMapping("/listFirstNewsChannel")
    public Object listFirstNewChannel() {
        List<NewsChannel> newsChannels =
                newsChannelServiceImpl.listFirstLevelNewsChannel();
        return AjaxResult.getOK(newsChannels);
    }

    /**
     * 根据父级Id获取子级对象
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/listChildById")
    @ApiOperation(value ="根据Id获取子级对象")
    public Object listChildById(@RequestParam(value = "id") Integer id) {
        List<NewsChannel> newsChannels = newsChannelServiceImpl.listChildNewsChannelById(id);
        return AjaxResult.getOK(newsChannels);
    }


    /**
     * 获取所有一级频道对象
     * auth wjy 2018-11-08 11:26
     * @param parentId 父级id
     * @return
     */
    @ApiOperation(value = "获取所有一级频道对象")
    @RequestMapping(value = "/listChannel",method=RequestMethod.GET)
    public Object listChannel(@RequestParam(value = "parentId")  Integer parentId) {
        List<NewsChannel> newsChannels =
                newsChannelServiceImpl.listChannel(parentId);
        return AjaxResult.getOK(newsChannels);
    }

    /**
     * 频道分页查询
     * @param req
     * @return
     */
    @ApiOperation(value =  "频道分页查询")
    @RequestMapping(value = "/pageChannel",method = RequestMethod.POST)
    public  AjaxResult pageArticle(@RequestBody  NewsChannelSearchReq req){
        Page<NewsChannel> newsChannelPage =
                newsChannelServiceImpl.pageChannel(req);
        if (newsChannelPage != null){
            return AjaxResult.getOK(newsChannelPage);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 更新频道对象
     *
     */
    @ApiOperation("更新频道对象")
    @RequestMapping(value = "/updateChannel",method=RequestMethod.POST)
    public Object updateNewsChannel(@RequestBody  NewsChannelUpdateReq req) {
        Boolean flag = newsChannelServiceImpl.updateNewsChannel(
                req.getId(),
                req.getName(),
                req.getDescription(),
                req.getParentId(),
                req.getSort());
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 根据Id删除频道对象
     *
     * @param id
     * @return
     */
    @ApiOperation("根据Id删除频道对象")
    @GetMapping("/deleteChannel")
    public Object deleteNewChannel(@RequestParam("id") Integer id) {
        if (newsChannelServiceImpl.deleteNewChannel(id)) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("获取频道详情")
    @GetMapping("/detail")
    public AjaxResult detail(@RequestParam("id") Integer id){
        NewsChannel newsChannel = newsChannelServiceImpl.findNewsChannelById(id);
        if (null != newsChannel){
            return AjaxResult.getOK(newsChannel);
        }
        return AjaxResult.getError(ResultCode.SystemNullPointerException);
    }

}

