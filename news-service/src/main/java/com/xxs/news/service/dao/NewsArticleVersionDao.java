package com.xxs.news.service.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.news.service.entity.NewsArticleVersion;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-11-02
 */
public interface NewsArticleVersionDao extends BaseMapper<NewsArticleVersion> {

}
