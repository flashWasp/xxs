package com.xxs.news.service.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 新闻频道--评论记录
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
@TableName("news_article_comment_history")
public class NewsArticleCommentHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 评论编码
     */
    @TableField("comment_no")
    private String commentNo;
    /**
     * 文章编码
     */
    @TableField("article_no")
    private String articleNo;
    /**
     * 评论内容内容
     */
    private String content;
    /**
     * 发送者编码
     */
    @TableField("sender_no")
    private String senderNo;
    /**
     * 接受者编码
     */
    @TableField("recevier_no")
    private String recevierNo;

    @TableField("sender_name")
    private String senderName;

    @TableField("recevier_name")
    private String  recevierName;

    /**
     * 父评论编码
     */
    @TableField("parent_comment_no")
    private String parentCommentNo;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 创建者UUId
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 更新者UUID
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;

    /**
     * 状态 0:正常 1:被删除
     */
    private Integer state;

    /**
     * 目标回复的评论编号
     */
    @TableField("target_comment_no")
    private String targetCommentNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommentNo() {
        return commentNo;
    }

    public void setCommentNo(String commentNo) {
        this.commentNo = commentNo;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderNo() {
        return senderNo;
    }

    public void setSenderNo(String senderNo) {
        this.senderNo = senderNo;
    }

    public String getRecevierNo() {
        return recevierNo;
    }

    public void setRecevierNo(String recevierNo) {
        this.recevierNo = recevierNo;
    }

    public String getParentCommentNo() {
        return parentCommentNo;
    }

    public void setParentCommentNo(String parentCommentNo) {
        this.parentCommentNo = parentCommentNo;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRecevierName() {
        return recevierName;
    }

    public void setRecevierName(String recevierName) {
        this.recevierName = recevierName;
    }

    public String getTargetCommentNo() {
        return targetCommentNo;
    }

    public void setTargetCommentNo(String targetCommentNo) {
        this.targetCommentNo = targetCommentNo;
    }

    @Override
    public String toString() {
        return "NewsArticleCommentHistory{" +
                "id=" + id +
                ", commentNo='" + commentNo + '\'' +
                ", articleNo='" + articleNo + '\'' +
                ", content='" + content + '\'' +
                ", senderNo='" + senderNo + '\'' +
                ", recevierNo='" + recevierNo + '\'' +
                ", senderName='" + senderName + '\'' +
                ", recevierName='" + recevierName + '\'' +
                ", parentCommentNo='" + parentCommentNo + '\'' +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                ", createUserUuid='" + createUserUuid + '\'' +
                ", updateUserUuid='" + updateUserUuid + '\'' +
                ", state=" + state +
                ", targetCommentNo='" + targetCommentNo + '\'' +
                '}';
    }
}
