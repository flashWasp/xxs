package com.xxs.news.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsArticleFavHistory;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 新闻模块--收藏记录 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
public interface NewsArticleFavHistoryRepository extends IService<NewsArticleFavHistory> {

    /**
     * 分页获取收藏对象
     * @param userNo
     * @param current
     * @param rows
     * @return
     */
    Page<NewsArticleFavHistory> pageFav(String userNo, Integer current, Integer rows);

    /**
     * 根据收藏编码获取对象
     * @param mapNo
     * @return
     */
    NewsArticleFavHistory selectByMapNo(String mapNo);

    /**
     * 根据文章编号统计数量
     * @param articleNo
     * @return
     */
    Integer selectCountByArticleNo(String articleNo);

    /**
     * 根据收藏编码删除收藏记录对象
     * @param mapNo
     * @return
     */
    Boolean del(String mapNo);

    /**
     * 根据用户编号 文章编号获取收藏编码对象
     * @param userNo
     * @param articleNo
     * @return
     */
    NewsArticleFavHistory selectOne(String userNo, String articleNo);
}
