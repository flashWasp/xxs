package com.xxs.news.service.repository.impl;


import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.dao.NewsArticleLikeHistoryDao;
import com.xxs.news.service.entity.NewsArticleLikeHistory;
import com.xxs.news.service.repository.NewsArticleLikeHistoryRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新闻模块--点赞记录 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-11-24
 */
@Service
public class NewsArticleLikeHistoryRepositoryImpl extends ServiceImpl<NewsArticleLikeHistoryDao, NewsArticleLikeHistory> implements NewsArticleLikeHistoryRepository {
    @Override
    public Page<NewsArticleLikeHistory> pageFav(String userNo, Integer current, Integer rows) {
        NewsArticleLikeHistory newsArticleLikeHistory = new NewsArticleLikeHistory();
        newsArticleLikeHistory.setUserNo(userNo);
        newsArticleLikeHistory.setState(0);
        Page<NewsArticleLikeHistory> page = new Page<NewsArticleLikeHistory>(current, rows);
        return selectPage(page,new EntityWrapper<NewsArticleLikeHistory>(newsArticleLikeHistory));
    }

    @Override
    public NewsArticleLikeHistory selectByMapNo(String mapNo) {
        return selectOne(new Condition().eq("map_no",mapNo).eq("state",0));
    }

    @Override
    public Integer selectCountByArticleNo(String articleNo) {
        NewsArticleLikeHistory newsArticleLikeHistory = new NewsArticleLikeHistory();
        newsArticleLikeHistory.setArticleNo(articleNo);
        newsArticleLikeHistory.setState(0);
        return selectCount(new EntityWrapper<NewsArticleLikeHistory>(newsArticleLikeHistory));
    }

    @Override
    public Boolean del(String mapNo) {
        return delete(new Condition().eq("map_no",mapNo));
    }

    @Override
    public NewsArticleLikeHistory selectOne(String userNo, String articleNo) {
        return selectOne(new Condition().eq("article_no",articleNo).eq("user_no",userNo));
    }

    @Override
    public Integer selectCountByUserNo(String userNo) {
        NewsArticleLikeHistory newsArticleLikeHistory = new NewsArticleLikeHistory();
        newsArticleLikeHistory.setUserNo(userNo);
        newsArticleLikeHistory.setState(0);
        return selectCount(new EntityWrapper<NewsArticleLikeHistory>(newsArticleLikeHistory));
    }

}
