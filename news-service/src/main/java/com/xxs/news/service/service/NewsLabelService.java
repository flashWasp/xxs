package com.xxs.news.service.service;

import com.xxs.news.service.entity.NewsLabel;

import java.util.List;

public interface NewsLabelService {

    /**
     * 创建标签对象
     * @param name
     * @return
     */
    String createLabel(String name);
    /**
     * 根据编码获取标签对象
     * @param labelNo
     * @return
     */
    NewsLabel findLabelByNo(String labelNo);


    /**
     * 更新标签对象
     * @param labelNo
     * @param name
     * @return
     */
    Boolean updateLabel(String labelNo,String name);


    /**
     * 删除标签
     * @param labelNo
     * @return
     */
    Boolean deleteLabelByNo(String labelNo);

    /**
     * 依据名称去查询Label对象
     * @param name
     * @return
     */
    List<NewsLabel> listLabelByName(String name);
    /**
     * 获取所有的Label对象
     * @return
     */
    List<NewsLabel> listAll();


}
