package com.xxs.news.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

/**
 * Author : Fung
 * Date : 2018/11/27 0027 下午 1:48
 * Desc :
 */

@Component("SessionDefaultRedisSerializer")
public class SessionDefaultRedisSerializer extends JdkSerializationRedisSerializer {
    private static final Logger LOG = LoggerFactory.getLogger(SessionDefaultRedisSerializer.class);

    public Object deserialize(@Nullable byte[] bytes) {
        Object deserialObj = null;
        try
        {
            deserialObj =  super.deserialize(bytes);
        }
        catch(Exception e)
        {
            LOG.warn("deserialize session Object error!", e);
        }
        return deserialObj;
    }





}
