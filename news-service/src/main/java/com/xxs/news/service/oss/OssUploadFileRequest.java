package com.xxs.news.service.oss;

import org.springframework.web.multipart.MultipartFile;

/**
 * OSS上传请求
 */
public class OssUploadFileRequest extends  OssTokenRequest{


    /**
     * 前缀Url
     */
    private String prefixUrl;

    /**
     * 文件对象
     */
    private MultipartFile file;

    /**
     * 上传用户Id
     */
    private Long userId;


    public String getPrefixUrl() {
        return prefixUrl;
    }

    public void setPrefixUrl(String prefixUrl) {
        this.prefixUrl = prefixUrl;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
