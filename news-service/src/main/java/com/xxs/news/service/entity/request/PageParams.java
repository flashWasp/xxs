package com.xxs.news.service.entity.request;

/**
 * 分页查询 公用类 wjy
 */
public class PageParams {

    /**
     * 当前页
     */
    private Integer page;
    /**
     * 页大小
     */
    private Integer rows;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
}
