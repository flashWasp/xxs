package com.xxs.news.service.oss;

/**
 * 请求父类
 */
public class OssTokenRequest {
    /**
     * 内存块
     */
    private String bulket;


    public String getBulket() {
        return bulket;
    }

    public void setBulket(String bulket) {
        this.bulket = bulket;
    }
}
