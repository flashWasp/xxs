package com.xxs.news.service.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.news.service.entity.NewsViewHistory;
import com.xxs.news.service.service.NewsViewHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 新闻模块-文章浏览记录 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Api("文章浏览记录")
@RestController
@RequestMapping("/newsViewHistory")
public class NewsViewHistoryController {

    @Autowired
    private NewsViewHistoryService newsViewHistoryService;

    /**
     * 根据用户分页获取浏览记录
     */
    @ApiOperation("根据用户分页获取浏览记录")
    @GetMapping(value = "/pageViewHistory")
    public AjaxResult pageViewHistory(@RequestParam("viewer") String viewer,
                                      @RequestParam("page") Integer page,
                                      @RequestParam("rows ") Integer rows){
        Page<NewsViewHistory> newsViewHistoryPage = newsViewHistoryService.pageViews(viewer, page, rows);
        if (null != newsViewHistoryPage){
            return  AjaxResult.getOK(newsViewHistoryPage);
        }
        return  AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 利用浏览记录id获取详细信息
     */
    @ApiOperation("删除浏览记录对象")
    @GetMapping(value = "/del")
    public AjaxResult del(@RequestParam("mapNo") String mapNo) {
        Boolean flag = newsViewHistoryService.del(mapNo);
        if (flag){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 获取浏览记录对象
     * @param mapNo
     * @return
     */
    @ApiOperation("获取浏览记录对象")
    @GetMapping(value = "/get")
    public AjaxResult get(@RequestParam("mapNo") String mapNo){
        NewsViewHistory newsViewHistory = newsViewHistoryService.get(mapNo);
        if (null != newsViewHistory){
            return AjaxResult.getOK(newsViewHistory);
        }
        return  AjaxResult.getError(ResultCode.ParamException);
    }

}

