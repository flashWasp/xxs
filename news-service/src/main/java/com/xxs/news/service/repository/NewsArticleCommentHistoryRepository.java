package com.xxs.news.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.news.service.entity.NewsArticleCommentHistory;

import java.util.List;

/**
 * <p>
 * 新闻频道--评论记录 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
public interface NewsArticleCommentHistoryRepository extends IService<NewsArticleCommentHistory> {

    Page<NewsArticleCommentHistory> pageArticleLevel1CommentHistoryByArticleNo(String articleNo, Integer pageNo, Integer pageSize);

    Page<NewsArticleCommentHistory> pageByArticleNo(String articleNo, Integer pageNo, Integer pageSize);

//    List<NewsArticleCommentHistory> listByArticleNo(String articleNo);

    Page<NewsArticleCommentHistory> listArticleCommentHistoryByParentNo(String parentCommentNo,Integer pageNo,Integer pageSize);

    List<NewsArticleCommentHistory> listArticleCommentHistoryByParentNo(String parentCommentNo);

    NewsArticleCommentHistory selectByComentId(String commentNo);


    List<NewsArticleCommentHistory> listByCommentNo(String parentNo);
}
