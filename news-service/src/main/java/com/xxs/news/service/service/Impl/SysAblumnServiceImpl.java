package com.xxs.news.service.service.Impl;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.album.MongoDbRepository;
import com.xxs.news.service.album.SysAblumResponse;
import com.xxs.news.service.service.SysAblumService;
import com.xxs.news.service.entity.SysAblum;
import com.xxs.news.service.repository.SysAblumRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.Date;

@Service
public class SysAblumnServiceImpl implements SysAblumService {

    @Autowired
    private SysAblumRepository sysAblumRepository;

    @Autowired
    private MongoDbRepository mongoDbRepository;


    /**
     * 图片上传大小限制，单位是M(2M)
     */
    public static Integer photoSize = 2;

    /**
     * 新增图片
     *
     * @return
     */
    @Transactional
    public String addAblum(MultipartFile img, String bulket, Double bili, Double x, Double y, Double w, Double h) throws Exception {
//        AbstractUploadImgManager.UploadImageBuilder uploadImageBuilder = new AbstractUploadImgManager.UploadImageBuilder(img, photoSize, img.getInputStream(),
//                bili, x, y, w, h
//        );
        //(ByteArrayInputStream) new SimpleUploadImgManager(uploadImageBuilder).uploadImage()
        InputStream is =  img.getInputStream();

        String fileId = mongoDbRepository.uploadFile(img,is);
        SysAblum sysAblum = packSysablum(bulket, img.getOriginalFilename(), fileId);
        sysAblumRepository.insert(sysAblum);
        return sysAblum.getImageNo();
    }





    /**
     * 根据编号获取图片对象
     *
     * @param imageNo
     * @return
     */
    @Transactional
    public SysAblumResponse findAblum(String imageNo) throws Exception {
        SysAblumResponse sysAblumResponse = new SysAblumResponse();
        SysAblum sysAblum = sysAblumRepository.selectByImageNo(imageNo);
        if (sysAblum != null) {
            BeanUtils.copyProperties(sysAblum, sysAblumResponse);
            sysAblumResponse.setFileIns(mongoDbRepository.findFile(imageNo).getIns());
            return sysAblumResponse;
        }
        return null;
    }

    /**
     * 根据编号删除图片对象
     *
     * @param imageNo
     * @return
     */
    @Transactional
    public Boolean deleteAblum(String imageNo) {
        SysAblum sysAblum = sysAblumRepository.selectByImageNo(imageNo);
        if (sysAblum != null) {
            sysAblum.setState(NewsContants.NEWS_ISDELETE);
            sysAblum.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
            sysAblum.setUpdateTime(new Date());
            return sysAblumRepository.updateAllColumnById(sysAblum);
        }
        return false;
    }

//    /**
//     * 获取图片集合对象
//     *
//     * @return
//     */
//    public List<SysAblumResponse> listAblum(List<String> imageNos) throws Exception {
//        List<SysAblum> sysAblums = sysAblumRepository.listByImageNos(imageNos);
//        List<GridFSDBFile> files = mongoDbRepository.listFileIns(imageNos);
//        List<SysAblumResponse> sysAblumResponses = new ArrayList<SysAblumResponse>();
//        if (files != null && !files.isEmpty()) {
//            if (sysAblums != null) {
//                for (GridFSDBFile file : files) {
//                    for (SysAblum sysAblum : sysAblums) {
//                        if (sysAblum != null && file != null && file.getId() == sysAblum.getImageNo()) {
//                            SysAblumResponse sysAblumResponse = new SysAblumResponse();
//                            BeanUtils.copyProperties(sysAblum, sysAblumResponse);
//                            sysAblumResponse.setFileIns(file.getInputStream());
//                            sysAblumResponses.add(sysAblumResponse);
//                        }
//                    }
//                }
//            }
//            return sysAblumResponses;
//        }
//        return null;
//    }


    /*============ Mysql ===============*/
    private SysAblum packSysablum(String bulket, String imageName, String fileId) {
        SysAblum sysAblum = new SysAblum();
        sysAblum.setCreateTime(new Date());
        sysAblum.setUpdateTime(new Date());
        sysAblum.setBulket(bulket);
        sysAblum.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysAblum.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysAblum.setImageName(imageName);
        sysAblum.setState(NewsContants.NEWS_NODELETE);
        sysAblum.setImageNo(fileId);
        return sysAblum;
    }

}
