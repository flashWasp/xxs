package com.xxs.news.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsChannel;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.news.service.entity.request.NewsChannelSearchReq;

import java.util.List;

/**
 * <p>
 * 新闻模块--频道 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
public interface NewsChannelRepository extends IService<NewsChannel> {

    /**
     * 频道分页查询
     */
    Page<NewsChannel> pageChannel(NewsChannelSearchReq req);
}
