package com.xxs.news.service.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.news.service.entity.NewsArticleCommentHistory;
import com.xxs.news.service.entity.request.NewsArticleCommentAddRequest;
import com.xxs.news.service.entity.response.CommentResponse;
import com.xxs.news.service.service.NewsArticleCommentHistorySerivce;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 新闻频道--评论记录 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Api("新闻频道--评论记录")
@RestController
@RequestMapping("/newsArticleComment")
public class NewsArticleCommentController {
    @Autowired
    private NewsArticleCommentHistorySerivce newsArticleCommentHistorySerivce;

    @ApiOperation("添加评论对象")
    @PostMapping("/createComment")
    public AjaxResult addNewsArticleComment(@RequestBody NewsArticleCommentAddRequest request) {
        NewsArticleCommentHistory newsArticleCommentHistory = newsArticleCommentHistorySerivce.addArticleCommentHistory(
                request.getArticleNo(),
                request.getContent(),
                TokenRedisRepository.getCurUserUUID(),
                request.getReceiverNo(),
                request.getParentCommentNo(),
                request.getSenderName(),
                request.getReceiverName(),
                request.getTargetCommentNo()
                );
        if (newsArticleCommentHistory != null) {
            return AjaxResult.getOK(newsArticleCommentHistory);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("根据编号获取评论对象")
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Object get(@RequestParam("commentNo") String commentNo) {
        NewsArticleCommentHistory comment = newsArticleCommentHistorySerivce.findOne(commentNo);
        if (comment != null) {
            return AjaxResult.getOK(comment);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }



    @ApiOperation("根据编号删除评论对象")
    @RequestMapping(value = "/deleteOne", method = RequestMethod.GET)
    public Object deleteOne(@RequestParam("commentNo") String commentNo) {
        Boolean flag = newsArticleCommentHistorySerivce.deleteArticleCommentHistory(commentNo);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("根据文章获取其所有评论")
    @RequestMapping(value = "/pageCommentByArticleNo", method = RequestMethod.GET)
    public Object pageCommentByArticleNo(@RequestParam(value = "articleNo") String articleNo, @RequestParam(value = "pageNo") Integer pageNo, @RequestParam(value = "pageSize") Integer pageSize) throws InvocationTargetException, IllegalAccessException {
        Page<NewsArticleCommentHistory> parent = newsArticleCommentHistorySerivce.pageByArticleNo(articleNo, pageNo, pageSize);
        Map<String, Object> result = new HashMap<>();
        if (parent != null) {
            result.put("pages", parent.getPages());
            result.put("total", parent.getTotal());
            result.put("current", parent.getCurrent());
            List<NewsArticleCommentHistory> records = parent.getRecords();
            List<CommentResponse> commentResponses = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(records)){
                for (NewsArticleCommentHistory comment:records) {
                    CommentResponse commentResponse = new CommentResponse();
                    BeanUtils.copyProperties(commentResponse,comment);
                    List<NewsArticleCommentHistory> child = newsArticleCommentHistorySerivce.listByCommentNo(comment.getCommentNo());
                    commentResponse.setChilds(child);
                    commentResponses.add(commentResponse);
                }
            }
            result.put("records",commentResponses);
            return AjaxResult.getOK(result);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

//    @ApiOperation("根据文章获取其所有评论")
//    @RequestMapping(value = "/listCommentByArticleNo",method = RequestMethod.GET)
//    public Object listCommentByArticleNo(@RequestParam(value = "articleNo") String articleNo) {
//        List<NewsArticleCommentHistory> newsArticleCommentHistoryPage = newsArticleCommentHistorySerivce.listByArticleNo(articleNo);
//        if (newsArticleCommentHistoryPage != null){
//            return AjaxResult.getOK(newsArticleCommentHistoryPage);
//        }
//        return AjaxResult.getError(ResultCode.ParamException);
//    }

    @ApiOperation("根据父级Id获取文章评论集合")
    @RequestMapping(value = "/pageArticleCommentByParentNo", method = RequestMethod.GET)
    public Object listArticleCommentHistoryByParentNo(@RequestParam("parentNo") String parentNo, @RequestParam("pageNo") Integer pageNo, @RequestParam("pageSize") Integer pageSize) {
        Page<NewsArticleCommentHistory> newsArticleCommentHistoryPage = newsArticleCommentHistorySerivce.listArticleCommentHistoryByParentNo(parentNo, pageNo, pageSize);
        if (newsArticleCommentHistoryPage != null) {
            return AjaxResult.getOK(newsArticleCommentHistoryPage);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


}

