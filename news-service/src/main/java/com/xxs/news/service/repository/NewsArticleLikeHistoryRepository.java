package com.xxs.news.service.repository;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.news.service.entity.NewsArticleLikeHistory;

/**
 * <p>
 * 新闻模块--点赞记录 服务类
 * </p>
 *
 * @author fung
 * @since 2018-11-24
 */
public interface NewsArticleLikeHistoryRepository extends IService<NewsArticleLikeHistory> {

    /**
     * 分页获取收藏对象
     * @param userNo
     * @param current
     * @param rows
     * @return
     */
    Page<NewsArticleLikeHistory> pageFav(String userNo, Integer current, Integer rows);

    /**
     * 根据收藏编码获取对象
     * @param mapNo
     * @return
     */
    NewsArticleLikeHistory selectByMapNo(String mapNo);

    /**
     * 根据文章编号统计数量
     * @param articleNo
     * @return
     */
    Integer selectCountByArticleNo(String articleNo);

    /**
     * 根据收藏编码删除收藏记录对象
     * @param mapNo
     * @return
     */
    Boolean del(String mapNo);

    /**
     * 根据用户编号 文章编号获取收藏编码对象
     * @param userNo
     * @param articleNo
     * @return
     */
    NewsArticleLikeHistory selectOne(String userNo, String articleNo);

    /**
     * 根据个人统计点赞数量
     * @param userNo
     * @return
     */
    Integer selectCountByUserNo(String userNo);
}
