package com.xxs.news.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsArticleFavHistory;

/**
 * 新闻文章 -- 喜爱收藏
 */
public interface NewsArticleFavHistoryService {

    /**
     * 添加当前喜爱收藏
     * @return
     */
    String add(String articleNo);

    /**
     * 获取收藏对象
     * @param mapNo
     * @return
     */
    NewsArticleFavHistory get(String mapNo);

    /**
     * 分页获取收藏对象
     * @param userNo
     * @param current
     * @param rows
     * @return
     */
    Page<NewsArticleFavHistory> pageFav(String userNo, Integer current, Integer rows);

    /**
     * 删除当前喜爱收藏
     * @return
     */
    Boolean del(String articleNo);

    /**
     * 统计个人喜爱收藏个数
     * @param articleNo
     * @return
     */
    Integer count(String articleNo);

    /**
     * 根据用户编号 文章标号获取收藏对象
     * @param userNo
     * @param articleNo
     * @return
     */
    NewsArticleFavHistory get(String userNo,String articleNo);

}


