package com.xxs.news.service.oss;

import java.util.List;

/**
 * OSS 查询服务对象
 */
public class OssQueryFileRequest {

    public Long  object;

    public List<Long> objects;

    public Long getObject() {
        return object;
    }

    public void setObject(Long object) {
        this.object = object;
    }

    public List<Long> getObjects() {
        return objects;
    }

    public void setObjects(List<Long> objects) {
        this.objects = objects;
    }

}
