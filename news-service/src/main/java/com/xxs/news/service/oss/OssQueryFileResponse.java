package com.xxs.news.service.oss;

/**
 * OSS文件 返回对象
 */
public class OssQueryFileResponse {
    /**
     * 文件Id
     */
    private Long fileId;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件块
     */
    private String bulket;

    /**
     * 全路径Url
     */
    private String fullUrl;

    /**
     * 前缀url
     */
    private String prefixUrl;

    /**
     * DB内存文件名称
     */
    private String storageName;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBulket() {
        return bulket;
    }

    public void setBulket(String bulket) {
        this.bulket = bulket;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getPrefixUrl() {
        return prefixUrl;
    }

    public void setPrefixUrl(String prefixUrl) {
        this.prefixUrl = prefixUrl;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

}
