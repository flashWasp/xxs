package com.xxs.news.service.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.springframework.data.elasticsearch.annotations.Document;


import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
@TableName("news_article")
@Document(indexName ="xxs",type = "newsarticle")
public class NewsArticle implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 文章编号
     */
    @TableField("article_no")
    private String articleNo;
    /**
     * 频道Id
     */
    @TableField("channelId")
    private Integer channelId;

    /**
     * 文章简介
     */
    private String summary;


    /**
     * 名称
     */
    private String title;
    /**
     * 观察量
     */
    private Integer views;
    /**
     * 文章内容
     */
    private String content;
    /**
     * 收藏数
     */
    private Integer favorites;

    /**
     * 点赞数
     */
    private Integer likes;

    /**
     * 封面
     */
    @TableField("image_no")
    private String imageNo;

    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 创建者UUId
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 更新者UUID
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;
    /**
     * 状态  draft   pendingReview reviewing  passReView  isDelete
     */
    private String state;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getFavorites() {
        return favorites;
    }

    public void setFavorites(Integer favorites) {
        this.favorites = favorites;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }



    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "NewsArticle{" +
                "id=" + id +
                ", articleNo='" + articleNo + '\'' +
                ", channelId=" + channelId +
                ", summary='" + summary + '\'' +
                ", title='" + title + '\'' +
                ", views=" + views +
                ", content='" + content + '\'' +
                ", favorites=" + favorites +
                ", likes=" + likes +
                ", imageNo='" + imageNo + '\'' +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                ", createUserUuid='" + createUserUuid + '\'' +
                ", updateUserUuid='" + updateUserUuid + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
