package com.xxs.news.service.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsArticleFavHistory;
import com.xxs.news.service.repository.NewsArticleFavHistoryRepository;
import com.xxs.news.service.service.NewsArticleFavHistoryService;
import com.xxs.news.service.service.NewsArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("newsArticleFavHistoryService")
public class NewsArticleFavHistoryServiceImpl implements NewsArticleFavHistoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsArticleFavHistoryServiceImpl.class);

    @Autowired
    private NewsArticleFavHistoryRepository newsArticleFavHistoryRepository;

    @Autowired
    private NewsArticleService articleService;

    @Override
    public String add(String articleNo) {
        NewsArticleFavHistory newsArticleFavHistoryDb = get(TokenRedisRepository.getCurUserUUID(), articleNo);
        if (newsArticleFavHistoryDb == null){
            NewsArticleFavHistory newsArticleFavHistory = new NewsArticleFavHistory();
            newsArticleFavHistory.setArticleNo(articleNo);
            newsArticleFavHistory.setMapNo(KeyUtil.getUniqueKey());
            newsArticleFavHistory.setCreateTime(new Date());
            newsArticleFavHistory.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
            newsArticleFavHistory.setUpdateTime(new Date());
            newsArticleFavHistory.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
            newsArticleFavHistory.setState(0);
            newsArticleFavHistory.setUserNo(TokenRedisRepository.getCurUserUUID());
            boolean flag = newsArticleFavHistoryRepository.insertAllColumn(newsArticleFavHistory);
            if (flag) {
                // 更新文章对象
                updateSkip(articleNo, 1);
                LOGGER.info("个人收藏添加 newsArticleFavHistoryId: " + newsArticleFavHistory.getId());
                return articleNo;
            }
        }
        return null;
    }

    @Override
    public NewsArticleFavHistory get(String mapNo) {
        return newsArticleFavHistoryRepository.selectByMapNo(mapNo);
    }

    @Override
    public Page<NewsArticleFavHistory> pageFav(String userNo, Integer current, Integer rows) {
        return newsArticleFavHistoryRepository.pageFav(userNo, current, rows);
    }

    @Override
    public Boolean del(String articleNo) {
        NewsArticleFavHistory newsArticleFavHistory = newsArticleFavHistoryRepository.selectOne(TokenRedisRepository.getCurUserUUID(), articleNo);
        if (newsArticleFavHistory != null) {
            LOGGER.info("个人收藏删除 mapNo : " + newsArticleFavHistory.getMapNo());
            // 更新文章对象
            updateSkip(articleNo, -1);
            return newsArticleFavHistoryRepository.del(newsArticleFavHistory.getMapNo());
        }
        return false;

}

    @Override
    public Integer count(String articleNo) {
        return newsArticleFavHistoryRepository.selectCountByArticleNo(articleNo);
    }

    @Override
    public NewsArticleFavHistory get(String userNo, String articleNo) {
        return newsArticleFavHistoryRepository.selectOne(userNo, articleNo);
    }

    /**
     * 更新步长
     *
     * @param articleNo
     * @param skip
     */
    private void updateSkip(String articleNo, Integer skip) {
        NewsArticle article = articleService.findOne(articleNo);
        if (article != null) {
            Integer favorites = article.getFavorites();
            article.setFavorites(favorites + skip);
            articleService.update(article);
        }
    }

}
