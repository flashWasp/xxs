package com.xxs.news.service.service;

import com.xxs.news.service.album.SysAblumResponse;
import org.springframework.web.multipart.MultipartFile;


public interface SysAblumService {


    String addAblum(MultipartFile img, String bulket, Double bili, Double x, Double y, Double w, Double h) throws Exception;

    SysAblumResponse findAblum(String imageNo) throws Exception;

    Boolean deleteAblum(String imageNo);

    //List<SysAblumResponse> listAblum(List<String> imageNos) throws Exception;
}
