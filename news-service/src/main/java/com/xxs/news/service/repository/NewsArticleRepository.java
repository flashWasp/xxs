package com.xxs.news.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.news.service.entity.NewsArticle;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
public interface NewsArticleRepository extends IService<NewsArticle> {
    /**
     * 根据草稿标记和文章编码获取最新对象
     * @param articleNo
     * @return
     */
    NewsArticle getArticleByNo(String articleNo);


    /**
     * 根据articleNo 获取其所有版本
     * @param articleNo
     * @return
     */
    Page<NewsArticle> listArticleVersionByArticleNo(String articleNo,Integer pageNo,Integer pageSize);


    /**
     * 根据条件获取分页对象
     * @param channelNo
     * @param createUserNo
     * @param pageNo
     * @param pageSize
     * @param startTime
     * @param endTime
     * @param title
     * @return
     */
    Page<NewsArticle> listNewsArticlePage(
            Integer channelNo,
            String createUserNo,
            String states,
            Integer pageNo,
            Integer pageSize,
            String startTime,
            String endTime,
            String title);

    List<NewsArticle> listArticleByViewAsc(Integer size);
}
