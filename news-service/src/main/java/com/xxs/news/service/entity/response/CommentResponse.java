package com.xxs.news.service.entity.response;

import com.baomidou.mybatisplus.annotations.TableField;
import com.xxs.news.service.entity.NewsArticleCommentHistory;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @Title: CommentResponse
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/3111:49
 */
public class CommentResponse {


    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 评论编码
     */
    private String commentNo;

    /**
     * 文章编码
     */
    private String articleNo;

    /**
     * 评论内容内容
     */
    private String content;

    /**
     * 发送者编码
     */
    private String senderNo;

    /**
     * 接受者编码
     */
    private String recevierNo;

    private String senderName;

    private String  recevierName;

    /**
     * 父评论编码
     */
    private String parentCommentNo;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者UUId
     */
    private String createUserUuid;

    /**
     * 更新者UUID
     */
    private String updateUserUuid;

    /**
     * 状态 0:正常 1:被删除
     */
    private Integer state;

    /**
     * 目标回复的评论编号
     */
    private String targetCommentNo;

    private List<NewsArticleCommentHistory> childs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommentNo() {
        return commentNo;
    }

    public void setCommentNo(String commentNo) {
        this.commentNo = commentNo;
    }

    public String getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(String articleNo) {
        this.articleNo = articleNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderNo() {
        return senderNo;
    }

    public void setSenderNo(String senderNo) {
        this.senderNo = senderNo;
    }

    public String getRecevierNo() {
        return recevierNo;
    }

    public void setRecevierNo(String recevierNo) {
        this.recevierNo = recevierNo;
    }

    public String getParentCommentNo() {
        return parentCommentNo;
    }

    public void setParentCommentNo(String parentCommentNo) {
        this.parentCommentNo = parentCommentNo;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public List<NewsArticleCommentHistory> getChilds() {
        return childs;
    }

    public void setChilds(List<NewsArticleCommentHistory> childs) {
        this.childs = childs;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRecevierName() {
        return recevierName;
    }

    public void setRecevierName(String recevierName) {
        this.recevierName = recevierName;
    }

    public String getTargetCommentNo() {
        return targetCommentNo;
    }

    public void setTargetCommentNo(String targetCommentNo) {
        this.targetCommentNo = targetCommentNo;
    }
}
