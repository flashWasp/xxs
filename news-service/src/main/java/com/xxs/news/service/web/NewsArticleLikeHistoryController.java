package com.xxs.news.service.web;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.news.service.entity.NewsArticleLikeHistory;
import com.xxs.news.service.service.NewsArticleLikeHistoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author : Fung
 * Date : 2018/11/24 0024 下午 5:20
 * Desc :
 */
@RequestMapping("/like")
@RestController
public class NewsArticleLikeHistoryController {
    @Autowired
    private NewsArticleLikeHistoryService newsArticleLikeHistoryService;

    /**
     * 点赞文章
     * @param articleNo
     * @return
     */
    @ApiOperation("点赞")
    @GetMapping("/doLike")
    public AjaxResult like(@RequestParam("articleNo") String articleNo) {
        String result = newsArticleLikeHistoryService.add(articleNo);
        if (result != null ){
            return AjaxResult.getOK(result);
        }
        return AjaxResult.getError(ResultCode.SystemException);
    }

    /**
     * 取消点赞
     * @param articleNo
     * @return
     */
    @ApiOperation("取消点赞")
    @GetMapping("/notLike")
    public AjaxResult notLike(@RequestParam("articleNo")String  articleNo){
        NewsArticleLikeHistory newsArticleLikeHistory = newsArticleLikeHistoryService.get(TokenRedisRepository.getCurUserUUID(), articleNo);
        if (newsArticleLikeHistory != null){
            Boolean flag = newsArticleLikeHistoryService.del(newsArticleLikeHistory.getMapNo());
            if (flag){
                return AjaxResult.getOK();
            }
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 分页获取点赞对象
     * @param page
     * @param rows
     * @return
     */
    @ApiOperation("分页获取点赞对象")
    @GetMapping("/page")
    public AjaxResult page(@RequestParam("page") Integer page,
                           @RequestParam("rows") Integer rows){
        Page<NewsArticleLikeHistory> newsArticleLikeHistoryPage = newsArticleLikeHistoryService.pageFav(TokenRedisRepository.getCurUserUUID(),
                page, rows
        );
        if (newsArticleLikeHistoryPage != null){
            return AjaxResult.getOK(newsArticleLikeHistoryPage);
        }
        return AjaxResult.getOK();
    }

    /**
     * 获取个人点赞总数
     * @return
     */
    @ApiOperation("获取个人点赞总数")
    @GetMapping("/countByUserNo")
    public AjaxResult countByUserNo(@RequestParam("userNo") String userNo){
        Integer count
                = newsArticleLikeHistoryService.countByUserNo(userNo);
        if ( null != null){
            return AjaxResult.getOK(count);
        }
        return AjaxResult.getError(ResultCode.SystemException);
    }

    /**
     * 根据文章统计点赞数
     * @param articleNo
     * @return
     */
    @ApiOperation("根据文章统计点赞数")
    @GetMapping("/countByArticleNo")
    public AjaxResult countByArticleNo(@RequestParam("articleNo") String articleNo){
        Integer count
                = newsArticleLikeHistoryService.countByArticleNo(articleNo);
        if ( null != null){
            return AjaxResult.getOK(count);
        }
        return AjaxResult.getError(ResultCode.SystemException);
    }


}
