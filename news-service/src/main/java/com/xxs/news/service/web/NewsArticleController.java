package com.xxs.news.service.web;

import com.baomidou.mybatisplus.plugins.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsArticleFavHistory;
import com.xxs.news.service.entity.NewsArticleLikeHistory;
import com.xxs.news.service.entity.request.NewsArticleReq;
import com.xxs.news.service.entity.request.NewsArticleSearchReq;
import com.xxs.news.service.repository.NewsArticleESRepository;
import com.xxs.news.service.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Api("文章接口")
@RestController
@RequestMapping("/newsArticle")
public class NewsArticleController {
    @Autowired
    private NewsArticleService newsArticleService;

    @Autowired
    private NewsViewHistoryService newsViewHistoryService;

    @Autowired
    private NewsArticleFavHistoryService newsArticleFavHistoryService;

    @Autowired
    private NewsArticleLikeHistoryService newsArticleLikeHistoryService;

    @Autowired
    private NewsArticleESService newsArticleESService;

    /**
     * 创建文章
     *
     * @return
     */
    @ApiOperation(value = "创建文章草稿")
    @RequestMapping(value = "/createNewArticle", method = RequestMethod.POST)
    public AjaxResult createNewsArticle(@RequestBody NewsArticleReq req) {
        req.setCmd("create");
        Boolean flag = newsArticleService.saveOrUpdateArticle(req);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 更新文章内容
     *
     * @param req
     * @return
     */
    @ApiOperation(value = "更新文章内容")
    @RequestMapping(value = "/updateNewsArticle", method = RequestMethod.POST)
    public AjaxResult updateNewsArticle(@RequestBody NewsArticleReq req) {
        req.setCmd("update");
        Boolean flag = newsArticleService.saveOrUpdateArticle(req);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 发布文章
     *
     * @return
     */
    @ApiOperation(value = "发布文章")
    @RequestMapping(value = "/issueArticle", method = RequestMethod.GET)
    public AjaxResult issueArticle(@RequestParam(value = "id") String id) {
        Boolean flag = newsArticleService.issueArticle(id);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 文章通过
     *
     * @return
     */
    @ApiOperation(value = "文章审核通过")
    @RequestMapping(value = "/pass", method = RequestMethod.GET)
    public AjaxResult pass(@RequestParam(value = "articleNo") String articleNo) throws JsonProcessingException {
        Boolean flag = newsArticleService.reviewArticle(articleNo, NewsContants.STATUS_PASS);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 文章审核不通过
     *
     * @param articleNo
     * @return
     */
    @ApiOperation(value = "文章审核被拒绝")
    @RequestMapping(value = "/notPass", method = RequestMethod.GET)
    public AjaxResult notPass(@RequestParam(value = "articleNo") String articleNo) throws JsonProcessingException {
        Boolean flag = newsArticleService.reviewArticle(articleNo, NewsContants.STATUS_DRAFT);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 删除文章
     *
     * @return
     */
    @ApiOperation(value = "删除文章")
    @RequestMapping(value = "/deleteArticle", method = RequestMethod.GET)
    public AjaxResult delArticle(@RequestParam(value = "id") String id) {
        Boolean flag = newsArticleService.delete(id);
        // 通知关注者已经删除文章
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation(value = "分页获取草稿的文章", consumes = "application/x-www-form-urlencoded")
    @RequestMapping(value = "/pageArticle", method = RequestMethod.POST)
    public AjaxResult pageArticle(@RequestBody NewsArticleSearchReq req) {
        Page<NewsArticle> newsArticlePage =
                newsArticleService.listNewsArticlePageByReq(req, NewsContants.STATUS_DRAFT);
        if (newsArticlePage != null) {
            return AjaxResult.getOK(newsArticlePage);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation(value = "分页获取发布的文章")
    @RequestMapping(value = "/pageIssueArticle", method = RequestMethod.POST)
    public AjaxResult pageIssueArticle(@RequestBody NewsArticleSearchReq req) {
        Page<NewsArticle> newsArticlePage =
                newsArticleService.listNewsArticlePageByReq(req, NewsContants.STATUS_REVIEWING);
        if (newsArticlePage != null) {
            return AjaxResult.getOK(newsArticlePage);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation(value = "根据Id获取已发布文章")
    @RequestMapping(value = "/getArticleById", method = RequestMethod.GET)
    public AjaxResult getArticleById(@RequestParam("id") String id) {
        NewsArticle article = newsArticleService.findOne(id);
        if (article != null &&
                (NewsContants.STATUS_PASS.equals(article.getState())
                        || NewsContants.STATUS_REVIEWING.equals(article.getState()))) {
            return AjaxResult.getOK(article);
        }
        return AjaxResult.getError(ResultCode.SystemNullPointerException);
    }

    @ApiOperation(value = "根据Id获取草稿")
    @RequestMapping(value = "/getDraftById", method = RequestMethod.GET)
    public AjaxResult getDraftById(@RequestParam("id") String id) {
        NewsArticle newsArticle = newsArticleService.findOne(id);
        if (newsArticle != null && newsArticle.getState().equals(NewsContants.STATUS_DRAFT)) {
            return AjaxResult.getOK(newsArticle);
        }
        return AjaxResult.getError(ResultCode.SystemNullPointerException);
    }

    @ApiOperation(value = "分页获取通过的文章")
    @RequestMapping(value = "/pagePassArticle", method = RequestMethod.POST)
    public AjaxResult pagePassArticle(@RequestBody NewsArticleSearchReq req) {
        Page<NewsArticle> newsArticlePage =
                newsArticleService.listNewsArticlePageByReq(req, NewsContants.STATUS_PASS);
        if (newsArticlePage != null) {
            return AjaxResult.getOK(newsArticlePage);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 获取热门文章集合
     * @param size
     * @return
     */
    @ApiOperation("获取热门文章集合")
    @GetMapping("/hotWebArticle")
    public AjaxResult hotWebArticle(HttpServletRequest request, @RequestParam("size") Integer size) {

//        redisTemplate.opsForValue().set("token","123");
//        String curUserUUID = TokenRedisRepository.getCurUserUUID();
//        System.out.println("token ====================================================="
//                + curUserUUID.toString());
        List<NewsArticle> newsArticles = newsArticleService.hotWebArticle(size);
 //       System.out.println(TokenRedisRepository.getCurUserUUID());
        http://localhost:8050/news/newsArticle/hotWebArticle?size=6
        if (!CollectionUtils.isEmpty(newsArticles)) {
            return AjaxResult.getOK(newsArticles);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


    /**
     * 获取前端文章对象
     * @return
     */
    @ApiOperation(value = "根据Id获取在线文章")
    @RequestMapping(value = "/getWebArticle", method = RequestMethod.GET)
    public AjaxResult getWebArticle(@RequestParam("articleNo") String articleNo) {
        NewsArticle newsArticle = newsArticleService.findOne(articleNo);
        if (newsArticle != null && newsArticle.getState().equals(NewsContants.STATUS_PASS)) {
            Map<String, Object> map = new HashMap<>();
            map.put("article", newsArticle);
            map.put("isFav", false);
            map.put("isLike", false);
            NewsArticleFavHistory newsArticleFavHistory = newsArticleFavHistoryService.get(TokenRedisRepository.getCurUserUUID(), articleNo);
            if (newsArticleFavHistory != null) {
                map.put("isFav", true);
            }
            NewsArticleLikeHistory newsArticleLikeHistory = newsArticleLikeHistoryService.get(TokenRedisRepository.getCurUserUUID(), articleNo);
            if (newsArticleLikeHistory != null) {
                map.put("isLike", true);
            }
            // 添加浏览量
            newsViewHistoryService.addView(articleNo, TokenRedisRepository.getCurUserUUID());
            Integer views = newsArticle.getViews();
            newsArticle.setViews(views + 1);
            newsArticleService.update(newsArticle);
            return AjaxResult.getOK(newsArticle);
        }
        return AjaxResult.getError(ResultCode.SystemNullPointerException);
    }

    @GetMapping("/search")
    public AjaxResult search(String content,Integer pageNo,Integer pageSize){
        AggregatedPage<NewsArticle> newsArticles = newsArticleESService.highLigthQuery(content,pageNo,pageSize);
        Map<String, Object> result = new HashMap<>();
        result.put("rows",newsArticles.getSize());
        result.put("pages",newsArticles.getTotalPages());
        result.put("records",newsArticles.getContent());
        result.put("total",newsArticles.getTotalElements());
        return AjaxResult.getOK(result);
    }
}

