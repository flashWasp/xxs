package com.xxs.news.service.web;


import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.news.service.entity.NewsLabel;
import com.xxs.news.service.service.Impl.NewsLabelServiceImpl;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 新闻模块-标签 前端控制器
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Api("标签管理")
@RestController
@RequestMapping("/newsLabel")
public class NewsLabelController {
    @Autowired
    private NewsLabelServiceImpl labelService;

    @ApiOperation(value = "新建标签", notes = "新建标签")
    @PostMapping(value = "/createLabel.action/{name}")
    public Object createLabel(@ApiParam(value = "name") @PathVariable  String name) {
        String labelNo = labelService.createLabel(name);
        if(labelNo != null){
            return AjaxResult.getOK(labelNo);
        }else{
            return AjaxResult.getError(ResultCode.ParamException);
        }
    }

    @ApiOperation(value = "删除标签", notes = "删除标签")
    @GetMapping("/deleteLabel.action/{labelNo}")
    public Object deleteLabel(@ApiParam(value = "labelNo") @PathVariable String labelNo) {
        Boolean flag = labelService.deleteLabelByNo(labelNo);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


    @ApiOperation(value = "根据编码获取标签对象", notes = "根据编码获取标签对象")
    @GetMapping("/findOneLabel.action/{labelNo}")
    public Object findOneLabel(@ApiParam(value = "labelNo") @PathVariable String labelNo) {
        NewsLabel label = labelService.findLabelByNo(labelNo);
        HashMap<String, Object> labelMap = new HashMap<>();
        labelMap.put("labelNo",label != null ?label.getLabelNo():null);
        labelMap.put("name",label != null ?label.getName():null);
        return AjaxResult.getOK(labelMap);
    }

    @ApiOperation(value = "根据编码获取标签对象", notes = "根据编码获取标签对象")
    @PostMapping("/updateLabel.action/{labelNo}/{name}")
    public Object updateLabel(@ApiParam(value = "labelNo") @PathVariable String labelNo , @ApiParam(value = "name") @PathVariable  String name) {
        if(labelService.updateLabel(labelNo, name)){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation(value = "获取所有标签对象",notes = "获取所有标签对象")
    @GetMapping("/listLabel.action")
    public Object listLabel(){
        List<NewsLabel> newsLabels = labelService.listAll();
        return AjaxResult.getOK(newsLabels);
    }






}

