package com.xxs.news.service;

import com.xxs.common.service.cache.manager.thread.RequestProcessorThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @Title: ApplicationRunner
 * @ProjectName master
 * @Description: 项目初始化
 * @date 2018/12/2315:54
 */
@Component
public class ApplicationRunner implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationRunner.class);

    @Override
    public void run(String... args) throws Exception {
        // 初始化Redis请求线程池
        LOGGER.info("项目初始化 Redis请求线程池开始");
        RequestProcessorThreadPool.init();
        LOGGER.info("项目初始化 Redis请求线程池结束");
    }
}
