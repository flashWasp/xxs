package com.xxs.news.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.dao.NewsArticleFavHistoryDao;
import com.xxs.news.service.entity.NewsArticleFavHistory;
import com.xxs.news.service.repository.NewsArticleFavHistoryRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新闻模块--收藏记录 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Service
public class NewsArticleFavHistoryRepositoryImpl extends ServiceImpl<NewsArticleFavHistoryDao, NewsArticleFavHistory> implements NewsArticleFavHistoryRepository {

    @Override
    public Page<NewsArticleFavHistory> pageFav(String userNo, Integer current, Integer rows) {
        NewsArticleFavHistory newsArticleFavHistory = new NewsArticleFavHistory();
        newsArticleFavHistory.setUserNo(userNo);
        newsArticleFavHistory.setState(0);
        Page<NewsArticleFavHistory> page = new Page<NewsArticleFavHistory>(current, rows);
        return selectPage(page,new EntityWrapper<NewsArticleFavHistory>(newsArticleFavHistory));
    }

    @Override
    public NewsArticleFavHistory selectByMapNo(String mapNo) {
        return selectOne(new Condition().eq("map_no",mapNo).eq("state",0));
    }

    @Override
    public Integer selectCountByArticleNo(String articleNo) {
        NewsArticleFavHistory newsArticleFavHistory = new NewsArticleFavHistory();
        newsArticleFavHistory.setArticleNo(articleNo);
        newsArticleFavHistory.setState(0);
        return selectCount(new EntityWrapper<NewsArticleFavHistory>(newsArticleFavHistory));
    }

    @Override
    public Boolean del(String mapNo) {
        return delete(new Condition().eq("map_no",mapNo));
    }

    @Override
    public NewsArticleFavHistory selectOne(String userNo, String articleNo) {
        return selectOne(new Condition().eq("article_no",articleNo).eq("user_no",userNo));
    }


}
