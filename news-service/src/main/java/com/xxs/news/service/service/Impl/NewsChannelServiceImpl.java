package com.xxs.news.service.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.entity.NewsChannel;
import com.xxs.news.service.entity.request.NewsChannelSearchReq;
import com.xxs.news.service.repository.NewsChannelRepository;
import com.xxs.news.service.service.NewsChannelService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service("newsChannelService")
public class NewsChannelServiceImpl implements NewsChannelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsArticleFavHistoryServiceImpl.class);

    @Autowired
    private NewsChannelRepository newsChannelRepository;

    /**
     * 创建新频道
     *
     * @param name
     * @param description
     * @param parentId
     * @return
     */
    public Integer createNewChannel(String name, String description, Integer parentId, Integer sort) {
        // 频道对象
        NewsChannel newsChannel = new NewsChannel();
        newsChannelRepository.insert(packNewsChannel(newsChannel, name, description, parentId,sort));
        LOGGER.info("插入频道 id:" + newsChannel.getId());
        return newsChannel.getId();
    }

    /**
     * 更新频道
     *
     * @param name
     * @param description
     * @param parentId
     * @return
     */
    public Boolean updateNewsChannel(Integer id, String name, String description, Integer parentId, Integer sort) {
        NewsChannel newsChannel = newsChannelRepository.selectById(id);
        // 获取父Id
        if (newsChannel != null) {
            String currentUserNo = TokenRedisRepository.getCurUserUUID();
            newsChannel.setUpdateUserUuid(currentUserNo);
            newsChannelRepository.updateAllColumnById(packNewsChannel(newsChannel, name, description, parentId,sort));
            LOGGER.info("更新频道 id:" + newsChannel.getId());
        }
        return true;
    }

    /**
     * 根据Id获取新闻频道对象
     *
     * @param id
     * @return
     */
    public NewsChannel findNewsChannelById(Integer id) {
        return newsChannelRepository.selectById(id);
    }

    /**
     * 根据Id获取父级Id集合
     *
     * @param id
     * @return
     */
    public String findParentIdsStrById(Integer id) {
        NewsChannel newsChannel = newsChannelRepository.selectById(id);
        if (newsChannel != null) {
            return newsChannel.getParentIds();
        }
        return null;
    }

    /**
     * 获取所有一级新闻频道对象
     *
     * @return
     */
    public List<NewsChannel> listFirstLevelNewsChannel() {
        NewsChannel newsChannelCondition = new NewsChannel();
        newsChannelCondition.setFirstParentId(0);
        return newsChannelRepository.selectList(new EntityWrapper<NewsChannel>(newsChannelCondition));
    }

    /**
     * 根据父级Id获取子级新闻频道对象
     *
     * @param id
     * @return
     */
    public List<NewsChannel> listChildNewsChannelById(Integer id) {
        NewsChannel newsChannelCondition = new NewsChannel();
        newsChannelCondition.setParentId(id);
        return newsChannelRepository.selectList(new EntityWrapper<NewsChannel>(newsChannelCondition));
    }

    /**
     * 通过parentId 获取列表  wjy
     * @param parentId
     * @return
     */
    @Override
    public List<NewsChannel> listChannel(Integer parentId) {
        NewsChannel newsChannelCondition = new NewsChannel();
        newsChannelCondition.setParentId(parentId);
        newsChannelCondition.setState(0);
        return newsChannelRepository.selectList(new EntityWrapper<>(newsChannelCondition));
    }

    /**
     * 根据父级Id获取子级Id集合
     * @param id
     * @return
     */
    public List<Integer> listChildIdsById(Integer id) {
        List<Integer> childIdList = new ArrayList<>();
        List<NewsChannel> newsChannels = listChildNewsChannelById(id);
        if (newsChannels != null && newsChannels.size() > 0){
            for (NewsChannel newChannel : newsChannels) {
                if(newChannel != null){
                    childIdList.add(newChannel.getId());
                }
            }
        }
        return childIdList;
    }


    /**
     * 封装新闻频道对象
     *
     * @param newsChannel
     * @param name
     * @param description
     * @param parentId
     * @return
     */
    private NewsChannel packNewsChannel(NewsChannel newsChannel, String name, String description,
                                        Integer parentId,Integer sort) {
        newsChannel.setName(name);
        newsChannel.setDescription(description);
        newsChannel.setSort(sort);
        // 设置父Id集合字符串
        String parentIdsStr = "";
        // 第一层频道
        if (parentId == null || parentId.equals(0)) {
            newsChannel.setParentId(0);
            newsChannel.setLevel(1);
            List<Integer> parentIds = new ArrayList<Integer>();
            parentIds.add(0);
            parentIdsStr = StringUtils.join(parentIds.toArray(), ",");
            newsChannel.setParentIds(parentIdsStr);
            newsChannel.setFirstParentId(0);
            // 第二层频道
        } else {
            newsChannel.setParentId(parentId);
            newsChannel.setLevel(2);
            // 获取父Id对象
            NewsChannel parentNewChannel = newsChannelRepository.selectById(parentId);
            // 设置父Id
            if (parentNewChannel != null) {
                parentIdsStr = parentNewChannel.getParentIds();
                List<String> parentIdStrList = new ArrayList<String>();
                if(parentIdsStr != null){
                    parentIdStrList.addAll(Arrays.asList(parentIdsStr));
                    Integer id = parentNewChannel.getId();
                    parentIdStrList.add(id!= null?id.toString():"0");
                    parentIdsStr = StringUtils.join(parentIdStrList.toArray(), ",");
                    newsChannel.setFirstParentId(Integer.parseInt(parentIdStrList.get(0)));
                }else{
                    newsChannel.setFirstParentId(Integer.parseInt("1"));
                }
                newsChannel.setParentIds(parentIdsStr);
            }
        }
        newsChannel.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        newsChannel.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        newsChannel.setCreateTime(new Date());
        return newsChannel;
    }

    /**
     * 删除频道对象
     * @param id
     * @return
     */
    @Transactional
    public Boolean deleteNewChannel(Integer id){
        NewsChannel newsChannel = newsChannelRepository.selectById(id);
        if(newsChannel != null){
            // 删除频道
            newsChannel.setState(NewsContants.NEWS_ISDELETE);
            newsChannelRepository.updateAllColumnById(newsChannel);
            LOGGER.info("删除频道 id:" + newsChannel.getId());
            return true;
        }
        return false;
    }

    /**
     * 频道分页查询  wjy
     * @param req
     * @return
     */
    @Override
    public Page<NewsChannel> pageChannel(NewsChannelSearchReq req) {
        if (req != null){
            return  newsChannelRepository.pageChannel(req);
        }
        return null;
    }
}
