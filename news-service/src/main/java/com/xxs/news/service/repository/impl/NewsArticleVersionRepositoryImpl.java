package com.xxs.news.service.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.dao.NewsArticleVersionDao;
import com.xxs.news.service.entity.NewsArticleVersion;
import com.xxs.news.service.repository.NewsArticleVersionRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-11-02
 */
@Service
public class NewsArticleVersionRepositoryImpl extends ServiceImpl<NewsArticleVersionDao, NewsArticleVersion> implements NewsArticleVersionRepository {

    @Override
    public Page<NewsArticleVersion> listVersionPageByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        NewsArticleVersion newsArticleVersion = new NewsArticleVersion();
        newsArticleVersion.setArticleNo(articleNo);
        newsArticleVersion.setState(NewsContants.STATUS_NODELETE);
        Page<NewsArticleVersion> newsArticleVersionPage = new Page<>();
        newsArticleVersionPage.setCurrent(pageNo);
        newsArticleVersionPage.setSize(pageSize);
        return newsArticleVersionPage.setRecords(baseMapper.selectPage(newsArticleVersionPage,new EntityWrapper<NewsArticleVersion>(newsArticleVersion).setSqlSelect("id, article_no, channelId AS channelId, title, views, favorites, image_no, updateTime AS updateTime, createTime AS createTime, createUserUuid AS createUserUuid, updateUserUuid AS updateUserUuid, state,summary,likes AS likes")));
    }
}
