package com.xxs.news.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.news.service.entity.NewsViewHistory;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 新闻模块-文章浏览记录 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
public interface NewsViewHistoryRepository extends IService<NewsViewHistory> {

    /**
     * 根据浏览者分页获取浏览记录
     * @return
     */
    Page<NewsViewHistory> pageViews(String viewer, Integer current, Integer rows);

    /**
     * 根据编码删除对象
     * @param mapNo
     * @return
     */
    Boolean del(String mapNo);

    /**
     * 获取浏览记录对象
     * @param mapNo
     * @return
     */
    NewsViewHistory get(String mapNo);


}
