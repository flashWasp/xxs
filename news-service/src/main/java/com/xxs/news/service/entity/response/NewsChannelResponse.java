package com.xxs.news.service.entity.response;

/**
 * 新闻频道返回对象
 *
 */
public class NewsChannelResponse {

    /**
     * id
     */
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 描述
     */
    private String description;
    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 更新人名称
     */
    private String updateUser;

    /**
     * 对象构造函数
     * @param id  id
     * @param name 名称
     * @param description 描述
     * @param updateTime 更新时间
     * @param updateUser 更新人
     */
    public NewsChannelResponse(Integer id, String name, String description, String updateTime, String updateUser) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.updateTime = updateTime;
        this.updateUser = updateUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }


    @Override
    public String toString() {
        return "NewsChannelResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", updateUser='" + updateUser + '\'' +
                '}';
    }
}
