package com.xxs.news.service.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.news.service.NewsContants;
import com.xxs.news.service.entity.NewsArticleCommentHistory;
import com.xxs.news.service.repository.NewsArticleCommentHistoryRepository;
import com.xxs.news.service.service.NewsArticleCommentHistorySerivce;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NewsArticleCommentHistorySerivceImpl implements NewsArticleCommentHistorySerivce {
    /**
     * 日志打印工具
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NewsArticleCommentHistorySerivceImpl.class);

    private static final String NEWSARTICLE_COMMENT = "newsArticle:comment";

    @Autowired
    private NewsArticleCommentHistoryRepository newsArticleCommentHistoryRepository;

    @Override
    public NewsArticleCommentHistory addArticleCommentHistory(String articleNo, String content,
                                                              String senderNo, String receiverNo,
                                                              String parentCommentNo,String senderName,
                                                              String receiverName,String targetCommentNo) {
        NewsArticleCommentHistory newsArticleCommentHistory = packComment(articleNo, content,
                senderNo, receiverNo,
                parentCommentNo,senderName,receiverName,targetCommentNo);
        boolean flag = newsArticleCommentHistoryRepository.insertAllColumn(newsArticleCommentHistory);
        LOGGER.info("文章评论添加 commentNo:" + newsArticleCommentHistory.getCommentNo() );
        if (flag){
            CacheHelper.update(NEWSARTICLE_COMMENT,newsArticleCommentHistory.getCommentNo(), JSONObject.toJSONString(newsArticleCommentHistory));
            return newsArticleCommentHistory;
        }
        return null;
    }

    @Override
    public NewsArticleCommentHistory findOne(String commentNo) {
        Object cacheObj = CacheHelper.read(NEWSARTICLE_COMMENT,commentNo);
        if (cacheObj != null){
            return JSONObject.parseObject(cacheObj.toString(),NewsArticleCommentHistory.class);
        }
        NewsArticleCommentHistory newsArticleComment = newsArticleCommentHistoryRepository.selectByComentId(commentNo);
        if (null != newsArticleComment) {
            CacheHelper.update(NEWSARTICLE_COMMENT,newsArticleComment.getCommentNo(),JSONObject.toJSONString(newsArticleComment));
            return newsArticleComment;
        }
        return null;
    }

    @Override
    public Boolean deleteArticleCommentHistory(String commentId) {
        NewsArticleCommentHistory comment = findOne(commentId);
        // 当前对象不为空
        if(comment != null){
            LOGGER.info( "评论被删除  commentId:" + commentId);
            List<NewsArticleCommentHistory> newsArticleCommentHistories = new ArrayList<>();
            comment.setState(NewsContants.NEWS_ISDELETE);
            CacheHelper.del(NEWSARTICLE_COMMENT,commentId);
            // 级联删除
            List<NewsArticleCommentHistory> newsArticleCommentHistoryPage = newsArticleCommentHistoryRepository.listArticleCommentHistoryByParentNo(comment.getCommentNo());
            if(newsArticleCommentHistoryPage != null ){
                for (NewsArticleCommentHistory newArticleComment:newsArticleCommentHistoryPage) {
                    if (newArticleComment != null){
                        newArticleComment.setState(NewsContants.NEWS_ISDELETE);
                        newsArticleCommentHistories.add(newArticleComment);
                        CacheHelper.del(NEWSARTICLE_COMMENT,newArticleComment.getCommentNo());
                    }
                }
            }
            if (!newsArticleCommentHistories.isEmpty()){
                newsArticleCommentHistoryRepository.updateAllColumnBatchById(newsArticleCommentHistories);
            }
            return newsArticleCommentHistoryRepository.updateById(comment);
        }
        return false;
    }

    @Override
    public Page<NewsArticleCommentHistory> listArticleCommentHistoryByParentNo(String parentCommentNo,Integer pageNo,Integer pageSize) {
        return newsArticleCommentHistoryRepository.listArticleCommentHistoryByParentNo(parentCommentNo,pageNo,pageSize);
    }

    @Override
    public Page<NewsArticleCommentHistory> pageByArticleNo(String articleNo, Integer pageNo, Integer pageSize) {
        return newsArticleCommentHistoryRepository.pageByArticleNo(articleNo,pageNo,pageSize);
    }

    @Override
    public List<NewsArticleCommentHistory> listByCommentNo(String parentNo) {
        return newsArticleCommentHistoryRepository.listByCommentNo(parentNo);
    }


    /**
     * 封装评论对象
     * @param articleNo
     * @param content
     * @param senderNo
     * @param receiverNo
     * @param parentCommentNo
     */
    private NewsArticleCommentHistory packComment(String articleNo, String content,
                             String senderNo, String receiverNo,
                             String parentCommentNo,String senderName,
                                                  String receiverName,String targetCommentNo) {
        NewsArticleCommentHistory newsArticleCommentHistory = new NewsArticleCommentHistory();
        newsArticleCommentHistory.setArticleNo(articleNo);
        newsArticleCommentHistory.setCommentNo(KeyUtil.getUniqueKey());
        newsArticleCommentHistory.setContent(content);
        newsArticleCommentHistory.setCreateTime(new Date());
        newsArticleCommentHistory.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        newsArticleCommentHistory.setUpdateTime(new Date());
        newsArticleCommentHistory.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        newsArticleCommentHistory.setSenderName(senderName);
        newsArticleCommentHistory.setRecevierName(receiverName);
        if (StringUtils.isNotBlank(parentCommentNo)){
            newsArticleCommentHistory.setParentCommentNo(parentCommentNo);
        }
        newsArticleCommentHistory.setSenderNo(senderNo);
        newsArticleCommentHistory.setRecevierNo(receiverNo);
        newsArticleCommentHistory.setState(NewsContants.NEWS_NODELETE);
        newsArticleCommentHistory.setTargetCommentNo(targetCommentNo);
        return  newsArticleCommentHistory;
    }
}
