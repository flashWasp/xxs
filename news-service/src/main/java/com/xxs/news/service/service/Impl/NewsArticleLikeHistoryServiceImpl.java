package com.xxs.news.service.service.Impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.entity.NewsArticleLikeHistory;
import com.xxs.news.service.repository.NewsArticleLikeHistoryRepository;
import com.xxs.news.service.service.NewsArticleLikeHistoryService;
import com.xxs.news.service.service.NewsArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Author : Fung
 * Date : 2018/11/24 0024 下午 4:13
 * Desc :
 */
@Service("newsArticleLikeHistoryService")
public class NewsArticleLikeHistoryServiceImpl implements NewsArticleLikeHistoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsArticleFavHistoryServiceImpl.class);

    @Autowired
    private NewsArticleLikeHistoryRepository newsArticleLikeHistoryRepository;

    @Autowired
    private NewsArticleService newsArticleService;

    @Override
    public String add(String articleNo) {
        NewsArticleLikeHistory newsArticleLikeHistoryDb = get(TokenRedisRepository.getCurUserUUID(), articleNo);
        if (newsArticleLikeHistoryDb == null){
            // 添加点赞历史对象
            NewsArticleLikeHistory newsArticleLikeHistory = new NewsArticleLikeHistory();
            newsArticleLikeHistory.setArticleNo(articleNo);
            newsArticleLikeHistory.setMapNo(KeyUtil.getUniqueKey());
            newsArticleLikeHistory.setCreateTime(new Date());
            newsArticleLikeHistory.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
            newsArticleLikeHistory.setUpdateTime(new Date());
            newsArticleLikeHistory.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
            newsArticleLikeHistory.setState(0);
            newsArticleLikeHistory.setUserNo(TokenRedisRepository.getCurUserUUID());
            boolean flag = newsArticleLikeHistoryRepository.insertAllColumn(newsArticleLikeHistory);
            if (flag) {
                // 更新文章对象
                updateSkip(newsArticleLikeHistory.getArticleNo(), 1);
                LOGGER.info("个人点赞添加 newsArticleFavHistoryId: " + newsArticleLikeHistory.getId());
                return articleNo;
            }
        }
        return null;
    }

    @Override
    public NewsArticleLikeHistory get(String mapNo) {
        return newsArticleLikeHistoryRepository.selectByMapNo(mapNo);
    }

    @Override
    public Page<NewsArticleLikeHistory> pageFav(String userNo, Integer current, Integer rows) {
        return newsArticleLikeHistoryRepository.pageFav(userNo, current, rows);
    }

    @Override
    public Boolean del(String mapNo) {
        Boolean flag = newsArticleLikeHistoryRepository.del(mapNo);
        if (flag) {
            LOGGER.info("个人点赞删除成功 mapNo : " + mapNo);
            NewsArticleLikeHistory newsArticleLikeHistory = newsArticleLikeHistoryRepository.selectByMapNo(mapNo);
            if (newsArticleLikeHistory != null) {
                // 更新文章对象
                updateSkip(newsArticleLikeHistory.getArticleNo(), -1);
            }
        }
        return flag;

    }

    @Override
    public Integer countByUserNo(String userNo) {
        return newsArticleLikeHistoryRepository.selectCountByUserNo(userNo);
    }

    @Override
    public Integer countByArticleNo(String articleNo) {
        return newsArticleLikeHistoryRepository.selectCountByArticleNo(articleNo);
    }

    @Override
    public NewsArticleLikeHistory get(String userNo, String articleNo) {
        return newsArticleLikeHistoryRepository.selectOne(userNo, articleNo);
    }




    /**
     * 更新点赞步长
     *
     * @param skip
     */
    private void updateSkip(String articleNo, Integer skip) {
        NewsArticle article = newsArticleService.findOne(articleNo);
        if (article != null) {
            Integer likes = article.getLikes();
            article.setLikes(likes + skip);
            newsArticleService.update(article);
        }
    }
}
