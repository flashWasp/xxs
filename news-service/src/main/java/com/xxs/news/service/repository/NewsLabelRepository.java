package com.xxs.news.service.repository;

import com.xxs.news.service.entity.NewsLabel;
import com.baomidou.mybatisplus.service.IService;
import groovyjarjarantlr.debug.NewLineEvent;

import java.util.List;

/**
 * <p>
 * 新闻模块-标签 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
public interface NewsLabelRepository extends IService<NewsLabel> {
    NewsLabel findLabelByNo(String labelNo);
    List<NewsLabel> listLabelByName(String name);
    List<NewsLabel> listAll();
}
