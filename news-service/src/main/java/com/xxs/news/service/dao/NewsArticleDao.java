package com.xxs.news.service.dao;

import com.xxs.news.service.entity.NewsArticle;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-31
 */
public interface NewsArticleDao extends BaseMapper<NewsArticle> {

}
