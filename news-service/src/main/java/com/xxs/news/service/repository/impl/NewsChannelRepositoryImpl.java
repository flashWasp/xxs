package com.xxs.news.service.repository.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.dao.NewsChannelDao;
import com.xxs.news.service.entity.NewsChannel;
import com.xxs.news.service.entity.request.NewsChannelSearchReq;
import com.xxs.news.service.repository.NewsChannelRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新闻模块--频道 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Service
public class NewsChannelRepositoryImpl extends ServiceImpl<NewsChannelDao, NewsChannel> implements NewsChannelRepository {

    /**
     * 分页查询 wjy
     * @param req
     * @return
     */
    @Override
    public Page<NewsChannel> pageChannel(NewsChannelSearchReq req) {
        NewsChannel article = new NewsChannel();
        article.setLevel(req.getLevel());
        article.setParentId(req.getParentId());
        article.setState(0);
        Wrapper<NewsChannel> entity = new EntityWrapper<>(article);
        if (StringUtils.isNotBlank(req.getName())){
            entity.like("name",req.getName());
        }
        Page<NewsChannel> newsChannelPage = new Page<>();
        newsChannelPage.setCurrent(req.getPage());
        newsChannelPage.setSize(req.getRows());
        Page<NewsChannel> newsChannelPage1 = selectPage(newsChannelPage, entity);
        return  newsChannelPage1;
    }
}
