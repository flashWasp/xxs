package com.xxs.news.service.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.news.service.entity.NewsArticleLikeHistory;

/**
 * <p>
 * 新闻模块--点赞记录 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-11-24
 */
public interface NewsArticleLikeHistoryDao extends BaseMapper<NewsArticleLikeHistory> {

}
