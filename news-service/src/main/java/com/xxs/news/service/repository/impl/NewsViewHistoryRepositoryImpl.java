package com.xxs.news.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.news.service.dao.NewsViewHistoryDao;
import com.xxs.news.service.entity.NewsViewHistory;
import com.xxs.news.service.repository.NewsViewHistoryRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新闻模块-文章浏览记录 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-30
 */
@Service
public class NewsViewHistoryRepositoryImpl extends ServiceImpl<NewsViewHistoryDao, NewsViewHistory> implements NewsViewHistoryRepository {

    @Override
    public Page<NewsViewHistory> pageViews(String viewer, Integer current, Integer rows) {
        NewsViewHistory newsViewHistory = new NewsViewHistory();
        newsViewHistory.setViewerNo(viewer);
        newsViewHistory.setState(0);
        Wrapper<NewsViewHistory> entity = new EntityWrapper<>(newsViewHistory);
        Page<NewsViewHistory> page = new Page<>(current, rows);
        page.setRecords(baseMapper.selectPage(page,entity));
        return page;
    }

    @Override
    public Boolean del(String mapNo) {
        return delete(new Condition().eq("map_no",mapNo));
    }

    @Override
    public NewsViewHistory get(String mapNo) {
        return selectOne(new Condition().eq("map_no", mapNo).eq("state",0));
    }
}
