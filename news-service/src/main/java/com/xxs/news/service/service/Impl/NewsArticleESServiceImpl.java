package com.xxs.news.service.service.Impl;

import com.xxs.news.service.entity.NewsArticle;
import com.xxs.news.service.repository.NewsArticleESRepository;
import com.xxs.news.service.service.NewsArticleESService;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.highlight.HighlightField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;


/**
 * @author Administrator
 * @Title: NewsArticleESServiceImpl
 * @ProjectName master
 * @Description: ES服务
 * @date 2018/12/1722:32
 */
@Service("newsArticleESService")
public class NewsArticleESServiceImpl implements NewsArticleESService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsArticleESServiceImpl.class);
    @Autowired
    private NewsArticleESRepository newsArticleESRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public String saveNewArticle(NewsArticle newsArticle) {
        String content = newsArticle.getContent();
        newsArticle.setContent(delHTMLTag(content));
        NewsArticle article = newsArticleESRepository.save(newsArticle);
        return article.getArticleNo();
    }

    @Override
    public   AggregatedPage<NewsArticle> highLigthQuery(String content, Integer pageNo, Integer pageSize) {
        Pageable pageable = new PageRequest((pageNo - 1 )* pageSize, pageSize);
        HighlightBuilder.Field contentHfield = new HighlightBuilder.Field("content")
                .preTags("<em style='color:red'>")
                .postTags("</em>")
                .fragmentSize(100);
        HighlightBuilder.Field titleHfield = new HighlightBuilder.Field("title")
                .preTags("<em style='color:red'>")
                .postTags("</em>")
                .fragmentSize(100);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withPageable(pageable)
                .withHighlightFields(contentHfield, titleHfield)
                .withQuery(multiMatchQuery(content, "title", "content")).
                        build();

        // 不需要高亮直接return ideas
        // AggregatedPage<Idea> ideas = elasticsearchTemplate.queryForPage(searchQuery, Idea.class);

        // 高亮字段
        AggregatedPage<NewsArticle> page = elasticsearchTemplate.queryForPage(searchQuery, NewsArticle.class, new SearchResultMapper() {

            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
                List<NewsArticle> chunk = new ArrayList<>();
                for (SearchHit searchHit : response.getHits()) {
                    if (response.getHits().getHits().length <= 0) {
                        return null;
                    }
                    NewsArticle newsArticle = new NewsArticle();
                    newsArticle.setId(Long.parseLong(searchHit.getSource().get("id").toString()));
                    newsArticle.setArticleNo(searchHit.getSource().get("articleNo").toString());
                    newsArticle.setChannelId(Integer.parseInt(searchHit.getSource().get("channelId").toString()));
                    newsArticle.setSummary(searchHit.getSource().get("summary").toString());
                    newsArticle.setViews(Integer.parseInt(searchHit.getSource().get("views").toString()));
                    newsArticle.setFavorites(Integer.parseInt(searchHit.getSource().get("favorites").toString()));
                    Object likes = searchHit.getSource().get("likes");
                    if (likes != null) {
                        newsArticle.setLikes(Integer.parseInt(likes.toString()));
                    }
                    newsArticle.setImageNo(searchHit.getSource().get("imageNo").toString());
                    String createTime = searchHit.getSource().get("createTime").toString();
                    System.out.println(createTime);
                    newsArticle.setCreateTime(new Date(Long.parseLong(createTime)));
                    String updateTime = searchHit.getSource().get("updateTime").toString();
                    newsArticle.setUpdateTime(new Date(Long.parseLong(updateTime)));
                    newsArticle.setUpdateUserUuid(searchHit.getSource().get("updateUserUuid").toString());
                    newsArticle.setState(searchHit.getSource().get("state").toString());
                    Map<String, HighlightField> highlightFields = searchHit.getHighlightFields();
                    //name or memoe
                    HighlightField ideaTitle = searchHit.getHighlightFields().get("title");
                    if (ideaTitle != null) {
                        newsArticle.setTitle(ideaTitle.fragments()[0].toString());
                    }
                    HighlightField ideaContent = searchHit.getHighlightFields().get("content");
                    if (ideaContent != null) {
                        newsArticle.setContent(ideaContent.fragments()[0].toString());
                    }
                    chunk.add(newsArticle);
                }
                if (chunk.size() > 0) {
                    return new AggregatedPageImpl<>((List<T>) chunk);
                }
                return null;
            }
        });
        return page;
    }

    /**
     * 功能：删除Html标签
     * 时间：2017-2-13 下午7:04:08
     * @param htmlStr
     * @return
     */
    private String delHTMLTag(String htmlStr) {
        String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
        String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
        String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式
        String regEx_space = "\\s*|\t|\r|\n";//定义空格回车换行符
        String regEx_chinese = "[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b]*?";//中文标点
        Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll(""); // 过滤script标签

        Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll(""); // 过滤style标签

        Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher m_html = p_html.matcher(htmlStr);
        htmlStr = m_html.replaceAll(""); // 过滤html标签

        Pattern p_space = Pattern.compile(regEx_space, Pattern.CASE_INSENSITIVE);
        Matcher m_space = p_space.matcher(htmlStr);
        htmlStr = m_space.replaceAll(""); // 过滤空格回车标签

        Pattern p_chinese = Pattern.compile(regEx_chinese, Pattern.CASE_INSENSITIVE);
        Matcher m_chinese = p_chinese.matcher(htmlStr);
        htmlStr = m_chinese.replaceAll(""); // 过滤中文标点标签
        return htmlStr.trim(); // 返回文本字符串
    }

}
