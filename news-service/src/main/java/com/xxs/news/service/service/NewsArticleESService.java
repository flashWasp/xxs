package com.xxs.news.service.service;

import com.xxs.news.service.entity.NewsArticle;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;

import java.util.List;

/**
 * @author Administrator
 * @Title: NewsArticleESService
 * @ProjectName master
 * @Description: TODO
 * @date 2018/12/1722:26
 */
public interface NewsArticleESService {

    String saveNewArticle(NewsArticle newsArticle);

    AggregatedPage<NewsArticle>  highLigthQuery(String content, Integer pageNo, Integer pageSize);


}
