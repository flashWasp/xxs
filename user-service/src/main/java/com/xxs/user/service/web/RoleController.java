package com.xxs.user.service.web;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.request.RoleCreateReq;
import com.xxs.user.service.model.request.RoleUpdateReq;
import com.xxs.user.service.service.PermissionService;
import com.xxs.user.service.service.RoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * role 用户角色管理
 */
@RestController
@RequestMapping("role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @Autowired
    private PermissionService permissionService;
    /**
     * 新建Role对象
     *
     * @return
     */
    @ApiOperation("新建role对象")
    @RequestMapping(value = "createRole", method = RequestMethod.POST)
    public AjaxResult saveRole(@RequestBody RoleCreateReq role) {
        SysRoleBase sysRoleBase = roleService.saveOne(role.getRole(), role.getDescription(), role.getPermissionIds());
        if (null != sysRoleBase) {
            return AjaxResult.getOK(sysRoleBase);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("分页查询角色")
    @RequestMapping(value = "pageRole", method = RequestMethod.GET)
    public AjaxResult pageRole(@RequestParam Integer page,
                               @RequestParam Integer rows,
                               @RequestParam String role,
                               @RequestParam Integer available) {
        Page<SysRoleBase> pageObj = roleService.pageRole(page, rows, role, available);
        if (null != pageObj) {
            List<SysRoleBase> records = pageObj.getRecords();
            if (null != records && !records.isEmpty()) {
                List<String> permissionIdsStr;
                for (SysRoleBase sysRoleBase : records) {
                    Long id = sysRoleBase.getId();
                    List<Long> permissionIds = permissionService.listIdByRoleId(id);
                    //  用long  会丢失精度
                    permissionIdsStr = permissionIds.stream().map(String::valueOf).collect(Collectors.toList());
                    sysRoleBase.setPermissionIds(permissionIdsStr);
                }
            }
            return AjaxResult.getOK(pageObj);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("列表查询角色")
    @RequestMapping(value = "listRole", method = RequestMethod.GET)
    public AjaxResult listRole() {
        List<SysRoleBase> sysRoleBases = roleService.listRole(0);
        if (!CollectionUtils.isEmpty(sysRoleBases)) {
            return AjaxResult.getOK(sysRoleBases);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("修改角色(包括逻辑删除，有效性更改)")
    @RequestMapping(value = "updateRole", method = RequestMethod.POST)
    public AjaxResult updateRole(@RequestBody RoleUpdateReq req) {
        Boolean flag = roleService.updateOne(req.getId(), req.getRole(), req.getDescription(), req.getAvailable(), req.getState(),req.getPermissionIds());
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


}
