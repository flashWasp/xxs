package com.xxs.user.service.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.user.service.dao.SysUserRoleDao;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.entity.SysUserInfo;
import com.xxs.user.service.model.entity.SysUserRole;
import com.xxs.user.service.repository.SysUserRoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户角色中间表 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Service
public class SysUserRoleRepositoryImpl extends ServiceImpl<SysUserRoleDao, SysUserRole> implements SysUserRoleRepository {

    @Override
    public List<SysUserRole> listSysRoleBaseByUserId(String userNo) {
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserUuid(userNo);
        sysUserRole.setState("0");
        Wrapper<SysUserRole> entity = new EntityWrapper<>(sysUserRole);
        return selectList(entity);
    }

    @Override
    public List<Long> listSysRoleBaseIdByUserId(String userNo) {
        List<SysUserRole> sysUserRoles = listSysRoleBaseByUserId(userNo);
        if (!CollectionUtils.isEmpty(sysUserRoles)){
            return listIdBySysUserRoles(sysUserRoles);
        }
        return null;
    }

    /**
     * 获取角色Id
     * @param sysUserRole
     * @return
     */
    private List<Long> listIdBySysUserRoles(List<SysUserRole> sysUserRole){
        return sysUserRole.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
    }


}
