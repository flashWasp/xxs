package com.xxs.user.service.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.user.service.app.UserConstant;
import com.xxs.user.service.dao.SysRoleBaseDao;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.repository.SysRoleBaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限角色表 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Service
public class SysRoleBaseRepositoryImpl extends ServiceImpl<SysRoleBaseDao, SysRoleBase> implements SysRoleBaseRepository {

    @Override
    public List<SysRoleBase> listRole(Integer available) {
        SysRoleBase sysRoleBase = new SysRoleBase();
        sysRoleBase.setAvailable(available);
        sysRoleBase.setState(UserConstant.STATE_NORMAL);
        return selectList(new EntityWrapper<>(sysRoleBase));
    }

    @Override
    public Page<SysRoleBase> pageRole(Integer available, Integer pageNo, Integer pageSize,String rowName) {
        SysRoleBase sysRoleBase = new SysRoleBase();
        sysRoleBase.setAvailable(available);
        sysRoleBase.setState(UserConstant.STATE_NORMAL);
        Page<SysRoleBase> page = new Page<>(pageNo, pageSize);
        Wrapper<SysRoleBase> entity = new EntityWrapper<>(sysRoleBase);
        entity.like("role",rowName);
        return selectPage(page,entity);
    }

    @Override
    public List<SysRoleBase> listRoleByIds(List<Long> ids) {
        Wrapper<SysRoleBase> entity = new EntityWrapper<>();
        entity.in("id",ids);
        return selectList(entity);
    }
}
