package com.xxs.user.service.model.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户基本表
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@TableName("sys_user_base")
public class SysUserBase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 默认主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * uuid内部主键
     */
    private String uuid;
    /**
     * 账号
     */
    private String account;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 加密盐
     */
    private String salt;
    /**
     * 状态  0:创建未认证 1:被删除 2:正常状态 3:被锁定
     */
    private String state;
    /**
     * 创建者UUID
     */
    @TableField("createUserUuId")
    private String createUserUuId;
    /**
     * 更新人UUId
     */
    @TableField("updateUserUuId")
    private String updateUserUuId;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreateUserUuId() {
        return createUserUuId;
    }

    public void setCreateUserUuId(String createUserUuId) {
        this.createUserUuId = createUserUuId;
    }

    public String getUpdateUserUuId() {
        return updateUserUuId;
    }

    public void setUpdateUserUuId(String updateUserUuId) {
        this.updateUserUuId = updateUserUuId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SysUserBase{" +
        ", id=" + id +
        ", uuid=" + uuid +
        ", account=" + account +
        ", userName=" + userName +
        ", password=" + password +
        ", salt=" + salt +
        ", state=" + state +
        ", createUserUuId=" + createUserUuId +
        ", updateUserUuId=" + updateUserUuId +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        "}";
    }
}
