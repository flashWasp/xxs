package com.xxs.user.service.service;

import com.xxs.user.service.model.entity.SysPermissionBase;

import java.util.List;

/**
 * Author : Fung
 * Date : 2018/11/11 0011 下午 5:35
 * Desc : 权限服务对象
 */
public interface PermissionService {

    /**
     * 创建权限对象
     * @param name
     * @param resourceType
     * @param permission
     * @param url
     * @param parentId
     * @param parentIds
     * @return
     */
    SysPermissionBase save(String name,String resourceType,
                           String permission,
                           String url,
                           Long parentId,
                           String parentIds
    );

    /**
     * 更新权限对象
     * @return
     */
    Boolean updateOne(Long id,String name,String resourceType,
                      String permission,
                      String url,
                      Long parentId,
                      String parentIds);

    /**
     * 删除权限对象
     * @param id
     * @return
     */
    Boolean delOne(Long id);

    /**
     * 根据权限Id获取权限对象
     * @param ids
     * @return
     */
    List<SysPermissionBase> listByIds(List<Long> ids);

    /**
     * 获取所有权限对象
     * @return
     */
    List<SysPermissionBase> listAll();

    /**
     * 根据父权限获取子权限集合对象
     * @param id
     * @return
     */
    List<SysPermissionBase> listByParentId(Long id);

    /**
     * 获取所有一级权限对象
     * @return
     */
    List<SysPermissionBase> listFirstLv();


    /*========== Permission 权限对象 ============*/

    /**
     * 根据角色Id获取权限Id集合
     * @param roleId
     * @return
     */
    List<Long> listIdByRoleId(Long roleId);

    /**
     * 根据角色Id获取权限集合
     * @param roleId
     * @return
     */
    List<SysPermissionBase> listByRoleId(Long roleId);

    /**
     * 根据权限Id获取权限对象
     * @param permissionId
     * @return
     */
    SysPermissionBase getById(Long permissionId);

    /**
     * 根据父级Id获取子权限Id集合
     * @param parentId
     * @return
     */
    List<Long> listIdByParentId(Long parentId);

    /**
     * 获取一级权限Id集合
     * @return
     */
    List<Long> listFirstLevIds();

}
