package com.xxs.user.service.model.request;

import java.util.List;

/**
 * 角色创建请求对象
 */
public class RoleCreateReq {

    /**
     * 角色名
     */
    private String role;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 是否可用
     */
    private Integer available;

    /**
     * 权限集合Id
     */
    private List<Long> permissionIds;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }
}
