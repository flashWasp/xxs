package com.xxs.user.service.service.impl;

import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.user.service.app.UserConstant;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.repository.SysPermissionBaseRepository;
import com.xxs.user.service.repository.SysRolePermissionRepository;
import com.xxs.user.service.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * Author : Fung
 * Date : 2018/11/11 0011 下午 5:35
 * Desc : 权限服务
 */
@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private SysPermissionBaseRepository sysPermissionBaseRepository;

    @Autowired
    private SysRolePermissionRepository sysRolePermissionRepository;

    @Override
    public SysPermissionBase getById(Long permissionId) {
        return sysPermissionBaseRepository.selectById(permissionId);
    }

    @Override
    public SysPermissionBase save(String name, String resourceType, String permission, String url,
                                  Long parentId, String parentIds) {
        return insertPermissionBase(name, resourceType, permission, url, parentId, parentIds);
    }

    @Override
    public Boolean updateOne(Long id, String name, String resourceType,
                             String permission, String url,
                             Long parentId, String parentIds) {
        SysPermissionBase sysPermissionBase = sysPermissionBaseRepository.selectById(id);
        if (null != sysPermissionBase) {
            sysPermissionBase.setName(name);
            sysPermissionBase.setResourceType(resourceType);
            sysPermissionBase.setPermission(permission);
            sysPermissionBase.setUrl(url);
            sysPermissionBase.setParentId(parentId);
            sysPermissionBase.setParentIds(parentIds);
            return sysPermissionBaseRepository.updateAllColumnById(sysPermissionBase);
        }
        return false;
    }
    /**
     * 列表查询所有基本权限
     *
     * @return
     */
    @Override
    public List<SysPermissionBase> listAll() {
        return sysPermissionBaseRepository.listPermissions();
    }

    @Override
    public List<SysPermissionBase> listByIds(List<Long> ids) {
        return sysPermissionBaseRepository.listSysPermissionBase(ids);
    }

    @Override
    public List<SysPermissionBase> listByParentId(Long id) {
        return sysPermissionBaseRepository.listPermissionByParentId(id);
    }

    @Override
    public List<Long> listIdByParentId(Long parentId) {
        return sysPermissionBaseRepository.listPermissionIdByParentId(parentId);
    }

    @Override
    public List<SysPermissionBase> listFirstLv() {
        return sysPermissionBaseRepository.listFirstLevelPermission();
    }

    @Override
    public List<Long> listFirstLevIds() {
        return sysPermissionBaseRepository.listFirstLevelPermissionIds();
    }

    @Override
    public List<SysPermissionBase> listByRoleId(Long roleId) {
        List<Long> permissionIds = sysRolePermissionRepository.listPermissionIdByRoleId(roleId);
        if (!CollectionUtils.isEmpty(permissionIds)) {
            return sysPermissionBaseRepository.listSysPermissionBase(permissionIds);
        }
        return null;
    }

    @Override
    public List<Long> listIdByRoleId(Long roleId) {
        return sysRolePermissionRepository.listPermissionIdByRoleId(roleId);
    }

    @Override
    public Boolean delOne(Long id) {
        SysPermissionBase sysPermissionBase = sysPermissionBaseRepository.selectById(id);
        if (sysPermissionBase != null) {
            sysPermissionBase.setState(UserConstant.STATE_DELETE);
            return sysPermissionBaseRepository.updateAllColumnById(sysPermissionBase);
        }
        return false;
    }

    /**
     * 插入权限角色
     *
     * @param name
     * @param resourceType
     * @param permission
     * @param url
     * @param parentId
     * @param parentIds
     * @return
     */
    private SysPermissionBase insertPermissionBase(String name, String resourceType, String permission, String url,
                                                   Long parentId, String parentIds) {
        SysPermissionBase sysPermissionBase = new SysPermissionBase();
        sysPermissionBase.setName(name);
        sysPermissionBase.setResourceType(resourceType);
        sysPermissionBase.setParentId(parentId);
        sysPermissionBase.setParentIds(parentIds);
        sysPermissionBase.setCreateTime(new Date());
        sysPermissionBase.setUpdateTime(new Date());
        sysPermissionBase.setState(UserConstant.STATE_UNVERIFIED);
        sysPermissionBase.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysPermissionBase.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysPermissionBase.setAvailable(0);
        sysPermissionBase.setUrl(url);
        sysPermissionBaseRepository.insert(sysPermissionBase);
        return sysPermissionBase;
    }

}
