package com.xxs.user.service.repository;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.user.service.model.entity.SysRoleBase;

import java.util.List;

/**
 * <p>
 * 权限角色表 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysRoleBaseRepository extends IService<SysRoleBase> {

    /**
     * 根据状态获取角色集合
     * @param
     * @return
     */
    List<SysRoleBase> listRole(Integer available);

    Page<SysRoleBase> pageRole(Integer available,Integer pageNo,Integer pageSize,String roleName);

    List<SysRoleBase> listRoleByIds(List<Long> ids);

}
