package com.xxs.user.service.service.impl;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.user.service.app.UserConstant;
import com.xxs.user.service.dao.SysUserRoleDao;
import com.xxs.user.service.model.dto.UserDto;
import com.xxs.user.service.model.dto.UserSimpleDto;
import com.xxs.user.service.model.entity.SysUserBase;
import com.xxs.user.service.model.entity.SysUserInfo;
import com.xxs.user.service.model.entity.SysUserRole;
import com.xxs.user.service.model.request.UserRequest;
import com.xxs.user.service.repository.SysUserBaseRepository;
import com.xxs.user.service.repository.SysUserInfoRepository;
import com.xxs.user.service.repository.SysUserRoleRepository;
import com.xxs.user.service.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserBaseRepository sysUserBaseRepository;

    @Autowired
    private SysUserInfoRepository sysUserInfoRepository;

    @Autowired
    private SysUserRoleRepository sysUserRoleRepository;

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    @Autowired
    protected HttpSession session;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 创建/更新用户
     *
     * @param userRequest
     * @return
     */
    @Override
    @Transactional
    public Boolean saveUser(UserRequest userRequest) {
        if (userRequest != null){
            // 插入用户信息
            String uuid = KeyUtil.getUniqueKey();
            String pwd = encryptPwd(userRequest.getPassword());
            Boolean saveUserBase = insertUserBase(uuid, userRequest.getUserName(), userRequest.getAccount(), pwd);
            Boolean saveUserInfo = insertUserInfo(uuid, userRequest.getMobile(), userRequest.getEmail());
//            Boolean saveUserRole = insertUserRole(userRequest.getRoleIds(), uuid);
            if (saveUserBase && saveUserInfo) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据UUID获取用户具体信息
     *
     * @param uuid
     * @return
     */
    public UserDto findUserDtoByUUID(String uuid) {
        // 用户系统信息
        SysUserBase sysUserBase = sysUserBaseRepository.findUserByUUID(uuid);
        // 用户基本信息
        SysUserInfo sysUserInfo = sysUserInfoRepository.findUserInfoByUUID(uuid);
        return userToUserDto(sysUserBase,sysUserInfo);
    }

    /**
     * 根据账号获取用户对象
     *
     * @param account
     * @return
     */
    public UserDto findUserDtoByAccount(String account) {
        SysUserBase sysUserBase = sysUserBaseRepository.findUserByAccount(account);
        SysUserInfo sysUserInfo = sysUserInfoRepository.findUserInfoByUUID(sysUserBase.getUuid());
        return  userToUserDto(sysUserBase,sysUserInfo);
    }

    @Override
    public UserSimpleDto findSimpleDtoByUserUUID(String uuid) {
        UserDto userDto = findUserDtoByUUID(uuid);
        return userDtoToSimple(userDto);
    }

    @Override
    public UserSimpleDto findSimpleUserByAccount(String account) {
        UserDto userDto = findUserDtoByAccount(account);
        return userDtoToSimple(userDto);

    }

    @Override
    public List<Long> listUserId() {
        return sysUserBaseRepository.listUserId();
    }

    @Override
    public Page<SysUserBase> pageUser(Integer page, Integer rows) {
        return sysUserBaseRepository.pageUser(page,rows);
    }

    @Override
    public Boolean updateUser(UserRequest request, String uuid) {
        SysUserBase user = sysUserBaseRepository.findUserByUUID(uuid);
        SysUserInfo userInfo = sysUserInfoRepository.findUserInfoByUUID(uuid);
        if (user != null && userInfo != null){
            //  先删后增
            sysUserRoleDao.updateByUserUuid(uuid);
            insertUserRole(request.getRoleIds(), uuid);
            userInfo.setMobile(request.getMobile());
            userInfo.setGender(request.getGender());
            userInfo.setEmail(request.getEmail());
            return sysUserInfoRepository.updateAllColumnById(userInfo);
        }
        return false;
    }

    @Override
    public Boolean updateState(String uuid, String state) {
        SysUserBase user = sysUserBaseRepository.findUserByUUID(uuid);
        SysUserInfo userInfo = sysUserInfoRepository.findUserInfoByUUID(uuid);
        if (null != user && null != userInfo){
            user.setState(state);
            boolean updateUser = sysUserBaseRepository.updateAllColumnById(user);
            boolean updateUserInfo = sysUserInfoRepository.updateAllColumnById(userInfo);
            return updateUser&&updateUserInfo;
        }
        return false;
    }

    @Override
    public Boolean updateUserPwd(String uuid, String newPwd) {
        SysUserBase user = sysUserBaseRepository.findUserByUUID(uuid);
        if (user != null){
            String encodePwd = passwordEncoder.encode(newPwd);
            user.setPassword(encodePwd);
            return sysUserBaseRepository.updateAllColumnById(user);
        }
        return false;
    }

    @Override
    public Boolean deleteUser(String uuid) {
        return sysUserBaseRepository.updateUserState(UserConstant.STATE_DELETE, uuid);
    }

    @Override
    public Page<SysUserBase> pageUser(Integer page, Integer rows, String account, String userName, String state) {
        return sysUserBaseRepository.pageUser(page,rows,account,userName,state);
    }

    /**
     * 通过用户uuid  查找用户信息扩展表
     * @param userUuid
     * @return
     */
    @Override
    public SysUserInfo findUserInfo(String userUuid) {
        return sysUserInfoRepository.findUserInfoByUUID(userUuid);
    }

    /**
     * 加密
     * @param pwd
     * @return
     */
    private String encryptPwd(String pwd){
        // 加密
        return passwordEncoder.encode(pwd);
    }

    /**
     * 插入用户基本信息
     * @param uuid
     * @param userName
     * @param account
     * @param password
     * @return
     */
    private Boolean insertUserBase(String uuid,String userName,String account,String password){
        SysUserBase sysUserBase = new SysUserBase();
        sysUserBase.setState(UserConstant.STATE_NORMAL);
        sysUserBase.setUuid(uuid);
        sysUserBase.setUserName(userName);
        sysUserBase.setUpdateUserUuId(TokenRedisRepository.getCurUserUUID());
        sysUserBase.setCreateUserUuId(TokenRedisRepository.getCurUserUUID());
        sysUserBase.setUpdateTime(new Date());
        sysUserBase.setCreateTime(new Date());
        sysUserBase.setAccount(account);
        sysUserBase.setPassword(password);
        return sysUserBaseRepository.insertAllColumn(sysUserBase);
    }

    /**
     * 插入用户详情
     * @param uuid
     * @param mobile
     * @param email
     * @return
     */
    private Boolean insertUserInfo(String uuid,String mobile,String email){
        SysUserInfo sysUserInfo = new SysUserInfo();
        sysUserInfo.setUserUuid(uuid);
        sysUserInfo.setMobile(mobile);
        sysUserInfo.setEmail(email);
        sysUserInfo.setLastLoginTime(new Date());
        sysUserInfo.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysUserInfo.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysUserInfo.setUpdateTime(new Date());
        sysUserInfo.setCreateTime(new Date());
        sysUserInfo.setState(UserConstant.STATE_NORMAL);
        return sysUserInfoRepository.insert(sysUserInfo);
    }


    /**
     * 组装 用户角色关联表
     * @param roleIds
     * @param userUuid
     * @return
     */
    private List<SysUserRole> packUserRole(List<Long> roleIds, String userUuid) {
        List<SysUserRole> sysUserRoleList = new ArrayList<>();
        for (Long roleId :roleIds) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserUuid(userUuid);
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
            sysUserRole.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
            sysUserRole.setState(UserConstant.STATE_NORMAL);
            sysUserRole.setUpdateTime(new Date());
            sysUserRole.setCreateTime(new Date());
            sysUserRoleList.add(sysUserRole);
        }
        return sysUserRoleList;
    }

    /**
     * 插入用户角色关联
     * @param roleIds
     * @return
     */
    private Boolean insertUserRole(List<Long> roleIds,String uuid){
        //  新增 用户角色 关联表
        if (roleIds != null && !roleIds.isEmpty()) {
           return  sysUserRoleRepository.insertBatch(packUserRole(roleIds,uuid));
        }
        return false;
    }

    /**
     * 用户信息组装成dto
     * @return
     */
    private UserDto userToUserDto(SysUserBase sysUserBase,SysUserInfo sysUserInfo){
        UserDto userDto = new UserDto();
        if (sysUserBase != null && sysUserInfo != null) {
            BeanUtils.copyProperties(sysUserBase, userDto);
            BeanUtils.copyProperties(sysUserInfo, userDto);
            return userDto;
        }
        return null;
    }

    /**
     * 封装用户dto到simpleDto
     * @param dto
     * @return
     */
    private UserSimpleDto userDtoToSimple(UserDto dto){
        UserSimpleDto userSimpleDto = new UserSimpleDto();
        BeanUtils.copyProperties(dto, userSimpleDto);
        return userSimpleDto;
    }
}
