package com.xxs.user.service.model.dto;

import java.util.Date;

public class UserDto  {

    /**
     * uuid内部主键
     */
    private String uuid;
    /**
     * 账号
     */
    private String account;
    /**
     * 用户名
     */

    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 加密盐
     */
    private String salt;
    /**
     * 状态  0:创建未认证 1:被删除 2:正常状态 3:被锁定
     */
    private Integer state;

    /**
     * 用户移动手机
     */
    private String mobile;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * 用户总登陆次数
     */

    private Integer loginCount;
    /**
     * IP地址
     */
    private String ip;

    /**
     * 创建者UUID
     */
    private String createUserUuId;

    /**
     * 更新人UUId
     */
    private String updateUserUuId;
    /**
     * 更新时间
     */

    private Date updateTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 用户最后一次登陆时间
     */
    private Date lastLoginTime;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCreateUserUuId() {
        return createUserUuId;
    }

    public void setCreateUserUuId(String createUserUuId) {
        this.createUserUuId = createUserUuId;
    }

    public String getUpdateUserUuId() {
        return updateUserUuId;
    }

    public void setUpdateUserUuId(String updateUserUuId) {
        this.updateUserUuId = updateUserUuId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Override
    public String toString() {
        return "UserSimpleDto{" +
                "uuid='" + uuid + '\'' +
                ", account='" + account + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", state=" + state +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", loginCount=" + loginCount +
                ", ip='" + ip + '\'' +
                ", createUserUuId='" + createUserUuId + '\'' +
                ", updateUserUuId='" + updateUserUuId + '\'' +
                ", updateTime=" + updateTime +
                ", createTime=" + createTime +
                ", lastLoginTime=" + lastLoginTime +
                '}';
    }



}
