package com.xxs.user.service.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.user.service.app.UserConstant;
import com.xxs.user.service.dao.SysPermissionBaseDao;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.model.entity.SysRolePermission;
import com.xxs.user.service.repository.SysPermissionBaseRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 权限基本表 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Service
public class SysPermissionBaseRepositoryImpl extends ServiceImpl<SysPermissionBaseDao, SysPermissionBase> implements SysPermissionBaseRepository {

    @Override
    public List<SysPermissionBase> listSysPermissionBase(List<Long> ids) {
        Wrapper<SysPermissionBase> entity = new EntityWrapper<>();
        entity.in("id",ids);
        return selectList(entity);
    }

    /**
     * 列表查询所有
     * @return
     */
    @Override
    public List<SysPermissionBase> listPermissions() {
        SysPermissionBase sysPermissionBase = new SysPermissionBase();
        sysPermissionBase.setState(UserConstant.STATE_NORMAL);
        sysPermissionBase.setAvailable(0);
        Wrapper<SysPermissionBase> entity = new EntityWrapper<>(sysPermissionBase);
        return selectList(entity);
    }

    @Override
    public List<Long> listPermissionIdByParentId(Long parentId) {
        List<SysPermissionBase> sysPermissionBases = listPermissionByParentId(parentId);
        if (!CollectionUtils.isEmpty(sysPermissionBases)){
            return listIdBySysPermissionBases(sysPermissionBases);
        }
        return null;
    }

    @Override
    public List<SysPermissionBase> listPermissionByParentId(Long parentId) {
        SysPermissionBase sysPermissionBase = new SysPermissionBase();
        sysPermissionBase.setParentId(parentId);
        Wrapper<SysPermissionBase> entity = new EntityWrapper<>(sysPermissionBase);
        return selectList(entity) ;
    }

    @Override
    public List<Long> listFirstLevelPermissionIds() {
        SysPermissionBase sysPermissionBase = new SysPermissionBase();
        sysPermissionBase.setParentId(null);
        Wrapper<SysPermissionBase> entity = new EntityWrapper<>(sysPermissionBase);
        List<SysPermissionBase> sysPermissionBases = selectList(entity);
        if (!CollectionUtils.isEmpty(sysPermissionBases)){
            return listIdBySysPermissionBases(sysPermissionBases);
        }
        return null;
    }

    @Override
    public List<SysPermissionBase> listFirstLevelPermission() {
        SysPermissionBase sysPermissionBase = new SysPermissionBase();
        sysPermissionBase.setParentId(null);
        Wrapper<SysPermissionBase> entity = new EntityWrapper<>(sysPermissionBase);
        return selectList(entity);
    }

    private List<Long> listIdBySysPermissionBases(List<SysPermissionBase> sysPermissionBases){
        return sysPermissionBases.stream().map(SysPermissionBase::getId).collect(Collectors.toList());
    }
}
