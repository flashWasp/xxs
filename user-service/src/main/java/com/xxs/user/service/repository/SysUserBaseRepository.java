package com.xxs.user.service.repository;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.entity.SysUserBase;

import java.util.List;

/**
 * <p>
 * 用户基本表 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysUserBaseRepository extends IService<SysUserBase> {

    SysUserBase findUserByUUID(String uuid);

    SysUserBase findUserByAccount(String account);

    Boolean updateUserState(String state,String uuid);

    List<Long> listUserId();

    Page<SysUserBase> pageUser(Integer page, Integer rows);

    Page<SysUserBase> pageUser(Integer page, Integer rows, String account, String userName, String state);
}
