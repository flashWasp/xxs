package com.xxs.user.service.repository;


import com.baomidou.mybatisplus.service.IService;
import com.xxs.user.service.model.entity.SysPermissionBase;

import java.util.List;

/**
 * <p>
 * 权限基本表 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysPermissionBaseRepository extends IService<SysPermissionBase> {
    /**
     * 根据id集合获取权限对象集合
     * @param ids
     * @return
     */
    List<SysPermissionBase> listSysPermissionBase(List<Long> ids);

    /**
     * 列表查询全部
     * @return
     */
    List<SysPermissionBase> listPermissions();
    /**
     * 根据父级Id获取子权限Id集合
     *
     * @param parentId
     * @return
     */
    List<Long> listPermissionIdByParentId(Long parentId);

    /**
     * 根据父级Id获取子权限集合
     *
     * @param parentId
     * @return
     */
    List<SysPermissionBase> listPermissionByParentId(Long parentId);

    /**
     * 获取一级权限Id集合
     *
     * @return
     */
    List<Long> listFirstLevelPermissionIds();

    /**
     * 获取一级权限对象集合
     *
     * @return
     */
    List<SysPermissionBase> listFirstLevelPermission();
}
