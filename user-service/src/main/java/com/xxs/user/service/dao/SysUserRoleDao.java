package com.xxs.user.service.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.user.service.model.entity.SysUserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户角色中间表 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Repository
public interface SysUserRoleDao extends BaseMapper<SysUserRole> {

    int updateByUserUuid(@Param("userUuid") String userUuid);
}
