package com.xxs.user.service.app;


import org.apache.catalina.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/** 项目生命周期 */
@Component
public class AppContext implements ApplicationContextAware {

  /** logger */
  private static final Logger logger = LoggerFactory.getLogger(AppContext.class);

  /** Application context. */
  private static ApplicationContext context = null;

  /** Session contexts. */
  private static Map<String, Session> sessionContexts = new ConcurrentHashMap<String, Session>();

  /** Session context of the current thread. */
  private static ThreadLocal<Session> sessionContext = new ThreadLocal<Session>();

  /** Response context of the current thread. */
  private static ThreadLocal<HttpServletResponse> responseContext =
      new ThreadLocal<HttpServletResponse>();

  /** Request context of the current thread. */
  private static ThreadLocal<HttpServletRequest> requestContext =
      new ThreadLocal<HttpServletRequest>();

  /** Start the application. */
  public static void startApplication() {
    logger.debug("Initialized spring ioc container");
  }

  /** Sets a request with the specified servlet request. */
  public static void setRequest(final HttpServletRequest request) {
    requestContext.set(request);

    logger.debug("Request started!");
  }

  /** Ends a request. */
  public static void endRequest() {
    final HttpServletRequest requestCxt = getRequest();

    if (null == requestCxt) {
      return;
    }

    requestContext.remove();

    logger.debug("Request ended!");
  }

  /** Sets a response with the specified servlet response. */
  public static void setResponse(final HttpServletResponse response) {
    responseContext.set(response);

    logger.debug("response started!");
  }

  /** Sets a session with the specified shiro session. */
  public static void setSession(final Session session) {
    final String sessionId = (String) session.getId();

    Session currentSessionContext = sessionContexts.get(sessionId);

    if (currentSessionContext == null) {
      sessionContexts.put(sessionId, session);
    }

    sessionContext.set(session);

    logger.debug("Session started!");
  }

  /** Ends a session. */
  public static void endSession(String... sessionIds) {
    final Session sessionCxt = getSession();
    if (null != sessionCxt) {
      sessionContext.remove();
    }

    if (sessionIds != null) {
      String sessionId = sessionIds[0];
      sessionContexts.remove(sessionId);
    }

    logger.debug("Session ended!");
  }

  /** Ends the application. */
  public static void endApplication() {
    endSession();
    endRequest();
    sessionContexts.clear();

    logger.debug("spring ioc container ended");
  }
  /**
   * Gets the session context of the current thread.
   *
   * @return session context
   */
  public static Session getSession() {
    return sessionContext.get();
  }

  /**
   * Gets the response context of the current thread.
   *
   * @return response context
   */
  public static HttpServletResponse getResponse() {
    return responseContext.get();
  }

  /**
   * Gets the request context of the current thread.
   *
   * @return request context
   */
  public static HttpServletRequest getRequest() {
    return requestContext.get();
  }

  /**
   * Gets application context
   *
   * @return application context
   */
  public static ApplicationContext getApplicationContext() {
    return AppContext.context;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.context = applicationContext;
  }

  // 传入线程中
  public static <T> T getBean(String beanName) {
    return (T) context.getBean(beanName);
  }

  // 国际化使用
  public static String getMessage(String key) {
    return context.getMessage(key, null, Locale.getDefault());
  }

  // 获取当前环境
  public static String getActiveProfile() {
    return context.getEnvironment().getActiveProfiles()[0];
  }
}
