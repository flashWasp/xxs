package com.xxs.user.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.user.service.app.UserConstant;
import com.xxs.user.service.dao.SysUserBaseDao;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.entity.SysUserBase;
import com.xxs.user.service.repository.SysUserBaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户基本表 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Service
public class SysUserBaseRepositoryImpl extends ServiceImpl<SysUserBaseDao, SysUserBase> implements SysUserBaseRepository {

    @Override
    public Boolean updateUserState(String state, String uuid) {
        SysUserBase sysUserBase = new SysUserBase();
        sysUserBase.setUuid(uuid);
        sysUserBase.setState(state);
        return updateAllColumnById(sysUserBase);
    }

    @Override
    public List<Long> listUserId() {
        return null;
    }

    @Override
    public Page<SysUserBase> pageUser(Integer page, Integer rows) {
        return null;
    }

    @Override
    public Page<SysUserBase> pageUser(Integer page, Integer rows,
                                      String account,
                                      String userName, String state) {
        SysUserBase sysUserBase = new SysUserBase();
        sysUserBase.setState(state);
        Wrapper<SysUserBase> entity = new EntityWrapper<>(sysUserBase);
        entity.in("account",account);
        entity.in("userName",userName);
        Page<SysUserBase> pageObj = new Page<SysUserBase>(page,rows);
        return selectPage(pageObj,entity);
    }

    @Override
    public SysUserBase findUserByUUID(String uuid) {
        SysUserBase sysUserBase = new SysUserBase();
        sysUserBase.setUuid(uuid);
        return selectOne(new EntityWrapper<SysUserBase>(sysUserBase));
    }


    @Override
    public SysUserBase findUserByAccount(String account) {
        return selectOne(new Condition().eq("account",account).eq("state",UserConstant.STATE_NORMAL));
    }

}
