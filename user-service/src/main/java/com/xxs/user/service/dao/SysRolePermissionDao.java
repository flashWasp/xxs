package com.xxs.user.service.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.user.service.model.entity.SysRolePermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色权限中间表 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Repository
public interface SysRolePermissionDao extends BaseMapper<SysRolePermission> {



    int updateByRoleId(@Param("roleId") Long roleId);
}
