package com.xxs.user.service.model.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 权限角色表
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@TableName("sys_role_base")
public class SysRoleBase implements Serializable {

    private static final long serialVersionUID = 1L;

    //  long类型返回前端 精度丢失
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 用户描述
     */
    private String role;
    /**
     * 角色简介
     */
    private String description;
    /**
     * 有效判断 0:可用 1:不可用(默认)
     */
    private Integer available;
    /**
     * 更新人
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;
    /**
     * 创建者
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 状态 0:正常 1:被删除
     */
    private String state;

    /**
     * 权限id  1,2,3
     */
    @TableField(exist = false)
    private List<String> permissionIds;

    public List<String> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<String> permissionIds) {
        this.permissionIds = permissionIds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SysRoleBase{" +
        ", id=" + id +
        ", role=" + role +
        ", description=" + description +
        ", available=" + available +
        ", updateUserUuid=" + updateUserUuid +
        ", createUserUuid=" + createUserUuid +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", state=" + state +
        "}";
    }
}
