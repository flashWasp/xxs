package com.xxs.user.service.model.request;

import java.io.Serializable;

/**
 * 用户集合查询条件请求
 */
public class UserListSearchRequest implements Serializable {

    /**
     * 页码
     */
    private Integer pageNo;
    /**
     * 页大小
     */
    private Integer pageSize;

    /**
     * 用户名
     */
    private String  userName ;

    /**
     * 用户状态 0:创建未认证 1:被删除 2:正常状态 3:被锁定
     */
    private Integer state;

    /**
     * 性别 1 表示男性 2 表示女性
     */
    private Integer gender;


    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }


    @Override
    public String toString() {
        return "UserListSearchRequest{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", userName='" + userName + '\'' +
                ", state=" + state +
                ", gender=" + gender +
                '}';
    }
}
