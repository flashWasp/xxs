package com.xxs.user.service.model.response;

import com.xxs.user.service.model.entity.SysUserBase;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * 员工 响应信息 wjy
 */
public class EmployeeRes extends SysUserBase{

    private String mobile;

    private String email;

    private Integer gender;

    private List<String> roleIds;

    private Integer loginCount;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }
}
