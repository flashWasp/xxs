package com.xxs.user.service.web;

import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.model.request.PermissionCreateReq;
import com.xxs.user.service.model.request.PermissionUpdateReq;
import com.xxs.user.service.service.PermissionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 获取所有的权限
     */
    @ApiOperation("获取所有权限")
    @RequestMapping(value = "listAll",method = RequestMethod.GET)
    public AjaxResult listAll() {
        List<SysPermissionBase> sysPermissionBases = permissionService.listAll();
        if (!CollectionUtils.isEmpty(sysPermissionBases)) {
            return AjaxResult.getOK(sysPermissionBases);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 获取一级权限
     *
     * @return
     */
    @ApiOperation("获取一级权限")
    @RequestMapping(value = "listFirstLv",method = RequestMethod.GET)
    public AjaxResult listFirstLv() {
        List<SysPermissionBase> sysPermissionBases = permissionService.listFirstLv();
        if (!CollectionUtils.isEmpty(sysPermissionBases)) {
            return AjaxResult.getOK(sysPermissionBases);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 根据父权限获取子权限
     */
    @ApiOperation("根据父权限获取子权限")
    @RequestMapping(value = "listByParentId",method = RequestMethod.GET)
    public AjaxResult listByParentId(@RequestParam("id") Long id) {
        List<SysPermissionBase> sysPermissionBases = permissionService.listByParentId(id);
        if (!CollectionUtils.isEmpty(sysPermissionBases)) {
            return AjaxResult.getOK(sysPermissionBases);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 根据角色获取权限对象
     */
    @ApiOperation("根据角色获取权限对象")
    @RequestMapping(value = "listPermissionByRoleId",method = RequestMethod.GET)
    public AjaxResult listPermissionByRoleId(@RequestParam("roleId")Long roleId) {
        List<SysPermissionBase> sysPermissionBases = permissionService.listByRoleId(roleId);
        if (!CollectionUtils.isEmpty(sysPermissionBases)) {
            return AjaxResult.getOK(sysPermissionBases);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 更新权限
     */
    @ApiOperation("更新权限")
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public AjaxResult update(@RequestBody  PermissionUpdateReq req){
        Boolean flag = permissionService.updateOne(req.getId(),
                req.getName(), req.getResourceType(),
                req.getPermission(),
                req.getUrl(),
                req.getParentId(),
                req.getParentIds());
        if (flag){
            return AjaxResult.getOK();
        }
        return  AjaxResult.getError(ResultCode.ParamException);
    }
    /**
     * 添加权限
     */
    @ApiOperation("添加权限")
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public AjaxResult add(@RequestBody  PermissionCreateReq req){
        SysPermissionBase sysPermissionBase = permissionService.save(req.getName(),
                req.getResourceType(),
                req.getPermission(),
                req.getUrl(),
                req.getParentId(),
                req.getParentIds());
        if (null != sysPermissionBase){
            return AjaxResult.getOK(sysPermissionBase);
        }
        return AjaxResult.getError(ResultCode.ParamException);

    }

    /**
     * 删除权限
     */
    @ApiOperation("删除权限")
    @RequestMapping(value = "del",method = RequestMethod.GET)
    public AjaxResult del(@RequestParam("id") Long id){
        Boolean flag = permissionService.delOne(id);
        if (flag){
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }
}
