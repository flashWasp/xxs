package com.xxs.user.service.app;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;


public final class AppProperties  {

    /**
     * logger
     */
    private final static Logger logger = LoggerFactory.getLogger(AppProperties.class);


    /**
     * 属性配置文件
     */
    private static final Properties config = new Properties();

    static{
        String activeProfile = AppContext.getActiveProfile();
        String trainPath = "/xxs/xxs-" + activeProfile + ".properties";
        final InputStream resourceAsStream = AppProperties.class.getResourceAsStream(trainPath);
        if (null != resourceAsStream) {
            try {
                config.load(resourceAsStream);
                logger.debug("Loaded xxs.properties");
            } catch(Exception e) {
                logger.error("Not found xxs.properties");

                throw new RuntimeException("Not found train.properties");
            }
        }

    }



    /**
     * Gets a configuration string property with the specified key.
     * from webbss.properties
     *
     * @param key the specified key
     * @return string property value corresponding to the specified key, returns {@code null} if not found
     */
    public static String get(final String key) {
        return config.getProperty(key);
    }


    public static Boolean getBoolean(final String key) {
        final String stringValue = get(key);

        if(stringValue != null) {
            return Boolean.valueOf(stringValue);
        }

        return false;
    }


    public static Float getFloat(final String key) {
        final String stringValue = get(key);
        if (null == stringValue) {
            return null;
        }

        return Float.valueOf(stringValue);
    }



    public static Integer getInt(final String key) {
        final String stringValue = get(key);
        if (null == stringValue) {
            return null;
        }

        return Integer.valueOf(stringValue);
    }


    public static Long getLong(final String key) {
        final String stringValue = get(key);
        if (null == stringValue) {
            return null;
        }

        return Long.valueOf(stringValue);
    }
}
