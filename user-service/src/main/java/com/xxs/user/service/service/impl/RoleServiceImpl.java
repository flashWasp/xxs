package com.xxs.user.service.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.user.service.app.UserConstant;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.entity.SysRolePermission;
import com.xxs.user.service.repository.SysRoleBaseRepository;
import com.xxs.user.service.repository.SysRolePermissionRepository;
import com.xxs.user.service.repository.SysUserRoleRepository;
import com.xxs.user.service.service.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author : Fung
 * Date : 2018/11/11 0011 下午 5:20
 * Desc :
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

    @Autowired
    private SysRoleBaseRepository sysRoleBaseRepository;

    @Autowired
    private SysRolePermissionRepository sysRolePermissionRepository;

    @Autowired
    private SysUserRoleRepository sysUserRoleRepository;

    /*============= Role 角色对象===============*/

    @Override
    public SysRoleBase getRole(Long id) {
        return sysRoleBaseRepository.selectById(id);
    }

    @Override
    @Transactional
    public SysRoleBase saveOne(String role, String description, List<Long> permissionIds) {
        SysRoleBase sysRoleBase = insertRoleBase(role, description);
        // 新建角色关联关系
        if (sysRoleBase != null) {
            Boolean flag = insertBatchRolePermission(permissionIds, sysRoleBase.getId());
            if (flag){
                return sysRoleBase;
            }
        }
        return null;
    }

    @Override
    @Transactional
    public Boolean updateOne(Long id, String role, String description, Integer available, String state, List<Long> permissionIds) {
        SysRoleBase sysRoleBase = packSysRoleBase(id, role, description, available, state);
        if (null != sysRoleBase) {
            //  修改 权限
            if (null != permissionIds && !permissionIds.isEmpty()) {
                //  先删
                sysRolePermissionRepository.updateByRoleId(id);
                // 后增
                insertBatchRolePermission(permissionIds, sysRoleBase.getId());
            }
            return sysRoleBaseRepository.updateAllColumnById(sysRoleBase);
        }
        return false;
    }

    @Override
    public List<SysRoleBase> listRole(Integer available) {
        return sysRoleBaseRepository.listRole(available);
    }

    @Override
    public Page<SysRoleBase> pageRole(Integer page, Integer rows, String role, Integer available) {
        return sysRoleBaseRepository.pageRole(available, page, rows, role);
    }

    @Override
    public List<SysRoleBase> listByUserId(String userNo) {
        List<Long> roleIds = sysUserRoleRepository.listSysRoleBaseIdByUserId(userNo);
        if (!CollectionUtils.isEmpty(roleIds)) {
            return sysRoleBaseRepository.listRoleByIds(roleIds);
        }
        return null;
    }

    @Override
    public List<Long> listIdByUserId(String userNo) {
        return sysUserRoleRepository.listSysRoleBaseIdByUserId(userNo);
    }

    /**
     * 创建基本角色对象
     * @param role
     * @param description
     * @return
     */
    private SysRoleBase insertRoleBase(String role,String description){
        // 新建角色
        SysRoleBase sysRoleBase = new SysRoleBase();
        sysRoleBase.setRole(role);
        sysRoleBase.setDescription(description);
        sysRoleBase.setAvailable(0);
        sysRoleBase.setState(UserConstant.STATE_NORMAL);
        sysRoleBase.setCreateTime(new Date());
        sysRoleBase.setUpdateTime(new Date());
        sysRoleBase.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysRoleBase.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysRoleBaseRepository.insertAllColumn(sysRoleBase);
        return sysRoleBase;
    }

    /**
     * 组装角色权限 关系表
     * @param permissionIds
     * @param roleId
     * @return
     */
    private List<SysRolePermission> packSysRolePermission(List<Long> permissionIds, Long roleId) {
        List<SysRolePermission> sysRolePermissionList = new ArrayList<>();
        for (Long permissionId : permissionIds) {
            SysRolePermission sysRolePermission = new SysRolePermission();
            sysRolePermission.setRoleId(roleId);
            sysRolePermission.setCreateTime(new Date());
            sysRolePermission.setUpdateTime(new Date());
            sysRolePermission.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
            sysRolePermission.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
            sysRolePermission.setPermissionId(permissionId);
            sysRolePermission.setState(UserConstant.STATE_NORMAL);
            sysRolePermissionList.add(sysRolePermission);
        }
        return sysRolePermissionList;
    }

    /**
     * 批量插入角色权限关联集合
     * @param permissionIds
     * @param id
     * @return
     */
    private Boolean insertBatchRolePermission(List<Long> permissionIds,Long id){
        if (!CollectionUtils.isEmpty(permissionIds)) {
            List<SysRolePermission> sysRolePermissionList = this.packSysRolePermission(permissionIds,id);
            return sysRolePermissionRepository.insertBatch(sysRolePermissionList);
        }
        return  false;
    }


    private SysRoleBase packSysRoleBase(Long id,String role, String description, Integer available, String state){
        SysRoleBase sysRoleBase = sysRoleBaseRepository.selectById(id);
        if (StringUtils.isNotBlank(description)) {
            sysRoleBase.setDescription(description);
        }
        if (StringUtils.isNotBlank(role)) {
            sysRoleBase.setRole(role);
        }
        if (null != available) {
            sysRoleBase.setAvailable(available);
        }
        if (null != state) {
            sysRoleBase.setState(state);
        }
        sysRoleBase.setUpdateTime(new Date());
        sysRoleBase.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        return sysRoleBase;
    }

}
