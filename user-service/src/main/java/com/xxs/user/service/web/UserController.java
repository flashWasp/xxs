package com.xxs.user.service.web;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.user.service.model.dto.UserDto;
import com.xxs.user.service.model.entity.SysUserBase;
import com.xxs.user.service.model.entity.SysUserInfo;
import com.xxs.user.service.model.request.UserRequest;
import com.xxs.user.service.model.response.EmployeeRes;
import com.xxs.user.service.service.MemberService;
import com.xxs.user.service.service.RoleService;
import com.xxs.user.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api("用户信息管理")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MemberService memberService;

    @ApiOperation(value = "新建用户")
    @RequestMapping(value = "/createEmployee",method = RequestMethod.POST)
    public AjaxResult saveUser(@RequestBody UserRequest req) {
        Boolean flag = userService.saveUser(req);
        if(flag) {
            Map<String, Object> params = new HashMap<>();
            params.put("email",req.getEmail());
            params.put("mobileNo",req.getMobile());
            params.put("account",req.getAccount());
            params.put("pwd",req.getPassword());
            return AjaxResult.getOK(memberService.registered(params));
        }
            return AjaxResult.getError(ResultCode.ParamException);
    }


    @ApiOperation(value = "更新用户")
    @RequestMapping(value = "/updateEmployee",method = RequestMethod.POST)
    public AjaxResult updateUser(@RequestBody UserRequest request){
        Boolean flag = userService.updateUser(request, request.getUuid());
        if (flag){
            return AjaxResult.getOK();
        }
            return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation(value = "更新员工状态")
    @RequestMapping(value = "/updateEmployeeState",method = RequestMethod.GET)
    public Object updateEmployeeState(@RequestParam String uuid,@RequestParam String state){
        Boolean flag = userService.updateState(uuid, state);
        if (flag){
            return  AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation("分页查询员工")
    @RequestMapping(value = "/pageEmployee",method = RequestMethod.GET)
    public AjaxResult pageEmployee(@RequestParam Integer page,
                                   @RequestParam Integer rows,
                                   @RequestParam(required = false) String account,
                                   @RequestParam(required = false) String userName,
                                   @RequestParam(required = false) String state){
        Page<SysUserBase> pageObj = userService.pageUser(page, rows, account, userName, state);
        if (null != pageObj){
            HashMap<String,Object> map = new HashMap<>();
            List<EmployeeRes> result = new ArrayList<>();
            List<SysUserBase> records = pageObj.getRecords();
            if (null != records && !records.isEmpty()) {
                for (SysUserBase sysUserBase : records) {
                    EmployeeRes employeeRes = new EmployeeRes();
                    SysUserInfo userInfo = userService.findUserInfo(sysUserBase.getUuid());
                    BeanUtils.copyProperties(sysUserBase,employeeRes);
                    employeeRes.setLoginCount(userInfo.getLoginCount());
                    employeeRes.setEmail(userInfo.getEmail());
                    employeeRes.setMobile(userInfo.getMobile());
                    employeeRes.setGender(userInfo.getGender());
                    //  获取权限id集合
                    List<Long> roleIds = roleService.listIdByUserId(sysUserBase.getUuid());
                    //  用long  会丢失精度
                    employeeRes.setRoleIds(roleIds.stream().map(String::valueOf).collect(Collectors.toList()));
                    result.add(employeeRes);
                }
            }
            map.put("total",pageObj.getTotal());
            map.put("rows",result);
            return AjaxResult.getOK(map);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


    @ApiOperation(value = "获取员工信息")
    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public Object get(@RequestParam String uuid){
        UserDto userDto = userService.findUserDtoByUUID(uuid);
        if (userDto != null){
            return  AjaxResult.getOK(userDto);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    @ApiOperation(value = "获取员工信息")
    @RequestMapping(value = "/getByAccount",method = RequestMethod.GET)
    public Object getByAccount(@RequestParam String account){
        UserDto userDto = userService.findUserDtoByAccount(account);
        if (userDto != null){
            return  AjaxResult.getOK(userDto);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }
}
