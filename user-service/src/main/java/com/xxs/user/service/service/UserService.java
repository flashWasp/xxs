package com.xxs.user.service.service;


import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.user.service.model.dto.UserDto;
import com.xxs.user.service.model.dto.UserSimpleDto;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.entity.SysUserBase;
import com.xxs.user.service.model.entity.SysUserInfo;
import com.xxs.user.service.model.request.UserRequest;

import java.util.List;

public interface UserService  {

    /*============= 用户对象  =============*/
    /* ===============  添加  ==================  */

    /**
     * 添加用户对象
     */
    Boolean saveUser(UserRequest userRequest);

    /**
     * 更新用户对象
     */
    Boolean updateUser(UserRequest userRequest, String uuid);

    /**
     * 更新用户状态
     * @param uuid
     * @param state
     * @return
     */
    Boolean updateState(String uuid,String state);

    /**
     * 通过新密码与旧密码验证 更新用户密码
     * @param uuid
     * @param newPwd
     * @return
     */
    Boolean updateUserPwd(String uuid,String newPwd);

    /* ===============  单对象查询  ==================  */

    UserDto findUserDtoByUUID(String uuid);

    UserDto findUserDtoByAccount(String account);

    UserSimpleDto findSimpleDtoByUserUUID(String uuid);

    UserSimpleDto findSimpleUserByAccount(String account);


    /* ===============  单对象查询  ==================  */
    List<Long> listUserId();

    Page<SysUserBase> pageUser(Integer page, Integer rows);

    /* ===============  删除  ==================  */

    /**
     * 删除用户对象
     */
    Boolean deleteUser(String uuid);





    Page<SysUserBase> pageUser(Integer page, Integer rows, String account, String userName, String state);

    SysUserInfo findUserInfo(String userUuid);
}
