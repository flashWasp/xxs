package com.xxs.user.service.repository;

import com.baomidou.mybatisplus.service.IService;
import com.xxs.user.service.model.entity.SysUserInfo;

;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysUserInfoRepository extends IService<SysUserInfo> {
    SysUserInfo findUserInfoByUUID(String uuid);
}
