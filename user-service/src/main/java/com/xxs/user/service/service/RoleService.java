package com.xxs.user.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.model.entity.SysRoleBase;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Author : Fung
 * Date : 2018/11/11 0011 下午 4:48
 * Desc :
 */
public interface RoleService {


    /*============= Role 角色对象===============*/
    /**
     * 保存对象
     * @param role
     * @param description
     * @return
     */
    SysRoleBase saveOne(String role,String description,List<Long> permissionIds);

    /**
     * 获取用户对象
     * @return
     */
    SysRoleBase getRole(Long id);

    /**
     * 更新用户角色
     * @return
     */
    Boolean updateOne(Long id,String role,String description,Integer available,String state,List<Long> permissionIds);

    /**
     * 根据状态获取用户角色
     * @return
     */
    List<SysRoleBase> listRole(Integer available);

    /**
     * 分页查询角色对象
     * @param page
     * @param rows
     * @param role
     * @param available
     * @return
     */
    Page<SysRoleBase> pageRole(Integer page,
                               Integer rows,
                               String role,
                               Integer available);





    /*============= 用户角色 ==============*/

    /**
     * 根据用户Id获取角色集合
     * @param userNo
     * @return
     */
    List<SysRoleBase> listByUserId(String userNo);

    /**
     * 根据用户Id集合获取角色Id集合
     * @param userNo
     * @return
     */
    List<Long> listIdByUserId(String userNo);
}
