package com.xxs.user.service.model.request;

import java.util.List;

/**
 * 角色更新请求
 */
public class RoleUpdateReq {
    /**
     * 角色Id
     */
    private Long id;

    /**
     * 角色名
     */
    private String role;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 是否可用
     */
    private Integer available;

    /**
     * 角色逻辑删除
     */
    private String state;

    /**
     * 权限集合Id
     */
    private List<Long> permissionIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }
}
