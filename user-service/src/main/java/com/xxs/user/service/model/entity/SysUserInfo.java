package com.xxs.user.service.model.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@TableName("sys_user_info")
public class SysUserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户uuid
     */
    @TableField("userUuid")
    private String userUuid;
    /**
     * 用户移动手机
     */
    private String mobile;
    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 1 表示男性 2 表示女性
     */
    private Integer gender;

    /**
     * 用户总登陆次数
     */
    @TableField("loginCount")
    private Integer loginCount;
    /**
     * IP地址
     */
    private String ip;
    /**
     * 用户最后一次登陆时间
     */
    @TableField("lastLoginTime")
    private Date lastLoginTime;

    @TableField("createUserUuid")
    private String createUserUuid;

    @TableField("updateUserUuid")
    private String updateUserUuid;

    @TableField("updateTime")
    private Date updateTime;

    @TableField("createTime")
    private Date createTime;

    @TableField("state")
    private String state;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }


    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SysUserInfo{" +
                ", id=" + id +
                ", userUuid=" + userUuid +
                ", mobile=" + mobile +
                ", email=" + email +
                ", loginCount=" + loginCount +
                ", ip=" + ip +
                ", gender=" + gender +
                ", lastLoginTime=" + lastLoginTime +
                "}";
    }
}
