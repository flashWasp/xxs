package com.xxs.user.service.repository;


import com.baomidou.mybatisplus.service.IService;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.model.entity.SysRolePermission;

import java.util.List;

/**
 * <p>
 * 角色权限中间表 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysRolePermissionRepository extends IService<SysRolePermission> {
    /**
     * 根据角色Id获取权限Id集合
     *
     * @param roleId
     * @return
     */
    List<Long> listPermissionIdByRoleId(Long roleId);

    /**
     * 删除用户对象
     * @param id
     */
    void updateByRoleId(Long id);
}
