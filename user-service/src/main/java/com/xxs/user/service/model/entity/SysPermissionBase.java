package com.xxs.user.service.model.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 权限基本表
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@TableName("sys_permission_base")
public class SysPermissionBase implements Serializable {

    private static final long serialVersionUID = 1L;

    //  long类型返回前端 精度丢失
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 资源类型
     */
    @TableField("resourceType")
    private String resourceType;
    /**
     * 资源路径
     */
    private String url;
    /**
     * 权限字符串
     */
    private String permission;
    /**
     * 父编号
     */
    @JsonSerialize(using=ToStringSerializer.class)
    @TableField("parentId")
    private Long parentId;
    /**
     * 父编号列表
     */
    @TableField("parentIds")
    private String parentIds;
    /**
     * 可用 0:不可用 1:可用
     */
    private Integer available;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;
    /**
     * 更新人
     */
    @TableField("updateUserUuid")
    private String updateUserUuid;
    /**
     * 创建人
     */
    @TableField("createUserUuid")
    private String createUserUuid;
    /**
     * 0:未删除 1:已删除
     */
    private String state;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUserUuid() {
        return updateUserUuid;
    }

    public void setUpdateUserUuid(String updateUserUuid) {
        this.updateUserUuid = updateUserUuid;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SysPermissionBase{" +
        ", id=" + id +
        ", name=" + name +
        ", resourceType=" + resourceType +
        ", url=" + url +
        ", permission=" + permission +
        ", parentId=" + parentId +
        ", parentIds=" + parentIds +
        ", available=" + available +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", updateUserUuid=" + updateUserUuid +
        ", createUserUuid=" + createUserUuid +
        ", state=" + state +
        "}";
    }
}
