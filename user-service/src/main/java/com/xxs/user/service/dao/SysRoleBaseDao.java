package com.xxs.user.service.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.user.service.model.entity.SysRoleBase;

/**
 * <p>
 * 权限角色表 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysRoleBaseDao extends BaseMapper<SysRoleBase> {

}
