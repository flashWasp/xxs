package com.xxs.user.service.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.user.service.dao.SysUserInfoDao;
import com.xxs.user.service.model.entity.SysUserInfo;
import com.xxs.user.service.repository.SysUserInfoRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Service
public class SysUserInfoRepositoryImpl extends ServiceImpl<SysUserInfoDao, SysUserInfo> implements SysUserInfoRepository {

    @Override
    public SysUserInfo findUserInfoByUUID(String uuid) {
        SysUserInfo sysUserInfo = new SysUserInfo();
        sysUserInfo.setUserUuid(uuid);
        return  selectOne(new EntityWrapper<SysUserInfo>(sysUserInfo));
    }


}
