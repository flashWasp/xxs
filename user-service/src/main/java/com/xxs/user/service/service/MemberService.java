package com.xxs.user.service.service;

import com.xxs.common.service.model.AjaxResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author Administrator
 * @Title: MemberService
 * @ProjectName master
 * @Description: 用户服务
 * @date 2018/12/1516:22
 */
@FeignClient("member-service")
public interface MemberService {

    /**
     * 注册用户信息 :
     * 版本1 :
     * 1）生成用户信息
     * 2）利用用户信息获取验证码 发送到邮箱
     * 3）验证用户信息
     * @param params
     * @return
     */
    @PostMapping("/member/registered")
    AjaxResult registered(@RequestBody Map<String,Object> params);

    /**
     * 验证用户信息
     * @param account
     * @param validCode
     * @return
     */
    @GetMapping("/member/valid")
    AjaxResult valid(@RequestParam("account") String account,
                     @RequestParam("validCode") String validCode);

}
