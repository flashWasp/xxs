package com.xxs.user.service.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.user.service.model.entity.SysPermissionBase;

/**
 * <p>
 * 权限基本表 Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysPermissionBaseDao extends BaseMapper<SysPermissionBase> {

}
