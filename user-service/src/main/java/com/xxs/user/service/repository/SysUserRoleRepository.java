package com.xxs.user.service.repository;


import com.baomidou.mybatisplus.service.IService;
import com.xxs.user.service.model.entity.SysRoleBase;
import com.xxs.user.service.model.entity.SysUserRole;

import java.util.List;

/**
 * <p>
 * 用户角色中间表 服务类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
public interface SysUserRoleRepository extends IService<SysUserRole> {
    /**
     * 根据用户获取角色对象
     */
    List<SysUserRole> listSysRoleBaseByUserId(String userNo);

    /**
     * 根据用户Id获取角色Id集合
     * @param userId
     * @return
     */
    List<Long> listSysRoleBaseIdByUserId(String userId);
}
