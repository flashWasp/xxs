package com.xxs.user.service.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.user.service.dao.SysRolePermissionDao;
import com.xxs.user.service.model.entity.SysPermissionBase;
import com.xxs.user.service.model.entity.SysRolePermission;
import com.xxs.user.service.repository.SysRolePermissionRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色权限中间表 服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-07-01
 */
@Service
public class SysRolePermissionRepositoryImpl extends ServiceImpl<SysRolePermissionDao, SysRolePermission> implements SysRolePermissionRepository {

    @Override
    public List<Long> listPermissionIdByRoleId(Long roleId) {
        SysRolePermission sysRolePermission = new SysRolePermission();
        sysRolePermission.setRoleId(roleId);
        Wrapper<SysRolePermission> entity = new EntityWrapper<>(sysRolePermission);
        entity.eq("state","noDelete");
        List<SysRolePermission> sysRolePermissions = baseMapper.selectList(entity);
        return sysRolePermissions.stream().map(SysRolePermission::getPermissionId).collect(Collectors.toList());
    }

    @Override
    public void updateByRoleId(Long id) {
        baseMapper.updateByRoleId(id);
    }


}
