package com.xxs.user.service.model.dto;

import java.util.Map;

/**
 * 用户简单对象
 */
public class UserSimpleDto {

    /**
     * 用户Id
     */
    private String uuid;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户电话号码
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 最后登录时间
     */
    private String lastLoginTime;

    /**
     * 额外元素
     */
    private Map<String,Object> extattr;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Map<String, Object> getExtattr() {
        return extattr;
    }

    public void setExtattr(Map<String, Object> extattr) {
        this.extattr = extattr;
    }

    @Override
    public String toString() {
        return "UserSimpleDto{"
                + "uuid='"
                + uuid
                + '\''
                + ", userName='"
                + userName
                + '\''
                + ", account='"
                + account
                + '\''
                + ", mobile='"
                + mobile
                + '\''
                + ", email='"
                + email
                + '\''
                + ", updateTime='"
                + updateTime
                + '\''
                + ", createTime='"
                + createTime
                + '\''
                + ", lastLoginTime='"
                + lastLoginTime
                + '\''
                + ", extattr='"
                + extattr
                + '\''
                + '}';
    }
}
