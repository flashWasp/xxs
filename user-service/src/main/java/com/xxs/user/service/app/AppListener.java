package com.xxs.user.service.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener  implements ServletContextListener {
    /**
     * Logger.
     */
    private final static Logger logger = LoggerFactory.getLogger(AppListener.class);


    /**
     * Servlet context.
     */
    private static ServletContext servletContext;

    /**
     * Gets the servlet context.
     *
     * @return the servlet context
     */
    public static ServletContext getServletContext() {
        if (null == servletContext) {
            throw new IllegalStateException("Initializes the servlet context first!");
        }

        return servletContext;
    }

    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        servletContext = servletContextEvent.getServletContext();
        AppContext.startApplication();
        logger.info("Initializing the context....");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        AppContext.endApplication();
        logger.info("Destroying the context....");
    }

}
