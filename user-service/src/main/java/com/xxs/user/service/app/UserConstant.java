package com.xxs.user.service.app;

/**
 * 用户模块常量
 */
public class UserConstant {


    /**
     * 未认证
     */
    public static final String STATE_UNVERIFIED = "isUnverified";

    /**
     * 正常
     */
    public static final String STATE_NORMAL = "isNormal";

    /**
     * 被停用
     */
    public static final String STATE_LOCKED = "isLocked";
    /**
     * 被删除
     */
    public static final String STATE_DELETE = "isDelete";

}
