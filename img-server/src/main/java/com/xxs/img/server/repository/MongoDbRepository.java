package com.xxs.img.server.repository;


import com.mongodb.gridfs.GridFSDBFile;
import com.xxs.common.service.exception.ServiceException;
import com.xxs.common.service.util.KeyUtil;
import com.xxs.img.server.entity.MongoImage;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.Date;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class MongoDbRepository {
    // 获得SpringBoot提供的mongodb的GridFS对象
    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoDbFactory mongoDbFactory;



    /*========== 文件存储 ===========*/

    public String uploadFile(MultipartFile file, InputStream is) throws IOException {

        String imageNo = KeyUtil.getUniqueKey();
        // 获得文件类型
        String contentType = file.getContentType();
        MongoImage mongoImage = new MongoImage();
        mongoImage.setCreateTime(new Date());
        mongoImage.setImageName(file.getOriginalFilename());
        mongoImage.setImageNo(imageNo);
        mongoImage.setType(contentType);
        mongoImage.setIns(IOUtils.toByteArray(is));
        mongoTemplate.save(mongoImage,"xxs");
        // 文件存储
        //GridFSFile gridFSFile = gridFsTemplate.store(is, contentType);
       // GridFSFile gridFSFile = gridFsTemplate.store(is,imageNo, contentType);
        return imageNo;
    }

    /**
     * 获取文件对象
     *
     * @param id
     * @return
     */
    public MongoImage findFile(String id) {
       Query query = new Query().addCriteria(Criteria.where("imageNo").is(id));
        //查找上传的文件
        MongoImage mongoImage = mongoTemplate.findOne(query, MongoImage.class,"xxs");
        return  mongoImage;
    }

    /**
     * 批量获取文件流对象
     * @param ids
     * @return
     */
    public List<GridFSDBFile> listFileIns(List<String> ids) {
        Query query = Query.query(Criteria.where("_id").in(ids));
        return gridFsTemplate.find(query);
    }

    /**
     * 删除文件对象
     *
     * @param id
     * @return
     */
    public Boolean deleteFile(String id) {
        Query query = Query.query(Criteria.where("_id").is(id));
        // 查询单个文件
        GridFSDBFile gfsfile = gridFsTemplate.findOne(query);
        if (gfsfile == null) {
            throw new ServiceException("文件不存在！");
        }
        gridFsTemplate.delete(query);
        return true;
    }


}
