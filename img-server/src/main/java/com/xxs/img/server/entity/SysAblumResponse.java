package com.xxs.img.server.entity;



/**
 * 系统相册返回值
 */
public class SysAblumResponse extends SysAblum {
    /**
     * 文件对象
     */
    private byte[] fileIns;

    public byte[] getFileIns() {
        return fileIns;
    }

    public void setFileIns(byte[] fileIns) {
        this.fileIns = fileIns;
    }
}
