package com.xxs.img.server;

public class AblumContants {


    /**
     * 删除状态标记位
     */

    public final  static Integer NEWS_NODELETE = 0;
    public final  static Integer NEWS_ISDELETE = 1;
    public final  static String STATUS_ISDELETE = "isDelete";
    public final static String STATUS_NODELETE = "noDelete";


}
