package com.xxs.img.server.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xxs.img.server.entity.SysAblum;

/**
 * <p>
 * 系统模块--图片服务 Mapper 接口
 * </p>
 *
 * @author caster - Fung
 * @since 2018-10-28
 */
public interface SysAblumDao extends BaseMapper<SysAblum> {

}
