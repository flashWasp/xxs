package com.xxs.img.server.entity;

import java.util.Date;

/**
 * Author : Fung
 * Date : 2018/11/1 0001 上午 9:46
 * Desc :
 */
public class MongoImage {

    /**
     * 图片编号
     */

    private String imageNo;

    /**
     * 图片名称
     */
    private String imageName;


    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者UUId
     */
    private String createUserUuid;


    private byte[] ins;

    private String type;

    public String getType() {

        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public byte[] getIns() {
        return ins;
    }

    public void setIns(byte[] ins) {
        this.ins = ins;
    }
}
