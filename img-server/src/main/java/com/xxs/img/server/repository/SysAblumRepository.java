package com.xxs.img.server.repository;


import com.baomidou.mybatisplus.service.IService;
import com.xxs.img.server.entity.SysAblum;

import java.util.List;

/**
 * <p>
 * 系统模块--图片服务 服务类
 * </p>
 *
 * @author caster - Fung
 * @since 2018-10-28
 */

public interface SysAblumRepository extends IService<SysAblum> {

    SysAblum selectByImageNo(String imageNo);


    List<SysAblum> listByImageNos(List<String> imageNos);
}
