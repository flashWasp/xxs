package com.xxs.img.server.service.impl;


import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.img.server.AblumContants;
import com.xxs.img.server.entity.SysAblum;
import com.xxs.img.server.entity.SysAblumResponse;
import com.xxs.img.server.repository.MongoDbRepository;
import com.xxs.img.server.repository.SysAblumRepository;
import com.xxs.img.server.service.SysAblumService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.util.*;
import java.io.InputStream;

@Service
public class SysAblumnServiceImpl implements SysAblumService {

    @Autowired
    private SysAblumRepository sysAblumRepository;

    @Autowired
    private MongoDbRepository mongoDbRepository;


    /**
     * 图片上传大小限制，单位是M(2M)
     */
    public static Integer photoSize = 2;

    /**
     * 新增图片
     *
     * @return
     */
    @Transactional
    public String addAblum(MultipartFile img, String bulket, Double bili, Double x, Double y, Double w, Double h) throws Exception {
        InputStream is =  img.getInputStream();
        String fileId = mongoDbRepository.uploadFile(img,is);
        SysAblum sysAblum = packSysablum(bulket, img.getOriginalFilename(), fileId);
        sysAblumRepository.insert(sysAblum);
        return sysAblum.getImageNo();
    }

    /**
     * 根据编号获取图片对象
     *
     * @param imageNo
     * @return
     */
    @Transactional
    public SysAblumResponse findAblum(String imageNo) throws Exception {
        SysAblumResponse sysAblumResponse = new SysAblumResponse();
        SysAblum sysAblum = sysAblumRepository.selectByImageNo(imageNo);
        if (sysAblum != null) {
            BeanUtils.copyProperties(sysAblum, sysAblumResponse);
            sysAblumResponse.setFileIns(mongoDbRepository.findFile(imageNo).getIns());
            return sysAblumResponse;
        }
        return null;
    }

    /**
     * 根据编号删除图片对象
     *
     * @param imageNo
     * @return
     */
    @Transactional
    public Boolean deleteAblum(String imageNo) {
        SysAblum sysAblum = sysAblumRepository.selectByImageNo(imageNo);
        if (sysAblum != null) {
            sysAblum.setState(AblumContants.NEWS_ISDELETE);
            sysAblum.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
            sysAblum.setUpdateTime(new Date());
            return sysAblumRepository.updateAllColumnById(sysAblum);
        }
        return false;
    }


    /*============ Mysql ===============*/
    private SysAblum packSysablum(String bulket, String imageName, String fileId) {
        SysAblum sysAblum = new SysAblum();
        sysAblum.setCreateTime(new Date());
        sysAblum.setUpdateTime(new Date());
        sysAblum.setBulket(bulket);
//        sysAblum.setCreateUserUuid(TokenRedisRepository.getCurUserUUID());
//        sysAblum.setUpdateUserUuid(TokenRedisRepository.getCurUserUUID());
        sysAblum.setCreateUserUuid("525515552225");
        sysAblum.setUpdateUserUuid("525515552225");
        sysAblum.setImageName(imageName);
        sysAblum.setState(AblumContants.NEWS_NODELETE);
        sysAblum.setImageNo(fileId);
        return sysAblum;
    }

}
