package com.xxs.img.server.repository.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.img.server.dao.SysAblumDao;
import com.xxs.img.server.entity.SysAblum;
import com.xxs.img.server.repository.SysAblumRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统模块--图片服务 服务实现类
 * </p>
 *
 * @author caster - Fung
 * @since 2018-10-28
 */
@Service
public class SysAblumRepositoryImpl extends ServiceImpl<SysAblumDao, SysAblum> implements SysAblumRepository {

    @Override
    public SysAblum selectByImageNo(String imageNo) {
        SysAblum sysAblum = new SysAblum();
        sysAblum.setImageNo(imageNo);
        return baseMapper.selectOne(sysAblum);
    }

    @Override
    public List<SysAblum> listByImageNos(List<String> imageNos) {
        return baseMapper.selectList(new EntityWrapper<SysAblum>().in("image_no",imageNos));
    }
}
