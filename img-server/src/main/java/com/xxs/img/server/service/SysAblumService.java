package com.xxs.img.server.service;

import com.xxs.img.server.entity.SysAblumResponse;
import org.springframework.web.multipart.MultipartFile;


public interface SysAblumService {


    String addAblum(MultipartFile img, String bulket, Double bili, Double x, Double y, Double w, Double h) throws Exception;

    SysAblumResponse findAblum(String imageNo) throws Exception;

    Boolean deleteAblum(String imageNo);

}
