package com.xxs.active.service;


import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

/**
 * 代码生成类
 * <p>
 * 使用者配置：包路径、输出文件路径、表对象数组
 * 即可生成：entity层、persistence层、repository层、web层
 * </p>
 *
 * @author
 * @date 2018年1月24日
 */
public class MpGenerator {

    @Test
    public  void generateCode() {
        String packageName = "com.xxs.active";
        String moduleName = "service";
        String outputDir = "D:\\codeGenerator";
        generateByTables(packageName, moduleName, outputDir, "active_following");
    }

    private static void generateByTables(String packageName, String moduleName, String outputDir, String... tableNames) {
        // 全局配置
        GlobalConfig config = new GlobalConfig();
        String dbUrl = "jdbc:mysql://127.0.0.1/xxstest?allowMultiQueries=true";
        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername("root")
                .setPassword("123456")
                .setDriverName("com.mysql.jdbc.Driver");
        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                // 全局大写命名
                .setCapitalMode(true)
                // 【实体】是否为lombok模型
                .setEntityLombokModel(false)
                .setDbColumnUnderline(true)
                // 表名生成策略 - 下划线转驼峰式
                .setNaming(NamingStrategy.underline_to_camel)
                // tableNames - 需要代码生成的表名，多个表名传数组
                .setInclude(tableNames);
        config.setActiveRecord(false)
                // 作者
                .setAuthor("fung")
                // 输出位置
                .setOutputDir(outputDir)
                // XML 二级缓存
                .setEnableCache(false)
                // XML ResultMap
                .setBaseResultMap(false)
                // XML columList
                .setBaseColumnList(true)
                .setOpen(true)
                // 是否覆盖已有文件
                .setFileOverride(true);
        config.setServiceName("%sRepository")
                .setServiceImplName("%sRepositoryImpl")
                .setMapperName("%sDao");
        new AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setEntity(moduleName + ".entity")
                                .setMapper(moduleName + ".dao")
                                .setXml(moduleName + ".mapper")
                                .setServiceImpl(moduleName + ".repository" + ".impl")
                                .setService(moduleName + ".repository")
                                .setController(moduleName + ".web")
                ).execute();

    }
}
