package com.xxs.active.service.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.sun.deploy.cache.Cache;
import com.xxs.active.service.entity.ActiveBase;
import com.xxs.active.service.entity.req.ActiveSearchReq;
import com.xxs.active.service.repository.ActiveBaseRepository;
import com.xxs.active.service.service.ActiveService;
import com.xxs.common.service.cache.CacheHelper;
import com.xxs.common.service.system.TokenRedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("activeService")
public class ActiveServiceImpl implements ActiveService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveServiceImpl.class);

    private static final String ACTIVE_BASE = "active:base";
    @Autowired
    private ActiveBaseRepository activeBaseRepository;


    @Override
    public Boolean add(ActiveBase activeBase) {
        CacheHelper.update(ACTIVE_BASE,activeBase.getActiveNo(), JSONObject.toJSONString(activeBase));
        return activeBaseRepository.insert(activeBase);
    }

    @Override
    public Boolean updateState(String no, String state) {
        ActiveBase activeBase = activeBaseRepository.selectByNo(no);
        if (activeBase != null) {
            activeBase.setState(state);
            boolean b = activeBaseRepository.updateAllColumnById(activeBase);
            if (b){
                CacheHelper.del(ACTIVE_BASE,no);
            }
            return b;
        }
        return false;
    }

    @Override
    public ActiveBase getByNo(String no) {
        Object cacheObj = CacheHelper.read(ACTIVE_BASE,no);
        if (cacheObj != null){
            return JSONObject.parseObject(cacheObj.toString(),ActiveBase.class);
        }
        ActiveBase activeBase = activeBaseRepository.selectByNo(no);
        if (null != activeBase) {
            CacheHelper.update(ACTIVE_BASE,no,JSONObject.toJSONString(activeBase));
            return activeBase;
        }
        return null;
    }

    @Override
    public Long getFollowing(String no) {
        ActiveBase activeBase = getByNo(no);
        if (activeBase != null) {
            return activeBase.getFollowing();
        }
        return null;
    }

    @Override
    public Boolean following(String no) {
        ActiveBase activeBase = getByNo(no);
        if (null != activeBase) {
            Long following = activeBase.getFollowing();
            activeBase.setFollowing(following + 1);
            return activeBaseRepository.updateAllColumnById(activeBase);
        }
        return false;
    }

    @Override
    public Boolean disFollowing(String activeNo) {
        ActiveBase activeBase = getByNo(activeNo);
        if (null != activeBase) {
            Long following = activeBase.getFollowing();
            activeBase.setFollowing(following - 1);
            return activeBaseRepository.updateAllColumnById(activeBase);
        }
        return false;
    }

    @Override
    public Page<ActiveBase> page(ActiveSearchReq req) {
        Integer pageNo = req.getPage();
        Integer rows = req.getRows();
        Page<ActiveBase> page = new Page<>(pageNo, rows);
        return activeBaseRepository.selectPage(page);
    }

    @Override
    public Page<ActiveBase> page(List<String> activeNos, Integer page, Integer rows) {
        return activeBaseRepository.selectPage(activeNos, page, rows);
    }

    @Override
    public Page<ActiveBase> pageBySelf(Integer page, Integer rows) {
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        return activeBaseRepository.selectPage(curUserUUID, page, rows);
    }
}
