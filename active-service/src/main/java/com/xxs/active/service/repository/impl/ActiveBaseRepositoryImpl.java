package com.xxs.active.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.active.service.dao.ActiveBaseDao;
import com.xxs.active.service.entity.ActiveBase;
import com.xxs.active.service.repository.ActiveBaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
@Service
public class ActiveBaseRepositoryImpl extends ServiceImpl<ActiveBaseDao, ActiveBase> implements ActiveBaseRepository {

    @Override
    public ActiveBase selectByNo(String no) {
        ActiveBase activeBase = new ActiveBase();
        activeBase.setActiveNo(no);
        return baseMapper.selectOne(activeBase);
    }

    @Override
    public Page<ActiveBase> selectPage(List<String> nos, Integer pageNo, Integer rows) {
        if (pageNo == null || rows == null){
            pageNo = 1;
            rows = Integer.MAX_VALUE;
        }
        Page<ActiveBase> page = new Page<>(pageNo, rows);
        baseMapper.selectPage(page, new Condition().in("active_no", nos));
        return page;
    }

    @Override
    public Page<ActiveBase> selectPage(String member_no, Integer pageNo, Integer rows) {
        if (pageNo == null || rows == null){
            pageNo = 1;
            rows = Integer.MAX_VALUE;
        }
        Page<ActiveBase> page = new Page<>(pageNo, rows);
        baseMapper.selectPage(page, new Condition().in("create_user_no", member_no));
        return page;
    }


}
