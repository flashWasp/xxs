package com.xxs.active.service.repository;

import com.baomidou.mybatisplus.service.IService;
import com.xxs.active.service.entity.ActiveFollowing;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
public interface ActiveFollowingRepository extends IService<ActiveFollowing> {

    /**
     * 条件获取关注对象集合
     * @param activeNo
     * @return
     */
    List<ActiveFollowing> list(String activeNo,String memberNo);

    /**
     * 根据用户编号统计其关注量
     * @param memberNo
     * @return
     */
    Integer countByMemberNo(String memberNo);

    /**
     * 根据条件删除关注
     * @param memberNo
     * @param activeNo
     * @return
     */
    Boolean del(String memberNo, String activeNo);


}
