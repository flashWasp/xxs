package com.xxs.active.service.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
public class ActiveFollowing implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 活动编号
     */
    private String activeNo;
    /**
     * 用户编号
     */
    private String memberNo;
    /**
     *  创建时间
     */
    private Long createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActiveNo() {
        return activeNo;
    }

    public void setActiveNo(String activeNo) {
        this.activeNo = activeNo;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ActiveFollowing{" +
        ", id=" + id +
        ", activeNo=" + activeNo +
        ", memberNo=" + memberNo +
        ", createTime=" + createTime +
        "}";
    }
}
