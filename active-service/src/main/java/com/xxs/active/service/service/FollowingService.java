package com.xxs.active.service.service;

import java.util.List;

/**
 * Author : Fung
 * Date : 2018/12/1 0001 下午 4:17
 * Desc : 关注服务
 */
public interface FollowingService {

    /**
     * 添加关注
     * @param no
     * @return
     */
    Boolean add(String no);

    /**
     * 获取个人关注活动列表
     * @param memberNo
     * @return
     */
    List<String> listActiveNoByMemberNo(String memberNo);

    /**
     * 根据活动编号获取所有关注者
     * @param activeNo
     * @return
     */
    List<String> listMemberNoByActiveNo(String  activeNo);


    /**
     * 获取个人 所有活动关注数量
     * @param memberNo
     * @return
     */
    Integer countByMemberNo(String memberNo);

    /**
     * 删除关注
     * @return
     */
    Boolean del(String memberNo,String active);

    /**
     *  删除个人所有关注
     * @param memberNo
     * @return
     */
    Boolean delByMemberNo(String memberNo);

    /**
     * 删除活动所有关注
     * @param active
     * @return
     */
    Boolean delByActiveNo(String active);

}
