package com.xxs.active.service.entity.req;

public class ActiveSearchReq {
    /**
     * 编号
     */
    private String activeNo;

    /**
     * 标题
     */
    private String title;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long endTime;

    /**
     * 状态
     * 编辑：edit
     * 申请中：apply
     * 通过:pass
     * 撤回中:revoke
     */
    private String state;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 页大小
     */
    private Integer rows;

    public String getActiveNo() {
        return activeNo;
    }

    public void setActiveNo(String activeNo) {
        this.activeNo = activeNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "ActiveSearchReq{" +
                "activeNo='" + activeNo + '\'' +
                ", title='" + title + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", state='" + state + '\'' +
                ", page=" + page +
                ", rows=" + rows +
                '}';
    }
}
