package com.xxs.active.service.repository.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xxs.active.service.dao.ActiveFollowingDao;
import com.xxs.active.service.entity.ActiveFollowing;
import com.xxs.active.service.repository.ActiveFollowingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
@Service
public class ActiveFollowingRepositoryImpl extends ServiceImpl<ActiveFollowingDao, ActiveFollowing> implements ActiveFollowingRepository {

    @Override
    public List<ActiveFollowing> list(String activeNo,String memberNo) {
        Condition condition = new Condition();
        if (memberNo != null && !memberNo.isEmpty()){
            condition.eq("member_no",memberNo);
        }
        if (activeNo != null && !activeNo.isEmpty()){
            condition.eq("active_no",activeNo);
        }
        return selectList(condition);
    }

    @Override
    public Integer countByMemberNo(String memberNo) {
        return selectCount(new Condition().eq("member_no",memberNo));
    }

    @Override
    public Boolean del(String memberNo, String activeNo) {
        Condition condition = new Condition();
        if (memberNo != null && !memberNo.isEmpty()){
            condition.eq("member_no",memberNo);
        }
        if (activeNo != null && !activeNo.isEmpty()){
            condition.eq("active_no",activeNo);
        }

        return delete(condition);
    }


}
