package com.xxs.active.service.entity;

/**
 * 往期链接对象
 */
public class Link {
    /**
     * 别名
     */
    private String alias;

    /**
     * 链接对象
     */
    private String link;
}
