package com.xxs.active.service.service.impl;

import com.xxs.active.service.entity.ActiveFollowing;
import com.xxs.active.service.repository.ActiveFollowingRepository;
import com.xxs.active.service.service.ActiveService;
import com.xxs.active.service.service.FollowingService;
import com.xxs.common.service.system.TokenRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Author : Fung
 * Date : 2018/12/1 0001 下午 4:17
 * Desc :
 */
@Service("followingService")
public class FollowingServiceImpl implements FollowingService {
    @Autowired
    private ActiveFollowingRepository activeFollowingRepository;

    @Override
    public Boolean add(String no) {
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        ActiveFollowing activeFollowing = new ActiveFollowing();
        activeFollowing.setActiveNo(no);
        activeFollowing.setCreateTime(System.currentTimeMillis());
        activeFollowing.setMemberNo(curUserUUID);
        return activeFollowingRepository.insertAllColumn(activeFollowing);
    }

    @Override
    public List<String> listMemberNoByActiveNo(String activeNo) {
        List<ActiveFollowing> list = activeFollowingRepository.list(activeNo, null);
        return list.stream().map(ActiveFollowing::getMemberNo).collect(Collectors.toList());
    }

    @Override
    public Integer countByMemberNo(String memberNo) {
        return activeFollowingRepository.countByMemberNo(memberNo);
    }

    @Override
    public Boolean del(String memberNo, String activeNo) {
        return activeFollowingRepository.del(memberNo, activeNo);

    }

    @Override
    public Boolean delByMemberNo(String memberNo) {
        return activeFollowingRepository.del(memberNo, null);

    }

    @Override
    public Boolean delByActiveNo(String activeNo) {
        return activeFollowingRepository.del(null, activeNo);
    }

    @Override
    public List<String> listActiveNoByMemberNo(String memberNo) {
        List<ActiveFollowing> list = activeFollowingRepository.list(null, memberNo);
        return list.stream().map(ActiveFollowing::getActiveNo).collect(Collectors.toList());
    }


}
