package com.xxs.active.service.entity.req;

import com.xxs.active.service.entity.Link;

import java.util.List;

/**
 * 活动创建请求对象
 */
public class ActiveCreateReq {

    /**
     * 活动名称
     */
    private String title;

    /**
     * 开始时间
     */
    private Long  startTime;

    /**
     * 结束时间
     */
    private Long  endTime;

    /**
     * 活动地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;

    /**
     * 图片编号
     */
    private String picNo;

    /**
     * 文章内容
     */
    private String content;

    /**
     * 链接
     */
    private List<Link> links;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPicNo() {
        return picNo;
    }

    public void setPicNo(String picNo) {
        this.picNo = picNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "ActiveCreateReq{" +
                "title='" + title + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", address='" + address + '\'' +
                ", remark='" + remark + '\'' +
                ", picNo='" + picNo + '\'' +
                ", content='" + content + '\'' +
                ", links=" + links +
                '}';
    }
}
