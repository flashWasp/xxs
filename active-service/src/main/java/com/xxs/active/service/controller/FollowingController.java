package com.xxs.active.service.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.active.service.entity.ActiveBase;
import com.xxs.active.service.service.ActiveService;
import com.xxs.active.service.service.FollowingService;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.system.TokenRedisRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 关注
 */
@RestController
@RequestMapping("/active")
public class FollowingController {

    @Autowired
    private FollowingService followingService;

    @Autowired
    private ActiveService activeService;

    /**
     * 关注活动
     *
     * @param activeNo
     * @return
     */
    @ApiOperation("关注活动")
    @GetMapping("/follow")
    @Transactional
    public AjaxResult follow(String activeNo) {
        Boolean following = followingService.add(activeNo) & activeService.following(activeNo) ;
        if (following) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 取消关注活动
     *
     * @param activeNo
     * @return
     */
    @ApiOperation("取消关注活动")
    @GetMapping("/disFollow")
    @Transactional
    public AjaxResult disFollow(String activeNo) {
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        Boolean disFollow = followingService.del(curUserUUID, activeNo) &
        activeService.disFollowing(activeNo);
        if (disFollow) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 根据活动获取其关注者集合
     *
     * @param activeNo
     * @return
     */
    @ApiOperation("根据活动获取其关注者集合")
    @GetMapping("/listMemberNoByActiveNo")
    public AjaxResult listMemberNoByActiveNo(@RequestParam("activeNo") String activeNo) {
        List<String> members = followingService.listMemberNoByActiveNo(activeNo);
        if (members != null) {
            return AjaxResult.getOK(members);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 获取个人关注的活动列表
     */
    @ApiOperation("获取个人关注的活动列表")
    @GetMapping("/pageSelfFollowing")
    public AjaxResult pageFollowingActive(@RequestParam("page") Integer page,
                                          @RequestParam("rows") Integer rows) {
        String curUserUUID = TokenRedisRepository.getCurUserUUID();
        List<String> activeNos = followingService.listActiveNoByMemberNo(curUserUUID);
        if (activeNos != null) {
            Page<ActiveBase> pageObj = activeService.page(activeNos, page, rows);
            if (null != pageObj) {
                return AjaxResult.getOK(pageObj);
            }
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }
}
