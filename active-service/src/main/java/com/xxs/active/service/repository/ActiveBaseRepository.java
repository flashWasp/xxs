package com.xxs.active.service.repository;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xxs.active.service.entity.ActiveBase;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
public interface ActiveBaseRepository extends IService<ActiveBase> {

    /**
     * 根据编号获取活动对象
     * @param no
     * @return
     */
    ActiveBase selectByNo(String no);

    Page<ActiveBase> selectPage(List<String> nos,Integer pageNo,Integer rows);

    /**
     * 根据用户编号获取其创建的活动
     * @param member_no
     * @param pageNo
     * @param rows
     * @return
     */
    Page<ActiveBase> selectPage(String  member_no,Integer pageNo,Integer rows);
}
