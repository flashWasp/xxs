package com.xxs.active.service.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.active.service.ActiveConstant;
import com.xxs.active.service.entity.ActiveBase;
import com.xxs.active.service.entity.Link;
import com.xxs.active.service.entity.req.ActiveCreateReq;
import com.xxs.active.service.entity.req.ActiveSearchReq;
import com.xxs.active.service.service.ActiveService;
import com.xxs.active.service.service.FollowingService;
import com.xxs.common.service.constant.XxsConstant;
import com.xxs.common.service.model.AjaxResult;
import com.xxs.common.service.model.ResultCode;
import com.xxs.common.service.system.TokenRedisRepository;
import com.xxs.common.service.util.KeyUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/active")
public class ActiveController {

    @Autowired
    private ActiveService activeService;

    @Autowired
    private FollowingService followingService;

    @ApiOperation("添加活动对象")
    @PostMapping("/add")
    public AjaxResult add(@RequestBody ActiveCreateReq req) {
        ActiveBase activeBase = new ActiveBase();
        activeBase.setTitle(req.getTitle());
        activeBase.setPicNo(req.getPicNo());
        activeBase.setFollowing(0L);
        activeBase.setStartTime(req.getStartTime());
        activeBase.setEndTime(req.getEndTime());
        activeBase.setLocaltion(req.getAddress());
        activeBase.setRemark(req.getRemark());
        activeBase.setContent(req.getContent());
        activeBase.setState(ActiveConstant.ACTIVE_STATE_EDIT);
        List<Link> links = req.getLinks();
        String uid = TokenRedisRepository.getCurUserUUID();
        activeBase.setCreateUserNo(uid);
        if (links != null){
            String link = JSONArray.toJSON(links).toString();
            activeBase.setLink(link);
        }
        activeBase.setActiveNo(KeyUtil.getUniqueKey());
        Boolean flag = activeService.add(activeBase);
        if (flag) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 申请活动
     * @param no
     * @return
     */
    @ApiOperation("申请活动")
    @GetMapping("/apply")
    public AjaxResult apply(@RequestParam("no") String no) {
        Boolean apply = activeService.updateState(no, ActiveConstant.ACTIVE_STATE_APPLY);
        if (apply) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 活动通过审核
     */
    @ApiOperation("通过活动")
    @GetMapping("/pass")
    public AjaxResult pass(@RequestParam("no") String no) {
        Boolean pass = activeService.updateState(no, ActiveConstant.ACTIVE_STATE_PASS);
        if (pass) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 撤回活动
     *
     * @return
     */
    @ApiOperation("撤回活动")
    @GetMapping("/revoke")
    public AjaxResult revoke(@RequestParam("no") String no) {
        // 活动撤回
        Boolean revoke = activeService.updateState(no, ActiveConstant.ACTIVE_STATE_REVOKE);
        if (revoke) {
            return AjaxResult.getOK();
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }


    /**
     * 删除活动
     * @param no
     * @return
     */
    @ApiOperation("删除活动")
    @GetMapping("/del")
    public AjaxResult del(String no){
        Boolean del = activeService.updateState(no, XxsConstant.XXS_STATE_ISDELETE);
        if (del){
            Boolean flag = followingService.delByActiveNo(no);
            if (flag){
                return AjaxResult.getOK();
            }
        }
        return AjaxResult.getError(ResultCode.FAILURE);
    }

    /**
     * 获取自身创建的活动
     * @param page
     * @param rows
     * @return
     */
    @ApiOperation("分页获取自身创建的活动")
    @GetMapping("/pageSelfActive")
    public AjaxResult pageSelfActive(@RequestParam("page") Integer page,
                                     @RequestParam("rows") Integer rows) {
        Page<ActiveBase> pageObj = activeService.pageBySelf(page, rows);
        if (pageObj != null) {
            return AjaxResult.getOK(pageObj);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 获取已通过审核的活动(前端使用)
     *
     * @return
     */
    @ApiOperation("分页获取已通过审核的活动")
    @GetMapping("/pagePass")
    public AjaxResult pagePass(@RequestParam("page") Integer page,
                               @RequestParam("rows") Integer rows) {
        ActiveSearchReq activeSearchReq = new ActiveSearchReq();
        activeSearchReq.setPage(page);
        activeSearchReq.setRows(rows);
        activeSearchReq.setState("pass");
        Page<ActiveBase> pageObj = activeService.page(activeSearchReq);
        if (pageObj != null) {
            return AjaxResult.getOK(pageObj);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 条件分页搜索活动（管理端使用）
     */
    @ApiOperation("分页条件搜索")
    @PostMapping("/page")
    public AjaxResult page(@RequestBody ActiveSearchReq req) {
        Page<ActiveBase> page = activeService.page(req);
        if (page != null) {
            return AjaxResult.getOK(page);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }

    /**
     * 获取活动详情
     * @param no
     * @return
     */
    @ApiOperation("获取活动详情")
    @GetMapping("/get")
    public AjaxResult get(@RequestParam("no") String no) {
        ActiveBase activeBase = activeService.getByNo(no);
        if (null != activeBase) {
            return AjaxResult.getOK(activeBase);
        }
        return AjaxResult.getError(ResultCode.ParamException);
    }


}
