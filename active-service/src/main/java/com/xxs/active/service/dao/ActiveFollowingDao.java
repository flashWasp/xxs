package com.xxs.active.service.dao;

import com.xxs.active.service.entity.ActiveFollowing;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
public interface ActiveFollowingDao extends BaseMapper<ActiveFollowing> {

}
