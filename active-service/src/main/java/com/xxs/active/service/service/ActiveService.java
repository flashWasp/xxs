package com.xxs.active.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.xxs.active.service.entity.ActiveBase;
import com.xxs.active.service.entity.req.ActiveSearchReq;

import java.util.List;

public interface ActiveService {

    /**
     * 添加活动对象
     * @param activeBase
     * @return
     */
    Boolean add(ActiveBase activeBase);

    /**
     * 更新状态
     * 编辑状态 申请状态  通过上线 撤回
     * @param state
     * @return
     */
    Boolean  updateState(String no,String state);

    /**
     * 根据编号获取活动对象
     * @param no
     * @return
     */
    ActiveBase getByNo(String no);

    /**
     * 根据编号获取关注量
     * @param no
     * @return
     */
    Long getFollowing(String no);

    /**
     * 关注活动
     * @param activeNo
     * @return
     */
    Boolean following(String  activeNo);

    /**
     * 删除关注
     * @param activeNo
     * @return
     */
    Boolean disFollowing(String activeNo);

    /**
     * 根据请求搜索
     * @param req
     * @return
     */
    Page<ActiveBase> page(ActiveSearchReq req);

    /**
     * 根据活动id集合分页对象
     * @param activeNos
     * @return
     */
    Page<ActiveBase> page(List<String> activeNos,Integer page,Integer rows);

    Page<ActiveBase> pageBySelf(Integer page, Integer rows);



}
