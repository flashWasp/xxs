package com.xxs.active.service.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fung
 * @since 2018-12-01
 */
public class ActiveBase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    private Long id;
    /**
     * 编号
     */
    private String activeNo;
    /**
     * 标题
     */
    private String title;
    /**
     * 开始时间
     */
    @TableField("startTime")
    private Long startTime;
    /**
     * 结束时间
     */
    @TableField("endTime")
    private Long endTime;
    /**
     * 地点
     */
    private String localtion;
    /**
     * 关注量
     */
    private Long following;
    /**
     * 备注
     */
    private String remark;
    /**
     * 状态 申请中：apply  通过:pass  撤回中:revoke  删除：delete 
     */
    private String state;
    /**
     * 封面编号
     */
    private String picNo;
    /**
     * 详细内容
     */
    private String content;
    /**
     * 关联链接
     */
    private String link;

    private String createUserNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActiveNo() {
        return activeNo;
    }

    public void setActiveNo(String activeNo) {
        this.activeNo = activeNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getLocaltion() {
        return localtion;
    }

    public void setLocaltion(String localtion) {
        this.localtion = localtion;
    }

    public Long getFollowing() {
        return following;
    }

    public void setFollowing(Long following) {
        this.following = following;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPicNo() {
        return picNo;
    }

    public void setPicNo(String picNo) {
        this.picNo = picNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCreateUserNo() {
        return createUserNo;
    }

    public void setCreateUserNo(String createUserNo) {
        this.createUserNo = createUserNo;
    }

    @Override
    public String toString() {
        return "ActiveBase{" +
                "id=" + id +
                ", activeNo='" + activeNo + '\'' +
                ", title='" + title + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", localtion='" + localtion + '\'' +
                ", following=" + following +
                ", remark='" + remark + '\'' +
                ", state='" + state + '\'' +
                ", picNo='" + picNo + '\'' +
                ", content='" + content + '\'' +
                ", link='" + link + '\'' +
                ", createUserNo='" + createUserNo + '\'' +
                '}';
    }
}
